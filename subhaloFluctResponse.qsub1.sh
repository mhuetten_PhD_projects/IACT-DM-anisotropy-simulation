#$ -S /bin/bash
#
# script to perform Dark Matter clumpy simulations AND detector response
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script


ODIR=OODIR
SCRIPTDIR=DIIIR
RUNDIR=RUUNN

CHANNEL=CHNNL
ISANNIHILATION=ANNHL
DMMASSES=(MASSS)
SIGMAVSTARTVALS=(SIGMV)
EMIN=FFFFF
EMAX=GGGGG
SKYMAPFILE=HHHHH

echo
echo " *** Prepare MC simulation ***"
echo "   for DM masses: "$DMMASSES
echo 

source /afs/ifh.de/user/m/mhuetten/setbatchenvironment_oldROOT.tcsh VERITAS
echo

# temporary directory
DDIR=$TMPDIR/Moritz
mkdir -p $DDIR
rm -f $DDIR/*
echo "Temporary directory:"
echo $DDIR

echo
echo " *** Transform Clumpy output into Fits image ***"
echo 

echo $SKYMAPFILE

cp $CLUMPY/bin/clumpy $DDIR

if [[ $SKYMAPFILE == "none" ]]; then
	cd $DDIR
	cp $ODIR/map-$SGE_TASK_ID/*.fits $DDIR/tmp1.fits

	clumpy -o2 tmp1.fits 2 1
	mv *JFACTOR_PER_SR* tmp2.fits
	python $RUNDIR/Inputfiles/makeFitsImage.py -i tmp2.fits

	#mv tmp2.fits $ODIR/map-$SGE_TASK_ID/skymap-orig.fits
	mv *image.fits $ODIR/map-$SGE_TASK_ID/skymap.fits
	mv pop* $ODIR/map-$SGE_TASK_ID/
	
	cd $ODIR/map-$SGE_TASK_ID/
	
	mv Inputfiles/clumpyV2.parameters .
	mv Inputfiles/clumpyV2.clumpy_params.txt tmp1.dat
	mv Logfiles/*.log .
	mv *.drawn clumps.drawn
	rm -r Logfiles
	rm -r Inputfiles
	rm annihil*
else
	# skymap fits file has already been computed before:
	cd $ODIR/map-$SGE_TASK_ID/
	cp $SKYMAPFILE skymap.fits
	cp $CLUMPY/clumpy_params_dummyspec2.txt tmp1.dat
fi

### compute spectra:

MEAN=$(python $RUNDIR/Inputfiles/printKeywordValue.py -i skymap.fits -k MEAN)
FLUX_TOT=$(python $RUNDIR/Inputfiles/printKeywordValue.py -i skymap.fits -k FLUX_TOT)

if [[ $CHANNEL == "bbbar" ]]; then
	CHANNELVEC="0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
	sed  's/0,0,0,0,0,0,0,0,0,0,0,0.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/'$CHANNELVEC'/' tmp1.dat > tmp2.dat
elif [[ $CHANNEL == "tautau" ]]; then 
	CHANNELVEC="0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
	sed  's/0,0,0,0,0,0,0,0,0,0,0,0.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0/'$CHANNELVEC'/' tmp1.dat > tmp2.dat
else
	echo "unnknown channel"
fi
rm tmp1.dat

cp -r $CLUMPY/PPPC4DMID-spectra .


i_dm=0
hundred=100
for DMMASS in  "${DMMASSES[@]}"
do
	SIGMAVSTART=${SIGMAVSTARTVALS[$i_dm]}
	SOURCENAME=$CHANNEL"_"$DMMASS
	echo
	echo "sigmav_start is: "$SIGMAVSTART
	# multiply by 100 (will be divided by hundred in sim.py script. workaround to overcome ctools model restrictions)
	SIGMAVSTART=`python -c "from math import *; print $SIGMAVSTART*$hundred"`

	echo "tweaked sigmav_start is: "$SIGMAVSTART
	echo "total flux in map is: " $FLUX_TOT "GeV^2/cm^5."
	i_dm=$(($i_dm + 1))
	
	sed  's/SIGMAVSTART/'$SIGMAVSTART'/' tmp2.dat > clumpy_params.dat
	
	$DDIR/clumpy -zp clumpy_params.dat $EMIN $EMAX 1000  $DMMASS kCIRELLI11_EW  kGAMMA $FLUX_TOT  $ISANNIHILATION  0.
	mv spectra*.txt spectrum_CIRELLI11_EW_${DMMASS}_GeV_${CHANNEL}.dat
	rm spectra*
	rm dnde*
	# set correct model
	sed  's|DUMMYPATH|'$ODIR/map-$SGE_TASK_ID'|' $SCRIPTDIR/subhaloFluctResponse.subhalo_model.xml > tmp1.dat
	sed  's|SOURCENAME|'$SOURCENAME'|' tmp1.dat > tmp3.dat
	sed  's|DUMMYNAME|'spectrum_CIRELLI11_EW_${DMMASS}_GeV_${CHANNEL}.dat'|' tmp3.dat > subhaloFluctResponse.subhalo_model_${DMMASS}_GeV.xml
	# also copy DGRB model:
	sed  's|DUMMYPATH|'$ODIR/map-$SGE_TASK_ID'|' $SCRIPTDIR/subhaloFluctResponse.DGRB_model.xml > subhaloFluctResponse.DGRB_model.xml
	rm tmp1.dat
	rm tmp3.dat
done

rm tmp2.dat

rm -r PPPC4DMID-spectra

#rm clumpy_params.dat


##sleep 20
echo " *** Job successfully finished ***"
##exit
