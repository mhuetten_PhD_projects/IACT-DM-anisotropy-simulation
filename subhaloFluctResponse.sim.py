#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
import os
import sys
import pandas
import getopt
from ctools import *
import cscripts.obsutils as obsutils
from gammalib import *
from math import *
import glob
import veripy
from time import *
from astropy.io import fits
import random
import matplotlib
matplotlib.use('Agg') # turn of interactive renderer (may otherwise not work on clusters)
from matplotlib import pyplot as plt
import gc
import resource
#import psutil


pgf_with_rc_fonts = {                      # setup matplotlib to use latex for output
    "pgf.texsystem": "pdflatex",        # change this if using xetex or lautex
    "text.usetex": False,                # use LaTeX to write all text
    "font.family": "sans-serif",
    "font.serif": [],                   # blank entries should cause plots to inherit fonts from the document
    "font.sans-serif": [],
    "font.monospace": [],
    "axes.labelsize": 14,               # LaTeX default is 10pt font.
    "text.fontsize": 10,
    "legend.fontsize": 14,               # Make the legend/label fonts a little smaller
    "xtick.labelsize": 10,
    "ytick.labelsize": 10,
    }
matplotlib.rcParams.update(pgf_with_rc_fonts)

##    main part:   ############################################################
###############################################################################
def main(argv):

    ###########################################################################
    # default input variables:
    
    
    
 
#     caldb = "nathan"
#     irf   = "data/veritas/nathan/bcf/dummy/irf_file.fits"
#     instrument = "veritas"
#     instrument_name = instrument
#      
#     instrument = "CTA"
#     instrument_name = instrument
# #     
#     # folder where events are saved:
#     #events_outdir = "/lustre/fs19/group/cta/users/mhuetten/CTA/FitsData/Eventlists"
#       
#     dead_time = 0.95
#     emin_TeV = 0.1 #pow(10,-0.5)
#     emax_TeV = pow(10,1.9)
#     dmmass=200
#     scaling=1
#     deltalog=0.2
#     pointingfile="/afs/ifh.de/user/m/mhuetten/projects/IACT-DM-anisotropy-simulation/subhaloFluctResponse.pointings.dat"
# #     
# #     signalmodel_file = '/afs/ifh.de/user/m/mhuetten//projects/IACT-DM-anisotropy-simulation/subhaloFluctResponse.subhalo_model.xml'
# #     signalmodel_file = '/afs/ifh.de/group/cta/scratch/mhuetten/CTA/ctools/models/crab.xml'
#     # survey or patterned observation:
#     survey = 0
#     offset  = 1.5
# 
#     workdir="/lustre/fs19/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/15-11-10-Test/"
#     is_dgrb=1

    ###########################################################################
    # read input variables, if given
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hw:o:t:c:i:n:d:e:f:m:s:l:t:u:p:g:a:b:j:k:x:v:b:q:", 
["workdir=","outdir=","instrument=","deadtime=","emin=","emax=","dmmass=","scaling=","deltalog=","offset=","survey=","pointingfile=","isdgrb=","issignal=","isbck=","isshuffle=","nsideman=","fnameappendix=","sigmavmin=","obswidth=","phirot="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'Wrong input, EXIT. The input options are:'
        print ' -i'
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'HELP: The input options are:'
            print ' -w'
            sys.exit()
        elif opt in ("-w", "--workdir"):
            workdir = arg
        elif opt in ("-o", "--outdir"):
            outdir = arg
        elif opt in ("-n", "--instrument"):
            instrument = arg
        elif opt in ("-d", "--deadtime"):
            dead_time = float(arg)
        elif opt in ("-e", "--emin"):
            emin_GeV = int(arg)
        elif opt in ("-f", "--emax"):
            emax_GeV = int(arg)#
        elif opt in ("-m", "--dmmass"):
            dmmass = float(arg)
        elif opt in ("-s", "--scaling"):
            scaling = float(arg)
        elif opt in ("-l", "--deltalog"):
            deltalog = float(arg)
        elif opt in ("-t", "--offset"):
            offset = float(arg)
        elif opt in ("-u", "--survey"):
            survey = int(arg)
        elif opt in ("-p", "--pointingfile"):
            pointingfile = arg
        elif opt in ("-g", "--isdgrb"):
            is_dgrb = int(arg)
        elif opt in ("-a", "--issignal"):
            is_signal = bool(int(arg))
        elif opt in ("-b", "--isbck"):
            is_bck = bool(int(arg))
        elif opt in ("-j", "--isshuffle"):
            is_shuffle = bool(int(arg))
        elif opt in ("-k", "--nsideman"):
            nside_man = int(arg)
        elif opt in ("-x", "--fnameappendix"):
            fnameappendix = int(arg)
        elif opt in ("-v", "--sigmavmin"):
            sigmavmin = float(arg)
        elif opt in ("-b", "--obswidth"):
            obswidth = float(arg)
        elif opt in ("-q", "--phirot"):
            phi_rot_seed = int(arg)
                           
    start_time = time()
    print ""
    print "Initializing script subhaloFluctResponse.sim.py, v1.1"
    
    # hardcoded roi size: determined by size of background cube
    roi_rad_deg = 6.0
    
    map_center = GSkyDir()
    if survey:
        offset = 0.
        if not is_signal or is_dgrb == 2:
            map_center.lb_deg( 0, -90 )

    
    
    
    #---------------------------------------------------------   
    # load pointings file:
    
    pointingdata = pandas.read_csv(pointingfile)
    pointingdata = list(pointingdata.T.itertuples())
    nskypatches = len(pointingdata[0][:]) - 1
    
    n_signalpatches_fix = 10
    patch_lastfull = int(np.floor(float(nskypatches)/float(n_signalpatches_fix))) * n_signalpatches_fix
    n_signalpatches_last = nskypatches - patch_lastfull
    
    # rotate map around center by random angle:
    is_rotate = True
    
    if is_rotate == True:
        random.seed(phi_rot_seed)
        phi_random = 2. * np.pi * random.random()
        phi_rot = phi_random
        print ""
        print "  Output map is rotated by angle",np.rad2deg(phi_rot),"degrees."
    
    #if survey:
            #nskypatches = 1
    totalobs_id = 1
    
    if is_signal and is_dgrb != 2:
        nside_check = -1
    else:    
        nside_check = nside_man
 
 
    #---------------------------------------------------------
    # Initialize Healpix-binned maps:
    
    
    
    emin_GeV_fix = 30. # fixed emin 300. GeV
    emin_TeV_fix = float(emin_GeV_fix) / 1000. # fixed emin 30 GeV
    
    emin_TeV = float(emin_GeV) / 1000.
    emax_TeV = float(emax_GeV) / 1000.
    
    
    
    n_thresholds = 8
    #e_GeV_thresholds = np.linspace(emin_GeV_fix , emin_GeV, n_thresholds)
    e_GeV_thresholds = np.array([30., 100., 300., 500., 1000.])
    n_thresholds = len(e_GeV_thresholds)
    e_TeV_thresholds =  e_GeV_thresholds / 1000.
    
    print ""
    print "Flexible energy thresholds in this simulation: E=[",emin_TeV,",",emax_TeV,"] TeV."
    print ""
    
    
    binarrays     = np.zeros((hp.nside2npix(nside_man), n_thresholds), dtype=int )
    nevents       = np.zeros( n_thresholds, dtype=int ) # for faster summing
    nevents_bck   = np.zeros( n_thresholds, dtype=int ) # for faster summing
    nevents_sig   = np.zeros( n_thresholds, dtype=int ) # for faster summing
    
    #---------------------------------------------------------
    # Simulate background events in small chunks.
    # We don't need all the event s at the same time (and their
    # timing and energy after binning, so we simulate some events,
    # bin them, and throw them away afterwards)
    
    timechunk = 1800. # seconds = 30 min.
    totaltime_check = 0.
    
    #--------------------------------------------------------------------------  
    # create signal models container
    signalmodels = GModels()
    observations_quad = GObservations()
    #---------------------------------------------------------
    # This is now the big loop which fills the binarrays with binned events
    # patch-by-patch (run-by-run) to save memory:
    
    quad_patch = 0
    
    print ""
    print "Number of skypatches in this simulation:",nskypatches
    print ""
    
    for patch in range(nskypatches):
        patch+=1 # ignore header line
        start_time_patch = time()
        
        print "==> simulate observation patch", patch
        
        #--------------------------------------------------------------------------  
        # create observations container
        observations = GObservations()
        
        # check for overlapping patches:
        psi = pointingdata[0][patch]
        theta  =  pointingdata[1][patch]
        
        if not survey:
            for checkpatch in range(nskypatches):
                checkpatch += 1 # ignore header line
                psicheck = pointingdata[0][checkpatch]
                thetacheck =  pointingdata[1][checkpatch]
                if abs(psicheck - psi)<2. and abs(theta - thetacheck)<2.:
                    # take first patch for all overlapping patches (always applies for itself!)
                    mappatch = min(patch, checkpatch)
                    break
        # for "survey", take only the first map, as there is only one connected large skymap:
        if survey:
            mappatch = 1
     
        # set pointing position to center of the map:
        lgal_obj_deg     = psi 
        bgal_obj_deg     = theta 
        
        #----------------------------------------------------------------------
        # find the target that the run is pointing at
        target = GSkyDir()
        target.lb_deg( lgal_obj_deg, bgal_obj_deg )
        ra_obj_deg  = target.ra_deg() 
        dec_obj_deg = target.dec_deg()
        if not survey:
            map_center = target
        
        #----------------------------------------------------------------------
        # create observation list:
        obsdeflist = []     
        if survey:
            #obsdeflist = make_pointings.set_extgal()
            pattern = "single"
        else: # single observation
            if offset > 0.:
                pattern = "four"
            else:
                pattern = "single"
        
        obsdeflist = obsutils.set_obs_patterns(pattern, ra_obj_deg, dec_obj_deg, offset)
        
        nb_of_obs = len(obsdeflist)
        print "Number of observations in patch:", nb_of_obs
        
        for i in range(nb_of_obs):
            obsdeflist[i]['deadc']      = dead_time
            obsdeflist[i]['emin']       = emin_TeV_fix
            obsdeflist[i]['emax']       = emax_TeV
            obsdeflist[i]['rad']        = roi_rad_deg
            #if not survey:
            obsdeflist[i]['irf']        = pointingdata[4][patch]
            obsdeflist[i]['caldb']      = pointingdata[3][patch]
            obsdeflist[i]['instrument'] = instrument
            obsdeflist[i]['duration']   = pointingdata[2][patch] / nb_of_obs # "four observations at the same time for pattern"
            print "obsdeflist",i,":" ,obsdeflist[i]
            #if survey:
            #    pntdir = GSkyDir()
            #    pntdir.lb_deg(obsdeflist[i]['lon'], obsdeflist[i]['lat'])
            #    obsdeflist[i]['ra']     = pntdir.ra_deg()
            #    obsdeflist[i]['dec']    = pntdir.dec_deg()
            #    obsdeflist[i]['duration']   = pointingdata[2][patch]
                
        # set observation container!
        observations = obsutils.set_obs_list(obsdeflist)
        
        # connect signal models to sky patches:
        idstring='' # start with empty string again
        for run in observations:
            id = "%6.6d" % totalobs_id
            run.id(id)
            idstring+=str(id)+','
            totalobs_id += 1
        print "  Observation IDs in patch",patch,":",idstring
            
        #----------------------------------------------------------------------
        # create signal model
        
        # read signal map info, if there is a map:
        if is_signal:
            
            # load signal model only once in case of survey:
            if survey and patch > 1:
                # add observation id to existing signal model:
                idstring_full = signalmodels[0].ids()
                idstring_full += ',' 
                idstring_full += idstring
                idstring_full=idstring_full[:-1] # throw away last comma
                # update id string
                if is_dgrb == 2:
                    for model in signalmodels:
                        model.ids(idstring_full)
                else:
                    signalmodels[0].ids(idstring_full)
                
                print "  ... survey observation. Model already loaded -> continue "
                
            else:
                if is_dgrb == 1:
                    signalmodel_file = workdir + "/map-"+ str(mappatch)+"/subhaloFluctResponse.DGRB_model.xml"
                elif is_dgrb == 0:
                    signalmodel_file = workdir + "/map-"+ str(mappatch)+"/subhaloFluctResponse.subhalo_model_"+str(int(dmmass))+"_GeV.xml"
                elif is_dgrb == 2:
                    signalmodel_file = "/afs/ifh.de/user/m/mhuetten/projects/IACT-DM-anisotropy-simulation/subhaloFluctResponse.DGRB2015_model.xml" #"${THESIS}/Data/Chapter06/ctools_models/cta_diffuse/DGRB_Abdo2010.xml" #"${CTOOLS}/models/crab_no_bck.xml"
                    
                print "  load model ", signalmodel_file
                signalmodel_raw = GModels(signalmodel_file)
                signalmodel = signalmodel_raw[0]
                print signalmodel
                
                if is_dgrb != 2:
                    print signalmodel.spatial()
                    fitsfile = signalmodel.spatial().filename().url()
                    #  read header:
                    hdulist_in = fits.open(fitsfile)
                    try:
                        fsky             = hdulist_in[0].header['F_SKY']
                        nside_new        = hdulist_in[0].header['NSIDE']
                        psi_map_deg      = hdulist_in[0].header['CRVAL1']
                        theta_map_deg    = hdulist_in[0].header['CRVAL2']
                        size_x_map_deg   = hdulist_in[0].header['NAXIS1'] * hdulist_in[0].header['CDELT1']
                        size_y_map_deg   = hdulist_in[0].header['NAXIS2'] * hdulist_in[0].header['CDELT2']
                    except:
                        raise IOError('  Wrong input file. Must contain Healpix full-sky map, e.g. created by Clumpy > 2015.06_corr3 with the -o2 option.')
                    hdulist_in.close() 
                    
                    # check if nside is the same in all input maps:
                    if nside_check != -1:
                        if nside_new != nside_check:
                            print("WARNING: not same resolution NSIDE for input skymaps.")
                    nside_check = nside_new
                
                # correct spectral part:
                #print signalmodel.spectral()
                
                # scale sigmav (In case of DGRB spectrum, set flux to sigmav percent of Crab flux/DGRB intensity)
                scaling_corr = scaling - 1 
                sigmav = sigmavmin * 10.**(deltalog*scaling_corr)
    
                #--------------------------       
                # First, get spectral normalizations:      
                if is_dgrb == 1:
                    normalization = signalmodel.spectral()['Prefactor'].value()                
                    print "  DGRB intensity is ",sigmav," times the DGRB intensity as measured by Fermi-LAT."
                elif is_dgrb == 0:    
                    normalization = signalmodel.spectral()['Normalization'].value()
                    # work-around because value must be between 1e-3, 1e3. Increased sigmav_Start accordingly to 3e-24 in clumpyV2.clumpy_params.txt:
                    normalization /= 100. 
                    print "  cross section is",sigmav,"cm^3/s =",10.**(deltalog*scaling_corr)," times the starting cross section",sigmavmin,"cm^3/s."
                elif is_dgrb == 2:
                    normalization = signalmodel.spectral()['Prefactor'].value()
                                      
                #--------------------------       
                # adopt normalization to end up with correct units, depending on input spectrum units:
                if is_dgrb == 1:
                    # spectrum is given as intensity, normalized spatial skymap requires flux,
                    # so normalize intensity to flux on skymap:
                    normalization = normalization * sigmavmin * 10.**(deltalog*scaling_corr) * 4.*np.pi * fsky
                    print "  Delta Omega is ",4.*np.pi * fsky," sr."
                elif is_dgrb == 0:
                    # input spectrum is already given in flux for sigmavmin on sky patch, so no correction necessary.
                    normalization = normalization * 10.**(deltalog*scaling_corr)
                elif is_dgrb == 2:
                    # for the ConstantValue spatial model, the input spectrum is treated as intensity.
                    normalization = normalization * sigmavmin * 10.**(deltalog*scaling_corr) * 4.*np.pi
                    print "  flux is ",sigmav ," times the Crab flux/DGRB intensity/...."
                
                print "  Normalization factor is ", normalization

                #--------------------------       
                # Finally, set normalization values:     
                if is_dgrb == 1:
                    signalmodel.spectral()['Prefactor'].scale(1)
                    signalmodel.spectral()['Prefactor'].value(normalization)
                elif is_dgrb == 0:
                    signalmodel.spectral()['Normalization'].scale(1)
                    signalmodel.spectral()['Normalization'].value(normalization)
                elif is_dgrb == 2:
                    #signalmodel.spatial()['RA'].value(ra_obj_deg)
                    #signalmodel.spatial()['DEC'].value(dec_obj_deg)
                    signalmodel.spectral()['Prefactor'].scale(1)
                    signalmodel.spectral()['Prefactor'].value(normalization)
                    print signalmodel.spectral()['Prefactor']
                
                signalmodel.eval
                
                #print "signal model:"
                #print signalmodel
                #print ""
                
                idstring=idstring[:-1] # throw away last comma
                signalmodel.ids(idstring)
                modelname = signalmodel.name()
                modelname = "skypatch "+str(patch)+": "+modelname
                signalmodel.name(modelname)
                if is_dgrb == 2:
                   fsky = 0.27300475
                   n_points = 343 # on fsky = 0.27300475
                   normalization = signalmodel.spectral()['Prefactor'].value()
                   modelname = signalmodel.name()
                   for i_point in range(n_points):
                       model_tmp = signalmodel
                       pointsource_dir = GSkyDir()
                       lgal_pointsource_deg = 360. * random.random()
                       sin27deg = np.sin(np.deg2rad(27.))
                       sin_bgal = 1. - (1 - sin27deg) * random.random()
                       bgal_pointsource_deg = - np.rad2deg(np.arcsin(sin_bgal))
                       pointsource_dir.lb_deg( lgal_pointsource_deg, bgal_pointsource_deg )
                       ra_pointsource_deg  = pointsource_dir.ra_deg() 
                       dec_pointsource_deg = pointsource_dir.dec_deg()
                       print i_point, "l:",lgal_pointsource_deg,", b:",bgal_pointsource_deg,", RA:",ra_pointsource_deg,", DEC:",dec_pointsource_deg
                       model_tmp.spatial()['RA'].value(ra_pointsource_deg)
                       model_tmp.spatial()['DEC'].value(dec_pointsource_deg)
                       model_tmp.spectral()['Prefactor'].value(normalization/float(n_points) * fsky)
                       model_tmp.name(modelname+" "+str(i_point+1))
                       signalmodels.append(model_tmp)
                       del model_tmp
                else:
                    signalmodels.append(signalmodel)
                print signalmodels
            #observations.models(models)
            
        #print "  signal models in patch", patch,":"

        
        
        #---------------------------------------------------------   
        # add background model:
        if is_bck:
            observations = obsutils.add_background_models(observations)
            #print "  background models in patch", patch,":"
            #print observations.models()
        
        
        
        #---------------------------------------------------------
        # Setting the eventlist output name
        for run in observations:
            
            eventlist_name="eventlist_"
            eventlist_name+=run.id()
            eventlist_name+=".fits"       
            run.eventfile(outdir+"/"+eventlist_name)
            run.ra_obj(target.ra() )
            run.dec_obj(target.dec() )
        
#     # old code: loop simulation for different e_min
#     # Set energy boundaries
#     e_min   = GEnergy(emin_TeV, "TeV")
#     e_max   = GEnergy(emax_TeV, "TeV")
#     ebounds = GEbounds(e_min, e_max)
# 
#     for run in observations:
#         # Allocate event list
#         events = GCTAEventList()
#         roi    = run.roi()
#         gti    = run.gti()
#         events.roi(roi)
#         events.gti(gti)
#         events.ebounds(ebounds)
#         run.events(events)    
#         
    
        # until here, only background models have been added to observations container!
        models = observations.models()
        print "  computation time for loading models in this patch:", round((time() - start_time_patch)/60,2), "minutes."
        
        #---------------------------------------------------------
        # Simulate background events only:
        if is_bck:
            start_time_sim_bck = time() 
            i_run = 0
            for run in observations:
                
                i_run += 1  
                
                obs_runwise = GObservations()
                obs_runwise.models(models)
                # Allocate event list
                ontime = run.ontime()
                
                n_simu_chunks = int(ontime / timechunk)
                
                if timechunk >= ontime:
                     n_simu_chunks = 1
                
                n_simu_chunks_tot = nskypatches * nb_of_obs * n_simu_chunks
                print "  total number of chunks:",n_simu_chunks_tot
                
                print "  # of background chunks in run:", n_simu_chunks
                timechunk_run = ontime / n_simu_chunks
                print "  timechunk_run:", timechunk_run
                
                run.ontime(timechunk_run)
                run.livetime(timechunk_run * dead_time)
                gti = GGti()
                gti.append(GTime(0.0), GTime(timechunk_run))
                run.events().gti(gti)
               
                obs_runwise.append(run)
                
                # return pointing position:
                pointing = run.pointing()
                center_dir = pointing.dir()
                
                for n in range(n_simu_chunks):
                    start_time_bck_chunk = time() 
                    print "  simulate chunk", n, "..."
                    totaltime_check += run.ontime()
                    
                    sim = ctobssim(obs_runwise)
                    sim['seed'].integer(random.randint(0,1000000))
                    sim.run()
                    obs_simu_runwise = sim.obs().copy()
                    del sim
                    
                    # bin simulated events
                    if is_shuffle:
                        print "  shuffle and bin background events in simulated chunk", n, "..."
                    else:
                        print "  bin background events in simulated chunk", n, "..."
                    for r in obs_simu_runwise :
                        run_simu = GCTAObservation(r)
                        for event in run_simu.events() :
                            dir = event.dir().dir()
                            # shuffle events:
                            if is_shuffle:
                                dir = shuffle(dir, center_dir)
                            # rotate events:
                            if is_rotate:
                                dir = rotate(dir, map_center, phi_rot)
                            ra = dir.ra()
                            de = dir.dec()
                            ind = hp.ang2pix( nside_man, 0.5 * np.pi - de, ra ) 
                            binarrays[ind, 0] += 1
                            nevents_bck[0] += 1
                            for i in range(n_thresholds-1):
                                e_min   = GEnergy(e_TeV_thresholds[i+1], "TeV")
                                if event.energy() > e_min:
                                    binarrays[ind, i+1] += 1
                                    nevents_bck[i+1] += 1
                             
                                    
                    del obs_simu_runwise
                    end_time_bck_chunk = round((time() - start_time_bck_chunk)/60,2)
                    if not survey:
                        n_simu_chunks_done =  (patch - 1) * nb_of_obs *  n_simu_chunks + (i_run - 1) * n_simu_chunks + n + 1
                        fraction_done = float(n_simu_chunks_done)/float(n_simu_chunks_tot)
                        time_per_patch = round((time() - start_time)/60,2) / float(n_simu_chunks_done)
                        time_expected_min = float(n_simu_chunks_tot) * (1. - fraction_done) * time_per_patch
                        time_expected_hrs = int(floor(time_expected_min/60.))
                        time_expected_tot_hrs = int(floor((float(n_simu_chunks_tot) * time_per_patch)/60.))
                        print "  ... done",n_simu_chunks_done, "chunks out of estimated",n_simu_chunks_tot,"total number of chunks."
                        print "  => estimated total run time for event simulation:",time_expected_tot_hrs,"hrs,",int(float(n_simu_chunks_tot) * time_per_patch - 60.*time_expected_tot_hrs), "minutes."
                        print "  => estimated remaining run time for event simulation:",time_expected_hrs,"hrs,",int(time_expected_min - 60.*time_expected_hrs), "minutes."
                              
            del obs_runwise
#                 # write out temporary event map image:
#             mapfig = plt.figure()
#             ax = plt.gca()
#             #ax.text(0.95, 0.01, 'colored text in axes coords', \
#             #                  verticalalignment='bottom', horizontalalignment='right', \
#             #                  transform=ax.transAxes)
#             number = "%5.5d" % patch
#             pad1 = "%"+str(3 - int(np.log10(round(totaltime_check/3600.,1))))+".d"
#             pad1 = pad1 %(0.1)
#             pad2 = "%"+str(9 - int(np.log10(nevents_bck[0])))+".d"
#             pad2 = pad2 %(0.1)
#             timelabel = r"$T_{\rm obs}=$" + pad1 + r"$" + str(round(totaltime_check/3600.,1)) +r"\,\rm{h},$ " + pad2 + r"$" + str(nevents_bck[0]) + r"\;\rm{events} \;\geq 30\,\rm{GeV}$"
#             png_name = outdir +'/eventmap'+number+'.png'
#             binarray_tmp = np.zeros(hp.nside2npix(nside_man))
#             binarray_tmp[:] = binarrays[:,0]
#             binarray_tmp[binarray_tmp == 0.] = -1.6375e+30
#             obj_plot = hp.cartview(binarray_tmp, rot = [ map_center.l_deg() , map_center.b_deg()], \
#                               lonra = [- 125./2, 125./2], \
#                               latra = [- 125./2, 125./2], \
#                               xsize=1997, \
#                               flip='astro', \
#                               coord=['C','G'],
#                               min = 1,
#                               max = 25,
#                               title = timelabel,
#                               cmap=plt.cm.plasma,
#                               cbar=True,
#                               unit="Events/bin",
#                               notext=False)#, \
#                               #return_projected_map = True)
#             hp.graticule()
#             savefig = plt.savefig(png_name, format='png', dpi=200, bbox_inches="tight")
#             mapfig.clf()
#             plt.close()
#             del savefig  
#             del obj_plot
#             del mapfig  
#             del binarray_tmp
#             gc.collect()
                
            print "  total obs. time until now: ", totaltime_check, "s"
            print "  simulated background events until now:",nevents_bck[0]
            print "  used memory (GB):",resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/(1024.)**2
            #print "  used memory with psutil:",process.memory_info().rss
            print "  computation time for background simulation in this patch:", round((time() - start_time_sim_bck)/60,2), "minutes."
        
        
        print "  computation time for patch (w/o signal simulation)",patch,":", round((time() - start_time_patch)/60,2), "minutes."
        
        #---------------------------------------------------------
        # Simulate signal events only:
        
        
        if is_signal:
            # calculate size of last patch chunk:            
            if patch > patch_lastfull:
                n_signalpatches = n_signalpatches_last
            else:
                n_signalpatches = n_signalpatches_fix
                   
            observations_quad.extend(observations)
            observations_quad.models(signalmodels)
            start_time_sim_sig = time() 
            quad_patch +=1
            if quad_patch == n_signalpatches:
                # simulate every four patches:
                print "" 
                sim = ctobssim(observations_quad)
                sim['seed'].integer(random.randint(0,1000000))
                #sim['rad'].real(5.) # radius of simulating events
                print "====> simulate signal events in last",n_signalpatches,"patches..."
                start_time_sim_sig2 = time() 
                sim.run()
                time_patches = round((time() - start_time_sim_sig2)/60,2)
                print "  ... done after", time_patches, "minutes. (",time_patches/n_signalpatches," minutes/patch)"
                obs_simu_signal = sim.obs().copy()
                del sim
                
                print "  bin simulated signal events..."
                for r in obs_simu_signal :
                    run_simu = GCTAObservation(r)
                    for event in run_simu.events() :
                        dir = event.dir().dir()
                        # rotate events:
                        #if is_rotate:
                            #dir = rotate(dir, map_center, phi_rot)
                        ra = dir.ra()
                        de = dir.dec()
                        ind = hp.ang2pix( nside_man, 0.5 * np.pi - de, ra ) 
                        binarrays[ind, 0] += 1
                        nevents_sig[0] += 1
                        for i in range(n_thresholds-1):
                            e_min   = GEnergy(e_TeV_thresholds[i+1], "TeV")
                            if event.energy() > e_min:
                                binarrays[ind, i+1] += 1
                                nevents_sig[i+1] += 1
                quad_patch = 0                
                del obs_simu_signal
                del observations_quad
                print "  computation time for signal simulation in last",n_signalpatches," patches:", round((time() - start_time_sim_sig)/60,2), "minutes."
                print "  simulated signal events until now:",nevents_sig[0]
                # calculated remaining run time:
                fraction_done = float(patch)/float(nskypatches)
                time_per_patch = round((time() - start_time)/60,2) / float(patch)
                time_expected_min = float(nskypatches) * (1. - fraction_done) * time_per_patch
                time_expected_hrs = int(floor(time_expected_min/60.))
                time_expected_tot_hrs = int(floor((float(nskypatches) * time_per_patch)/60.))
                print "  => estimated total run time for event simulation:",time_expected_tot_hrs,"hrs,",int(float(nskypatches) * time_per_patch - 60.*time_expected_tot_hrs), "minutes."
                print "  => estimated remaining run time for event simulation:",time_expected_hrs,"hrs,",int(time_expected_min - 60.*time_expected_hrs), "minutes."
                
                # empty observations container:
                observations_quad = GObservations()
            
        if not is_signal:
            # calculated remaining run time:
            fraction_done = float(patch)/float(nskypatches)
            time_per_patch = round((time() - start_time)/60,2) / float(patch)
            time_expected_min = float(nskypatches) * (1. - fraction_done) * time_per_patch
            time_expected_hrs = int(floor(time_expected_min/60.))
            time_expected_tot_hrs = int(floor((float(nskypatches) * time_per_patch)/60.))
            print "  => estimated total run time for bck event simulation:",time_expected_tot_hrs,"hrs,",int(float(nskypatches) * time_per_patch - 60.*time_expected_tot_hrs), "minutes."
            print "  => estimated remaining run time for bck event simulation:",time_expected_hrs,"hrs,",int(time_expected_min - 60.*time_expected_hrs), "minutes."

        print ""    
        del observations
        
    #------- end of large patch loop ------------------------------------------
        
    print "total computation time for simulation events:", round((time() - start_time)/60,2), "minutes."
    print "# of signal events: ", nevents_sig[0]
    print "# of background events: ", nevents_bck[0]
    print ""
    
    #---------------------------------------------------------
    # Write out results:
    
    nevents = nevents_bck + nevents_sig

    fnameappendix_str = "%2.2d" % fnameappendix
 
         
    if survey:
        if is_signal and is_dgrb != 2:
            # set output counts map dimensions to the input clumpy map:
            map_center.lb_deg( psi_map_deg, theta_map_deg )
            roi_rad_deg = 0.4535 * max(size_x_map_deg, size_y_map_deg)
            ra_obj_deg  = map_center.ra_deg() 
            dec_obj_deg = map_center.dec_deg()
        else:
            roi_rad_deg = 0.5 * 122 
            ra_obj_deg  = map_center.ra_deg() 
            dec_obj_deg = map_center.dec_deg()
    
    
    for i in range(n_thresholds):
        print "fill map ",i, "with Healpix blind value.."  
        # any bin with '0' gets replaced with the healpy 'unseen' value -1.6375e+30
        binarray_tmp = np.zeros(hp.nside2npix(nside_man))
        binarray_tmp[:] = binarrays[:,i]
        binarray_tmp[binarray_tmp == 0.] = -1.6375e+30
        emin_GeV_str = str(int(e_GeV_thresholds[i]))
        
        out_file = outdir + "/rawmap_"+emin_GeV_str+"GeV_"+fnameappendix_str+".fits"
        if nevents[i] > 0:
            hp.write_map(out_file, binarray_tmp, partial=True, coord='C', dtype=np.int16)#, column_names="Countsmap", column_units="counts")
            hdulist_out = fits.open(out_file, mode='update')
            hdulist_out[1].header.set('EXTNAME', 'Skymap')
            hdulist_out[1].header.set('COORDSYS', 'C')
            hdulist_out[1].header.set('PSI_0', ra_obj_deg ) # last target position
            hdulist_out[1].header.set('THETA_0', dec_obj_deg ) # last target position
            hdulist_out[1].header.set('SIZE_X', 2.1 * roi_rad_deg + offset )
            hdulist_out[1].header.set('SIZE_Y', 2.1 * roi_rad_deg + offset )
            hdulist_out[1].header.set('NEVENTS', nevents[i], 'total # of events in map')
            print "------------------------------"
            print nevents[i], " total events above", emin_GeV_str, "GeV"
            hdulist_out[1].header.set('N_SIG', nevents_sig[i], '# of signal events in map')
            print "  ", nevents_sig[i], " signal events above", emin_GeV_str, "GeV"
            hdulist_out[1].header.set('N_BCK', nevents_bck[i], '# of background events in map')
            print "  ", nevents_bck[i], " background events above", emin_GeV_str, "GeV"
            print "------------------------------"
            hdulist_out[1].header.set('EMIN', e_TeV_thresholds[i], 'lower bound of energy bin in TeV')
            hdulist_out[1].header.set('EMAX', emax_TeV, 'upper bound of energy bin in TeV')
            hdulist_out[1].header.set('TTYPE1', 'PIXEL')
            hdulist_out[1].header.set('TTYPE2', 'Counts')
            if is_signal:
                hdulist_out[1].header.set('MODEL', modelname, 'Name of input signal model')
                if is_dgrb == 0:  
                    hdulist_out[1].header.set('SIGMAV', sigmav, '[cm^3/s] WIMP thermal cross-section.')
                    hdulist_out[1].header.set('DMMASS', dmmass, '[GeV] WIMP mass.')
                if is_dgrb == 1:  
                    hdulist_out[1].header.set('SIGMAV', sigmav, 'percentage of DGRB intensity.')
                if is_dgrb == 2:  
                    hdulist_out[1].header.set('SIGMAV', sigmav, 'percentage of DGRB intensity.')
            hdulist_out.flush()
            hdulist_out.close()
        del binarray_tmp
        
#         out_file = outdir + "/cl_rawmap_"+emin_GeV_str+"GeV_"+fnameappendix_str+".fits"
#         if nside_man > 4096:
#             LMAX = 4096
#         else:
#             LMAX = nside_man
#         cl = hp.anafast(binarrays[:,i], lmax=LMAX)
#         hp.write_cl(out_file, cl)
#         hdulist_out = fits.open(out_file, mode='update')
#         hdulist_out[1].header.set('EXTNAME', 'CountsMapAPS')
#         hdulist_out[1].header.set('NSIDE', nside_man, ' HEALPIX resolution of binning')
#         hdulist_out[1].header.set('NEVENTS', nevents[i], '# of events in map')
#         hdulist_out[1].header.set('N_SIG', nevents_sig[i], '# of signal events in map')
#         hdulist_out[1].header.set('N_BCK', nevents_bck[i], '# of background events in map')
#         hdulist_out[1].header.set('EMIN', e_TeV_thresholds[i], 'lower bound of energy bin in TeV')
#         hdulist_out[1].header.set('EMAX', emax_TeV, 'upper bound of energy bin in TeV')
#         hdulist_out[1].header.set('TTYPE1', 'Counts')
#         if is_signal:
#             hdulist_out[1].header.set('MODEL', modelname, 'Name of input signal model')
#             if is_dgrb == 0:  
#                 hdulist_out[1].header.set('SIGMAV', sigmav, '[cm^3/s] WIMP thermal cross-section.')
#                 hdulist_out[1].header.set('DMMASS', dmmass, '[GeV] WIMP mass.')
#             if is_dgrb == 1:  
#                 hdulist_out[1].header.set('SIGMAV', sigmav, 'percentage of DGRB intensity.')
#         hdulist_out.flush()
#         hdulist_out.close()
#         del cl
     
    
#     obs0 = GCTAObservation(obs[0])
#     ra_deg = pointing.ra_deg()
#     dec_deg = pointing.dec_deg()
#     roi_rad_deg = obs0.roi().radius()
#     tmin = obs0.events().tstart().secs()
#     tmax = obs0.events().tstop().secs()
    

#     e_min   = GEnergy(emin_TeV, "TeV")
#     # Clone observations and reset energy thresholds
#     select = ctselect(obs_simu)
#     select["ra"]   = "UNDEFINED"
#     select["dec"]  = "UNDEFINED"
#     select["rad"]  = "UNDEFINED"
#     select["tmin"] = "UNDEFINED"
#     select["tmax"] = "UNDEFINED"
#     select["emin"].real(e_min.TeV())
#     select["emax"].real(e_max.TeV())
#     #select["outobs"] = outdir + "/selectevents.fits"
#     select.run()
#  
#     # now delete obs_simu object:
#     del obs_simu
#     
#     obs_select = select.obs().copy()
#     
#     del select


    print ""
    print "script subhaloFluctResponse.sim.py successfully finished."
    print "total computation time:", round((time() - start_time)/60,2), "minutes."


##    shuffle events:   #######################################################
###############################################################################
def shuffle(dir, center_dir):
    # shuffle events around center_dir, keeping the distance from center_dir constant.
    
    ra_center  = center_dir.ra()
    dec_center = np.pi / 2 - center_dir.dec()
    
    phi_random = 2. * np.pi * random.random()
    
    # get distance of event position from center position:
    radius = center_dir.dist(dir)
    
    # shuffle:
    dec_shuffled, ra_shuffled = veripy.polarCoordinatesOnSphere(dec_center, ra_center, radius, phi_random)
    
    dir.radec(ra_shuffled[0], np.pi / 2 - dec_shuffled[0])
    
    return dir


##    rotate events:   #######################################################
###############################################################################
def rotate(dir, center_dir, phi_rot):
    # rotate direction around center_dir, by angle phi_rot
    # work in ra. dec
    
    ra  = dir.ra()
    theta = np.pi / 2 - dir.dec()
    
    ra_center  = center_dir.ra()
    theta_center = np.pi / 2 - center_dir.dec()

    # get unit vectors:
    dir_vec = hp.ang2vec(theta, ra)
    center_dir_vec = hp.ang2vec(theta_center, ra_center)
    
    dir_rot_vec = (1. - np.cos(phi_rot)) * np.dot(dir_vec, center_dir_vec) * center_dir_vec + \
                   np.cos(phi_rot) * dir_vec + np.sin(phi_rot)  * np.cross(center_dir_vec, dir_vec)
    
    theta_rot, ra_rot = hp.vec2ang(dir_rot_vec)
    
    ra_rot = ra_rot[0]
    dec_rot = np.pi / 2 - theta_rot[0]

    dir.radec(ra_rot, dec_rot)
    
    return dir

##    execute main part:    ###################################################
###############################################################################

if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################
