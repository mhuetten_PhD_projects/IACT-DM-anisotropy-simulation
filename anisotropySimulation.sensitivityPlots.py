import numpy as np
#import matplotlib
from matplotlib import pyplot as plt
from matplotlib import rc,rcParams
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
#import pickle 
#import os
#import sys
#import getopt
#from matplotlib.backends.backend_pdf import PdfPages as pdf
#from matplotlib.patches import Rectangle
from textwrap import wrap
#import pyfits as pf
from numpy.ma.core import min
from scipy.interpolate import UnivariateSpline


dumpDirectory = "/afs/ifh.de/group/cta/scratch/mhuetten/Plot-Bilder/"

ctaDMsensitivityTauTau = np.loadtxt(dumpDirectory+"ctaDMsensitivityTauTau.txt")#, comments="#", names=True)
ctaDMsensitivityMuMu = np.loadtxt(dumpDirectory+"ctaDMsensitivityMuMu.txt")#, comments="#", names=True)

veritasDMsensitivitySegueTauTau = np.loadtxt(dumpDirectory+"veritasDMsensitivityTauTauSegue_corr.txt")#, comments="#", names=True)
veritasDMsensitivitySegueWW = np.loadtxt(dumpDirectory+"veritasDMsensitivityWWSegue_corr.txt")#, comments="#", names=True)

veritasDMsensitivityCombTauTau = np.loadtxt(dumpDirectory+"veritasDMsensitivityTauTauCombDwarf.txt")#, comments="#", names=True)
veritasDMsensitivityCombbb = np.loadtxt(dumpDirectory+"veritasDMsensitivitybbCombDwarf.txt")#, comments="#", names=True)

veritasMyResultTauTau = np.loadtxt(dumpDirectory+"veritasMyResultTauTau.txt")#, comments="#", names=True)
veritasMyResultWW = np.loadtxt(dumpDirectory+"veritasMyResultWW.txt")#, comments="#", names=True)

#ctaMyResultTauTau = np.loadtxt(dumpDirectory+"ctaMyResultTauTau.txt")#, comments="#", names=True)
#ctaMyResultWW = np.loadtxt(dumpDirectory+"ctaMyResultWW.txt")#, comments="#", names=True)


# remote plots:
ctaDMsensitivityTauTau_x = ctaDMsensitivityTauTau[:,0]
ctaDMsensitivityTauTau_y = ctaDMsensitivityTauTau[:,1]
ctaDMsensitivityMuMu_x = ctaDMsensitivityMuMu[:,0]
ctaDMsensitivityMuMu_y = ctaDMsensitivityMuMu[:,1]

veritasDMsensitivitySegueTauTau_x = veritasDMsensitivitySegueTauTau[:,0]
veritasDMsensitivitySegueTauTau_y = veritasDMsensitivitySegueTauTau[:,1]

veritasDMsensitivityWW_x = veritasDMsensitivitySegueWW[:,0]
veritasDMsensitivityWW_y = veritasDMsensitivitySegueWW[:,1]

veritasDMsensitivityCombTauTau_x = veritasDMsensitivityCombTauTau[:,0]
veritasDMsensitivityCombTauTau_y = veritasDMsensitivityCombTauTau[:,1]

veritasDMsensitivityCombbb_x = veritasDMsensitivityCombbb[:,0]
veritasDMsensitivityCombbb_y = veritasDMsensitivityCombbb[:,1]

xmin_ctaDMsensitivityTauTau = min(ctaDMsensitivityTauTau_x)
xmin_ctaDMsensitivityMuMu = min(ctaDMsensitivityMuMu_x)
xmin_veritasDMsensitivitySegueTauTau = min(veritasDMsensitivitySegueTauTau_x)
xmin_veritasDMsensitivityWW = min(veritasDMsensitivityWW_x)
xmin_veritasDMsensitivityCombTauTau = min(veritasDMsensitivityCombTauTau_x)
xmin_veritasDMsensitivityCombbb = min(veritasDMsensitivityCombbb_x)

xmax_ctaDMsensitivityTauTau = max(ctaDMsensitivityTauTau_x)
xmax_ctaDMsensitivityMuMu = max(ctaDMsensitivityMuMu_x)
xmax_veritasDMsensitivitySegueTauTau = max(veritasDMsensitivitySegueTauTau_x)
xmax_veritasDMsensitivityWW = max(veritasDMsensitivityWW_x)
xmax_veritasDMsensitivityCombTauTau = max(veritasDMsensitivityCombTauTau_x)
xmax_veritasDMsensitivityCombbb = max(veritasDMsensitivityCombbb_x)

xmin = min([xmin_ctaDMsensitivityTauTau,xmin_ctaDMsensitivityMuMu,xmin_veritasDMsensitivitySegueTauTau,xmin_veritasDMsensitivityWW])
xmax = max([xmax_ctaDMsensitivityTauTau,xmax_ctaDMsensitivityMuMu,xmax_veritasDMsensitivitySegueTauTau,xmax_veritasDMsensitivityWW])



#measured data:
veritasMyResultTauTau_x = veritasMyResultTauTau[:,0]
veritasMyResultTauTau_y = veritasMyResultTauTau[:,1]
veritasMyResultTauTau_err = veritasMyResultTauTau[:,2]

veritasMyResultWW_x = veritasMyResultWW[:,0]
veritasMyResultWW_y = veritasMyResultWW[:,1]
veritasMyResultWW_err = veritasMyResultWW[:,2]
#ctaMyResultTauTau_x = ctaMyResultTauTau[:,0]
#ctaMyResultTauTau_y = ctaMyResultWW[:,1]
 
xmin_veritasMyResultTauTau = min(veritasMyResultTauTau_x)
xmin_veritasMyResultWW = min(veritasMyResultWW_x)
 
xmax_veritasMyResultTauTau = max(veritasMyResultTauTau_x)
xmax_veritasMyResultWW = max(veritasMyResultWW_x)
 
xmin_data = max([xmin_veritasMyResultTauTau,xmin_veritasMyResultWW])
xmax_data = min([xmax_veritasMyResultTauTau,xmax_veritasMyResultWW])
 
# move to log scale for interpolation:


# VERITAS paper data:
veritasDMsensitivityTauTau_x_smooth_data_log = np.linspace(np.log10(xmin_veritasDMsensitivitySegueTauTau),np.log10(xmax_veritasDMsensitivitySegueTauTau), 100)
veritasDMsensitivityWW_x_smooth_data_log = np.linspace(np.log10(xmin_veritasDMsensitivityWW),np.log10(xmax_veritasDMsensitivityWW), 100)
veritasDMsensitivityCombTauTau_x_smooth_data_log = np.linspace(np.log10(xmin_veritasDMsensitivityCombTauTau),np.log10(xmax_veritasDMsensitivityCombTauTau), 100)
veritasDMsensitivityCombbb_x_smooth_data_log = np.linspace(np.log10(xmin_veritasDMsensitivityCombbb),np.log10(xmax_veritasDMsensitivityCombbb), 100)

veritasDMsensitivityTauTau_y_smoothSpline_log = UnivariateSpline(np.log10(veritasDMsensitivitySegueTauTau_x),np.log10(veritasDMsensitivitySegueTauTau_y),k=5)
veritasDMsensitivityWW_y_smoothSpline_log = UnivariateSpline(np.log10(veritasDMsensitivityWW_x),np.log10(veritasDMsensitivityWW_y),k=5)
veritasDMsensitivityCombTauTau_y_smoothSpline_log = UnivariateSpline(np.log10(veritasDMsensitivityCombTauTau_x),np.log10(veritasDMsensitivityCombTauTau_y),k=5)
veritasDMsensitivityCombbb_y_smoothSpline_log = UnivariateSpline(np.log10(veritasDMsensitivityCombbb_x),np.log10(veritasDMsensitivityCombbb_y),k=5)

veritasDMsensitivityTauTau_y_smooth_log = veritasDMsensitivityTauTau_y_smoothSpline_log(veritasDMsensitivityTauTau_x_smooth_data_log)
veritasDMsensitivityWW_y_smooth_log = veritasDMsensitivityWW_y_smoothSpline_log(veritasDMsensitivityWW_x_smooth_data_log)
veritasDMsensitivityCombTauTau_y_smooth_log = veritasDMsensitivityCombTauTau_y_smoothSpline_log(veritasDMsensitivityCombTauTau_x_smooth_data_log)
veritasDMsensitivityCombbb_y_smooth_log = veritasDMsensitivityCombbb_y_smoothSpline_log(veritasDMsensitivityCombbb_x_smooth_data_log)

veritasDMsensitivityTauTau_y_smooth = 10**(veritasDMsensitivityTauTau_y_smooth_log)
veritasDMsensitivityWW_y_smooth = 10**(veritasDMsensitivityWW_y_smooth_log)
veritasDMsensitivityCombTauTau_y_smooth = 10**(veritasDMsensitivityCombTauTau_y_smooth_log)
veritasDMsensitivityCombbb_y_smooth = 10**(veritasDMsensitivityCombbb_y_smooth_log)

veritasDMsensitivityTauTau_x_smooth_data = 10**(veritasDMsensitivityTauTau_x_smooth_data_log)
veritasDMsensitivityWW_x_smooth_data = 10**(veritasDMsensitivityWW_x_smooth_data_log)
veritasDMsensitivityCombTauTau_x_smooth_data = 10**(veritasDMsensitivityCombTauTau_x_smooth_data_log)
veritasDMsensitivityCombbb_x_smooth_data = 10**(veritasDMsensitivityCombbb_x_smooth_data_log)

# CTA paper data:
ctaDMsensitivityTauTau_x_smooth_data_log = np.linspace(np.log10(xmin_ctaDMsensitivityTauTau),np.log10(xmax_ctaDMsensitivityTauTau), 100)
ctaDMsensitivityMuMu_x_smooth_data_log = np.linspace(np.log10(xmin_ctaDMsensitivityMuMu),np.log10(xmax_ctaDMsensitivityMuMu), 100)

ctaDMsensitivityTauTau_y_smoothSpline_log = UnivariateSpline(np.log10(ctaDMsensitivityTauTau_x),np.log10(ctaDMsensitivityTauTau_y),k=5)
ctaDMsensitivityMuMu_y_smoothSpline_log = UnivariateSpline(np.log10(ctaDMsensitivityMuMu_x),np.log10(ctaDMsensitivityMuMu_y),k=5)

ctaDMsensitivityTauTau_y_smooth_log = ctaDMsensitivityTauTau_y_smoothSpline_log(ctaDMsensitivityTauTau_x_smooth_data_log)
ctaDMsensitivityMuMu_y_smooth_log = ctaDMsensitivityMuMu_y_smoothSpline_log(ctaDMsensitivityMuMu_x_smooth_data_log)

ctaDMsensitivityTauTau_y_smooth = 10**(ctaDMsensitivityTauTau_y_smooth_log)
ctaDMsensitivityMuMu_y_smooth = 10**(ctaDMsensitivityMuMu_y_smooth_log)

ctaDMsensitivityTauTau_x_smooth_data = 10**(ctaDMsensitivityTauTau_x_smooth_data_log)
ctaDMsensitivityMuMu_x_smooth_data = 10**(ctaDMsensitivityMuMu_x_smooth_data_log)




# my VERITAS results:
veritasMyResultTauTau_x_smooth_data_log = np.linspace(np.log10(xmin_veritasMyResultTauTau),np.log10(xmax_veritasMyResultTauTau), 100)
veritasMyResultWW_x_smooth_data_log = np.linspace(np.log10(xmin_veritasMyResultWW),np.log10(xmax_veritasMyResultWW), 100)

veritasMyResultTauTau_y_smoothSpline_log = UnivariateSpline(np.log10(veritasMyResultTauTau_x),np.log10(veritasMyResultTauTau_y),k=3)
veritasMyResultWW_y_smoothSpline_log = UnivariateSpline(np.log10(veritasMyResultWW_x),np.log10(veritasMyResultWW_y),k=3)

veritasMyResultTauTau_y_smooth_log = veritasMyResultTauTau_y_smoothSpline_log(veritasMyResultTauTau_x_smooth_data_log)
veritasMyResultWW_y_smooth_log = veritasMyResultWW_y_smoothSpline_log(veritasMyResultWW_x_smooth_data_log)

veritasMyResultTauTau_y_smooth = 10**(veritasMyResultTauTau_y_smooth_log)
veritasMyResultWW_y_smooth = 10**(veritasMyResultWW_y_smooth_log)

veritasMyResultTauTau_x_smooth_data = 10**(veritasMyResultTauTau_x_smooth_data_log)
veritasMyResultWW_x_smooth_data = 10**(veritasMyResultWW_x_smooth_data_log)

# model bands:

yup = [3e-26,3e-26]
ylow = [3e-27,3e-27]

##########################
# Plot 1:

rc('text',usetex=True)
rcParams.update({'font.size': 16})

plot1 = plt.figure(figsize=(14, 9))

p4 = plt.plot(veritasMyResultWW_x_smooth_data,veritasMyResultWW_y_smooth,color='darkred',alpha=1.,linestyle="-", linewidth="1.5",label=r"VERITAS Anisotropies (simulation), $W^+W^-$, $300\,\mathrm{h}$") 
p3 = plt.plot(veritasMyResultTauTau_x_smooth_data,veritasMyResultTauTau_y_smooth,color='blue',alpha=1.,linestyle="-",linewidth="1.5", label=r"VERITAS Anisotropies (simulation), $\tau^+\tau^-$, $300\,\mathrm{h}$")

p6 = plt.errorbar(veritasMyResultWW_x,veritasMyResultWW_y,yerr=veritasMyResultWW_err,color='darkred',alpha=1.,linewidth="1.5",linestyle="",marker='o')
p5 = plt.errorbar(veritasMyResultTauTau_x,veritasMyResultTauTau_y,yerr=veritasMyResultTauTau_err,color='blue',alpha=1.,linewidth="1.5",linestyle="",marker='o')

p2 = plt.plot(veritasDMsensitivityWW_x_smooth_data,veritasDMsensitivityWW_y_smooth,color='darkred',alpha=1.,linestyle="--",linewidth="1.5", label=r"VERITAS limit (Segue 1, corrected), $W^+W^-$, $48\,\mathrm{h}$")
p1 = plt.plot(veritasDMsensitivityTauTau_x_smooth_data,veritasDMsensitivityTauTau_y_smooth,color='blue',alpha=1.,linestyle="--",linewidth="1.5", label=r"VERITAS limit (Segue 1, corrected), $\tau^+\tau^-$, $48\,\mathrm{h}$")

p7 = plt.plot(veritasDMsensitivityCombbb_x_smooth_data,veritasDMsensitivityCombbb_y_smooth,color='darkred',alpha=1.,linestyle=":",linewidth="2", label=r"VERITAS limit (combined Dwarfs), $b\bar{b}$")
p8 = plt.plot(veritasDMsensitivityCombTauTau_x_smooth_data,veritasDMsensitivityCombTauTau_y_smooth,color='blue',alpha=1.,linestyle=":",linewidth="2", label=r"VERITAS limit (combined Dwarfs), $\tau^+\tau^-$")


#p2a = plt.plot(ctaDMsensitivityMuMu_x_smooth_data,ctaDMsensitivityMuMu_y_smooth,color='orange',alpha=1.,linestyle="--",linewidth="1.5", label=r"CTA projection (Galactic Center), $\mu^+\mu^-$, $500\,\mathrm{h}$")
#p1a = plt.plot(ctaDMsensitivityTauTau_x_smooth_data,ctaDMsensitivityTauTau_y_smooth,color='red',alpha=1.,linestyle="--",linewidth="1.5", label=r"CTA projection (Galactic Center), $\tau^+\tau^-$, $500\,\mathrm{h}$")



p7 = plt.fill_between([xmin,xmax],ylow,yup,color='gainsboro',alpha=1. )

plt.legend(loc='upper right',prop={'size':15})
plt.xlabel('Dark Matter particle mass $m_{\chi}$ $[\mathrm{GeV}]$'); plt.ylabel(r'$\langle\sigma v \rangle$ $[\mathrm{cm^3\,s^{-1}}]$'); plt.grid()
plt.xscale('log')
plt.yscale('log')
#plt.title(r"$\alpha > \beta$")
pylab.xlim([xmin,10**4])
pylab.ylim([10**(-27),10**(-19)])
saveplot=""
#plt.savefig("/afs/ifh.de/user/m/mhuetten/hu-data/Promotion Dokumente/Grafiken/Plots-eigene/DManisotropiesSensitivityV1.png", dpi = 600)

 
plt.show()