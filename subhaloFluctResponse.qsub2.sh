#$ -S /bin/bash
#
# script to perform Dark Matter clumpy simulations AND detector response
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script

ODIR=OODIR
SCRIPTDIR=DIIIR

TOTALTIME=AAAAA
CALIBDB=BBBBB
IRF=CCCCC
INSTRUMENT=DDDDD
DEADTIME=EEEEE
EMIN=FFFFF
EMAX=GGGGG
OFFSET=HHHHH
SURVEY=IIIII
POINTINGFILE=JJJJJ
DMMASS=KKKKK
DELTALOG=LLLLL
ISDGRB=MMMMM
RUNDIR=NNNNN
ISSIGNAL=OOOOO
ISBACKGROUND=PPPPP
ISSHUFFLE=QQQQQ
NSIDEMANUAL=RRRRR
MODULEBOOL=SSSSS
SIGMAVSTART=TTTTT
PHIROT=UUUUU

source /afs/ifh.de/user/m/mhuetten/setbatchenvironment.sh VERITAS
echo ""

cd $ODIR
cd ..
WORKDIR="$(pwd)"

cd $ODIR


if [[ $SGE_TASK_ID == "1000" ]]; then
	mkdir -p $WORKDIR/Logfiles
	# background simulation
	python $RUNDIR/Inputfiles/subhaloFluctResponse.sim.py --workdir $WORKDIR  --outdir $ODIR  --instrument $INSTRUMENT --deadtime $DEADTIME --emin $EMIN --emax $EMAX --dmmass $DMMASS --scaling $SGE_TASK_ID --deltalog $DELTALOG --offset $OFFSET --survey $SURVEY --pointingfile $POINTINGFILE --isdgrb $ISDGRB --issignal $ISSIGNAL --isbck $ISBACKGROUND --isshuffle $ISSHUFFLE --nsideman $NSIDEMANUAL --fnameappendix $MODULEBOOL --sigmavmin $SIGMAVSTART --phirot $PHIROT | tee $WORKDIR/Logfiles/subhaloSim-Background-${MODULEBOOL}.log
else
	mkdir -p Logfiles
	mkdir -p relicX$SGE_TASK_ID	
	#copydir="/lustre/fs19/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/16-06-15-DMsurvey500h_varExposure_paranal_fine/tautau/realisation-1/mchi_${DMMASS}GeV/relicX$SGE_TASK_ID/"
	#cp $copydir/*_0${MODULEBOOL}.fits relicX$SGE_TASK_ID/
	python $RUNDIR/Inputfiles/subhaloFluctResponse.sim.py --workdir $WORKDIR  --outdir $ODIR/relicX$SGE_TASK_ID  --instrument $INSTRUMENT --deadtime $DEADTIME --emin $EMIN --emax $EMAX --dmmass $DMMASS --scaling $SGE_TASK_ID --deltalog $DELTALOG --offset $OFFSET --survey $SURVEY --pointingfile $POINTINGFILE --isdgrb $ISDGRB --issignal $ISSIGNAL --isbck $ISBACKGROUND --isshuffle $ISSHUFFLE --nsideman $NSIDEMANUAL --fnameappendix $MODULEBOOL --sigmavmin $SIGMAVSTART  --phirot $PHIROT | tee $ODIR/Logfiles/subhaloSim-relicX${SGE_TASK_ID}-${MODULEBOOL}.log
fi

##sleep 20
echo " *** Job successfully finished ***"
##exit
