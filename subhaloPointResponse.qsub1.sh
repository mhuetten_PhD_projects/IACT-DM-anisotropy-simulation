#$ -S /bin/bash
#
# script to perform Dark Matter clumpy simulations AND detector response
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script


ODIR=OODIR
SCRIPTDIR=DIIIR
RUNDIR=RUUNN

TOTALTIME=AAAAA
OBSWIDTH=ROOOI
ITER=IITER
CALIBDB=BBBBB
IRF=CCCCC
INSTRUMENT=DDDDD
DEADTIME=EEEEE
EMIN=FFFFF
EMAX=GGGGG
CHANNEL=CHNNL
ISANNIHILATION=ANNHL
HALOFILE=HAALO
OFFSET=HHHHH
DMMASS=KKKKK
SIGMA=JJJJJ
ENUMBINS=BIIIN

echo
echo " *** Prepare MC simulation ***"
echo "   for DM mass: "$DMMASS
echo 

source /afs/ifh.de/user/m/mhuetten/setbatchenvironment.sh VERITAS
echo

# temporary directory
DDIR=$TMPDIR/Moritz
mkdir -p $DDIR
rm -f $DDIR/*
echo "Temporary directory:"
echo $DDIR



cp $CLUMPY/bin/clumpy $DDIR


### generate parameter file for spectra computation:
cd $ODIR
cd ..
REALISATIONDIR="$(pwd)"
cd $ODIR

cp $CLUMPY/clumpy_params_dummyspec.txt tmp1.dat

MEAN=$(python $RUNDIR/Inputfiles/printKeywordValue.py -i $HALOFILE -k MEAN)
FLUX_TOT=$(python $RUNDIR/Inputfiles/printKeywordValue.py -i $HALOFILE -k FLUX_TOT)
echo "total flux: ", $FLUX_TOT

if [[ $CHANNEL == "bbbar" ]]; then
	CHANNELVEC="0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
	sed  's/CHANNELVEC/'$CHANNELVEC'/' tmp1.dat > tmp2.dat
elif [[ $CHANNEL == "tautau" ]]; then 
	CHANNELVEC="0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
	sed  's/CHANNELVEC/'$CHANNELVEC'/' tmp1.dat > tmp2.dat
elif [[ $CHANNEL == "ww" ]]; then 
	CHANNELVEC="0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1.0,0,0,0,0,0,0,0,0,0,0,0,0"
	sed  's/CHANNELVEC/'$CHANNELVEC'/' tmp1.dat > tmp2.dat
else
	echo "unnknown channel"
fi
sed  's|output|'-1'|' tmp2.dat > tmp3.dat
sed  's|3e-26|'3e-22'|' tmp3.dat > clumpy_params.dat

cp -r $CLUMPY/PPPC4DMID-spectra .

EMIN_GEV=`echo 1000 \* $EMIN |bc`
EMAX_GEV=`echo 1000 \* $EMAX |bc`


echo " *** generate spectra ***"
$DDIR/clumpy -zp clumpy_params.dat $EMIN_GEV $EMAX_GEV 1000  $DMMASS kCIRELLI11_EW  kGAMMA $FLUX_TOT  $ISANNIHILATION  0.
rm -r PPPC4DMID-spectra
rm dnde*
#rm clumpy_params.dat
mv *.txt $ODIR/spectrum_CIRELLI11_EW_${DMMASS}_GeV_${CHANNEL}.dat

NAME="MORITZ"
### generate ctools model file:
sed  's|HALOFILE|'$HALOFILE'|' $RUNDIR/Inputfiles/subhaloPointResponse.subhalo_model.xml > tmp1.dat
sed  's|SPECTRUMFILE|'spectrum_CIRELLI11_EW_${DMMASS}_GeV_${CHANNEL}.dat'|' tmp1.dat > tmp2.dat
sed  's|NAME|'$NAME'|' tmp2.dat > tmp3.dat
sed  's|SPECTRUMPATH|'$ODIR'|' tmp3.dat > subhaloPointResponse.subhalo_model_${DMMASS}_GeV.xml

### generate ctools parameter file:
sed  's|INMODEL|'$ODIR/subhaloPointResponse.subhalo_model_${DMMASS}_GeV.xml'|' $RUNDIR/Inputfiles/subhaloPointResponse.cssens.par > tmp1.dat
sed  's|CALIBDB|'$CALIBDB'|' tmp1.dat > tmp2.dat
sed  's|IRF|'$IRF'|' tmp2.dat > tmp3.dat
sed  's|TOTALTIME|'$TOTALTIME'|' tmp3.dat > tmp4.dat
sed  's|DEADTIME|'$DEADTIME'|' tmp4.dat > tmp5.dat
sed  's|EMIN|'$EMIN'|' tmp5.dat > tmp6.dat
sed  's|EMAX|'$EMAX'|' tmp6.dat > tmp7.dat
sed  's|OFFSET|'$OFFSET'|' tmp7.dat > tmp8.dat
sed  's|OBSWIDTH|'$OBSWIDTH'|' tmp8.dat > tmp9.dat
sed  's|ITER|'$ITER'|' tmp9.dat > tmp10.dat
sed  's|ENUMBINS|'$ENUMBINS'|' tmp10.dat > tmp11.dat
sed  's|SIGMA|'$SIGMA'|' tmp11.dat > tmp12.dat
sed  's|NAME|'$NAME'|' tmp12.dat > cssens.par


rm tmp*


echo " *** calculate sensitivity ***"
cp  $RUNDIR/Inputfiles/subhaloPointResponse.cssens.py .
cp  $RUNDIR/Inputfiles/subhaloPointResponse.ts_stat.py .
echo "using parameter file:" $ODIR/cssens.par
python subhaloPointResponse.cssens.py -p $ODIR/cssens.par | tee $REALISATIONDIR/Logfiles/sens_${DMMASS}_GeV.log

# clean up:

rm *.py
#rm *.xml
#rm spectrum*
#rm clumpy_params.dat
#rm cssens.par
mv cssens.log $REALISATIONDIR/Logfiles/cssens_${DMMASS}_GeV.log


# calculate sensitivity





##sleep 20
echo " *** Job successfully finished ***"
##exit
