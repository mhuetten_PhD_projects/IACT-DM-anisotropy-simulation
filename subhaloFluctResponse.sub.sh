#!/bin/sh
#
# script for running full Dark Matter anisotropy simulations:
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]; then
	echo
	echo " Submission script for running full Dark Matter anisotropy simulations,"
	echo " which can also be used for running a given number of realisations parallely "
	echo " for a statistical variation of the result. Also, a runparameter can be specified,"
	echo " for which all variations of this parameter are run parallely."
	echo 
	echo " For a given number of identical simulations,"
	echo " - firstly, a clumpy skymap is computed with multipole evaluation"
	echo " - secondly, a Detector Monte Carlo is run with again a multipole evaluation"
	echo
	echo " anisotropySimulation.sub.sh <anisotropySimulation.parameters> [number of simulations] [developing mode] [job priority] [anisotropySimulation.runparameter]"
	echo " exception for module 5:"
	echo " anisotropySimulation.sub.sh <anisotropySimulation.parameters>     5         [plotted column] [plot skymap bool] [plot bin maps bool] [anisotropySimulation.runparameter]"
	echo
	echo "  <anisotropySimulation.parameters> :	for the parameter files use template without changing line numbers"
	echo "  					= 5 : plot healpix files (locally). If there are more than 1 realisations, the first realisation is plotted."
	echo "  [number of simulations] :		repeat same calculation for n times (for statistical purposes)"
	echo "  [developing mode] :			= 0 : switched off (default)"
	echo "  					= 1 : switched on: jobs take only 4G and 30 minutes max. on the cluster"
	echo "  [module bool] :		= 1 : do all (make skymaps and instrument response)"
	echo "  					= 2 : do only instrument response (assume that skymaps are alread provided)"
	echo "  [anisotropySimulation.runparameter]:	In case of choosing a varying parameter in the anisotropySimulation.runparameter file,"
	echo "  					write name of parameter in first line, then all values you want to compute."
	echo "  					Example:"
	echo "    					user_rse"
	echo "    					5.0"
	echo "    					10.0"
	echo "    					15.0"
	echo

   exit
fi

DMCLUMPDIR=/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation

#read variables from parameter file:

# read parameters:
export runname=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')

export pointingfile=$(cat $1	| head -n22 | tail -n1 | sed -e 's/^[ \t]*//')
export obswidth=$(cat $1	    | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')
export SURVEY=$(cat $1	        | head -n28 | tail -n1 | sed -e 's/^[ \t]*//')
SURVEY=${SURVEY:0:1} 
export OFFSET=$(cat $1          | head -n31 | tail -n1 | sed -e 's/^[ \t]*//')

export INSTRUMENT=$(cat $1      | head -n38 | tail -n1 | sed -e 's/^[ \t]*//')

export CHANNEL=$(cat $1         | head -n44 | tail -n1 | sed -e 's/^[ \t]*//')
export DMMASSES=$(cat $1        | head -n47 | tail -n1 | sed -e 's/^[ \t]*//')
export EMINVALS=$(cat $1        | head -n50 | tail -n1 | sed -e 's/^[ \t]*//')
export ISANNIHILATION=$(cat $1  | head -n53 | tail -n1 | sed -e 's/^[ \t]*//')
export SIGMAVSTARTVALS=$(cat $1  | head -n56 | tail -n1 | sed -e 's/^[ \t]*//')
export DELTALOG=$(cat $1        | head -n59 | tail -n1 | sed -e 's/^[ \t]*//')
export NPOINTSXSECTION=$(cat $1 | head -n62 | tail -n1 | sed -e 's/^[ \t]*//')

export ISDGRB=$(cat $1          | head -n69 | tail -n1 | sed -e 's/^[ \t]*//')
export ISSIGNAL=$(cat $1        | head -n72 | tail -n1 | sed -e 's/^[ \t]*//')
export ISBACKGROUND=$(cat $1    | head -n75 | tail -n1 | sed -e 's/^[ \t]*//')
export ISSHUFFLE=$(cat $1       | head -n78 | tail -n1 | sed -e 's/^[ \t]*//')

export NSIDEMANUAL=$(cat $1     | head -n81 | tail -n1 | sed -e 's/^[ \t]*//')

export COMBINERUNS=$(cat $1     | head -n84 | tail -n1 | sed -e 's/^[ \t]*//')

export SKYMAPFILE=$(cat $1      | head -n87 | tail -n1 | sed -e 's/^[ \t]*//')

#export TOTALTIME=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export CALDB=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export IRF=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export INSTRUMENT=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export DEADTIME=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export EMIN=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export EMAX=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export OFFSET=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export SURVEY=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')


TOTALTIME=20
CALIBDB="prod2"
IRF="South_50h"

DEADTIME=0.95
EMIN_TOTAL=30
EMAX_TOTAL=199000

DMMASSES=($DMMASSES)
EMINVALS=($EMINVALS)
SIGMAVSTARTVALS=($SIGMAVSTARTVALS)

DMMASSES_STR=$(IFS=" " ; echo "${DMMASSES[*]}")
SIGMAVSTARTVALS_STR=$(IFS=" " ; echo "${SIGMAVSTARTVALS[*]}")

# number of repeated realisations:
samplenumber=1
if [ -n "$2" ]; then
samplenumber=$2
fi

# Scriptdirectory is current directory, save it:
SCRIPTDIR="$(pwd)"

# Save the date:
DATE=`date +"%y%m%d"`

# Make Logfile directory for submission scripts and batch logfiles output:
cd $SCRATCH/LOGS/IACT-SIMULATION
#mkdir -p $DATE
QLOG=$SCRATCH/LOGS/IACT-SIMULATION/ #$DATE

# developing mode?
devmode=0
if [ -n "$3" ]; then
devmode=$3
fi
if [[ $devmode == "0" ]]; then
	wallclockTime=47:59:59
	cpuTime=47:59:59
	memorySize=8G
	tmpdirSize=24G
elif [[ $devmode == "1" ]]; then
	wallclockTime=00:29:59
	cpuTime=00:29:59
	memorySize=4G
	tmpdirSize=1G
else
	echo "EXIT: devmode variable must be either 0 or 1"
	echo
	exit
fi


# module bool
modulebool="0"
if [ -n "$4" ]; then
	modulebool=$4
fi


if [[ $COMBINERUNS == "1" ]]; then
	while true; do
	    read -p "Do you want to combine existing runs? (yes/no) " yn
	    case $yn in
	        [yes]* ) break;;
	        [no]* ) exit;;
	        * ) echo "Please answer yes or no.";;
	    esac
	done
	echo "ok, continue..."
	modulebool="1"
fi


if [[ $modulebool == "0" ]]; then
	while true; do
    read -p "Do you really want to delete all old files? (yes/no) " yn
    case $yn in
        [yes]* ) break;;
        [no]* ) exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo "ok, continue..."
fi



# job priority?
priority=0
if [ -n "$5" ]; then
	priority=$5
fi



cd $outdir
mkdir -p $runname
cd $runname
RUNDIR="$(pwd)"

# empty directory if start from scratch:
if [[ $modulebool == "0" ]]; then
	echo "remove old data."
	rm -r *
fi
# now forget outdir

# skeleton script
FSCRIPT1="subhaloFluctResponse.qsub1"
FSCRIPT2="subhaloFluctResponse.qsub2"
FSCRIPT3="subhaloFluctResponse.qsub3"

###############################################################################
# If a runparameter file is parsed:
###############################################################################

if [ -n "$6" ]; then
	cd $RUNDIR
	#rm -r Inputfiles
	mkdir -p Inputfiles
	
	if [[ $moduleBool == "3" || $moduleBool == "4" || $moduleBool == "5" ]]; then
		echo "module 3, 4 or 5: don't copy input files"
		runParameterFile=$outdir/$runname/Inputfiles/anisotropySimulation.runparameter
	elif [[ $moduleBool == "1" || ${DMspectrumName:0:10} == "astroPower" || ${DMspectrumName} == "noDarkMatter" ]]; then
		runParameterFile=$6
		cp anisotropySimulation.* $RUNDIR/Inputfiles/
		rm $RUNDIR/Inputfiles/anisotropySimulation.clumpy_params.txt
	elif [[ $moduleBool == "2" ]]; then
		runParameterFile=$outdir/$runname/Inputfiles/anisotropySimulation.runparameter
	fi

	# number of parameters:
	export runparameter=$(cat $runParameterFile 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $runParameterFile | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter
	echo
		
	cd $SCRIPTDIR
	
	###########################################################################
	# now loop over all varied runparameter values:
	for AFIL in $PARAMETERS
	do
		if [[ $moduleBool != "4" && $moduleBool != "5" ]]; then	
			echo "now starting parameter run $runparameter = $AFIL"
			# create output directories:   
			cd $RUNDIR
			mkdir $runparameter-$AFIL
			#rm -r Logfiles
			mkdir -p $runparameter-$AFIL/Logfiles
		else
			echo "now evaluating parameter run $runparameter = $AFIL"
		fi
		
		cd $RUNDIR/$runparameter-$AFIL
		ODIR="$(pwd)"

		cd $SCRIPTDIR
		if [[ $moduleBool == "1" || ${DMspectrumName:0:10} == "astroPower" || ${DMspectrumName} == "noDarkMatter" ]]; then
			rm $RUNDIR/Inputfiles/anisotropySimulation.clumpy_params.txt
		fi
		
		SCRIPTLOG=$QLOG
		
		case $runparameter in
			"user_rse") user_rse=$AFIL;;
			"alphaIntDeg") alphaIntDeg=$AFIL;;
			"gDM_MMIN_SUBS") gDM_MMIN_SUBS=$AFIL;;
			"gGAL_DPDM_SLOPE") gGAL_DPDM_SLOPE=$AFIL;;
			"gGAL_RHOSOL") gGAL_RHOSOL=$AFIL;;
			"gGAL_SUBS_N_INM1M2") gGAL_SUBS_N_INM1M2=$AFIL;;
			"gGAL_CLUMPS_FLAG_PROFILE") gGAL_CLUMPS_FLAG_PROFILE=$AFIL;;
			"gGAL_DPDV_FLAG_PROFILE") gGAL_DPDV_FLAG_PROFILE=$AFIL;;
			"sigmav") sigmav=$AFIL;;
			"mchi") mchi=$AFIL;;
			"thresholdBin") thresholdBin=$AFIL;;
		esac
		
		if [[ $moduleBool != "4" && $moduleBool != "5" ]]; then			
			###################################################################
			# now loop over all identical (statistical) realisations:
			for i in $(seq 1 1 $samplenumber)
			do
				if [[ $samplenumber == "1" ]]; then
				FNAM="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL"
				FNAM23="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL-mod23"
				onlyOneRunBool=1
				else 
				FNAM="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL-$i"
				FNAM23="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL-$i-mod23"
				onlyOneRunBool=0
				echo "now starting sample realisation number $i"   
				fi
							
				sed -e "s|FFFFF|$user_rse|" \
					-e "s|ZHHHH|$seed|" \
					-e "s|PEEED|$QLOG|" \
					-e "s|OODIR|$ODIR|" \
					-e "s|DAATE|$DATE|" \
					-e "s|MMMMM|$gGAL_RHOSOL|" \
					-e "s|NNNNN|$gGAL_RSOL|" \
					-e "s|OOOOO|$gGAL_RVIR|" \
					-e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
					-e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
					-e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
					-e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
					-e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
					-e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
					-e "s|WWWWW|$gDM_MMIN_SUBS|" \
					-e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
					-e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
					-e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
					-e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
					-e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
					-e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
					-e "s|AAAAA|$psiZeroDeg|" \
					-e "s|BBBBB|$thetaZeroDegGal|" \
					-e "s|CCCCC|$psiWidthDeg|" \
					-e "s|DDDDD|$thetaWidthDeg|" \
					-e "s|EEEEE|$alphaIntDeg|" \
					-e "s|RRRRR|$AFIL|" \
					-e "s|RUUUN|$RUNDIR|" \
					-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
					-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
					-e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
					-e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
					-e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
					-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
					-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
					-e "s|ZEEEE|$gDM_RHOSAT|" \
					-e "s|ZFFFF|$gGAL_SUBS_M1|" \
					-e "s|ZGGGG|$gGAL_SUBS_M2|" \
					-e "s|ZIIII|$TWOsigmaFOVdeg|" \
					-e "s|ZJJJJ|$TWOsigmaPSFdeg|" \
					-e "s|ZNNNN|$i|" \
					-e "s|ZOOOO|$mchi|" \
					-e "s|ZPPPP|$DMspectrumName|" \
					-e "s|ZQQQQ|$effAreaFile|" \
					-e "s|ZRRRR|$sigmav|" \
					-e "s|ZSSSS|$sensitivityFile|" \
					-e "s|ZTTTT|$tobs|" \
					-e "s|ZUUUU|$backgroundmodel|" \
					-e "s|ZVVVV|$astroAnisotropies|" \
					-e "s|ZWWWW|$onlyOneRunBool|" \
					-e "s|ZXXXX|$moduleBool|" \
					-e "s|ZYYYY|$energyBins|" \
					-e "s|ZKKKK|$noise|" \
					-e "s|ZLLLL|$elevation|" \
					-e "s|ZMMMM|$azimutBin|" \
					-e "s|ZZAAA|$phiZeroAstroFlux|" \
					-e "s|ZZBBB|$thresholdBin|" \
					-e "s|ZZCCC|$alpha|" \
					-e "s|ZZDDD|$wobbleOffset|" \
					-e "s|ZZEEE|$thetaSquared|" \
					-e "s|ZZFFF|$IRFspectralindex|" \
					-e "s|ZZGGG|$MCvsREC|" \
					-e "s|ZZHHH|$gLIST_HALOES|" \
					-e "s|ZZIII|$gLIST_Bool|" \
					-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh
				
				chmod u+x $FNAM.sh
				echo "script name is:" $FNAM.sh
	
				if [[ $devmode == "1" ]]; then
					echo "submitting job in developing mode (30 minutes," $memorySize "RAM)"
				fi
	
				if [[ $moduleBool == "1" ]]; then
					echo "compute Dark Matter skymap"
					qsub -V -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -js $priority "$FNAM.sh"
				elif [[ $moduleBool == "2" ]]; then
					echo "compute instrument response (skymap.healpix file in output directory needed)"	
					JOBID=`qsub -V -terse -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -t 1-$energyBins:1 -js $priority "$FNAM.sh"`
					echo "Job array" $JOBID "("${FNAM}.sh") for filling energy energy bins has been submitted."
					JOBID=${JOBID:0:7}
					sed -e "s|moduleBool=2|moduleBool=3|" $FNAM.sh > $FNAM23.sh
					JOBID2=`qsub -V -terse -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -js $priority -hold_jid $JOBID "$FNAM23.sh"`
					echo "Then combine energy bins with job" $JOBID2 "(" $FNAM23.sh ")"
				elif [[ $moduleBool == "3" ]]; then
					echo "combine eventmap-binXX.healpix files"
					qsub -V -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -js $priority "$FNAM.sh"
				else
					echo "EXIT: moduleBool variable must be either 1, 2, 3, 4, or 5"
					echo
					exit
				fi
				echo
			done

		elif [[ $moduleBool == "4" ]]; then
			###################################################################
			# module 4: evaluate power spectra of statistical realisations locally
			echo "evaluating power spectra..."
			# grep number of runs:
			cd $ODIR/spectra-mc
			echo "ODIR is:" $ODIR
			realisations=$(ls -1 combinedRawMap* | wc -l)
			echo "number of spectrum realisations in spectra-mc folder:" $realisations
			echo "evaluated number of spectrum realisations (from input):" $samplenumber
			cd $SCRIPTDIR
			python $SCRIPTDIR/anisotropySimulation.evaluatePowerSpectra.py -a $alphaIntDeg -f $TWOsigmaFOVdeg  -p $TWOsigmaPSFdeg -l $psiZeroDeg -b $thetaZeroDegGal -c $energyBins -d $thresholdBin -r $effAreaFile -q $sensitivityFile -n $noise -v $elevation -z $azimutBin -t $tobs -s $sigmav -m $mchi -y $DMspectrumName -k $backgroundmodel -j $astroAnisotropies -x $phiZeroAstroFlux -g $samplenumber -w $backgroundOnlyPath -i $ODIR -o $ODIR/spectra-mc | tee $ODIR/Logfiles/module4-evaluatePowerSpectra_thresholdBin$thresholdBin.log
		elif [[ $moduleBool == "5" ]]; then
			cd $ODIR
			if [ -d "realisation-1" ]; then
  				WORKDIR=$ODIR/realisation-1
  			else
  				WORKDIR=$ODIR
			fi
			cd $SCRIPTDIR
			echo "plot healpix files"
			python $SCRIPTDIR/anisotropySimulation.quickPlotEventmap.py -a $alphaIntDeg -l $psiZeroDeg -f $TWOsigmaFOVdeg -b $thetaZeroDegGal -c $energyBins -d $thresholdBin -r $effAreaFile -q $sensitivityFile -n $noise -v $elevation -z $azimutBin -t $tobs -s $sigmav -m $mchi -y $DMspectrumName -k $backgroundmodel -j $astroAnisotropies -x $phiZeroAstroFlux -g $alpha -u $thetaSquared -w $wobbleOffset -p $showDMskymapBool -e $showBinMapsBool -i $WORKDIR -o $column
		fi
	done
fi

###############################################################################
# If NO runparameter file exists:
###############################################################################

if [ ! -n "$6" ]; then

	echo "submitting single job to batch"

	# RUNDIR is equal ODIR in this case!
	
	AFIL=singlerundummy
	# AFIL is not used in this case!
	
	cd $RUNDIR
	ODIR="$(pwd)"
	cd $SCRIPTDIR

	if [[ $COMBINERUNS != "1" ]]; then

		#rm -r Logfiles
		#rm -r Inputfiles
		
		mkdir -p $RUNDIR/Inputfiles
		#mkdir -p Logfiles
		cp $CLUMPY/python_helperscripts/makeFitsImage.py $RUNDIR/Inputfiles/
		cp $CLUMPY/python_helperscripts/printKeywordValue.py $RUNDIR/Inputfiles/
		
		cp subhaloFluctResponse.* $RUNDIR/Inputfiles/
		rm $RUNDIR/Inputfiles/subhaloFluctResponse.pointings.dat
		rm $RUNDIR/Inputfiles/*.runparameter
		echo $pointingfile
		cp $pointingfile $RUNDIR/Inputfiles/subhaloFluctResponse.pointings${modulebool}.dat
		mv $RUNDIR/Inputfiles/subhaloFluctResponse.parameters $RUNDIR/Inputfiles/subhaloFluctResponse.parameters${modulebool}
		pointingfile=$RUNDIR/Inputfiles/subhaloFluctResponse.pointings${modulebool}.dat
	fi 
	
	if [[ $COMBINERUNS == "1" ]]; then
		cp subhaloFluctResponse.* $RUNDIR/Inputfiles/
	fi
	
	SCRIPTLOG=$QLOG

	startnum=$((41))
        offset=$((40))

	one=$((1))
	endnum=$(($startnum+$samplenumber-$one))

	###########################################################################
	# now loop over all identical (statistical) realisations:
	for i in  $(seq $startnum 1 $endnum)
	do
		realisationnumber=$i # this rename has just been done because of some buggy reading of the sed command...
		
		if [[ $samplenumber == "1" ]]; then
			FNAM1="$SCRIPTLOG/subhaloFluctResponse-$runname-Stage1"
			FNAM2="$SCRIPTLOG/subhaloFluctResponse-$runname-Stage2"
			FNAM3="$SCRIPTLOG/subhaloFluctResponse-$runname-Combine"
			onlyOneRunBool=1
			outdir_tmp=$outdir
			runname_tmp=$runname
		else 
			FNAM1="$SCRIPTLOG/subhaloFluctResponse-$runname-Stage1-$i"
			FNAM2="$SCRIPTLOG/subhaloFluctResponse-$runname-Stage2-$i"
			FNAM3="$SCRIPTLOG/subhaloFluctResponse-$runname-Combine-$i"
			onlyOneRunBool=0
			echo "now starting sample realisation number $i"
			mkdir -p $ODIR/realisation-${i}
			outdir_tmp=$outdir/$runname
			runname_tmp=realisation-${i}
			
			j=$(($i+$offset))
			k=$(($i-$offset))
			runname_tmp2=realisation-${j}
			runname_tmp3=realisation-${k}
		fi
		
		if [[ $devmode == "1" ]]; then
			echo "submitting job in developing mode (30 minutes," $memorySize "RAM)"
		fi
	
		if [[ $ISSIGNAL == "1" ]]; then
			
			sed -e "s|OODIR|$outdir_tmp/$runname_tmp|"  \
				-e "s|DIIIR|$SCRIPTDIR|" \
				-e "s|CHNNL|$CHANNEL|" \
				-e "s|ANNHL|$ISANNIHILATION|" \
				-e "s|FFFFF|$EMIN_TOTAL|" \
	 			-e "s|GGGGG|$EMAX_TOTAL|" \
				-e "s|MASSS|$DMMASSES_STR|" \
				-e "s|SIGMV|$SIGMAVSTARTVALS_STR|" \
				-e "s|HHHHH|$SKYMAPFILE|" \
				-e "s|RUUNN|$RUNDIR|"  ${FSCRIPT1}.sh > ${FNAM1}.sh
# 			-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
# 			-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
# 			-e "s|ZEEEE|$gDM_RHOSAT|" \
# 			-e "s|ZFFFF|$gGAL_SUBS_M1|" \
# 			-e "s|ZGGGG|$gGAL_SUBS_M2|" \
# 			-e "s|ZIIII|$TWOsigmaFOVdeg|" \
# 			-e "s|ZJJJJ|$TWOsigmaPSFdeg|" \
# 			-e "s|ZNNNN|$i|" \
# 			-e "s|ZOOOO|$mchi|" \
# 			-e "s|ZPPPP|$DMspectrumName|" \
# 			-e "s|ZQQQQ|$effAreaFile|" \
# 			-e "s|ZRRRR|$sigmav|" \
# 			-e "s|ZSSSS|$sensitivityFile|" \
# 			-e "s|ZTTTT|$tobs|" \
# 			-e "s|ZUUUU|$backgroundmodel|" \
# 			-e "s|ZVVVV|$astroAnisotropies|" \
# 			-e "s|ZWWWW|$onlyOneRunBool|" \
# 			-e "s|ZXXXX|$moduleBool|" \
# 			-e "s|ZYYYY|$energyBins|" \
# 			-e "s|ZKKKK|$noise|" \
# 			-e "s|ZLLLL|$elevation|" \
# 			-e "s|ZMMMM|$azimutBin|" \
# 			-e "s|ZZAAA|$phiZeroAstroFlux|" \
# 			-e "s|ZZBBB|$thresholdBin|" \
# 			-e "s|ZZCCC|$alpha|" \
# 			-e "s|ZZDDD|$wobbleOffset|" \
# 			-e "s|ZZEEE|$thetaSquared|" \
# 			-e "s|ZZFFF|$IRFspectralindex|" \
# 			-e "s|ZZGGG|$MCvsREC|" \
# 			-e "s|ZZHHH|$gLIST_HALOES|" \
# 			-e "s|ZZIII|$gLIST_Bool|" \
# 			-e "s|DIIIR|$SCRIPTDIR|"  ${FSCRIPT1}.sh > ${FNAM}1.sh
			chmod u+x ${FNAM1}.sh				
				
			cd $DMCLUMPDIR
			JOBSID1=""
			
			lineno=0
			
		
			if [[ $modulebool == "0" ]]; then 
					[ ! -f $pointingfile ] && { echo "$pointingfile file not found"; exit 99; }

					
				outdir_tmp2=$outdir_tmp/$runname_tmp
				
				sed -i '12s|.*|'${outdir_tmp2}'|' clumpyV2.parameters
				sed -i '40s|.*|'${obswidth}'|' clumpyV2.parameters
				sed -i '52s|.*|'${ISANNIHILATION}'|' clumpyV2.parameters
				
				## do only simulate several patches if not in overlapping survey mode:
				if [[ $SURVEY == "0"   && $ISDGRB != "2" ]]; then 
					while IFS="," read -r psi theta ssn tel status || [[ -n "$status" ]]
					do
						if [[ $lineno == 0 ]]; then 
							lineno=$(($lineno + 1))
							continue
						fi
							
						runname_tmp2=map-${lineno}
						# replace output directory of clumps simulation
						sed -i '9s|.*|'${runname_tmp2}'|' clumpyV2.parameters
						sed -i '31s|.*|'${psi}'|' clumpyV2.parameters
						sed -i '34s|.*|'${theta}'|' clumpyV2.parameters

						JOBID1=$(./clumpyV2.sub.sh clumpyV2.parameters 1 0 2>&1)
						JOBID1=${JOBID1:(-8)}
						echo "Job " $JOBID1 " for computing DM skymap has been submitted."
						JOBSID1+=$JOBID1
						JOBSID1+=","
						lineno=$(($lineno + 1))
						
					done < $pointingfile
					
				elif [[ $SURVEY == "1"   && $ISDGRB != "2" ]]; then

					
					if [[ $SKYMAPFILE == "none" ]]; then
						runname_tmp2=map-1
						sed -i '9s|.*|'${runname_tmp2}'|' clumpyV2.parameters
						JOBID1=$(./clumpyV2.sub.sh clumpyV2.parameters 1 0 2>&1)
						JOBID1=${JOBID1:(-8)}
						echo "Job " $JOBID1 " for computing DM skymap has been submitted."
						JOBSID1+=$JOBID1
						JOBSID1+=","
					else
						JOBSID1=00000001
						mkdir -p $outdir_tmp2/map-1
					fi
					lineno=2
					
				else 
					echo "ISDGRB=2 Job.... Note: SURVEY must be either 0 or 1. Is:" $SURVEY
				fi	
				lineno=$(($lineno - 1))
				echo "Have "$lineno" observation positions in this simulation."
			fi
			
			cd $SCRIPTDIR
			
			# transform FITS map job:
			if [[ $modulebool == "0"  && $ISDGRB != "2" ]]; then 
				JOBID2=`qsub -V -terse -j y -m a -l h_cpu=00:29:59 -l h_rt=00:29:59  -l os=sl6 -l h_rss=4G -l tmpdir_size=4G -o $QLOG/ -e $QLOG/ -js $priority -hold_jid "$JOBSID1" -t 1-$lineno:1  "${FNAM1}.sh"`
				JOBID2=${JOBID2:0:8}
				echo "Then transform FITS maps in job" $JOBID2
				echo "script name is:" ${FNAM1}.sh
			else
					JOBID2="10000000"
			fi
			
			i_dm=0
			
			for DMMASS in  "${DMMASSES[@]}"
			do
				mchidir=$outdir_tmp/$runname_tmp/mchi_${DMMASS}GeV/
				signalODIR=$outdir_tmp/$runname_tmp3/mchi_${DMMASS}GeV/
				backgroundODIR=/lustre/fs19/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/17-12-12-DMandAstroSurvey500h_homExp_1deg/dgrb_background/$runname_tmp/   #  
				backgroundODIR2= #/lustre/fs19/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/16-08-02-DMsurvey500h_RipkenCompareSig/dgrb_2010/$runname_tmp2/   #  
				echo "...DM Mass "$DMMASS" GeV..."
				
				# get EMIN:
				EMIN=${EMINVALS[$i_dm]}
				# set EMAX to DM mass in GeV:
				EMAX=$DMMASS
				# get sigmav to start with:
				SIGMAVSTART=${SIGMAVSTARTVALS[$i_dm]}
				echo "...Energy interval: E=["$EMIN","$EMAX"] GeV, <sigmav>_start="$SIGMAVSTART"..."
				
				
				if [[ $COMBINERUNS != "1" ]]; then 
					# Then simulate!
			
					# create directories:
					mkdir -p $mchidir
					
					sed -e "s|OODIR|$mchidir|"  \
						-e "s|DIIIR|$SCRIPTDIR|" \
			  			-e "s|AAAAA|$TOTALTIME|" \
			 			-e "s|BBBBB|$CALIBDB|" \
			 			-e "s|CCCCC|$IRF|" \
			 			-e "s|DDDDD|$INSTRUMENT|" \
			 			-e "s|EEEEE|$DEADTIME|" \
			 			-e "s|FFFFF|$EMIN|" \
			 			-e "s|GGGGG|$EMAX|" \
			 			-e "s|HHHHH|$OFFSET|" \
			 			-e "s|IIIII|$SURVEY|"  \
			  			-e "s|JJJJJ|$pointingfile|" \
			 			-e "s|KKKKK|$DMMASS|" \
			 			-e "s|LLLLL|$DELTALOG|" \
			 			-e "s|MMMMM|$ISDGRB|" \
			 			-e "s|OOOOO|$ISSIGNAL|" \
			 			-e "s|PPPPP|$ISBACKGROUND|" \
			 			-e "s|QQQQQ|$ISSHUFFLE|" \
			 			-e "s|RRRRR|$NSIDEMANUAL|" \
			 			-e "s|SSSSS|$modulebool|" \
			 			-e "s|TTTTT|$SIGMAVSTART|" \
			 			-e "s|UUUUU|$realisationnumber|" \
			 			-e "s|NNNNN|$RUNDIR|"  ${FSCRIPT2}.sh > ${FNAM2}-${DMMASS}.sh
			 		chmod u+x ${FNAM2}-${DMMASS}.sh

					JOBID3=`qsub -V -terse -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority -hold_jid $JOBID2 -t 1-$NPOINTSXSECTION:1 "${FNAM2}-${DMMASS}.sh"`
					echo "Then compute instrument response for DM mass in job" $JOBID3
					echo "script name is:" ${FNAM2}-${DMMASS}.sh
				else
					# combine runs
                                        echo "Combined files written to "$mchidir
					echo "Signal files taken from "$signalODIR
					echo "Background files taken from "$backgroundODIR
					#echo "2nd Background files taken from "$backgroundODIR2
					# create directories:
					mkdir -p $mchidir

					sed -e "s|OODIR|$mchidir|"  \
						-e "s|SODIR|$signalODIR|" \
						-e "s|IREAL|$i|" \
						-e "s|BODIR|$backgroundODIR|" \
						-e "s|BODR2|$backgroundODIR2|" \
						-e "s|OOOOO|$ISSIGNAL|" \
			 			-e "s|PPPPP|$ISBACKGROUND|" \
			 			-e "s|QQQQQ|${EMINVALS[$i_dm]}|" \
			 			-e "s|NNNNN|$RUNDIR|"  ${FSCRIPT3}.sh > ${FNAM3}-${DMMASS}.sh
			 		chmod u+x ${FNAM3}-${DMMASS}.sh
			
					JOBID5=`qsub -V -terse -j y -m a -l h_cpu=23:59:59 -l h_rt=23:59:59  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority  -t 1-$NPOINTSXSECTION:1 "${FNAM3}-${DMMASS}.sh"`
					echo "Combining runs for DM mass in job" $JOBID5
					echo "script name is:" ${FNAM3}-${DMMASS}.sh
					
				fi
				i_dm=$(($i_dm + 1))
			done
		fi
		
		if [[ $ISSIGNAL == "0" ]]; then
			
			# in case of a DM backgrond only simulation for a DM patch,
			# simulate DM patch for each DM mass, because of different
			# energy thresholds
			if [[ $ISDGRB == "0" ]]; then
				
				i_dm=0
				SIGMAVSTART=1e-40
				for DMMASS in  "${DMMASSES[@]}"
				do
					OODIR=$outdir_tmp/$runname_tmp/mchi_${DMMASS}GeV/Background
					mkdir -p $OODIR
					backgroundODIR=/lustre/fs19/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/17-12-12-DMandAstroSurvey500h_homExp_1deg/dgrb_background/$runname_tmp/Background
					backgroundODIR2= #/lustre/fs19/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/16-08-02-DESdwarfs1000hWobble_paranal_background/$runname_tmp/mchi_${DMMASS}GeV/Background
					# get EMIN:
					EMIN=${EMINVALS[$i_dm]}
					i_dm=$(($i_dm + 1))
					# set EMAX to DM mass
					EMAX=$DMMASS
					
					echo "...Background only hypothesis for DM Mass "$DMMASS" GeV..."
					echo "...DM Mass "$DMMASS" GeV. E=["$EMIN","$EMAX"] GeV..." 
					
					if [[ $COMBINERUNS != "1" ]]; then 
						mkdir -p $OODIR 

						sed -e "s|OODIR|$OODIR|"  \
							-e "s|DIIIR|$SCRIPTDIR|" \
				  			-e "s|AAAAA|$TOTALTIME|" \
				 			-e "s|BBBBB|$CALIBDB|" \
				 			-e "s|CCCCC|$IRF|" \
				 			-e "s|DDDDD|$INSTRUMENT|" \
				 			-e "s|EEEEE|$DEADTIME|" \
				 			-e "s|FFFFF|$EMIN|" \
				 			-e "s|GGGGG|$EMAX|" \
				 			-e "s|HHHHH|$OFFSET|" \
				 			-e "s|IIIII|$SURVEY|"  \
				  			-e "s|JJJJJ|$pointingfile|" \
				 			-e "s|KKKKK|$DMMASS|" \
				 			-e "s|LLLLL|$DELTALOG|" \
				 			-e "s|MMMMM|$ISDGRB|"  \
				 			-e "s|OOOOO|$ISSIGNAL|" \
				 			-e "s|PPPPP|$ISBACKGROUND|" \
				 			-e "s|QQQQQ|$ISSHUFFLE|" \
				 			-e "s|RRRRR|$NSIDEMANUAL|" \
				 			-e "s|SSSSS|$modulebool|" \
				 			-e "s|TTTTT|$SIGMAVSTART|" \
				 			-e "s|UUUUU|$realisationnumber|" \
					 		-e "s|NNNNN|$RUNDIR|" ${FSCRIPT2}.sh > ${FNAM2}-${DMMASS}-Bck.sh
			 			chmod u+x ${FNAM2}-${DMMASS}-Bck.sh
						qsub -V -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority -t "1000" "${FNAM2}-${DMMASS}-Bck.sh"
						echo " Submitted. Script name is:" ${FNAM2}-${DMMASS}-Bck.sh
					else
						# combine maps and calculate power spectrum of background only

						echo "take runs from folder 1:" $backgroundODIR
						echo "take runs from folder 2:" $backgroundODIR2

						sed -e "s|OODIR|$OODIR|"  \
							-e "s|OOOOO|$ISSIGNAL|" \
							-e "s|BODIR|$backgroundODIR|" \
							-e "s|BODR2|$backgroundODIR2|" \
				 			-e "s|PPPPP|$ISBACKGROUND|" \
				 			-e "s|QQQQQ|$EMIN|" \
				 			-e "s|NNNNN|$RUNDIR|"  ${FSCRIPT3}.sh > ${FNAM3}-${DMMASS}-Bck.sh
				 		chmod u+x ${FNAM3}-${DMMASS}-Bck.sh
				
						JOBID5=`qsub -V -terse -j y -m a -l h_cpu=11:59:59 -l h_rt=11:59:59  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority  -t "1000" "${FNAM3}-${DMMASS}-Bck.sh"`
						echo "Calculate power spectrum for  DM mass in job" $JOBID5 " , priority: "$priority
						echo "script name is:" ${FNAM3}-${DMMASS}-Bck.sh
					fi
				done
			else
				SIGMAVSTART=1e-40
				DMMASS=1e-40
				# create background run:
				if [[ $ISSIGNAL == "0" ]]; then
					OODIR=$outdir_tmp/$runname_tmp/Background
					mkdir -p $OODIR
				elif [[ $ISDGRB == "2" ]]; then
					OODIR=$outdir_tmp/$runname_tmp/Pointsource
					mkdir -p $OODIR
				fi
				EMIN=${EMINVALS[0]}
				EMAX=${DMMASSES[0]}
				echo "...Background only hypothesis for DGRB spectrum..."
				echo "   Energy interval["$EMIN","$EMAX"] GeV..." 	
				
				if [[ $COMBINERUNS != "1" ]]; then 
					sed -e "s|OODIR|$OODIR|"  \
						-e "s|DIIIR|$SCRIPTDIR|" \
			  			-e "s|AAAAA|$TOTALTIME|" \
			 			-e "s|BBBBB|$CALIBDB|" \
			 			-e "s|CCCCC|$IRF|" \
			 			-e "s|DDDDD|$INSTRUMENT|" \
			 			-e "s|EEEEE|$DEADTIME|" \
			 			-e "s|FFFFF|$EMIN|" \
			 			-e "s|GGGGG|$EMAX|" \
			 			-e "s|HHHHH|$OFFSET|" \
			 			-e "s|IIIII|$SURVEY|"  \
			  			-e "s|JJJJJ|$pointingfile|" \
			 			-e "s|KKKKK|$DMMASS|" \
			 			-e "s|LLLLL|$DELTALOG|" \
			 			-e "s|MMMMM|$ISDGRB|"  \
			 			-e "s|OOOOO|$ISSIGNAL|" \
			 			-e "s|PPPPP|$ISBACKGROUND|" \
			 			-e "s|QQQQQ|$ISSHUFFLE|" \
			 			-e "s|RRRRR|$NSIDEMANUAL|" \
			 			-e "s|SSSSS|$modulebool|" \
			 			-e "s|TTTTT|$SIGMAVSTART|" \
			 			-e "s|UUUUU|$realisationnumber|" \
				 		-e "s|NNNNN|$RUNDIR|" ${FSCRIPT2}.sh > ${FNAM2}-Bck.sh
			 		chmod u+x ${FNAM2}-Bck.sh
			 		
					if [[ $ISSIGNAL == "0" ]]; then
						echo "...Background only hypothesis..."
						qsub -V -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority -t "1000" "${FNAM2}-Bck.sh"
					#echo "Then compute Background only hypothesis in job" $JOBID4
					echo "script name is:" ${FNAM2}-Bck.sh
					elif [[ $ISDGRB == "2" ]]; then
						echo "...calculating point source response..."
						qsub -V -j y -m a -l h_cpu=$cpuTime -l h_rt=$wallclockTime  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority -t 1-$NPOINTSXSECTION:1 "${FNAM2}-Bck.sh"
					#echo "Then compute Background only hypothesis in job" $JOBID4
					echo "script name is:" ${FNAM2}-Bck.sh
					fi
				else
					# combine maps and calculate power spectrum of background only

					echo "take runs from folder 1:" $OODIR
					backgroundODIR2=/lustre/fs19/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/17-12-12-DMandAstroSurvey500h_homExp_1deg/dgrb_background/$runname_tmp/Background/
                                        echo "take runs from folder 2:" $backgroundODIR2

						sed -e "s|OODIR|$OODIR|"  \
						-e "s|OOOOO|$ISSIGNAL|" \
						-e "s|BODIR|$OODIR|" \
						-e "s|BODR2|$backgroundODIR2|" \
			 			-e "s|PPPPP|$ISBACKGROUND|" \
			 			-e "s|QQQQQ|$EMIN|" \
			 			-e "s|NNNNN|$RUNDIR|"  ${FSCRIPT3}.sh > ${FNAM3}-${DMMASS}-Bck.sh
			 		chmod u+x ${FNAM3}-${DMMASS}-Bck.sh
			
					JOBID5=`qsub -V -terse -j y -m a -l h_cpu=11:59:59 -l h_rt=11:59:59  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority  -t "1000" "${FNAM3}-${DMMASS}-Bck.sh"`
					echo "Calculate power spectrum for  DM mass in job" $JOBID5 " , priority: "$priority
					echo "script name is:" ${FNAM3}-${DMMASS}-Bck.sh
				fi
			fi
		fi

	done
fi
exit
