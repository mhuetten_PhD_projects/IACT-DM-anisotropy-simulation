#!/bin/sh
#
# script for running full Dark Matter anisotropy simulations:
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]; then
	echo
	echo " Submission script for running full Dark Matter anisotropy simulations,"
	echo " which can also be used for running a given number of realisations parallely "
	echo " for a statistical variation of the result. Also, a runparameter can be specified,"
	echo " for which all variations of this parameter are run parallely."
	echo 
	echo " For a given number of identical simulations,"
	echo " - firstly, a clumpy skymap is computed with multipole evaluation"
	echo " - secondly, a Detector Monte Carlo is run with again a multipole evaluation"
	echo
	echo " anisotropySimulation.sub.sh <anisotropySimulation.parameters> [number of simulations] [developing mode] [job priority] [anisotropySimulation.runparameter]"
	echo " exception for module 5:"
	echo " anisotropySimulation.sub.sh <anisotropySimulation.parameters>     5         [plotted column] [plot skymap bool] [plot bin maps bool] [anisotropySimulation.runparameter]"
	echo
	echo "  <anisotropySimulation.parameters> :	for the parameter files use template without changing line numbers"
	echo "  [module bool] :			= 1 : compute Dark Matter skymap"
	echo "  					= 2 : compute instrument response (skymap.healpix file in output directory needed)"
	echo "  					= 3 : combine eventmap-binXX.healpix files"
	echo "  					= 4 : evaluating power spectra (locally)"
	echo "  					= 5 : plot healpix files (locally). If there are more than 1 realisations, the first realisation is plotted."
	echo "  [number of simulations] :		repeat same calculation for n times (for statistical purposes)"
	echo "  [developing mode] :			= 0 : switched off (default)"
	echo "  					= 1 : switched on: jobs take only 4G and 30 minutes max. on the cluster"
	echo "  [anisotropySimulation.runparameter]:	In case of choosing a varying parameter in the anisotropySimulation.runparameter file,"
	echo "  					write name of parameter in first line, then all values you want to compute."
	echo "  					Example:"
	echo "    					user_rse"
	echo "    					5.0"
	echo "    					10.0"
	echo "    					15.0"
	echo

   exit
fi

DMCLUMPDIR=/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation

#read variables from parameter file:

# read parameters:
export runname=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')

export HALOFILE=$(cat $1	    | head -n21 | tail -n1 | sed -e 's/^[ \t]*//')
export OBSWIDTH=$(cat $1	        | head -n24 | tail -n1 | sed -e 's/^[ \t]*//')
export OBSTIME=$(cat $1	        | head -n27 | tail -n1 | sed -e 's/^[ \t]*//')
export OFFSET=$(cat $1          | head -n30 | tail -n1 | sed -e 's/^[ \t]*//')

export EMIN=$(cat $1            | head -n33 | tail -n1 | sed -e 's/^[ \t]*//')
export EMAX=$(cat $1            | head -n36 | tail -n1 | sed -e 's/^[ \t]*//')

export SIGMA=$(cat $1           | head -n39 | tail -n1 | sed -e 's/^[ \t]*//')
export DEADTIME=$(cat $1        | head -n42 | tail -n1 | sed -e 's/^[ \t]*//')

export INSTRUMENT=$(cat $1      | head -n48 | tail -n1 | sed -e 's/^[ \t]*//')
export CALIBDB=$(cat $1         | head -n51 | tail -n1 | sed -e 's/^[ \t]*//')
export IRF=$(cat $1             | head -n54 | tail -n1 | sed -e 's/^[ \t]*//')

export CHANNEL=$(cat $1         | head -n60 | tail -n1 | sed -e 's/^[ \t]*//')
export DMMASSES=$(cat $1        | head -n63 | tail -n1 | sed -e 's/^[ \t]*//')

export ITER=$(cat $1            | head -n69 | tail -n1 | sed -e 's/^[ \t]*//')
export ENUMBINS=$(cat $1        | head -n72 | tail -n1 | sed -e 's/^[ \t]*//')



#export TOTALTIME=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export CALDB=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export IRF=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export INSTRUMENT=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export DEADTIME=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export EMIN=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export EMAX=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export OFFSET=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
#export SURVEY=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')


TOTALTIME=$OBSTIME
ISANNIHILATION=1


DMMASSES=($DMMASSES)
DMMASSES_STR=$(IFS=" " ; echo "${DMMASSES[*]}")
SIGMA=($SIGMA)


# number of repeated realisations:
samplenumber=1
if [ -n "$2" ]; then
samplenumber=$2
fi

# Scriptdirectory is current directory, save it:
SCRIPTDIR="$(pwd)"

# Save the date:
DATE=`date +"%y%m%d"`

# Make Logfile directory for submission scripts and batch logfiles output:
cd $SCRATCH/LOGS/IACT-SIMULATION
#mkdir -p $DATE
QLOG=$SCRATCH/LOGS/IACT-SIMULATION/ #$DATE
SCRIPTLOG=$QLOG


# developing mode?
devmode=0
if [ -n "$3" ]; then
devmode=$3
fi
if [[ $devmode == "0" ]]; then
	computingTime=23:59:59
	memorySize=2G
	tmpdirSize=2G
elif [[ $devmode == "1" ]]; then
	computingTime=00:29:59
	memorySize=1G
	tmpdirSize=4G
else
	echo "EXIT: devmode variable must be either 0 or 1"
	echo
	exit
fi		

# job priority?
priority=0
if [ -n "$4" ]; then
priority=$4
fi



cd $outdir
mkdir -p $runname
cd $runname
RUNDIR="$(pwd)"

#rm -r *
# now forget outdir

# skeleton script
FSCRIPT1="subhaloPointResponse.qsub1"
FSCRIPT2="subhaloPointResponse.qsub2"

###############################################################################
# If a runparameter file is parsed:
###############################################################################

if [ -n "$5" ]; then
	cd $RUNDIR
	#rm -r Inputfiles
	mkdir -p Inputfiles
	
	if [[ $moduleBool == "3" || $moduleBool == "4" || $moduleBool == "5" ]]; then
		echo "module 3, 4 or 5: don't copy input files"
		runParameterFile=$outdir/$runname/Inputfiles/anisotropySimulation.runparameter
	elif [[ $moduleBool == "1" || ${DMspectrumName:0:10} == "astroPower" || ${DMspectrumName} == "noDarkMatter" ]]; then
		runParameterFile=$5
		cp anisotropySimulation.* $RUNDIR/Inputfiles/
		rm $RUNDIR/Inputfiles/anisotropySimulation.clumpy_params.txt
	elif [[ $moduleBool == "2" ]]; then
		runParameterFile=$outdir/$runname/Inputfiles/anisotropySimulation.runparameter
	fi

	# number of parameters:
	export runparameter=$(cat $runParameterFile 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $runParameterFile | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter
	echo
		
	cd $SCRIPTDIR
	
	###########################################################################
	# now loop over all varied runparameter values:
	for AFIL in $PARAMETERS
	do
		if [[ $moduleBool != "4" && $moduleBool != "5" ]]; then	
			echo "now starting parameter run $runparameter = $AFIL"
			# create output directories:   
			cd $RUNDIR
			mkdir $runparameter-$AFIL
			#rm -r Logfiles
			mkdir -p $runparameter-$AFIL/Logfiles
		else
			echo "now evaluating parameter run $runparameter = $AFIL"
		fi
		
		cd $RUNDIR/$runparameter-$AFIL
		ODIR="$(pwd)"

		cd $SCRIPTDIR
		if [[ $moduleBool == "1" || ${DMspectrumName:0:10} == "astroPower" || ${DMspectrumName} == "noDarkMatter" ]]; then
			rm $RUNDIR/Inputfiles/anisotropySimulation.clumpy_params.txt
		fi
		
		case $runparameter in
			"user_rse") user_rse=$AFIL;;
			"alphaIntDeg") alphaIntDeg=$AFIL;;
			"gDM_MMIN_SUBS") gDM_MMIN_SUBS=$AFIL;;
			"gGAL_DPDM_SLOPE") gGAL_DPDM_SLOPE=$AFIL;;
			"gGAL_RHOSOL") gGAL_RHOSOL=$AFIL;;
			"gGAL_SUBS_N_INM1M2") gGAL_SUBS_N_INM1M2=$AFIL;;
			"gGAL_CLUMPS_FLAG_PROFILE") gGAL_CLUMPS_FLAG_PROFILE=$AFIL;;
			"gGAL_DPDV_FLAG_PROFILE") gGAL_DPDV_FLAG_PROFILE=$AFIL;;
			"sigmav") sigmav=$AFIL;;
			"mchi") mchi=$AFIL;;
			"thresholdBin") thresholdBin=$AFIL;;
		esac
		
		if [[ $moduleBool != "4" && $moduleBool != "5" ]]; then			
			###################################################################
			# now loop over all identical (statistical) realisations:
			for i in $(seq 1 1 $samplenumber)
			do
				if [[ $samplenumber == "1" ]]; then
				FNAM="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL"
				FNAM23="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL-mod23"
				onlyOneRunBool=1
				else 
				FNAM="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL-$i"
				FNAM23="$SCRIPTLOG/anisotropySimulation-$runname-$runparameter-$AFIL-$i-mod23"
				onlyOneRunBool=0
				echo "now starting sample realisation number $i"   
				fi
							
				sed -e "s|FFFFF|$user_rse|" \
					-e "s|ZHHHH|$seed|" \
					-e "s|PEEED|$QLOG|" \
					-e "s|OODIR|$ODIR|" \
					-e "s|DAATE|$DATE|" \
					-e "s|MMMMM|$gGAL_RHOSOL|" \
					-e "s|NNNNN|$gGAL_RSOL|" \
					-e "s|OOOOO|$gGAL_RVIR|" \
					-e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
					-e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
					-e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
					-e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
					-e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
					-e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
					-e "s|WWWWW|$gDM_MMIN_SUBS|" \
					-e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
					-e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
					-e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
					-e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
					-e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
					-e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
					-e "s|AAAAA|$psiZeroDeg|" \
					-e "s|BBBBB|$thetaZeroDegGal|" \
					-e "s|CCCCC|$psiWidthDeg|" \
					-e "s|DDDDD|$thetaWidthDeg|" \
					-e "s|EEEEE|$alphaIntDeg|" \
					-e "s|RRRRR|$AFIL|" \
					-e "s|RUUUN|$RUNDIR|" \
					-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
					-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
					-e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
					-e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
					-e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
					-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
					-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
					-e "s|ZEEEE|$gDM_RHOSAT|" \
					-e "s|ZFFFF|$gGAL_SUBS_M1|" \
					-e "s|ZGGGG|$gGAL_SUBS_M2|" \
					-e "s|ZIIII|$TWOsigmaFOVdeg|" \
					-e "s|ZJJJJ|$TWOsigmaPSFdeg|" \
					-e "s|ZNNNN|$i|" \
					-e "s|ZOOOO|$mchi|" \
					-e "s|ZPPPP|$DMspectrumName|" \
					-e "s|ZQQQQ|$effAreaFile|" \
					-e "s|ZRRRR|$sigmav|" \
					-e "s|ZSSSS|$sensitivityFile|" \
					-e "s|ZTTTT|$tobs|" \
					-e "s|ZUUUU|$backgroundmodel|" \
					-e "s|ZVVVV|$astroAnisotropies|" \
					-e "s|ZWWWW|$onlyOneRunBool|" \
					-e "s|ZXXXX|$moduleBool|" \
					-e "s|ZYYYY|$energyBins|" \
					-e "s|ZKKKK|$noise|" \
					-e "s|ZLLLL|$elevation|" \
					-e "s|ZMMMM|$azimutBin|" \
					-e "s|ZZAAA|$phiZeroAstroFlux|" \
					-e "s|ZZBBB|$thresholdBin|" \
					-e "s|ZZCCC|$alpha|" \
					-e "s|ZZDDD|$wobbleOffset|" \
					-e "s|ZZEEE|$thetaSquared|" \
					-e "s|ZZFFF|$IRFspectralindex|" \
					-e "s|ZZGGG|$MCvsREC|" \
					-e "s|ZZHHH|$gLIST_HALOES|" \
					-e "s|ZZIII|$gLIST_Bool|" \
					-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh
				
				chmod u+x $FNAM.sh
				echo "script name is:" $FNAM.sh
	
				if [[ $devmode == "1" ]]; then
					echo "submitting job in developing mode (30 minutes," $memorySize "RAM)"
				fi
	
				if [[ $moduleBool == "1" ]]; then
					echo "compute Dark Matter skymap"
					qsub -V -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -js $priority "$FNAM.sh"
				elif [[ $moduleBool == "2" ]]; then
					echo "compute instrument response (skymap.healpix file in output directory needed)"	
					JOBID=`qsub -V -terse -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -t 1-$energyBins:1 -js $priority "$FNAM.sh"`
					echo "Job array" $JOBID "("${FNAM}.sh") for filling energy energy bins has been submitted."
					JOBID=${JOBID:0:7}
					sed -e "s|moduleBool=2|moduleBool=3|" $FNAM.sh > $FNAM23.sh
					JOBID2=`qsub -V -terse -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -js $priority -hold_jid $JOBID "$FNAM23.sh"`
					echo "Then combine energy bins with job" $JOBID2 "(" $FNAM23.sh ")"
				elif [[ $moduleBool == "3" ]]; then
					echo "combine eventmap-binXX.healpix files"
					qsub -V -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ -js $priority "$FNAM.sh"
				else
					echo "EXIT: moduleBool variable must be either 1, 2, 3, 4, or 5"
					echo
					exit
				fi
				echo
			done

		elif [[ $moduleBool == "4" ]]; then
			###################################################################
			# module 4: evaluate power spectra of statistical realisations locally
			echo "evaluating power spectra..."
			# grep number of runs:
			cd $ODIR/spectra-mc
			echo "ODIR is:" $ODIR
			realisations=$(ls -1 combinedRawMap* | wc -l)
			echo "number of spectrum realisations in spectra-mc folder:" $realisations
			echo "evaluated number of spectrum realisations (from input):" $samplenumber
			cd $SCRIPTDIR
			python $SCRIPTDIR/anisotropySimulation.evaluatePowerSpectra.py -a $alphaIntDeg -f $TWOsigmaFOVdeg  -p $TWOsigmaPSFdeg -l $psiZeroDeg -b $thetaZeroDegGal -c $energyBins -d $thresholdBin -r $effAreaFile -q $sensitivityFile -n $noise -v $elevation -z $azimutBin -t $tobs -s $sigmav -m $mchi -y $DMspectrumName -k $backgroundmodel -j $astroAnisotropies -x $phiZeroAstroFlux -g $samplenumber -w $backgroundOnlyPath -i $ODIR -o $ODIR/spectra-mc | tee $ODIR/Logfiles/module4-evaluatePowerSpectra_thresholdBin$thresholdBin.log
		elif [[ $moduleBool == "5" ]]; then
			cd $ODIR
			if [ -d "realisation-1" ]; then
  				WORKDIR=$ODIR/realisation-1
  			else
  				WORKDIR=$ODIR
			fi
			cd $SCRIPTDIR
			echo "plot healpix files"
			python $SCRIPTDIR/anisotropySimulation.quickPlotEventmap.py -a $alphaIntDeg -l $psiZeroDeg -f $TWOsigmaFOVdeg -b $thetaZeroDegGal -c $energyBins -d $thresholdBin -r $effAreaFile -q $sensitivityFile -n $noise -v $elevation -z $azimutBin -t $tobs -s $sigmav -m $mchi -y $DMspectrumName -k $backgroundmodel -j $astroAnisotropies -x $phiZeroAstroFlux -g $alpha -u $thetaSquared -w $wobbleOffset -p $showDMskymapBool -e $showBinMapsBool -i $WORKDIR -o $column
		fi
	done
fi

###############################################################################
# If NO runparameter file exists:
###############################################################################

if [ ! -n "$5" ]; then

	echo "submitting single job to batch"

	# RUNDIR is equal ODIR in this case!
	
	AFIL=singlerundummy
	# AFIL is not used in this case!
	
	cd $RUNDIR
	ODIR="$(pwd)"
	#rm -r Logfiles
	#rm -r Inputfiles
	
	mkdir -p Inputfiles
	mkdir -p Batchlogs
	QLOG=$ODIR/Batchlogs

		
	cd $SCRIPTDIR


	cp $CLUMPY/python_helperscripts/printKeywordValue.py $RUNDIR/Inputfiles/
	
	cp subhaloPointResponse.* $RUNDIR/Inputfiles/
	rm $ODIR/Inputfiles/*.runparameter

	startnum=$((1))
	one=$((1))
	endnum=$(($startnum+$samplenumber-$one))

	###########################################################################
	# now loop over all identical (statistical) realisations:
	for i in  $(seq $startnum 1 $endnum)
	do
		realisationnumber=$i # this rename has just been done because of some buggy reading of the sed command...
		
		if [[ $samplenumber == "1" ]]; then
			FNAM1="$SCRIPTLOG/subhaloPointResponse-$runname"
			onlyOneRunBool=1
			outdir_tmp=$outdir
			runname_tmp=$runname
		else 
			FNAM1="$SCRIPTLOG/subhaloPointResponse-$runname-$i"
			onlyOneRunBool=0
			echo "now starting sample realisation number $i"
			mkdir -p $ODIR/realisation-${i}
			outdir_tmp=$outdir/$runname
			runname_tmp=realisation-${i}
		fi
		
		if [[ $devmode == "1" ]]; then
			echo "submitting job in developing mode (30 minutes," $memorySize "RAM)"
		fi

		mkdir -p $outdir_tmp/$runname_tmp/Logfiles	
		
		i_sigma=-1
		
		for DMMASS in  "${DMMASSES[@]}"
		do
			# calculate sensitivity for all masses
			i_sigma=$(($i_sigma+1))
			# create directories:
			mkdir -p $outdir_tmp/$runname_tmp/mchi_${DMMASS}GeV
			echo  $outdir_tmp/$runname_tmp/mchi_${DMMASS}GeV
			
			sed -e "s|OODIR|$outdir_tmp/$runname_tmp/mchi_${DMMASS}GeV|"  \
				-e "s|DIIIR|$SCRIPTDIR|" \
				-e "s|RUUNN|$RUNDIR|" \
	  			-e "s|AAAAA|$TOTALTIME|" \
				-e "s|ROOOI|$OBSWIDTH|" \
				-e "s|IITER|$ITER|" \
				-e "s|BIIIN|$ENUMBINS|" \
	 			-e "s|BBBBB|$CALIBDB|" \
	 			-e "s|CCCCC|$IRF|" \
	 			-e "s|DDDDD|$INSTRUMENT|" \
	 			-e "s|EEEEE|$DEADTIME|" \
	 			-e "s|FFFFF|$EMIN|" \
	 			-e "s|GGGGG|$EMAX|" \
	 			-e "s|CHNNL|$CHANNEL|" \
				-e "s|ANNHL|$ISANNIHILATION|" \
				-e "s|HAALO|$HALOFILE|" \
	 			-e "s|HHHHH|$OFFSET|" \
	 			-e "s|KKKKK|$DMMASS|" \
	 			-e "s|JJJJJ|${SIGMA[$i_sigma]}|" \
	 			-e "s|NNNNN|$RUNDIR|"  ${FSCRIPT1}.sh > ${FNAM1}-${DMMASS}.sh
	 		chmod u+x ${FNAM1}-${DMMASS}.sh
	
			echo "...DM Mass "$DMMASS" GeV..." 
	
			JOBID1=`qsub -V -terse -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$tmpdirSize -o $QLOG/ -e $QLOG/ -js $priority -hold_jid 8259014 "${FNAM1}-${DMMASS}.sh"`
			echo "Compute instrument response for DM mass in job" $JOBID1
			echo "script name is:" ${FNAM1}-${DMMASS}.sh
			echo ""
			done

		


	done
fi
exit
