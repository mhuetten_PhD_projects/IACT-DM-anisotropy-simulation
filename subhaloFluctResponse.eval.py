#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
import os
import sys
import getopt
import pickle
from ctools import *
import obsutils
from gammalib import *
from math import *
import glob
import math
from multiprocessing import Process, Queue, Pool
import veripy
try:
    import matplotlib.pyplot as plt
    has_matplotlib = True
except:
    has_matplotlib = False
#import time
#from time import gmtime, strftime
from time import *


##    main part:   ############################################################
###############################################################################
def main(argv):

###########################################################################
    # read input variables
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:",["infile="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'Wrong input, EXIT. The input options are:'
        print ' -i'
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'HELP: The input options are:'
            print ' -i'
            sys.exit()
        elif opt in ("-i", "--in"):
            infile = arg
        
    
    start_time = time()
    print " initializing script ctools_evaluate_simulation.py"

    map = hp.read_map(infile, partial=True)
    
    hp.write_map(infile, map, partial=False)
    hdulist_out = fits.open(infile, mode='update')
    hdulist_out[1].header.set('COORDSYS', 'C')
    hdulist_out.flush()
    hdulist_out.close()
    

    #     #---------------------------------------------------------
#     # Generate spectral points
#     spectrum = obsutils.spectrum(obs_simu, "Crab", ebounds)
#     plt.figure(1)
#     plt.title("Crab spectrum")
#     
#     # Plot spectrum
#     plt.loglog(spectrum['energy']['value'], \
#                spectrum['flux']['value'], 'ro', label='Crab')
#     plt.errorbar(spectrum['energy']['value'], \
#                  spectrum['flux']['value'], \
#                  spectrum['flux']['ed_value'], fmt=None, ecolor='r')
#     
#     # Put labels
#     plt.xlabel("Energy ("+spectrum['energy']['unit']+")")
#     plt.ylabel("Flux ("+spectrum['flux']['unit']+")")
#     plt.legend(loc="lower left")
# 
#     #---------------------------------------------------------
#     # show events within theta² cut:
#     # get observation properties from first run in observations:
#     thetasquarecut(obs_simu, ebounds, target, 0.09)
#     
#     # Show plots
#     
#     plt.show()

#     for r in simu:
#         run = GCTAObservation(r)
#         print run
#       # 'run' is a GCTAObservation object
#       
# #       # loop over each event in each chunk
# #         for event in run.events():
# #           evid   = event.event_id()   # event number
# #           chron  = event.time()       # gammalib.GTime()
# #           energy = event.energy()     # gammalib.GEnergy()
# #           skydir = event.dir().dir()  # gammalib.GSkyDir()
# #           detx   = event.dir().detx() # float (x position of event in camera coordinates (radians), 0 = camera center)
# #           dety   = event.dir().dety() # float (y position of event in camera coordinates (radians), 0 = camera center)
# #           print 'event %d, %.1f GeV, from %s' % (evid, energy.GeV(), skydir )
# 





# ================================================================= #
# Generate a theta square cut #
# ================================================================= #
def thetasquarecut(obs, ebounds, pointing, theta):
    """
    Returns spectral points in the given binning for the passed observations
    and source.

    Please note that specpoints can make spectra for any given spectral
    model, as long as it has a "Prefactor" or "Integral" parameter which
    scales the function
    
    Parameters:
     obs     - Observation container
     source  - Source name for which spectrum should be computed
     ebounds - GEbounds energy boundaries
    Keywords:
     None
    """
    # get observation properties from first run in observations:
    obs0 = GCTAObservation(obs[0])
    ra_deg = pointing.ra_deg()
    dec_deg = pointing.dec_deg()
    roi_rad_deg = obs0.roi().radius()
    tmin = obs0.events().tstart().secs()
    tmax = obs0.events().tstop().secs()
    
    # Loop over energy bins, fit and write result to spectrum
    for i in range(len(ebounds)):
        # Clone observations and reset energy thresholds
        select = ctselect(obs)
        select["ra"].real(ra_deg)
        select["dec"].real(dec_deg)
        select["rad"].real(theta)
        select["tmin"].real(0.0)
        select["tmax"].real(0.0)
        select["emin"].real(ebounds.emin(i).TeV())
        select["emax"].real(ebounds.emax(i).TeV())
        select.run()
        obs_select = select.obs()
        for j in obs_select:
            run = GCTAObservation(j)
            # 'run' is a GCTAObservation object
            print ""
            print run.events()


##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################
