
import numpy as np
import gammalib
import ctools
import os

psffile = os.environ["HDATA_PATH"]+"/psf/psf_23523.fits"
Aefffile = os.environ["HDATA_PATH"]+"/Aeff/Aeff_23523.fits"
bgfile = os.environ["HDATA_PATH"]+"/background/hist_alt10_az0.fits"


def hess_inst_background(obs_id):
        
        spec = gammalib.GModelSpectralConst()
        spec["Value"].min(0.1)
        spec["Value"].max(10)
        spec["Value"].fix()
        bck = gammalib.GCTAModelIrfBackground(spec)
        bck.ids(str(obs_id))
        bck.instruments("HESS")
        bck.name("bck_"+str(obs_id))  
        bck.tscalc(False)
        return bck  

if __name__ == "__main__":
    
    pointings = []
    
    ra = 83.6331
    dec = 22.01
    
    for i in range(5):
        skydir=gammalib.GSkyDir()
        pointings.append(skydir)
    pointings[0].radec_deg(ra,dec-0.5)
    pointings[1].radec_deg(ra+0.5,dec)
    pointings[2].radec_deg(ra-0.5,dec)
    pointings[3].radec_deg(ra,dec+0.5)
    pointings[4].radec_deg(ra,dec)

    
    nruns = 1
    nhours = 20
    obs_id = 0

    runlist = []
    obs = gammalib.GObservations()
    bgmodels = gammalib.GModels()
    
    for pointing in pointings:
        print "Simulate",str(nhours)+"h", "on",pointing
        for run in range(nruns):
            point = gammalib.GCTAPointing(pointing)
            point.zenith(45)
            point.azimuth(0)
                     
            instdir = gammalib.GCTAInstDir();
            instdir.dir().radec_deg(pointing.ra_deg(),pointing.dec_deg());  
            roi = gammalib.GCTARoi(instdir,2.5)
            gti = gammalib.GGti()
            gti.append(gammalib.GTime(float(obs_id*nhours*3600)),gammalib.GTime(float((obs_id+1)*nhours*3600)))
            emin = 0.5
            emax = 50
            
            ebounds = gammalib.GEbounds(gammalib.GEnergy(emin,"TeV"),gammalib.GEnergy(emax,"TeV"))
            
            observation = gammalib.GCTAObservation("HESS")
            observation.pointing(point)
            observation.ontime(nhours*3600)
            observation.livetime(0.95*nhours*3600)
            observation.deadc(0.95)
            observation.id(str(obs_id))
            observation.obs_id(obs_id)
            observation.name("CrabNebula")
            
            response = gammalib.GCTAResponseIrf()
            response.load_psf(psffile)
            response.load_aeff(Aefffile)
            response.load_background(bgfile)
            observation.response(response)
            events = gammalib.GCTAEventList()
            events.roi(roi)
            events.gti(gti)
            events.ebounds(ebounds)
            
            observation.events(events)
            
            bck = hess_inst_background(obs_id)
            bck["Value"].value(np.random.normal(1.0,0.2))
            bgmodels.append(bck)
            obs.append(observation)
            runlist.append(obs_id)
            obs_id+=1


    obs.models(bgmodels)
    
    models = gammalib.GModels("crab_no_background.xml")
    obs.models().append(models[0])
    
    
    obs.models().save("input_models.xml")
    f=open("simulated_runlist_crab.list","w")
    for run in runlist:
        f.write(str(run)+" \n")
    f.close()
    
    sim = ctools.ctobssim(obs)
    sim["outfile"].filename("obs.xml")
    sim["prefix"].string("events_")
    sim["chatter"].integer(0)
    sim.execute()
        
#     bin = ctbin(sim.obs())
#     bin["nxpix"].integer(400)
#     bin["nypix"].integer(400)
#     bin["proj"].string("TAN")
#     bin["coordsys"].string("CEL")
#     bin["xref"].real(83.6331)
#     bin["yref"].real(22.01)
#     bin["enumbins"].integer(1)
#     bin["ebinalg"].string("LOG")
#     bin["emin"].real(0.1)
#     bin["emax"].real(100)
#     bin["binsz"].real(0.02)
#     bin["outfile"].filename("cntmap.fits")
#     bin["chatter"].integer(0)
#     bin.execute()
    
            
    
            
            
            
            
            
            
            