#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
import os
import sys
import getopt
import pickle
import ctools
from ctools import obsutils
import gammalib
from math import *
import glob
import math
from multiprocessing import Process, Queue, Pool
import veripy
try:
    import matplotlib.pyplot as plt
    has_matplotlib = True
except:
    has_matplotlib = False
#import time
#from time import gmtime, strftime
import time
import random


##    main part:   ############################################################
###############################################################################
def main(argv):

###########################################################################
    # read input variables
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:",["infile="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'Wrong input, EXIT. The input options are:'
        print ' -i'
        sys.exit(2)
    
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'HELP: The input options are:'
            print ' -i'
            sys.exit()
        elif opt in ("-i", "--in"):
            infile = arg
    #---------------------------------------------------------
    #---------------------------------------------------------    
    
    script_runtime = time.time()
    print " initializing script veripy_source_simulation.py"

    # Get likelihood fit for spectral shape of background:
    
    #get_VTS_bckspectrum()
    
    #quit() # quit at this point


    #---------------------------------------------------------
    # set ROI manually:
    roi_rad_deg = 2.5
    
    #---------------------------------------------------------
    # set energy bounds and energy binning manually:
    emin_TeV = pow(10,-0.5)
    emax_TeV = pow(10,1.5)
    e_min   = gammalib.GEnergy(emin_TeV, "TeV")
    e_max   = gammalib.GEnergy(emax_TeV, "TeV")
    ebounds = gammalib.GEbounds(10, e_min, e_max)
    
    #---------------------------------------------------------
    # set input and output directories:
    runlistname   = '/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/Inputdata/CrabTutorial/tutorialrunlist.dat' # name of runlist
    #runlistname   = '/afs/ifh.de/user/n/nkelhos/scratch/veripyctools/runlists/short/crab.runlist' # name of runlist
    #fitsdir       = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/FitsData"   # path to directory containing fits files
    fitsdir       = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/Results/v500-dev08/Crab/Fits"   # path to directory containing fits files
    events_outdir = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/FitsData/simu-chunks"
    
    # load list of VERITAS data runs, containing all events and irfs
    runlist  = veripy.read_runlist( runlistname ) 
    obs_data = veripy.load_chunk_list( runlist, fitsdir )
    
    total_ontime = 0
 
    
    #---------------------------------------------------------
    # remove the events from the data observation container and copy all the
    # instrument responses, together with additional infos
    # needed for the simulation into an empty container obs_empty:
    obs_empty = gammalib.GObservations()

    for run_raw in obs_data:
        
        #---------------------------------------------------------
        # take data run with all its information as basis for simulation run
        run = gammalib.GCTAObservation(run_raw)
        
        #---------------------------------------------------------
        # Set ROI for simulation
        roi     = gammalib.GCTARoi()
        instdir = gammalib.GCTAInstDir()
        instdir.dir(run.pointing().dir())
        roi.centre(instdir)
        roi.radius(roi_rad_deg)

        #---------------------------------------------------------
        # Allocate empty events list (delete all events from data run)
        events_simu = gammalib.GCTAEventList()
        events_simu.roi(roi)
        gti = run.gti()
        total_ontime += gti.ontime()
        
        # manipulate GTI to get fake GTI
        #duration = 180000
#         gti = gammalib.GGti()
#         gti.append(gammalib.GTime(0.0), gammalib.GTime(duration))
        
        events_simu.gti(gti)
        
#         run.ontime(duration)
#         run.livetime(duration)
        
        #events_simu.ebounds(ebounds)
        
        run.events(events_simu)
        # Setting the eventlist output name
        eventlist_name="events_simu_"
        eventlist_name+=run.id()
        eventlist_name+=".fits"       
        run.eventfile( events_outdir+"/"+eventlist_name )
        
        #---------------------------------------------------------
        # add (empty) run!
        obs_empty.append(run)
        
        #---------------------------------------------------------
        # add better model for background spectrum to observation container:
        spec = gammalib.GModelSpectralPlaw( 480, -2.5, gammalib.GEnergy(500, 'MeV') )
        #spec["Prefactor"].min( 0.1 )
        #spec["Prefactor"].max( 1000. )
        #spec["Prefactor"].fix()
        back = gammalib.GCTAModelIrfBackground( spec )
        back.ids( run.id() )
        back.name( 'plaw_%s' % run.id() )
        back.instruments('VERITAS')
        #bck["Value"].value(np.random.normal(1.0,0.2))
        back.tscalc(False)
        obs_empty.models().append( back )    

    #---------------------------------------------------------
    # add crab model for simulation to observation container obs_empty
    crab_model_file  = '${CTOOLS}/share/models/crab.xml'
    crab_model       = gammalib.GModels( crab_model_file )
    crab_sourcemodel = crab_model[0]
    crab_model_name  = crab_sourcemodel.name()
    # Recover models from observations
    models = obs_empty.models()
    # Add the new background the the GModels
    models.append(crab_sourcemodel)    
    # Put container back in observation container
    obs_empty.models(models)

    #---------------------------------------------------------
    # set skymap center to model source position
    ra_obj_deg  = crab_sourcemodel.spatial().ra()
    dec_obj_deg = crab_sourcemodel.spatial().dec()
    
    #---------------------------------------------------------
    # Simulate events:
    sim = ctools.ctobssim(obs_empty)
    sim["seed"].integer(random.randint(0, 1000))
    sim["emin"].real(emin_TeV)
    sim["emax"].real(emax_TeV)
    #sim["inmodel"].filename(crab_model_file)
    sim["prefix"].string("events_")
    sim["chatter"].integer(1)
    sim.run()
    obs_simu = sim.obs()
    
    for run_raw in obs_simu:
        run = gammalib.GCTAObservation(run_raw)
        print run
    
    
    like=obsutils.fit(obs_simu,log=True, debug=False, edisp=False)
    #print like.obs().models()
    
    #quit()

    #---------------------------------------------------------
    # Save simulated chunks:
    for run_raw in obs_simu:
        run = gammalib.GCTAObservation(run_raw)
        run.save( run.eventfile(),  True )

    roi_rad_deg = roi.radius()
    roi_centre  = roi.centre()
    roi_centre_dir = roi_centre.dir()
    ra_deg      = roi_centre.dir().ra_deg()
    dec_deg     = roi_centre.dir().dec_deg()

    select1 = ctools.ctselect(obs_data)
    select1["ra"].real(ra_deg)
    select1["dec"].real(dec_deg)
    select1["rad"].real(180.)
    select1["tmin"].real(0.0)
    select1["tmax"].real(0.0)
    select1["emin"].real(ebounds.emin(0).TeV())
    select1["emax"].real(ebounds.emax(9).TeV())
    select1.run()
    obs_select = select1.obs()

   
   
    #---------------------------------------------------------
    # bin the events
    # res indicates how many bins there will be per unit solid angle
    # res=10 : ~3000 pixels per veritas fov
    eng = veripy.MapEngineCounts( obs_simu, res=11 )
    
    # find the target that the run is pointing at
    target = gammalib.GSkyDir()
    target.radec_deg( ra_obj_deg, dec_obj_deg )
    
    #---------------------------------------------------------
    # PlotSource is a quick way to zoom in on a veritas source, 
    # try healpy.mollview for a full-sky plot
    # drawsrcpos will circle the target with a 'r'ed circle
    ps = veripy.PlotSource( eng, radius=3.25, zoomdir=target, unit='counts', \
                            title= crab_model_name + ' simulation Counts Map',\
                             grid=1, drawsrcpos='r')
    
#     #---------------------------------------------------------
#     # Generate spectral points
#     spectrum = obsutils.spectrum( obs_simu, crab_model_name, ebounds)
#     plt.figure(1)
#     plt.title( crab_model_name + " spectrum")
#          
#     # Plot spectrum
#     plt.loglog(spectrum['energy']['value'], \
#                spectrum['flux']['value'], 'ro', label= crab_model_name )
#     plt.errorbar(spectrum['energy']['value'], \
#                  spectrum['flux']['value'], \
#                  spectrum['flux']['ed_value'], fmt=None, ecolor='r')
#          
#     # Put labels
#     plt.xlabel("Energy ("+spectrum['energy']['unit']+")")
#     plt.ylabel("Flux ("+spectrum['flux']['unit']+")")
#     plt.legend(loc="lower left")
    
    #---------------------------------------------------------
    # show events within theta² cut:
    theta_deg = 0.23
    source_roi     = gammalib.GCTARoi()
    source_dir     = gammalib.GCTAInstDir()
    source_dir.dir( target )
    source_roi.centre( source_dir )
    source_roi.radius( theta_deg )
    
#     print 'ctselect Summary for data run:'
#     print_countmap_infos_ctselect(obs_data, ebounds, source_roi)
#     
#     print 'ctselect Summary for simulation run:'
#     print_countmap_infos_ctselect(obs_simu, ebounds, source_roi)

    print 'Total observation time:'
    print total_ontime, 'seconds.'

    print 'Summary for data run:'
    print_countmap_infos(obs_data, ebounds, source_roi)

    print 'Summary for simulation run:'
    print_countmap_infos(obs_simu, ebounds, source_roi)
    
    #---------------------------------------------------------  
    # Show plots   
    ps.show()
    plt.show()


# ================================================================= #
# Show count statistics and theta² cut
# ================================================================= #
def print_countmap_infos(obs, ebounds, source_roi):
    """
    Returns infos for counts map
     
    Parameters:
     obs         - Observation container
     ebounds     - GEbounds energy boundaries
     source_roi  - GCTARoi specifying the spatial cut region
    Keywords:
     None
    """
    print ' loop through events...'
 
    roi_rad_deg = source_roi.radius()
    roi_centre  = source_roi.centre()
    roi_centre_dir = roi_centre.dir()
    ra_deg      = roi_centre.dir().ra_deg()
    dec_deg     = roi_centre.dir().dec_deg()
 
    events_cutEbins = np.zeros(len(ebounds))
    events_cutEbinsTheta = np.zeros(len(ebounds))
    events_total = 0
    events_total_inbounds = 0
    events_total_inboundsTheta = 0
    for run_raw in obs:
        run = gammalib.GCTAObservation( run_raw)
        events_total += run.events().number()
        for i in range(len(ebounds)):
            emin_bin = ebounds.emin(i).TeV()
            emax_bin = ebounds.emax(i).TeV()
            for event in run.events():
                if event.energy().TeV() > emin_bin and event.energy().TeV() <= emax_bin :
                    events_cutEbins[i] +=1
                    if roi_centre_dir.dist_deg(event.dir().dir()) <= roi_rad_deg:
                        events_cutEbinsTheta[i] +=1
            #skydir = event.dir().dir()  # gammalib.GSkyDir()
 
    for i in range(len(ebounds)):
        print '[%6.3f TeV, %6.3f TeV]: %5d events. In ON region:  %5d events ' % (ebounds.emin(i).TeV(), ebounds.emax(i).TeV(), events_cutEbins[i], events_cutEbinsTheta[i] )
        events_total_inbounds +=  events_cutEbins[i]
        events_total_inboundsTheta +=  events_cutEbinsTheta[i]
    print '-------------------------------------------------------------------'
    print '                          %5d events                 %5d events' % (events_total_inbounds, events_total_inboundsTheta)
    print events_total, 'total events overall'
    print ''
    

# ================================================================= #
# Show count statistics and theta² cut
# ================================================================= #
def print_countmap_infos_ctselect(obs, ebounds, source_roi):
    """
    Returns infos for counts map
    
    Parameters:
     obs         - Observation container
     ebounds     - GEbounds energy boundaries
     source_roi  - GCTARoi specifying the spatial cut region
    Keywords:
     None
    """
    print ' loop through events...'

    roi_rad_deg = source_roi.radius()
    roi_centre  = source_roi.centre()
    roi_centre_dir = roi_centre.dir()
    ra_deg      = roi_centre.dir().ra_deg()
    dec_deg     = roi_centre.dir().dec_deg()

    events_cutEbins = np.zeros(len(ebounds))
    events_cutEbinsTheta = np.zeros(len(ebounds))
    events_total = 0
    events_total_inbounds = 0
    events_total_inboundsTheta = 0

    #cut only for energy (all events in FOV)
    for i in range(len(ebounds)):
        emin_bin = ebounds.emin(i).TeV()
        emax_bin = ebounds.emax(i).TeV()
        select1 = ctools.ctselect(obs)
        select1["ra"].real(ra_deg)
        select1["dec"].real(roi_rad_deg)
        select1["rad"].real(180.)
        select1["tmin"].real(0.0)
        select1["tmax"].real(0.0)
        select1["emin"].real(ebounds.emin(i).TeV())
        select1["emax"].real(ebounds.emax(i).TeV())
        select1.run()
        obs_select = select1.obs()
        for run_raw in obs_select:
            run = gammalib.GCTAObservation( run_raw )
            events_cutEbins[i] += run.events().number()


    # cut for ROI (theta squared):
    for i in range(len(ebounds)):
        emin_bin = ebounds.emin(i).TeV()
        emax_bin = ebounds.emax(i).TeV()
        select2 = ctools.ctselect(obs)
        select2["ra"].real(ra_deg)
        select2["dec"].real(dec_deg)
        select2["rad"].real(180.)
        select2["tmin"].real(0.0)
        select2["tmax"].real(0.0)
        select2["emin"].real(ebounds.emin(i).TeV())
        select2["emax"].real(ebounds.emax(i).TeV())
        select2.run()
        obs_select = select2.obs()
        for run_raw in obs_select:
            run = gammalib.GCTAObservation( run_raw )
            events_cutEbins[i] += run.events().number()
            
    # cross check against total events in container:
    for run_raw in obs:
        run = gammalib.GCTAObservation( run_raw)
        events_total += run.events().number()

    for i in range(len(ebounds)):
        print '[%6.3f TeV, %6.3f TeV]: %5d events. In ON region:  %5d events ' % (ebounds.emin(i).TeV(), ebounds.emax(i).TeV(), events_cutEbins[i], events_cutEbinsTheta[i] )
        events_total_inbounds +=  events_cutEbins[i]
        events_total_inboundsTheta +=  events_cutEbinsTheta[i]
    print '-------------------------------------------------------------------'
    print '                          %5d events                 %5d events' % (events_total_inbounds, events_total_inboundsTheta)
    print events_total, 'total events overall'
    print ''
    
# ================================================================= #
# Fit background spectrum #
# ================================================================= #
def get_VTS_bckspectrum():
    
    runlistname = '/afs/ifh.de/user/n/nkelhos/scratch/veripyctools/runlists/short/tycho.runlist' # name of runlist
    #runlistname = '/afs/ifh.de/user/n/nkelhos/scratch/veripyctools/runlists/short/crab.runlist' # name of runlist
    fitsdir     = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/FitsData"   # path to directory containing fits files
    
    # load our list of runs
    runlist = veripy.read_runlist( runlistname ) 
    obs_data = veripy.load_chunk_list( runlist, fitsdir )

    new_models = gammalib.GModels()
    obs_data.models(new_models)
    
    totaleventlist = gammalib.GCTAEventList()
    
    for run_raw in obs_data:
        run = gammalib.GCTAObservation( run_raw )
         
        # setup background model
        spec = gammalib.GModelSpectralPlaw()
        spec["PivotEnergy"].value(10.e+05)
        back = gammalib.GCTAModelIrfBackground( spec )
        run_id = run.id()
        back.name( 'plaw_%s' % run.id() )
        back.ids( run.id() )
        back.instruments('VERITAS')
         
        obs_data.models().append( back )
        
        for event in run.events():
            totaleventlist.append(event)

    like=obsutils.fit(obs_data,log=True, debug=False, edisp=False)
    models = like.obs().models()
    bck = models[0]
    print bck.spectral()
    print ""
    
    #print obs_total
    
    obs_total = gammalib.GCTAObservation(obs_data[0])
    #roi = 
    
    
        
    obs_total.events(totaleventlist)
    
    obs_total_container = gammalib.GObservations()
    
    obs_total_container.append(obs_total)         
 
    for run_raw in obs_total_container:
        run = gammalib.GCTAObservation( run_raw )    
        
        # setup background model
        spec = gammalib.GModelSpectralPlaw()
        #spec["Prefactor"].min( 0.1 )
        #spec["Prefactor"].max( 10000000. )
        spec["Prefactor"].value(14687.4)
        spec["Prefactor"].fix()
        back = gammalib.GCTAModelIrfBackground( spec )
        run_id = run.id()
        back.name( 'plaw_%s' % run.id() )
        back.ids( run.id() )
        back.instruments('VERITAS')
         
        obs_total_container.models().append( back )

    

#                     
    # add better model for background spectrum:
#     run_id = 'VR74716_CH8'
#     spec = gammalib.GModelSpectralPlaw(15, -2.5, gammalib.GEnergy(500, 'MeV') )
#     back = gammalib.GCTAModelIrfBackground( spec )
#     back.name( 'plaw_%s' % run_id )
#     back.ids( run_id )
#     back.instruments('VERITAS')
#     obs_data.models().append( back )
 
#     like = ctools.ctlike(obs_data)
#     like["chatter"].integer(1)
#     like["refit"].boolean(True)
#     like["inmodel"].filename('')
#     like["irf"].string('')
#     print like
#     like.run()
#     # Show model fitting results
#     print(like.obs().models()[0])

    like=obsutils.fit(obs_total_container,log=True, debug=False, edisp=False)  
    #print like.obs().models()

  


##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################
