#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
from matplotlib import pyplot as plt
import os
import sys
import pandas
import getopt
import pickle
#from ctools import *
import obsutils
import make_pointings
from gammalib import *
from math import *
import glob
import math
from multiprocessing import Process, Queue, Pool
#import veripy
from time import *
from astropy.io import fits
from scipy import interpolate
from scipy.interpolate import interp1d
from scipy.interpolate import UnivariateSpline

import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.

import pickle 
import time

from matplotlib.backends.backend_pdf import PdfPages as pdf
from matplotlib.patches import Rectangle
from matplotlib.transforms import Bbox
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
import scipy.stats as stats
import scipy
import bisect

##    main part:   ############################################################
###############################################################################
def main(argv):


    CALDB_PATH = os.environ['CALDB']
    
    IRF = "0.5h_avg"
    CALDB_READ = "paranal_orig"
    CALDB_WRITE = "paranal_smoothoff"
    INSTRUMENT = 'cta'
    
    
    a_prefac = -1/2.
    a_power= 6
    b_prefac = -1./4.
    b_power= 2
    
    lin_param = 0.0
    sigma_fix = 4 # degrees
    sigma_emin= 2.5 # degrees
    sigma_emax= 5 # degrees

    
    fitspath_in  = CALDB_PATH + '/data/' + INSTRUMENT + '/' + CALDB_READ + '/bcf/' + IRF + '/'
    fitspath_out = CALDB_PATH + '/data/' + INSTRUMENT + '/' + CALDB_WRITE + '/bcf/' + IRF + '/'
    

    hdulist = fits.open(fitspath_in +'irf_file.fits')
    print "*****************"
    print "INFO:"
    print hdulist.info()
    print "*****************"
    print "EXTENSION NAMES:"
    for extension in hdulist:
        print " ", extension.name
    print "*****************"
    print "Scale effective area..."
     
    # extract data:
      
    effarea_cols   = hdulist[1].columns
    effarea_data   = hdulist[1].data
    effarea_header = hdulist[1].header
      
    energy_low  = effarea_data["ENERG_LO"][:][0]
    energy_high = effarea_data["ENERG_HI"][:][0]
    energy_mean = np.array((energy_high + energy_low )/2)
    energy_logmean = np.array(10**((np.log10(energy_high) + np.log10(energy_low) )/2))

    
    theta_low  = effarea_data["THETA_LO"][:][0]
    theta_high = effarea_data["THETA_HI"][:][0]
      
    aeff = effarea_data["EFFAREA"][:][0]
  
    aeff_reco = effarea_data["EFFAREA_RECO"][:][0]
      
    theta = np.array((theta_high + theta_low)/2)
      
      
    n_energy = len(energy_low)
    n_energy_new = 500
    
    n_theta  = len(theta)
    n_theta_new = 45
      
    theta_low_new = np.linspace(theta_low[0], theta_high[-1], n_theta_new, endpoint=False)
    theta_high_new = np.linspace(theta_low_new[1], theta_high[-1], n_theta_new, endpoint=True)
    theta_new = np.array((theta_low_new + theta_high_new)/2)
    
    energy_low_new = 10**(np.linspace(np.log10(energy_low[0]), np.log10(energy_high[-1]), n_energy_new, endpoint=False))
    energy_high_new = 10**np.linspace(np.log10(energy_low_new[1]), np.log10(energy_high[-1]), n_energy_new, endpoint=True)
    energy_mean_new = np.array((energy_high_new + energy_low_new )/2)
    energy_logmean_new = np.array(10**((np.log10(energy_high_new) + np.log10(energy_low_new) )/2))

      
    aeff_new = np.zeros((n_theta_new, n_energy))
    aeff_new_reco = np.zeros((n_theta_new, n_energy))
      
  
      
    for i_energy in range(n_energy):
        # create interpolation functions:
            
        if aeff[0,i_energy] != 0.0:
            print "Fit eff. area in offset!"

                
            def AeffOptimizer( parameters,  x):
                return polyGaussEven(x, parameters[0], parameters[1], parameters[2],  parameters[3]) - aeff[:,i_energy]
    
            ## begin fit
            startparams = [ 1.31, 1.5, 1.4, aeff[0,i_energy] ]
            leastsquarefit, cov = scipy.optimize.leastsq(AeffOptimizer, startparams, (theta,))
    
            fitvals = leastsquarefit
            print fitvals
    
#         interpol = interpolate.InterpolatedUnivariateSpline(theta, aeff[:,i_energy])
            aeff_new[:,i_energy] = polyGaussEven(theta_new, fitvals[0], fitvals[1], fitvals[2], fitvals[3])
            #aeff_new[:,i_energy] = polyGauss(theta_new, fitvals1[0], fitvals1[1], fitvals1[2], fitvals1[3], fitvals1[4], fitvals1[5])
            aeff_new_reco[:,i_energy]  = polyGaussEven(theta_new, fitvals[0], fitvals[1], fitvals[2], fitvals[3])
         
#             plt.plot(theta_new,aeff_new[:,i_energy])
#             plt.scatter(theta,aeff[:,i_energy], color='g', s=50)
#             plt.xlim([0,6])
#             plt.xlabel("offset theta")
#             plt.ylabel("eff. area [m²]")
#             title = "energy bin ["+ str(energy_low[i_energy])+","+str(energy_high[i_energy])+"] TeV"
#             plt.title(title)
#                 
#             nr = "%2.2d" % i_energy
#             name = "fit_aeff"+ nr + ".png"
#             #plt.savefig(name, format='png')
#             plt.show()
        else:
            print "no on axis data..."
        
    #effarea_data[effarea_cols.names[5]][:] = effarea_data[effarea_cols.names[4]][:]
        
    # change eff area columns
    #effarea_cols.del_col("EFFAREA")
    
    # fit in energy dimension:
    aeff_new_efit = np.zeros((n_theta_new, n_energy_new))
    aeff_new_efit_reco = np.zeros((n_theta_new, n_energy_new))
    
    energy_logmean_tmp = energy_logmean
    energy_logmean_tmp = np.append(energy_logmean_tmp, 1.05*energy_logmean_new[-1])
    energy_logmean_tmp = np.insert(energy_logmean_tmp, 0, 0.99*energy_logmean_new[0])
    
    for i_theta in range(n_theta_new):
        
        aeff_new_tmp = aeff_new[i_theta,:]
        aeff_new_tmp= np.append(aeff_new_tmp, 0.5* aeff_new_tmp[-1])
        aeff_new_tmp= np.insert(aeff_new_tmp, 0, 0.001* aeff_new_tmp[0])
        
        f_spline = UnivariateSpline(energy_logmean_tmp, aeff_new_tmp, k=2, s=5)
        f_polyfit = np.poly1d(np.polyfit(np.log10(energy_logmean_tmp), np.log10(aeff_new_tmp), 16))
        
        aeff_new_efit[i_theta,:] = 10**(f_polyfit(np.log10(energy_logmean_new)))
        aeff_new_efit_reco[i_theta,:] = f_polyfit(energy_logmean_new)
        
#         plt.plot(energy_logmean_new, aeff_new_efit[i_theta,:])
#         plt.scatter(energy_logmean, aeff_new[i_theta,:], color='g', s=50)
#         plt.xlabel("energy")
#         plt.xscale('log')
#         plt.yscale('log')
#         plt.ylabel("eff. area [m²]")
#         title = "theta bin ["+ str(theta_low_new[i_theta])+","+str(theta_high_new[i_theta])+"] degs"
#         plt.title(title)
#         
#         nr = "%2.2d" % i_energy
#         name = "fit_aeff"+ nr + ".png"
#         #plt.savefig(name, format='png')
#         plt.show()

    for i_energy in range(n_energy_new):
        # create interpolation functions:

        def AeffOptimizer( parameters,  x):
                    return polyGaussEven(x, parameters[0], parameters[1], parameters[2],  parameters[3]) - aeff_new_efit[:,i_energy]
        
        ## begin fit
        startparams = [ 1.31, 1.5, 1.4, aeff_new_efit[0,i_energy] ]
        leastsquarefit, cov = scipy.optimize.leastsq(AeffOptimizer, startparams, (theta_new,))

        fitvals = leastsquarefit
        print fitvals

#        interpol = interpolate.InterpolatedUnivariateSpline(theta, aeff[:,i_energy])
        aeff_new_efit[:,i_energy] = polyGaussEven(theta_new, fitvals[0], fitvals[1], fitvals[2], fitvals[3])
        #aeff_new[:,i_energy] = polyGauss(theta_new, fitvals1[0], fitvals1[1], fitvals1[2], fitvals1[3], fitvals1[4], fitvals1[5])
        aeff_new_efit_reco[:,i_energy]  = polyGaussEven(theta_new, fitvals[0], fitvals[1], fitvals[2], fitvals[3])

#         plt.plot(theta_new,aeff_new_efit[:,i_energy])
# 
#         plt.xlim([0,6])
#         plt.xlabel("offset theta")
#         plt.ylabel("eff. area [m²]")
#         title = "energy bin ["+ str(energy_low_new[i_energy])+","+str(energy_high_new[i_energy])+"] TeV"
#         plt.title(title)
#              
#         nr = "%2.2d" % i_energy
#         name = "fit_aeff"+ nr + ".png"
#         #plt.savefig(name, format='png')
#         plt.show()

    
    del hdulist[1]
   
    # create new columns
    format_e = str(n_energy) + 'E'
    col1 = fits.Column(name="ENERG_LO", format=format_e, array=[energy_low])
    col2 = fits.Column(name="ENERG_HI", format=format_e, array=[energy_high])
       
    format_th = str(n_theta_new) + 'E'
    col3 = fits.Column(name="THETA_LO", format=format_th, array=[theta_low_new])
    col4 = fits.Column(name="THETA_HI", format=format_th, array=[theta_high_new])
       
    format_aeff = str(n_theta_new*n_energy) + 'E'
    col5 = fits.Column(name="EFFAREA", format=format_aeff, array=[aeff_new])
    col6 = fits.Column(name="EFFAREA_RECO", format=format_aeff, array=[aeff_new_reco])
       
       
    cols = fits.ColDefs([col1, col2, col3, col4, col5, col6])
       
    dim_aeff = "("+str(n_energy)+ ","+ str(n_theta_new) +")"
    cols.change_attrib("EFFAREA", "dim", dim_aeff)
    cols.change_attrib("EFFAREA_RECO", "dim", dim_aeff)
   
    hdu_new = fits.BinTableHDU.from_columns(cols)
       
       
    # update header:
    for card in effarea_header:#
        if card[0:5] == 'TUNIT':
            print card, effarea_header[card], effarea_header.comments[card]
            hdu_new.header.append((card, effarea_header[card], effarea_header.comments[card]))
   
       
    del effarea_header[:26]
    del effarea_header[-2:]
    for card in effarea_header:
        print card, effarea_header[card], effarea_header.comments[card]
        hdu_new.header.append((card, effarea_header[card], effarea_header.comments[card]))
   
   
    hdulist.append(hdu_new)

    
    print "*****************"
    print "Scale background..."


   
    plt.figure()
     
    background_cols = hdulist[3].columns
    background_data = hdulist[3].data
    background_header = hdulist[3].header
 
    detx_low_column  = 0
    detx_high_column = 1
    dety_low_column  = 2
    dety_high_column = 3

     
    n_x = 90 
    n_y = 90
    if n_x % 2 == 0:
        n_half_new = n_x/2
    else:
        n_half_new = np.floor(n_x/2.) + 1
    
     
    theta_high_max = 6 #deg.
     
    delta_theta_x = float(2 * theta_high_max)/float(n_x)
    delta_theta_y = float(2 * theta_high_max)/float(n_y)

    detx_low  = background_data["DETX_LO"][:][0]
    detx_high = background_data["DETX_HI"][:][0]
    dety_low  = background_data["DETY_LO"][:][0]
    dety_high = background_data["DETY_HI"][:][0]
    
    n_x_old = len(detx_low)
    n_y_old = len(dety_low)
    if n_x_old % 2 == 0:
        n_half = n_x_old/2
    else:
        n_half = np.floor(n_x_old/2.) + 1
        
    
    detx_center = np.array((detx_high + detx_low)/2)
    dety_center = np.array((dety_high + dety_low)/2)


    bgd = background_data["BGD"][:][0]
    
    detx_low_new  = np.linspace(-theta_high_max, theta_high_max-delta_theta_x, n_x)
    detx_high_new = np.linspace(-theta_high_max+delta_theta_x, theta_high_max, n_x)
    dety_low_new  = np.linspace(-theta_high_max, theta_high_max-delta_theta_y, n_y)
    dety_high_new = np.linspace(-theta_high_max+delta_theta_y, theta_high_max, n_y)

    detx_center_new = np.array((detx_high_new + detx_low_new)/2)
    dety_center_new = np.array((dety_high_new + dety_low_new)/2)
     
    n_energy = len(background_data["ENERG_LO"][:][0])

    bgd_new = np.zeros((n_energy, n_x, n_y))
    
    
    detx_center_half = detx_center[-n_half:]
    detx_center_half_new = detx_center_new[-n_half_new:]


    for i_energy in range(n_energy):
        
        rate_max = max(background_data["BGD"][:][0][i_energy].flatten())
        #print "max rate:", rate_max
 
         
        bgd_slice = np.zeros(n_x_old)
        bgd_slice_half = np.zeros(n_half)
        # extract 1D profile:
        for i_x in range(n_x_old):
            bgd_slice[i_x] = bgd[i_energy][i_x][int(n_y_old/2)]
         
        bgd_slice_half = bgd_slice[-n_half:]
        
#         print aeff[:,i_energy]
        for i in range(len(aeff[:,i_energy])):
            aeff[i,i_energy] = bgd_slice_half[i]
        
        #bgd_slice_half = aeff[:,i_energy]/10000.
        
        def BckOptimizer( parameters,  x):
                return polyGaussEven(x, parameters[0], parameters[1], parameters[2],  parameters[3]) - aeff[:,i_energy]
        ## begin fit
        startparams = [ 1.5, 1.6, 1.4, aeff[0,i_energy] ]
        leastsquarefit, cov = scipy.optimize.leastsq(BckOptimizer, startparams, (detx_center_half,))
        fitvals = leastsquarefit
        test = polyGaussEven(detx_center_half_new, fitvals[0], fitvals[1], fitvals[2], fitvals[3])

        print

        print "Fill background cube..."
        for i_x in range(n_x):
            for i_y in range(n_y):
                theta = np.sqrt(detx_center_new[i_x]**2 + dety_center_new[i_y]**2)
                fit = polyGaussEven(theta, fitvals[0], fitvals[1], fitvals[2], fitvals[3])

                bgd_new[i_energy][i_x][i_y] = fit
         
#         title = "0.5h average, energy bin ["+ str(energy_low[i_energy])+","+str(energy_high[i_energy])+"] TeV"    
#         plt.plot(detx_center_new, bgd_new[i_energy][:][int(n_y/2)], label=str(i_energy))
#         plt.scatter(detx_center_half,  bgd_slice_half, color='g', s=50)
#         plt.xlim([0,7.5])
#         plt.xlabel("offset theta [deg]")
#         plt.ylabel("bck. rate [1/s/MeV/sr]")
#          
#         plt.title(title)
#         nr = "%2.2d" % i_energy
#         name = "fit_bck"+ nr + ".png"
#         #plt.savefig(name, format='png')
#         plt.show()


    del hdulist[3]
   
    # create new columns
    format_e = str(n_energy) + 'E'
    col1 = fits.Column(name="ENERG_LO", format=format_e, array=[energy_low])
    col2 = fits.Column(name="ENERG_HI", format=format_e, array=[energy_high])
       
    format_nx = str(n_x) + 'E'
    col3 = fits.Column(name="DETX_LO", format=format_nx, array=[detx_low_new])
    col4 = fits.Column(name="DETX_HI", format=format_nx, array=[detx_high_new])
    
    format_ny = str(n_y) + 'E'   
    col5 = fits.Column(name="DETY_LO", format=format_ny, array=[dety_low_new])
    col6 = fits.Column(name="DETY_HI", format=format_ny, array=[dety_high_new])
    
    
    format_bgd = str(n_x*n_y*n_energy) + 'E'
    col7 = fits.Column(name="BGD", format=format_bgd, array=[bgd_new])
       
       
    cols = fits.ColDefs([col3, col4, col5, col6, col1, col2, col7])
       
    dim_bgd = "("+str(n_x)+ ","+ str(n_y) + ","+ str(n_energy)+")"
    cols.change_attrib("BGD", "dim", dim_bgd)

   
    hdu_new = fits.BinTableHDU.from_columns(cols)
       
       
   
    for card in background_header:#
        if card[0:5] == 'TUNIT':
            print card, background_header[card], background_header.comments[card]
            hdu_new.header.append((card, background_header[card], background_header.comments[card]))
   
       
    del background_header[:29]
    del background_header[-1:]
    for card in effarea_header:
        print card, background_header[card], background_header.comments[card]
        if card == "CBD50001":
            background_header[card] = 'DETX(-'+str(theta_high_max)+'-'+str(theta_high_max)+')deg'
        if card == "CBD60001":
            background_header[card] = 'DETY(-'+str(theta_high_max)+'-'+str(theta_high_max)+')deg'
        hdu_new.header.append((card, background_header[card], background_header.comments[card]))
#    
   
    hdulist.append(hdu_new)


   
    print "write output to:", fitspath_out + 'irf_file.fits.gz'
    hdulist.writeto(fitspath_out + 'irf_file.fits.gz', clobber='true')
    
    hdulist.close()
    

##    execute main part:    ###################################################
###############################################################################


def polyGaussEven(x, a, b, c, d):
    return d * np.exp(-( np.power(x/a, 6) + np.power(x/b, 4)+  np.power(x/c, 2)))


def gauss(x, d):
    return d  * np.exp(-x**2/2.)

if __name__ == "__main__":
    main(sys.argv[1:])
    


##    end of file    ##########################################################
###############################################################################
