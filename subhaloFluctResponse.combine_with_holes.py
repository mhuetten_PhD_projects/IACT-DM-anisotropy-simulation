#!/usr/bin/python
# -*- coding: utf-8 -*-

###############################################################################
#    Authors:
#    This file belongs to the Clumpy code, http://lpsc.in2p3.fr/clumpy
#    For any questions, please contact moritz.huetten@desy.de
###############################################################################
 
# Minimal example Python script for transforming FITS files created by Clumpy 
# with the -o2 option) into FITS images according to the WCS standard.
#
# For a more complete introduction into healpy and its plotting features, please
# have a look at the nice documentation at http://healpy.readthedocs.org
#
# You need to have installed 
#   - matplotlib
#   - numpy 
#   - healpy 
#   - astropy.io
# to run this script.

# example for usage:
# 1.) ./bin/clumpy -g7 clumpy_params.txt 180. 0. 4. 4. 1
# 2.) ./bin/clumpy -o2 output/annihil_gal2D_LOS180,0_FOV4x4_rse1_alphaint0.10deg_nside1024.fits 1 1
# 3.) python python_helperscripts/makeFitsImage.py --infile output/annihil_gal2D_LOS180,0_FOV4x4_rse1_alphaint0.10deg_nside1024-JFACTOR-Jtot.fits
# if you hace healpy version >= 1.9.0, directly do
# 2.) python python_helperscripts/makeFitsImage.py --infile output/annihil_gal2D_LOS180,0_FOV4x4_rse1_alphaint0.10deg_nside1024.fits --extension 1 --column 1

###############################################################################
#    import modules:
###############################################################################

import os
import sys
import getopt
import warnings
import matplotlib
matplotlib.use('Agg') # turn of interactive renderer (may otherwise not work on clusters)
import healpy as hp # warning: due to a bug in healpy, importing it before pylab can cause a segmentation fault in some circumstances.
if float(hp.__version__[:3]) < 1.89:
        warnings.warn('WARNING: Your healpy version is older than 1.9.0.\n Reading part-sky map in/or explicit indexing will not work.\n')
import numpy as np
from astropy.io import fits
import gammalib

###############################################################################
##    main part: 
###############################################################################

def main(argv):
    
    ###########################################################################
    #  read input variables:
    
    infiles_string = 'empty'
    outdir = 'none'
    suffix = 'none'

    i_ext  = -1
    i_col  = -1
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:o:s:",["infiles=","outdir=","suffix="])
    except getopt.GetoptError:
        print('Wrong input. The input options are:')
        print('-i or --infiles for reading the input FITS files')
        print('-h for help')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print(' HELP option')
            sys.exit()
        elif opt in ("-i", "--infiles"):
            infiles_string = str(arg)
        elif opt in ("-o", "--outdir"):
            outdir = str(arg)
        elif opt in ("-s", "--suffix"):
            suffix = str(arg)


    
    if infiles_string == 'empty':
        print(' Please parse an input FITS file with -i file.fits or --infile file.fits\n\
 Use the -h option for further help.')
        sys.exit()

    ###########################################################################
    
    
    # separate input files in string:
    inputfiles = infiles_string.split()
    print " Input files:"
    print inputfiles

    if len(inputfiles) < 3:
        if len(inputfiles) == 1:
            if "Background" not in inputfiles[0]:
                print "only one Not-backgrund input file. Nothing do to. Quit"
                sys.exit()
        else:
            print "ERROR: Less than three input files. Probably some simulations crashed."
            sys.exit()
    
    if len(inputfiles) > 4:
        print "ERROR: More than four input files. Probably some code bug."
        sys.exit()
    #  read headers:
    i_file = 0
    nside_check = -1
    
    nevents     = 0
    nevents_sig = 0
    nevents_bck = 0
    
    for file in inputfiles:
        
        print "...processing",file
        
        if i_ext == -1:
            warnings.warn('  no input extension specified. Set to first extension.')
            i_ext = 1
        if i_col == -1:
            warnings.warn('  no input column specified. Set to first column.')
            i_col = 1
        
        
        hdulist_in = fits.open(file)
        pixtype = hdulist_in[i_ext].header['PIXTYPE']
        if pixtype[:7] != 'HEALPIX':
            raise IOError(' No Healpix map in file. Exit')
        
        partial = hdulist_in[i_ext].header['OBJECT']
        if partial[:7] == 'PARTIAL':
            fullsky = False
        elif partial[:7] == 'FULLSKY':
            fullsky = True
        else:
            raise IOError(' Wring OBJECT keyword. Must be PARTIAL or FULLSKY')

        ext_name = hdulist_in[i_ext].header['EXTNAME']
        columns = hdulist_in[i_ext].columns

        try:
            nside = hdulist_in[i_ext].header['NSIDE']
        except:
            raise IOError('  Neccessary input information missing.\n Must contain Healpix full-sky map, e.g. created by Clumpy > 2015.06_corr3 with the -o2 option.')
        
        try:
            ra_obj_deg  = hdulist_in[i_ext].header['PSI_0'] # last target position
            dec_obj_deg = hdulist_in[i_ext].header['THETA_0']
            size_x      = hdulist_in[i_ext].header['SIZE_X']
            size_y      = hdulist_in[i_ext].header['SIZE_Y']
            emin_TeV    = hdulist_in[i_ext].header['EMIN']
            emax_TeV    = hdulist_in[i_ext].header['EMAX']
        except:
            raise IOError('  Missing info to merge output maps.')
        
        # check for consistent nside between all maps:
        if nside_check != -1:
            if nside != nside_check:
                raise IOError('  All input maps must have same NSIDE.')
            else:
                nside_check = nside
        else:
            nside_check = nside
            # create combined map:
            map_combined = np.zeros(hp.nside2npix(nside))

        try:    
            nevents     += hdulist_in[i_ext].header['NEVENTS']
            nevents_sig += hdulist_in[i_ext].header['N_SIG']
            nevents_bck += hdulist_in[i_ext].header['N_BCK']
        except:
            raise IOError('  Missing info to merge output maps.')
        
        tbdata  = hdulist_in[i_ext].data

        
        columns = hdulist_in[i_ext].columns
        if fullsky == False:
            column  = columns[i_col] # zeroth column is PIXEL
        else:
            column  = columns[i_col -1 ]

        
        npix_fov = len(tbdata[column.name])

        
        
        # create combined map:

        #  read map:
        print 'read input map...'
        
        if fullsky == True:
            #map_in = tbdata[0][:]
            map_in = hp.read_map(file)
            
        else:
            #for i in range(len(tbdata)):
                #map_combined[tbdata[i][0]] += tbdata[i][1]
            map_in = hp.read_map(file, partial=True, field=i_col-1, hdu=i_ext)
        
        hdulist_in.close()
        "... done."
           
        map_in[map_in == -1.6375e+30] = 0
        map_combined += map_in
        
        i_file += 1

    # now exclude regions from TeVCat catalogue:
        # load TeVCAT sources:
    datadir = "/afs/ifh.de/group/cta/scratch/mhuetten/Thesis/Data/Chapter06/"
    
    tevcat_coords = np.loadtxt(datadir + 'tevcat_coords_deg.txt')
    tevcat_names_file = open(datadir + "tevcat_names.txt", "r")
    tevcat_names = [line.rstrip('\n') for line in tevcat_names_file]
    tevcat_names_file.close()

    exclude_sigma = np.deg2rad(1.)
    
    tevcat_dir = gammalib.GSkyDir()
    center_dir = gammalib.GSkyDir()
    center_dir.radec_deg( ra_obj_deg, dec_obj_deg )
    
    
    for i in range(len(tevcat_coords)):
        tevcat_dir.radec_deg( tevcat_coords[i,0], tevcat_coords[i,1] )
        if tevcat_dir.dist_deg(center_dir) < 60.:
            print ".. generate skymap hole for source",tevcat_names[i]
            tevcat_dir_vec = hp.dir2vec(tevcat_coords[i,0], tevcat_coords[i,1], lonlat=True)
            # get healpix bins around source:
            pixel_exclude = hp.query_disc(nside, tevcat_dir_vec, 3 * exclude_sigma)
            print "     -> calculate mask in",len(pixel_exclude),"pixels."
            for pixel in pixel_exclude:
                # get distance to source position:
                pos_pixel = hp.pix2vec(nside, pixel)
                distance = hp.rotator.angdist(pos_pixel, tevcat_dir_vec)
                #print "    .. pixel distance:", np.rad2deg(distance)
                map_combined[pixel] = int(round( map_combined[pixel] * (1 - np.exp( - distance**2 / (2 * exclude_sigma**2) ))  ))
                #print "    .. pixel distance:", np.rad2deg(distance), "deg, damping:",(1 - np.exp( - distance**2 / (2 * exclude_sigma**2) ))

    
    map_combined[map_combined == 0] = -1.6375e+30
        
         
        
    # save output map:
    if suffix != 'none':
        outfile_map = 'rawmap_'+str(int(1000*emin_TeV))+'GeV_combined'+suffix+'.fits'
    else:
        outfile_map = 'rawmap_'+str(int(1000*emin_TeV))+'GeV_combined.fits'
    
    if outdir != 'none':
        outpath_map = outdir + '/' + outfile_map
    else:
        outpath_map = outfile_map
    
    print "write output map:", outpath_map
    hp.write_map(outpath_map, map_combined, partial=True, coord='C', dtype=np.int16)#, column_names="Countsmap", column_units="counts")
    
    hdulist_out = fits.open(outpath_map, mode='update')
    hdulist_out[1].header.set('EXTNAME', 'Skymap')
    hdulist_out[1].header.set('COORDSYS', 'C')
    hdulist_out[1].header.set('PSI_0', ra_obj_deg ) # last target position
    hdulist_out[1].header.set('THETA_0', dec_obj_deg ) # last target position
    hdulist_out[1].header.set('SIZE_X', size_x )
    hdulist_out[1].header.set('SIZE_Y', size_y)
    hdulist_out[1].header.set('NEVENTS', nevents, 'total # of events in map')
    print "------------------------------"
    print nevents, " total events above", emin_TeV, "GeV"
    hdulist_out[1].header.set('N_SIG', nevents_sig, '# of signal events in map')
    print "  ", nevents_sig, " signal events above", emin_TeV, "GeV"
    hdulist_out[1].header.set('N_BCK', nevents_bck, '# of background events in map')
    print "  ", nevents_bck, " background events above", emin_TeV, "GeV"
    print "------------------------------"
    hdulist_out[1].header.set('EMIN', emin_TeV, 'lower bound of energy bin in TeV')
    hdulist_out[1].header.set('EMAX', emax_TeV, 'upper bound of energy bin in TeV')
    hdulist_out[1].header.set('TTYPE1', 'PIXEL')
    hdulist_out[1].header.set('TTYPE2', 'Counts')
    hdulist_out.flush()
    hdulist_out.close()


  
    
    
    # calculate power spectrum:
    
    print "calculate power spectrum..."
    cl = hp.anafast(map_combined, lmax=nside)
    
    outfile_cl = 'cl_'+outfile_map
    
   
    if outdir != 'none':
        outpath_cl = outdir + '/' + outfile_cl
    else:
        outpath_cl = outfile_cl
    
     
     
    print "... done. Write spectrum to file:", outpath_cl
    hp.write_cl(outpath_cl, cl)
    hdulist_out = fits.open(outpath_cl, mode='update')
    hdulist_out[1].header.set('EXTNAME', 'CountsMapAPS')
    hdulist_out[1].header.set('NSIDE', nside, ' HEALPIX resolution of binning')
    hdulist_out[1].header.set('NEVENTS', nevents, '# of events in map')
    hdulist_out[1].header.set('N_SIG', nevents_sig, '# of signal events in map')
    hdulist_out[1].header.set('N_BCK', nevents_bck, '# of background events in map')
    hdulist_out[1].header.set('EMIN', emin_TeV, 'lower bound of energy bin in TeV')
    hdulist_out[1].header.set('EMAX', emax_TeV, 'upper bound of energy bin in TeV')
    hdulist_out[1].header.set('TTYPE1', 'Counts')
    hdulist_out.flush()
    hdulist_out.close()
    del cl

    print( "finished.")
    print( " ")
    print( " ")
    
if __name__ == "__main__":
     
    main(sys.argv[1:])

##    end of file    ##########################################################
############################################################################### 
