#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from matplotlib.patches import Rectangle
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
import MoritzCTAtools


def chisqg(ydata,ymod,sd=None):  
      """  
 Returns the chi-square error statistic as the sum of squared errors between  
 Ydata(i) and Ymodel(i). If individual standard deviations (array sd) are supplied,   
 then the chi-square error statistic is computed as the sum of squared errors  
 divided by the standard deviations.     Inspired on the IDL procedure linfit.pro.  
 See http://en.wikipedia.org/wiki/Goodness_of_fit for reference.  
   
 x,y,sd assumed to be Numpy arrays. a,b scalars.  
 Returns the float chisq with the chi-square statistic.  
   
 Rodrigo Nemmen  
 http://goo.gl/8S1Oo  
      """  
      # Chi-square statistic (Bevington, eq. 6.9)  
      if sd==None:  
           chisq=np.sum((ydata-ymod)**2)  
      else:  
           chisq=np.sum( ((ydata-ymod)/sd)**2 )  
        
      return chisq  


##    main part:   ############################################################
###############################################################################

def main(argv):

#     alphaIntDeg = 0.02
#     TWOsigmaFOVdeg = 7.5
#     TWOsigmaPSFdeg = 0.2
#     psi0deg = 90.0
#     theta0degGal = 0.0
#     totalBins = 25
#     irffile = "/afs/ifh.de/group/cta/scratch/mhuetten/CTA/EffectiveAreasCTA/DESY.d20140309.Erec1.R2.ID0NIM2.prod2-LeoncitoPP-NS.S.2a.180000s.root"
#     sensitivityFile = "/afs/ifh.de/group/cta/scratch/mhuetten/CTA/EffectiveAreasCTA/DESY.d20140309.Erec1.R2.ID0NIM2.prod2-LeoncitoPP-NS.S.2a.180000s.root"
#     noise = 325
#     elevation = 20 
#     azimutBin = 2
#     tobs = 300
#     sigmav = 4e-23
#     mchi = 750.0
#     signalModel = "noDarkMatter"
#     backgroundModel = "VTSdata"
#     inputDirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/mchi750GeV-tautau-300h-10realisations/sigmav-4e-23/" 
#     outputDirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/mchi750GeV-tautau-300h-10realisations/sigmav-4e-23/"
#     thresholdBin = 7
#     Phi0astroflux = 2.05e-6
#     backgroundOnlyDirectory="/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/Background-300h-500realisations/"
#     realisations = 10
#     astroAnisotropiesModel = "isotropic"
    
    ###########################################################################
    # read input variables
    try:
        opts, args = getopt.getopt(sys.argv[1:],"ha:f:p:l:b:c:d:r:q:n:v:z:t:s:m:y:k:j:x:g:w:i:o:",["alphaIntDeg=","fov=","psf=","psi=","theta=","totalbins=","thresholdbin=","irffile=","sensitivityfile=","noise=","elevation=","azimutbin=","tobs=","sigmav=","mchi=","signalmodel=","backgroundmodel=","astroanisotropiesmodel=","phizeroastro=","realisations=","backgroundonlypath=","dirin=","dirout="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'The input options are:'
        print ' -a <AlphaIntDeg>'
        print ' -f <TWOsigmaFOVdeg>'
        print ' -p <TWOsigmaPSFdeg>'
        print ' -l <psi0deg>'
        print ' -b <theta0degGal>'
        print ' -c <totalbins>'
        print ' -d <thresholdbin>'
        print ' -r <iRffile>'
        print ' -q <sensitivityfile>'
        print ' -n <Noise>'
        print ' -v <eleVation>'
        print ' -z <aZimutbin>'
        print ' -t <Tobs>'
        print ' -s <Sigmav>'
        print ' -m <Mchi>'
        print ' -y <signalmodel>'
        print ' -k <backgroundmodel>'
        print ' -j <astroanisotropiesmodel>'
        print ' -x <phizeroastro>'
        print ' -g <realisations>'
        print ' -w <backgroundonlypath>'
        print ' -i <input directory>'
        print ' -o <output directory>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print ' -a <AlphaIntDeg>'
            print ' -f <TWOsigmaFOVdeg>'
            print ' -p <TWOsigmaPSFdeg>'
            print ' -l <psi0deg>'
            print ' -b <theta0degGal>'
            print ' -c <totalbins>'
            print ' -d <thresholdbin>'
            print ' -r <iRffile>'
            print ' -q <sensitivityfile>'
            print ' -n <Noise>'
            print ' -v <eleVation>'
            print ' -z <aZimut>'
            print ' -t <Tobs>'
            print ' -s <Sigmav>'
            print ' -m <Mchi>'
            print ' -y <signalmodel>'
            print ' -k <backgroundmodel>'
            print ' -j <astroanisotropiesmodel>'
            print ' -x <phizeroastro>'
            print ' -g <realisations>'
            print ' -w <backgroundonlypath>'            
            print ' -i <input directory>'
            print ' -o <output directory>'
            sys.exit()
        elif opt in ("-a", "--alphaIntDeg"):
            alphaIntDeg = float(arg)
        elif opt in ("-f", "--fov"):
            TWOsigmaFOVdeg = float(arg)
        elif opt in ("-p", "--psf"):
            TWOsigmaPSFdeg = float(arg)
        elif opt in ("-l", "--psi"):
            psi0deg = float(arg)
        elif opt in ("-b", "--theta"):
            theta0degGal = float(arg)
        elif opt in ("-c", "--totalbins"):
            totalBins = int(arg)
        elif opt in ("-d", "--thresholdbin"):
            thresholdBin = int(arg)
        elif opt in ("-r", "--irffile"):
            irffile = arg
        elif opt in ("-q", "--sensitivityfile"):
            sensitivityFile = arg
        elif opt in ("-n", "--noise"):
            noise = int(arg)
        elif opt in ("-v", "--elevation"):
            elevation = float(arg) 
        elif opt in ("-z", "--azimutbin"):
            azimutBin = int(arg) 
        elif opt in ("-t", "--tobs"):
            tobs = float(arg) 
        elif opt in ("-s", "--sigmav"):
            sigmav = float(arg)
        elif opt in ("-m", "--mchi"):
            mchi = float(arg)
        elif opt in ("-y", "--signalmodel"):
            signalModel = str(arg)
        elif opt in ("-k", "--backgroundmodel"):
            backgroundModel = str(arg)
        elif opt in ("-j", "--astroanisotropiesmodel"):
            astroAnisotropiesModel = str(arg)
        elif opt in ("-x", "--phizeroastro"):
            Phi0astroflux = float(arg)
        elif opt in ("-g", "--realisations"):
            realisations = int(arg)
        elif opt in ("-w", "--backgroundonlypath"):
            backgroundOnlyDirectory = arg 
        elif opt in ("-i", "--dirin"):
            inputDirectory = arg 
        elif opt in ("-o", "--dirout"):
            outputDirectory = arg
            
    start_time = time.time()
    print " initializing program anisotropySimulation.evaluatePowerSpectra.py"
    print  " "
    ###########################################################################
    ## first computations from input variables:
    # convert alphaInt into radians:
    alphaInt = alphaIntDeg/180.0*np.pi
        
    # determine dimension of healpix map, adapted to resolution of clumpy data: 
    nside = MoritzCTAtools.alphaInt2nside(alphaInt)
    # give resolution of skymap in rads, determined by grid resolution:
    resolution = hp.nside2resol(nside) * 2.0 / np.sqrt(np.pi)
    resolutionDeg = resolution * 180.0 / np.pi
    
    thetaSquared = 0.008
     
    ###########################################################################
    # recalculate input values:
        
    # calculate number of pixels on sphere from NSIDE:
    npix=hp.nside2npix(nside)
    pixelareaHealpix = 4*np.pi/npix
    
    # give celestial position in rads:
    psi0 =  np.pi*psi0deg/180.0
    psi0lat = -psi0 # healpy counts phi clockwise!!
    theta0deg=90.0-theta0degGal # got to interval theta in [0,180 deg]
    theta0 = np.pi*theta0deg/180.0
    # give field of view in radians:
    sigmaFOV = 0.5*np.pi*TWOsigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
    # give PSF in radians:
    sigmaPSF = 0.5*np.pi*TWOsigmaPSFdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
    # determine maximum calculable multipole element lmax:
    lmax = int(np.pi/hp.nside2resol(nside))
    # give observation time in seconds:
    tobs = tobs * 3600
    
    if signalModel == "noDarkMatter":
        sigmavString = "background only"
    else:
        sigmavString = "<sigma*v> = "+str(sigmav)+" cm^3/s"
        
    INDIR = inputDirectory
    inputDirectory = inputDirectory+"/spectra-mc"
    BACKGROUNDDIR = backgroundOnlyDirectory
    backgroundOnlyDirectory = backgroundOnlyDirectory+"/spectra-mc"
     
    print  " ** INPUT PARAMETERS: **"
    print " Input directory: ", inputDirectory
    print " Output directory: ", outputDirectory
    print " NSIDE of healpix map:",nside, "pixels"
    print " resolution of healpix map (full pixel diameter for approximately circular pixels):", round(resolutionDeg,3), "degrees"
    print " size of healpix map:",npix, "pixels"
    print " size of healpix map pixel:",pixelareaHealpix, "sr"
    print " Full diameter of FOV acceptance (1 sigma): ",TWOsigmaFOVdeg,"°"
    print " Full diameter of gaussian PSF (1 sigma): ",TWOsigmaPSFdeg,"°" 
    print " Center position in sky in celestial coord, latitude: ",theta0deg,"°" 
    print " Center position in sky in celestial coord, longitude ",psi0deg,"°" 
    print " Resolution of skymap in degs: ",resolutionDeg, "°"
    print " Maximum resolvable multipole index l (lmax): ",lmax
    print " Number of evaluated sample realisations: ",realisations
    print " ** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS ADD FLAG -h AT STARTING **"
    print " "
        
    realizationVector=np.arange(realisations)
    print " number of evaluated sample realisations (check output): ", len(realizationVector)
    print ""
     
    ################################# clumpy data#######################################
     
    # #Load first dmClumps simulation output spectrum:
    # firstspectrumfilepath = inputDirectory+"/spectra-clumpy/clumpy-realisation-1.spectrum.fits"
    #  
    #  
    # data = pf.getdata(firstspectrumfilepath)
    # clOUT = data.field(0)
    # firstspectrum = clOUT[1:]
    #  
    #  
    # clumpyspectrum = np.ndarray((len(runnumbers), len(firstspectrum)))
    # print "dimension of combined clumpy spectra data matrix: ", clumpyspectrum.shape
    #  
    # print "plotting clumpy power spectra..."
    #  
    # for i in range(len(runnumbers)):
    #     print i
    #     clumpyspectrafilepath = inputDirectory+"/spectra-clumpy/clumpy-realisation-"+str(runnumbers[i]+1)+".spectrum.fits"
    #   
    #     data = pf.getdata(clumpyspectrafilepath)
    #     clOUT = data.field(0)
    #       
    #     inputspectrum = clOUT[1:]
    #     for j in range(len(inputspectrum)):
    #         clumpyspectrum[i,j] = inputspectrum[j]
    #   
    # plot_powerspectrum = plt.figure(figsize=(12, 8)) 
    #   
    #   
    #        
    # ellVector = np.arange(len(firstspectrum))
    # print len(ellVector)
    # # 
    # # 
    # for i in range(len(runnumbers)):
    #  
    #     COLOR = 'pink'
    #           
    #     p1 = plt.plot(ellVector, ellVector * (ellVector+1) * clumpyspectrum[i,:]/(2*np.pi),color=COLOR,  linestyle='-', linewidth=1.5)#, label="realisation "+str(runnumbers[i]+1))   
    #   
    #   
    # # # include background bands:
    # # backgrundruns = 2
    # # backgrundclspectrum = np.ndarray((backgrundruns, len(firstspectrum)))
    # #   
    # # for i in range(backgrundruns):
    # #     print i
    # #     inputname_clOutput = str(outputDirectory)+"/Background"+str(i+1)+"/%s.fits" % filename 
    # #   
    # #     data = pf.getdata(inputname_clOutput)
    # #     clOUT = data.field(0)
    # #       
    # #     inputspectrum = clOUT[1:]
    # #     for j in range(len(inputspectrum)):
    # #         backgrundclspectrum[i,j] = inputspectrum[j]    
    # #   
    # #   
    # # backgrundclbands = np.ndarray((2, len(firstspectrum)))
    # #   
    # # for k in range( len(firstspectrum)):
    # #         backgrundclbands[0,k]= min(backgrundclspectrum[:,k])
    # #         backgrundclbands[1,k]= max(backgrundclspectrum[:,k])
    # #   
    # # p1 = plt.fill_between(ellVector, ellVector * (ellVector+1) * backgrundclbands[0,:]/(2*np.pi),ellVector * (ellVector+1) * backgrundclbands[1,:]/(2*np.pi),color='gray',alpha=1., label="Isotropic background")
    # #                
    # title = 'Angular power spectrum of CLUMPY skymap  around galactic position (Psi,theta)=(%s,%s) degrees,  width of simulated map (Delta Psi,Delta theta)=(%s,%s). Resolution = %s degs, user_rse = %s minimal clump mass = %s M_sun and %s realisations.'%(psi0deg,theta0deg,psiWidthDeg,thetaWidthDeg,alpha_int,user_rse,minmass,realisations)
    # plt.title('\n'.join(wrap(title,80)))
    # plt.subplots_adjust(top=0.85)
    # plt.legend(loc='upper left')
    # plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
    # plt.xscale('log')
    # plt.yscale('log')
    # pylab.xlim([1,lmax])
    # #pylab.ylim([10**(-4),10**6])
    # #plt.text(5, 0.2,'gray bands: Isotropic background', horizontalalignment='center',verticalalignment='baseline')
    #  
    #       
    # #plt.show()
    
    ###########################################################################
    ## MC data:
     
    #Load first mc simulation output spectrum:
    firstSpectrumFilePath = inputDirectory+"/combinedNormalizedMap_thresholdBin"+str(thresholdBin)+"-realisation-1.spectrum.fits"
          
    firstSpectrumData = pf.getdata(firstSpectrumFilePath)
    clOUT = firstSpectrumData.field(0)
    firstSpectrumVector = clOUT[1:]
        
    mcNormalizedSpectrum = np.ndarray((len(realizationVector), len(firstSpectrumVector)))
    mcRawSpectrum = np.ndarray((len(realizationVector), len(firstSpectrumVector)))
    #print " dimension of combined MC spectra data matrix: ", mcNormalizedSpectrum.shape
     
     
    # combine all realisations into one matrix:
    for realisation in range(len(realizationVector)):
        normalizedSpectrumFilePath = inputDirectory+"/combinedNormalizedMap_thresholdBin"+str(thresholdBin)+"-realisation-"+str(realizationVector[realisation]+1)+".spectrum.fits"
        rawSpectrumFilePath = inputDirectory+"/combinedRawMap_thresholdBin"+str(thresholdBin)+"-realisation-"+str(realizationVector[realisation]+1)+".spectrum.fits"
        
        normalizedSpectrumData = pf.getdata(normalizedSpectrumFilePath)
        rawSpectrumData = pf.getdata(rawSpectrumFilePath)
        clOUTnormalized = normalizedSpectrumData.field(0)
        clOUTraw = rawSpectrumData.field(0)
          
        normalizedSpectrumVector = clOUTnormalized[1:]
        rawSpectrumVector = clOUTraw[1:]
        
        for j in range(len(normalizedSpectrumVector)):
            mcNormalizedSpectrum[realisation,j] = normalizedSpectrumVector[j]
            mcRawSpectrum[realisation,j] = rawSpectrumVector[j]
            
    #save combined spectra matrix to file:
    #spectraCombinedOutputFilePath = outputDirectory+"/combinedNormalizedSpectra.spectra"
    #np.savetxt(spectraCombinedOutputFilePath, mcNormalizedSpectrum)
     
    # calculate mean and standard deviation for each l by assuming normal distributed values:
    #print " calculate mean and standard deviation for each l:"
    normalizedAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    rawAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    for i in range(len(firstSpectrumVector)):
        normalizedAverageSpectrum[0,i] = np.mean(mcNormalizedSpectrum[:,i])
        normalizedAverageSpectrum[1,i] = np.std(mcNormalizedSpectrum[:,i])
        rawAverageSpectrum[0,i] = np.mean(mcRawSpectrum[:,i])
        rawAverageSpectrum[1,i] = np.std(mcRawSpectrum[:,i])
        
    #save average spectrum to file:
    normalizedAverageSpectrumOutputFilePath = outputDirectory+"/averageNormalizedSpectrum_thresholdBin"+str(thresholdBin)+".spectra"
    rawAverageSpectrumOutputFilePath = outputDirectory+"/averageRawSpectrum_thresholdBin"+str(thresholdBin)+".spectra"
    np.savetxt(normalizedAverageSpectrumOutputFilePath, normalizedAverageSpectrum)
    np.savetxt(rawAverageSpectrumOutputFilePath, rawAverageSpectrum)
   
    # calculate mean and standard deviation from theoretical formulae:
      
#    CPoisson = 4*np.pi/nevents
#     sigmaPoissonVectorUpper = np.zeros(len(firstspectrum)) 
#     sigmaPoissonVectorLower = np.zeros(len(firstspectrum)) 
#     for i in range(len(firstspectrum)):
#         #sigmaPoissonVectorUpper[i] = CPoisson + CPoisson  * np.sqrt(2/((1-np.cos(sigmaFOV/2))*(i+1))) 
#         #sigmaPoissonVectorLower[i] = CPoisson - CPoisson  * np.sqrt(2/((1-np.cos(sigmaFOV/2))*(i+1)))
#         sigmaPoissonVectorUpper[i] = CPoisson + CPoisson * 5/ np.sqrt(i+1)
#         sigmaPoissonVectorLower[i] = CPoisson - CPoisson * 5/ np.sqrt(i+1)
#          
#     ######### histogram: ###########
#      
#     ellhist=1000
#      
#     nbins = int(np.sqrt(realisations))
#      
#     hist, bins = np.histogram(ellhist * (ellhist+1) *mcspectrum[:,ellhist]/(2*np.pi), bins=nbins)
#      
#     width = 0.7 * (bins[1] - bins[0])
#     center = (bins[:-1] + bins[1:]) / 2
#     plot_histogram = plt.figure(figsize=(12, 10)) 
#      
#     #plt.bar(center, hist, align='center')#, width=width)
#     #plt.hist(mcspectrum[:,ellhist],bins=20)#, 10,  facecolor='blue', alpha=0.75)
#     plt.hist(ellhist * (ellhist+1) *mcspectrum[:,ellhist]/(2*np.pi),bins=nbins)#, 10,  facecolor='blue', alpha=0.75)
#     title = 'Histogram @ multipole moment l=%s for angular power spectrum from normalized event skymap for total events n=%s, fsig=%s, Gaussian FOV = %s degs., Gaussian PSF = %s degs., DM ratio = %s,  resolution = %s degs and %s realisations.'%(ellhist,nevents,fsig,sigmaFOVdeg,sigmaPSFdeg,fDM,round(resolutiondeg,3),realisations)
#     plt.title('\n'.join(wrap(title,80)))
#     plt.xlabel('l(l+1)c_l/2pi'); plt.ylabel('# realisations in bin')
#     #################################
     
    
    ########################################################################### 
    print " plotting power spectra..."
    
    chisq = float('nan')
    dof = float('nan') 
    
    ellVector = np.arange(len(firstSpectrumVector))
    
    # plot means for NORMALIZED spectrum:
    plot_powerspectrumNormalized = plt.figure(figsize=(14, 7)) 

#    p5 = plt.plot(ellVector, ellVector * (ellVector+1) * CPoisson/(2*np.pi),color='grey',  linestyle='--', linewidth=1.5)#, label="realisation "+str(runnumbers[i]+1)) 
#    p6 = plt.plot(ellVector, ellVector * (ellVector+1) * sigmaPoissonVectorUpper/(2*np.pi),color='red',  linestyle='--', linewidth=1.5)#, label="realisation "+str(runnumbers[i]+1)) 
#    p7 = plt.plot(ellVector, ellVector * (ellVector+1) * sigmaPoissonVectorLower/(2*np.pi),color='green',  linestyle='--', linewidth=1.5)#, label="realisation "+str(runnumbers[i]+1))  
    # include background bands and do chi-squared test:
    if BACKGROUNDDIR != INDIR:
        backgroundNormalizedAverageSpectrumInputFilePath = backgroundOnlyDirectory+"/averageNormalizedSpectrum_thresholdBin"+str(thresholdBin)+".spectra"
        backgroundNormalizedAverageSpectrum = np.loadtxt(backgroundNormalizedAverageSpectrumInputFilePath)
        #p7 = plt.plot(ellVector, ellVector * (ellVector+1) * (backgroundNormalizedAverageSpectrum[0,:]+backgroundNormalizedAverageSpectrum[1,:])/(2*np.pi),color='gray',  linestyle='-', linewidth=1)#, label="realisation "+str(runnumbers[i]+1)) 
        #p8 = plt.plot(ellVector, ellVector * (ellVector+1) * (backgroundNormalizedAverageSpectrum[0,:]-backgroundNormalizedAverageSpectrum[1,:])/(2*np.pi),color='gray',  linestyle='-', linewidth=1)#, label="realisation "+str(runnumbers[i]+1)) 
        p3 = plt.plot(ellVector, ellVector * (ellVector+1) * backgroundNormalizedAverageSpectrum[0,:]/(2*np.pi),color='b',  linestyle='-', linewidth=1,label="isotropic background only")#, label="realisation "+str(runnumbers[i]+1)) 
        p4 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (backgroundNormalizedAverageSpectrum[0,:]+backgroundNormalizedAverageSpectrum[1,:])/(2*np.pi),ellVector * (ellVector+1) * (backgroundNormalizedAverageSpectrum[0,:]-backgroundNormalizedAverageSpectrum[1,:])/(2*np.pi),color='b',alpha=0.4, label="Isotropic background")
    
        #######################################################################
        ## chi-squared test:
        minEll = 600
        maxEll = 1000
        dof = maxEll - minEll
        sdVector = np.zeros(dof)
        dataVector = np.zeros(dof)
        backgroundVector = np.zeros(dof)
        for i in range(dof):
            dataVector[i] = normalizedAverageSpectrum[0,minEll+i]
            backgroundVector[i] = backgroundNormalizedAverageSpectrum[0,minEll+i]
            sdVector[i] = np.sqrt(normalizedAverageSpectrum[1,minEll+i]**2 + backgroundNormalizedAverageSpectrum[1,minEll+i]**2)
        chisq = chisqg(dataVector,backgroundVector,sdVector)
        print ""
        print " ********************************"
        print " chi^2 =:",chisq
        print " chi^2/d.o.f =:",chisq/dof
        print " ********************************"
        print ""
    
    p2 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]+normalizedAverageSpectrum[1,:])/(2*np.pi),ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]-normalizedAverageSpectrum[1,:])/(2*np.pi),color='r', alpha=0.4)#, label="realisation "+str(runnumbers[i]+1)) 
    p1 = plt.plot(ellVector, ellVector * (ellVector+1) * normalizedAverageSpectrum[0,:]/(2*np.pi),color='r',  linestyle='-', linewidth=1., label=sigmavString) 
        
    title = 'Angular power spectrum from NORMALIZED event skymap for mchi = %s GeV, %s, annihilation channel = %s, tobs= %s h, above threshold energy bin %s, background model = %s, astrophysical anisotropies model = %s and %s realisations.\n chi^2/d.o.f = %s'%(mchi,sigmavString,signalModel,round(tobs/3600,2),thresholdBin,backgroundModel,astroAnisotropiesModel,realisations,chisq/dof)
    plt.title('\n'.join(wrap(title,80)))
    plt.subplots_adjust(top=0.85)
    plt.legend(loc='upper left')
    plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1,lmax])
    pylab.ylim([10**(-2),10**(5)])
    #plt.text(5, 0.2,'gray bands: Isotropic background', horizontalalignment='center',verticalalignment='baseline')
    
    
    # plot means for RAW spectrum:
    plot_powerspectrumRaw = plt.figure(figsize=(14, 7))  
#    p6 = plt.plot(ellVector, ellVector * (ellVector+1) * sigmaPoissonVectorUpper/(2*np.pi),color='red',  linestyle='--', linewidth=1.5)#, label="realisation "+str(runnumbers[i]+1)) 
#    p7 = plt.plot(ellVector, ellVector * (ellVector+1) * sigmaPoissonVectorLower/(2*np.pi),color='green',  linestyle='--', linewidth=1.5)#, label="realisation "+str(runnumbers[i]+1))  
    
    # include background bands:
    if BACKGROUNDDIR != INDIR:
        backgroundRawAverageSpectrumInputFilePath = backgroundOnlyDirectory+"/averageRawSpectrum_thresholdBin"+str(thresholdBin)+".spectra"
        backgroundRawAverageSpectrum = np.loadtxt(backgroundRawAverageSpectrumInputFilePath)    

        #p7 = plt.plot(ellVector, ellVector * (ellVector+1) * (backgroundNormalizedAverageSpectrum[0,:]+backgroundNormalizedAverageSpectrum[1,:])/(2*np.pi),color='gray',  linestyle='-', linewidth=1)#, label="realisation "+str(runnumbers[i]+1)) 
        #p8 = plt.plot(ellVector, ellVector * (ellVector+1) * (backgroundNormalizedAverageSpectrum[0,:]-backgroundNormalizedAverageSpectrum[1,:])/(2*np.pi),color='gray',  linestyle='-', linewidth=1)#, label="realisation "+str(runnumbers[i]+1)) 
        p5 = plt.plot(ellVector, ellVector * (ellVector+1) * backgroundRawAverageSpectrum[0,:]/(2*np.pi),color='b',  linestyle='-', linewidth=1,label="isotropic background only")#, label="realisation "+str(runnumbers[i]+1)) 
        p6 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (backgroundRawAverageSpectrum[0,:]+backgroundRawAverageSpectrum[1,:])/(2*np.pi),ellVector * (ellVector+1) * (backgroundRawAverageSpectrum[0,:]-backgroundRawAverageSpectrum[1,:])/(2*np.pi),color='b',alpha=0.4, label="Isotropic background")
        
    p2 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (rawAverageSpectrum[0,:]+rawAverageSpectrum[1,:])/(2*np.pi),ellVector * (ellVector+1) * (rawAverageSpectrum[0,:]-rawAverageSpectrum[1,:])/(2*np.pi),color='r', alpha=0.4)#, label="realisation "+str(runnumbers[i]+1)) 
    p1 = plt.plot(ellVector, ellVector * (ellVector+1) * rawAverageSpectrum[0,:]/(2*np.pi),color='r',  linestyle='-', linewidth=1., label=sigmavString) 

               
    title = 'Angular power spectrum from RAW event skymap for mchi = %s GeV, %s, annihilation channel = %s, tobs= %s h, above threshold energy bin %s, background model = %s, astrophysical anisotropies model = %s and %s realisations.'%(mchi,sigmavString,signalModel,round(tobs/3600,2),thresholdBin,backgroundModel,astroAnisotropiesModel,realisations)
    plt.title('\n'.join(wrap(title,80)))
    plt.subplots_adjust(top=0.85)
    plt.legend(loc='upper left')
    plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1,lmax])
    #pylab.ylim([min(rawAverageSpectrum[0,:])/10.,max(rawAverageSpectrum[0,:])*10.])
    print " minimum value in raw average spectrum:",min(rawAverageSpectrum[0,:])
    print " maximum value in raw average spectrum:",max(rawAverageSpectrum[0,:])
    pylab.ylim([10**(-7),10**(0)])
    #plt.text(5, 0.2,'gray bands: Isotropic background', horizontalalignment='center',verticalalignment='baseline')
     
    plt.show()
    
    print " program anisotropySimulation.evaluatePowerSpectra.py successfully finished."
    print " total computation time:", round((time.time() - start_time)/60,2), "minutes."

##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################