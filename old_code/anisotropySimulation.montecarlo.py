#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
import time
import os
import sys
import getopt
import MoritzIRFfilesTools
import MoritzSphericalTools
import MoritzIACTTools
import MoritzFunctionTools
import pickle


##    main part:   ############################################################
###############################################################################

def main(argv):

 	alphaIntDeg = 0.02
 	TWOsigmaFOVdeg = 3.5
 	psi0deg = 90.0
 	theta0degGal = 0.0
 	energyBin = 12
 	#irffile = "/afs/ifh.de/group/cta/scratch/mhuetten/VERITAS/effArea-v451-auxv01-CARE_June1425-Cut-NTel2-PointSource-Moderate-GEO-V6-ATM21-T1234.root"
 	irffile = "/afs/ifh.de/group/cta/scratch/mhuetten/VERITAS/effArea-d20131031-cut-N3-Point-005CU-Moderate-ATM21-V6-T1234-d20131115.root"
 	#irffile = "/afs/ifh.de/group/cta/scratch/mhuetten/CTA/EffectiveAreasCTA/DESY.d20130830.Erec1.W2.ID0NIM2-TYP0N0-TYP1N0-TYP2N0.prod2-LeoncitoPP-NS.2a.180000s.root"
 	noise = 325
 	elevation = 20 
 	azimutBin = 2
 	tobs = 50
 	sigmav = 3e-26
 	mchi = 500.0
 	signalModel = "tautau"
 	#signalModel = "tautau"
 	Phi0astroflux = 1.53e-6
 	alpha = 0.08
 	inputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/skymap.healpix" 
 	outputdirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/14-09-11-TEST"
 	backgroundModel = "VTSdata"
 	sensitivityFile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v430/VTS.sensitivity.V6.moderate.root"
 	#sensitivityFile = "/afs/ifh.de/group/cta/scratch/mhuetten/CTA/EffectiveAreasCTA/DESY.d20140309.Erec1.R2.ID0NIM2.prod2-LeoncitoPP-NS.S.2a.180000s.root"
 	astroAnisotropiesModel = "isotropic"
 	pointSourceWobbleOffset = 0.5
 	thetaSquared = 0.008
 	IRFspectralindex = 2.4
	MCvsREC = "A_MC"

	###########################################################################
	# read input variables
	try:
		opts, args = getopt.getopt(sys.argv[1:],"ha:f:l:b:e:r:q:n:v:z:t:s:m:y:k:j:x:g:u:w:p:c:i:o:",["alphaIntDeg=","fov=","psi=","theta=","energybin=","irffile=","sensitivityfile=","noise=","elevation=","azimutbin=","tobs=","sigmav=","mchi=","signalmodel=","backgroundmodel=","astroanisotropiesmodel=","phizeroastro=","alpha=","thetasquared=","wobbleoffset=","irfspectrum=","mcvsrec=","filein=","dirout="])
	except getopt.GetoptError:
		# This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
		# The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
		print 'Wrong input, EXIT. The input options are:'
		print ' -a <AlphaIntDeg>'
		print ' -f <TWOsigmaFOVdeg>'
		print ' -l <psi0deg>'
		print ' -b <theta0degGal>'
		print ' -e <Energybin>'
		print ' -r <iRffile>'
		print ' -q <sensitivityfile>'
		print ' -n <Noise>'
		print ' -v <eleVation>'
		print ' -z <aZimutbin>'
		print ' -t <Tobs>'
		print ' -s <Sigmav>'
		print ' -m <Mchi>'
		print ' -y <signalmodel>'
		print ' -k <backgroundmodel>'
		print ' -j <astroanisotropiesmodel>'
		print ' -x <phizeroastro>'
		print ' -g <alpha>'
		print ' -u <thetasquared>'
		print ' -w <wobbleoffset>'
		print ' -p <irfspectrum>'
		print ' -c <mcvsrec>'			
		print ' -i <input file>	 (no standard, ends with .healpix)'
		print ' -o <output dir.>'
		sys.exit(2)
	
	for opt, arg in opts:
		if opt == '-h':
			# help option
			print 'HELP: The input options are:'
			print ' -a <AlphaIntDeg>'
			print ' -f <TWOsigmaFOVdeg>'
			print ' -l <psi0deg>'
			print ' -b <theta0degGal>'
			print ' -e <Energybin>'
			print ' -r <iRffile>'
			print ' -q <sensitivityfile>'
			print ' -n <Noise>'
			print ' -v <eleVation>'
			print ' -z <aZimut>'
			print ' -t <Tobs>'
			print ' -s <Sigmav>'
			print ' -m <Mchi>'
			print ' -y <signalmodel>'
			print ' -k <backgroundmodel>'
			print ' -j <astroanisotropiesmodel>'
			print ' -x <phizeroastro>'
			print ' -g <alpha>'
			print ' -u <thetasquared>'
			print ' -w <wobbleoffset>'
			print ' -p <irfspectrum>'
			print ' -c <mcvsrec>'					
			print ' -i <input file>	 (no standard, ends with .healpix)'
			print ' -o <output dir.>'
			sys.exit()
		elif opt in ("-a", "--alphaIntDeg"):
			alphaIntDeg = float(arg)
		elif opt in ("-f", "--fov"):
			TWOsigmaFOVdeg = float(arg)
		elif opt in ("-l", "--psi"):
			psi0deg = float(arg)
		elif opt in ("-b", "--theta"):
			theta0degGal = float(arg)
		elif opt in ("-e", "--energybin"):
			energyBin = int(arg)
		elif opt in ("-r", "--irffile"):
			irffile = arg
		elif opt in ("-q", "--sensitivityfile"):
			sensitivityFile = arg
		elif opt in ("-n", "--noise"):
			noise = int(arg)
		elif opt in ("-v", "--elevation"):
			elevation = float(arg) 
		elif opt in ("-z", "--azimutbin"):
			azimutBin = int(arg) 
		elif opt in ("-t", "--tobs"):
			tobs = float(arg) 
		elif opt in ("-s", "--sigmav"):
			sigmav = float(arg)
		elif opt in ("-m", "--mchi"):
			mchi = float(arg)
		elif opt in ("-y", "--signalmodel"):
			signalModel = str(arg)
		elif opt in ("-k", "--backgroundmodel"):
			backgroundModel = str(arg)
		elif opt in ("-j", "--astroanisotropiesmodel"):
			astroAnisotropiesModel = str(arg)
		elif opt in ("-x", "--phizeroastro"):
			Phi0astroflux = float(arg)
		elif opt in ("-g", "--alpha"):
			alpha = float(arg)
		elif opt in ("-u", "--thetasquared"):
			thetaSquared = float(arg)
		elif opt in ("-w", "--wobbleoffset"):
			pointSourceWobbleOffset = float(arg)
		elif opt in ("-p", "--irfspectrum"):
			IRFspectralindex = float(arg)
		elif opt in ("-c", "--mcvsrec"):
			MCvsREC = str(arg)
		elif opt in ("-i", "--filein"):
			inputfile = arg 
		elif opt in ("-o", "--dirout"):
			outputdirectory = arg
	
	start_time = time.time()
	print " initializing program anisotropySimulation.montecarlo.py"
	
	###########################################################################
	# first computations from input variables:
	
	# convert alphaInt into radians:
	alphaInt = alphaIntDeg/180.0*np.pi
	
	# determine dimension of healpix map, adapted to resolution of clumpy data: 
	nside = MoritzSphericalTools.alphaInt2nside(alphaInt)
	# give resolution of skymap in rads, determined by grid resolution:
	resolution = hp.nside2resol(nside) * 2.0 / np.sqrt(np.pi)
	resolutionDeg = resolution * 180.0 / np.pi
	
	###########################################################################
	# recalculate input values:
	
	# calculate number of pixels on sphere from NSIDE:
	npix=hp.nside2npix(nside)
	pixelareaHealpix = 4*np.pi/npix
	
	# give celestial position in rads:
	psi0 =  np.pi*psi0deg/180.0 # attention: source of possible error: healpy counts phi clockwise
	theta0deg=90.0-theta0degGal # got to interval theta in [0,180 deg]
	theta0 = np.pi*theta0deg/180.0
	# give field of view in radians:
	sigmaFOV = 0.5*np.pi*TWOsigmaFOVdeg/180 # HALF THE VALUE OF INPUT SIGMA IN DEGREE!!!!
	# give PSF in radians:
	lmax = int(np.pi/hp.nside2resol(nside))
	# give observation time in seconds:
	tobs = tobs * 3600
	
	print " check again input values:"
	print " NSIDE of healpix map:",nside, "pixels"
	print " resolution of healpix map (full pixel diameter for approximately circular pixels):", round(resolutionDeg,3), "degrees"
	print " size of healpix map:",npix, "pixels"
	print " size of healpix map pixel:",pixelareaHealpix, "sr"
	
	print ""
	
	###########################################################################

	DarkMatterBool = MoritzFunctionTools.darkMatterBool(signalModel)

	# skymap.healpix file is restricted to ROI!
	
	if DarkMatterBool == True:
		print " Load skymap.healpix ..."
		inputDataMatrix = np.loadtxt(inputfile)
		# Constrain calculation to ROI on the sphere:
		# the ROIpixels vector simply contains all healpix-map pixel numbers of interest. 	
		npixROI = len(inputDataMatrix[:,0])
		npixROI = int(npixROI)
		ROIpixelsIN = inputDataMatrix[:,0]
		ROIpixels = np.zeros(npixROI)
		for i in range(npixROI):
			ROIpixels[i] = int(float(ROIpixelsIN[i]))
		
			
		# convert into units of cm and kilograms:
		mSunSquaredbykpctothefifth = 1.41431*10**(-47) # kg^2/cm^5
				
		signalSkymap = mSunSquaredbykpctothefifth * inputDataMatrix[:,5]
		
		averageJfactor = 0.0
		# calculate average by running average:
		for i in range(npixROI):
			averageJfactor = averageJfactor + (signalSkymap[i] - averageJfactor)/float(i+1.0)
		spikeJfactor = max(signalSkymap)
		print " average l.o.s. integral value in signal skymap is: <los_tot> =", averageJfactor,"kg^2/cm^5 sr^-1"
		print " maximum l.o.s. integral value in signal skymap is: los_max =", spikeJfactor,"kg^2/cm^5 sr^-1"
		print " "
		
	else:
		if signalModel[:10] == "astroPower":
			powerLawSpectrumIndex = float(signalModel[10:])
			print " Astrophysical point source with power-law energy spectrum for testing modeled."
			print "  point source wobble offset:",pointSourceWobbleOffset
			print "  alpha:",alpha
			print "  theta^2:",thetaSquared
			print " astrophysical spectrum differential flux at 1 GeV is: dPhi/dE =",Phi0astroflux,"cm^-2 s^-1 GeV^-1"
			print "                                      ... at 1 TeV is: dPhi/dE =",Phi0astroflux/1000.**(powerLawSpectrumIndex-1),"cm^-2 s^-1 TeV^-1"
			print " spectral index of astrophysical power-law spectrum is",powerLawSpectrumIndex,"."
			print " "
			
			# generate skymap for astropysical point source:
			psiMIN = psi0 - 2*sigmaFOV
			print " psiMin",psiMIN/np.pi*180.
			psiMAX = psi0 + 2*sigmaFOV
			print " psiMAX",psiMAX/np.pi*180.		
			thetaMIN = theta0 - 2*sigmaFOV
			print " thetaMIN",thetaMIN/np.pi*180.
			thetaMAX = theta0 + 2*sigmaFOV
			print " thetaMAX",thetaMAX/np.pi*180.
			pixelnrMIN = hp.ang2pix(nside,thetaMIN,psiMIN)-2
			print " pixelnrMIN",pixelnrMIN
			pixelnrMAX = hp.ang2pix(nside,thetaMAX,psiMAX)+2
			print " pixelnrMAX",pixelnrMAX
			ROIpixels = []
			print " generate ROI..."
			for i in range(pixelnrMIN,pixelnrMAX):
				[theta, psi] = hp.pix2ang(nside,i)
				if psi >= psiMIN and psi <= psiMAX:
					ROIpixels.append(i)
			ROIpixels = np.asarray(ROIpixels)
			npixROI = len(ROIpixels)
			
			signalSkymap = np.zeros(npixROI)
			# choose coordinates of source for hardcoded wobble offset:
			psiSource = psi0
			thetaSource = theta0 + pointSourceWobbleOffset/180.*np.pi
			print " Source position at Psi =",psiSource/np.pi*180.0,"degrees,"
			print "                  Theta =",thetaSource/np.pi*180.0,"degrees."
			sourcePixelnr = hp.ang2pix(nside,thetaSource,psiSource)
			print " Source pixel nr:", sourcePixelnr
			sourceIndex = np.where(ROIpixels==sourcePixelnr)
			signalSkymap[sourceIndex] = Phi0astroflux/pixelareaHealpix # cm^-2 s^-1 sr^-1 GeV^-1
			print ""
			
		if signalModel == "noDarkMatter":
			print "IGRB and background without Dark Matter modeled."
			print " "
			signalSkymap = np.zeros(npixROI)
			averageJfactor = 0.0
	
	print " number of pixels in ROI:",  npixROI
	print ""
	
	print " energy bin:",energyBin
	print " observation time =",	tobs, "s"
	print " <sigmav> =",sigmav, "cm^3/s"
	print " m_chi =",mchi, "GeV"
	print " signalModel:",signalModel
	print " IRF file:",irffile
	print "  noise level:",noise
	print "  elevation zenit angle:",elevation,"degrees"
	print "  azimut bin:",azimutBin
	print "  energy table:",MCvsREC
	if signalModel[:10] == "astroPower":
		IRFspectralindex = round(powerLawSpectrumIndex,1)
		print "  spectral index for chosen IRF is set to point source spectral index =",IRFspectralindex
	else:
		print "  spectral index for chosen IRF:",IRFspectralindex
	print ""
	
	###########################################################################
	# load instrument response file:
	if "CTA" in irffile:
		CTAbool = True
		print " IRF file detected as CTA IRF file..."
	elif "VTS" or "VERITAS" in irffile:
		CTAbool = False
		print " IRF file detected as VTS IRF file..."
	
	IRFParameterValues, AeffOverEnergyMatrix = MoritzIRFfilesTools.loadEffectiveAreas(irffile,CTAbool)
	print ""
	# extract energy interval:
	IRFenergyBinVector = IRFParameterValues[8]
	totalIRFenergyBins = len(IRFParameterValues[8])
	print " total number of energy bins in IRF file: ", totalIRFenergyBins
	
	if energyBin > totalIRFenergyBins:
		print "EXIT: this energy bin number exceeds the energy bins in the IRF file!"
		sys.exit()
	
	energyBinIndex = energyBin-1
	
	thisEnergy = IRFParameterValues[8][energyBinIndex]
	print " this bin corresponds the mean energy of ", thisEnergy, "GeV."	
	# calculate bin limits:
	logThisEnergy = np.log10(thisEnergy/1000.0)
	
	if energyBinIndex == 0:
		logNeighbourBinEnergy = np.log10(IRFParameterValues[8][energyBinIndex+1]/1000.0)
		notFirstBinBool = False
	else:
		logNeighbourBinEnergy = np.log10(IRFParameterValues[8][energyBinIndex-1]/1000.0)
		notFirstBinBool = True
				
	DeltaLogEnergy = round(np.abs(logThisEnergy-logNeighbourBinEnergy),1)
	print " DeltaLogEnergy = ", DeltaLogEnergy
	
	thisEnergyBinUpperBound = int(1000.0*10**(logThisEnergy+0.5*DeltaLogEnergy))
	if notFirstBinBool == True:
		lastEnergy = IRFParameterValues[8][energyBinIndex-1]
		logLastEnergy = np.log10(lastEnergy/1000.0)
		lastEnergyBinUpperBound = int(1000.0*10**(logLastEnergy+0.5*DeltaLogEnergy))
		thisEnergyBinLowerBound = lastEnergyBinUpperBound
	else:
		thisEnergyBinLowerBound = int(1000.0*10**(logThisEnergy-0.5*DeltaLogEnergy))

	
	DeltaEnergy = thisEnergyBinUpperBound - thisEnergyBinLowerBound
	
	print " Energy bin interval is: [",thisEnergyBinLowerBound,"GeV, ",thisEnergyBinUpperBound,"GeV]."
	print " -> Delta Energy =",DeltaEnergy,"GeV."
	
	# extract used effective area for this bin:
 
	IRFWoffVectorEbin, AeffOverWoffVector, IRFParameterValues, IRFpopt, IRFcov = MoritzIRFfilesTools.makeAeffOverWoff(IRFParameterValues, AeffOverEnergyMatrix, thisEnergy,  elevation, azimutBin, noise, IRFspectralindex, MCvsREC,CTAbool)
	print " Wobble offset values in IRF file:", IRFParameterValues[4]
	print " Wobble offset values with appended last entry:",IRFWoffVectorEbin
	
# 	xs = np.linspace(0, IRFWoffVectorEbin[-1], 200)
# 	ys = np.zeros(len(xs))
# 	for k in range(len(xs)):
# 		ys[k], iteration = MoritzIRFfilesTools.fitWobbleOffset(IRFWoffVectorEbin, AeffOverWoffVector, IRFpopt, xs[k],0,CTAbool)
# 	AeffOverWoff_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_"+str(energyBin)+".p"
# 	AeffOverWoff_smooth_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_smooth_"+str(energyBin)+".p"
# 	pickle.dump( [xs, ys], open( AeffOverWoff_smooth_outputfile, "wb" ) )
# 	pickle.dump( [IRFWoffVectorEbin, AeffOverWoffVector], open( AeffOverWoff_outputfile, "wb" ) )
# 	 
# 	AeffOverEnergyAllWoff = []
# 	for bla in range(len(IRFParameterValues[4])):
# 		AeffOverEnergy = MoritzIRFfilesTools.pickEffectiveArea(IRFParameterValues, AeffOverEnergyMatrix, elevation, azimutBin, IRFParameterValues[4][bla], noise, IRFspectralindex, "MCvsREC")
# 		AeffOverEnergyAllWoff.append(AeffOverEnergy)
# 	AeffOverEnergyAllWoff_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverEnergyAllWoff_"+str(energyBin)+".p"
# 	WoffVector_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/WoffVector_"+str(energyBin)+".p"
# 	pickle.dump( AeffOverEnergyAllWoff, open( AeffOverEnergyAllWoff_outputfile, "wb" ) )
# 	pickle.dump( IRFParameterValues[4], open( WoffVector_outputfile, "wb" ) )
	print ""
	
	###########################################################################
	# load point spread function from sensitivity file:
	angResLogEnergyBinVector, angularResolutionOverEnergyVector = MoritzIRFfilesTools.loadAngularResolution(sensitivityFile)
	angularResolutionOverEnergyVectorIRFbins = MoritzIRFfilesTools.interpolateAngularResolution(angularResolutionOverEnergyVector, angResLogEnergyBinVector, IRFenergyBinVector)
	
	# angularResolution: 68% containment radius
	angularResolution = angularResolutionOverEnergyVectorIRFbins[energyBinIndex]
	# this corresponds to a Gaussian sigma of:
	sigmaPSFdeg = angularResolution/1.515
	sigmaPSF = sigmaPSFdeg/180.0*np.pi
	
	###########################################################################
	# calculate signal spectrum:

	integratedSignalBinFactor = MoritzFunctionTools.darkMatterModelFactor(signalModel, thisEnergy, sigmav, mchi, thisEnergyBinLowerBound, thisEnergyBinUpperBound)
	# at this point, Dark Matter and Astro-source skymaps have the same dimension! 
	PhiSignalSkymap  = integratedSignalBinFactor * signalSkymap # cm^-2 s^-1 sr^-1
	
	# only in Dark Matter case (extended source) give average intensity in this bin:
	if DarkMatterBool == True:
		PhiSignalAverage = integratedSignalBinFactor * averageJfactor
		print " average signal intensity in this bin is: Phi_DM =",PhiSignalAverage,"cm^-2 s^-1 sr^-1"
		
	else:
		# it makes no sense to calculate an average intensity for a point source.
		# instead, give flux from source in units cm^-2 s^-1
		PhiSignalAverage = integratedSignalBinFactor * Phi0astroflux
		print " point source flux integrated over this bin is: Phi_source =",PhiSignalAverage,"cm^-2 s^-1"
	
	
	###########################################################################
	# calculate background spectrum:
	
	# always calculate electron spectrum:
	dPhidEelectrons = MoritzFunctionTools.electronSpectrum(thisEnergy)
	PhiElectrons = dPhidEelectrons * DeltaEnergy
	
	if backgroundModel == "electrons":
		print " background events are only isotropically distributed cosmic electrons according to Egberts (2011)."
		PhiBackground = PhiElectrons
	elif backgroundModel == "VTSdata":
		print " background rates are taken from sensitivity file:",sensitivityFile
		PhiBackground = float('NaN')
		backgroundRatesMatrix, backgroundParameterValues = MoritzIRFfilesTools.loadBackgroundRates(sensitivityFile)
		backgroundRatesMatrixIRFbins = MoritzIRFfilesTools.interpolateBackgroundRates(backgroundRatesMatrix, backgroundParameterValues, IRFenergyBinVector)
    	# change units from s-1 degs^-2 (the format within the root file) to s-1 sr^-1:
		backgroundRatesMatrixIRFbins = (180.0/np.pi)**2 * backgroundRatesMatrixIRFbins
		backgroundWoffVectorEbin =  backgroundParameterValues[1]
		backgroundRateOverWoffVector = backgroundRatesMatrixIRFbins[energyBinIndex,:]
		backgroundWoffVectorEbin, backgroundRateOverWoffVector, backgroundpopt, backgroundpcov = MoritzIRFfilesTools.prepareBackgroundRateOverWoffForFitting(backgroundWoffVectorEbin, backgroundRateOverWoffVector)	
	 
	elif backgroundModel == "none":
		print " background has been to be neglected. No background events will be generated."
		PhiBackground = 0.0
		PhiElectrons = 0.0
	 
	else:
		print "EXIT: wrong name for background model given! (model '",signalModel,"' doesn't yet exist.)"
		sys.exit()
	
	if backgroundModel != "none":
		print " Calculate elecron contribution to background:"
		print " electron differential intensity at",thisEnergy,"GeV is: dPhi_el/dE =",dPhidEelectrons,"cm^-2 s^-1 sr^-1 GeV^-1."
		print " integrated electron intensity in this bin is: Phi_el =",PhiElectrons,"cm^-2 s^-1 sr^-1."
	print ""

	###########################################################################
	# calculate IGRB intensities:
	
	if astroAnisotropiesModel == "none":
		print " IGRB has been chosen to be neglected. No IGRB events (despite Dark Matter) will be generated."
	else:  
		# firstly, calculate total IGRB intensity:
		print " calculate total IGRB intensity according to Abdo (2010)..."
		E0 = 0.1 # GeV
		IGRBspectralIndex = 2.41
		x = thisEnergy/E0
		dNdEIGRB = 1/E0 * MoritzFunctionTools.dNdxPowerLaw(x,IGRBspectralIndex)
		PhiOelectrons = 1.45 * 10**(-5) # "cm^-2 s^-1 sr^-1 (0.1 GeV)^-1
		dPhideEIGRB = PhiOelectrons * dNdEIGRB
		print " IGRB differential intensity at",thisEnergy,"GeV is: dPhi_IGRB/dE =",dPhideEIGRB,"cm^-2 s^-1 sr^-1 GeV^-1."
		PhiIGRBAverage = dPhideEIGRB * DeltaEnergy
		
		if signalModel[:10] == "astroPower":
			PhiIGRBastroAverage = PhiIGRBAverage
		else:		
			PhiIGRBastroAverage = PhiIGRBAverage - PhiSignalAverage
		DMexceedsIGRBbool = 0
		if PhiIGRBastroAverage < 0.0:
			PhiIGRBAverage = PhiSignalAverage
			PhiIGRBastroAverage = 0.0
			DMexceedsIGRBbool = 1
			print " Dark Matter intensity in this energy bin exceeds Fermi IGRB extrapolation!"
			print " total average IGRB intensity is set to Dark Matter intensity."
		print " integrated total average IGRB intensity at this energy bin is: Phi_IGRB =",PhiIGRBAverage,"cm^-2 s^-1 sr^-1."
		
		if astroAnisotropiesModel == "isotropic":
			PhiIGRBastroSkymap = np.zeros(npixROI) + PhiIGRBastroAverage
		else:
			print "EXIT: wrong name for IGRB model given! (model '",astroAnisotropiesModel,"' doesn't yet exist.)"
			sys.exit()
	
	print ""
	
	###########################################################################
	# save all intensities to file for later combining to integrated intensities in
	# module 3 (anisotropySimulation.combineBins.py):
	
	filename = "averageFluxes_bin"+str(energyBin)
	outputpath_averageFluxes = str(outputdirectory)+"/%s.txt" % filename
	print " write average intensities to file:", outputpath_averageFluxes
	
	f = open(outputpath_averageFluxes, 'w')
	f.write("# energy bin interval: E_mean = ")
	f.write(str(thisEnergy))
	f.write(" GeV, interval: [ ")
	f.write(str(thisEnergyBinLowerBound))
	f.write(" GeV, ")
	f.write(str(thisEnergyBinUpperBound))
	f.write(" GeV].")
	f.write("\n")
	if signalModel[:10] == "astroPower":
		f.write("# Source flux in units of cm^-2 s^-1, all other average intensities in units of cm^-2 s^-1 sr^-1.")
		f.write("\n")
		f.write("# Source flux | average total IGRB intensity | average astrophysical IGRB intensity only | average background intensity | electron intensity | expected electron events | counted electron events | ")
	else:
		f.write("# All intensities in units of cm^-2 s^-1 sr^-1.")
		f.write("\n")
		f.write("# average signal intensity | average total IGRB intensity | average astrophysical IGRB intensity only | average background intensity | electron intensity | expected electron events | counted electron events | ")
	f.write("\n")
	f.write(str(PhiSignalAverage))
	f.write(" ")
	f.write(str(PhiIGRBAverage))
	f.write(" ")
	f.write(str(PhiIGRBastroAverage))
	f.write(" ")
	f.write(str(PhiBackground))
	f.write(" ")
	f.write(str(PhiElectrons))
	
	###########################################################################
	# now compute instrument response: fill eventmap
	print " fill eventmap with events..."
	# simulated event skymap containing signal events 
	# without Poissonian variance and point spreading:
	eventmap = np.zeros((npixROI,7)) #- 1.6375*10**(30)
	
	totalExpectedSignalEvents = 0.0
	totalPoissonSignalEvents = 0
	totalExpectedBackgroundEvents = 0.0
	totalPoissonBackgroundEvents = 0
	totalExpectedIGRBastroEvents = 0.0
	totalPoissonIGRBastroEvents = 0
	totalExpectedElectronEvents = 0.0
	totalPoissonElectronEvents = 0
		
	totalPoissonAllEventsCheck = 0
	totalPoissonMissedEventsCheck = 0
	
	OnePointBool = False
	TwoPointsBool = "Only one signal pixel."
	
	for ROIpixelnr in range(npixROI):
		eventmap[ROIpixelnr,0] = ROIpixels[ROIpixelnr]
		angles = hp.pix2ang(nside, int(ROIpixels[ROIpixelnr]))
		thetaPixel  = angles[0]
		psiPixel = angles[1]
		#################################################
		PhiSignal = PhiSignalSkymap[ROIpixelnr] # cm^-2 s^-1 sr^-1
		PhiIGRBastro = PhiIGRBastroSkymap[ROIpixelnr] # cm^-2 s^-1 sr^-1
		# PhiBackground is assumed as isotropic...
		#################################################
		Woff = 180.0/np.pi * MoritzSphericalTools.angularDistance(theta0, psi0, thetaPixel, psiPixel)
		Aeff, IRFiteration = MoritzIRFfilesTools.fitWobbleOffset(IRFWoffVectorEbin, AeffOverWoffVector, IRFpopt, Woff, 0, CTAbool)
		if IRFiteration > 0:
			print " ...iterated effective area fitting",IRFiteration,"times for Wobble offset",Woff,"degrees."
		Aeff = 10**4 * Aeff # m^2 -> cm^2
		if signalModel[:10] == "astroPower":
			if  PhiSignal != 0.0:
				if OnePointBool == True:
					TwoPointsBool= "Attention! More than one signal pixel."
				OnePointBool = True
				AeffPointSource = Aeff
				offsetPointSource = Woff
			
		#################################################
		expectedSignalEvents = PhiSignal * Aeff * tobs * pixelareaHealpix
		expectedIGRBastroEvents = PhiIGRBastro * Aeff * tobs * pixelareaHealpix
		if backgroundModel == "VTSdata":
			backgroundRate, backgroundRateIteration = MoritzIRFfilesTools.fitWobbleOffset(backgroundWoffVectorEbin, backgroundRateOverWoffVector, backgroundpopt, Woff, 0, CTAbool)
			if backgroundRateIteration > 0:
				print " ...iterated background rate fitting",backgroundRateIteration,"times for Wobble offset",Woff,"degrees."
			expectedBackgroundEvents = backgroundRate * tobs * pixelareaHealpix
		else:
			expectedBackgroundEvents = PhiBackground * Aeff * tobs * pixelareaHealpix
		
		expectedElectronEvents = PhiElectrons * Aeff * tobs * pixelareaHealpix
		#################################################
		eventmap[ROIpixelnr,1] = expectedSignalEvents
		eventmap[ROIpixelnr,3] = expectedIGRBastroEvents
		eventmap[ROIpixelnr,5] = expectedBackgroundEvents
		
		# count expected events in bin: 
		totalExpectedSignalEvents = totalExpectedSignalEvents + expectedSignalEvents
		totalExpectedIGRBastroEvents = totalExpectedIGRBastroEvents + expectedIGRBastroEvents
		totalExpectedBackgroundEvents = totalExpectedBackgroundEvents + expectedBackgroundEvents
		totalExpectedElectronEvents = totalExpectedElectronEvents + expectedElectronEvents
		# eventmap with Poissonian variance (integer signal events) and point spreading:		
		poissonSignalEvents = int(np.random.poisson(expectedSignalEvents))
		poissonIGRBastroEvents = int(np.random.poisson(expectedIGRBastroEvents))
		poissonBackgroundEvents = int(np.random.poisson(expectedBackgroundEvents))
		
		if backgroundModel != "electrons":
			poissonElectronEvents = int(np.random.poisson(expectedElectronEvents))
		else:
			poissonElectronEvents = poissonBackgroundEvents
		
		# count 'measured' events in bin:		 
		totalPoissonSignalEvents = totalPoissonSignalEvents + poissonSignalEvents
		totalPoissonIGRBastroEvents = totalPoissonIGRBastroEvents + poissonIGRBastroEvents
		totalPoissonBackgroundEvents = totalPoissonBackgroundEvents + poissonBackgroundEvents
		totalPoissonElectronEvents = totalPoissonElectronEvents + poissonElectronEvents

		# displacement of events according to point spread function:
		for eventnumber in range(poissonSignalEvents):
			thetaDisp, psiDisp = MoritzSphericalTools.pointSpreadDisplacement(thetaPixel, psiPixel, sigmaPSF)
			randomPixelnr = hp.ang2pix(nside, thetaDisp, psiDisp, nest=False)
	  		if randomPixelnr in ROIpixels:  
	 			 randomPixelROIindex = np.where(ROIpixels==randomPixelnr)
	 			 eventmap[randomPixelROIindex,2] = eventmap[randomPixelROIindex,2] + 1
	 			 totalPoissonAllEventsCheck = totalPoissonAllEventsCheck + 1
	 		else:
	 			print "WARNING: event at pixel that is not contained in ROI."
	 			totalPoissonMissedEventsCheck = totalPoissonMissedEventsCheck + 1

	 	for eventnumber in range(poissonIGRBastroEvents):
			thetaDisp, psiDisp = MoritzSphericalTools.pointSpreadDisplacement(thetaPixel, psiPixel, sigmaPSF)
			randomPixelnr = hp.ang2pix(nside, thetaDisp, psiDisp, nest=False)
	  		if randomPixelnr in ROIpixels:  
	 			 randomPixelROIindex = np.where(ROIpixels==randomPixelnr)
	 			 eventmap[randomPixelROIindex,4] = eventmap[randomPixelROIindex,4] + 1
	 			 totalPoissonAllEventsCheck = totalPoissonAllEventsCheck + 1
	 		else:
	 			print "WARNING: event at pixel that is not contained in ROI."
	 			totalPoissonMissedEventsCheck = totalPoissonMissedEventsCheck + 1

	 	for eventnumber in range(poissonBackgroundEvents):
			thetaDisp, psiDisp = MoritzSphericalTools.pointSpreadDisplacement(thetaPixel, psiPixel, sigmaPSF)
			randomPixelnr = hp.ang2pix(nside, thetaDisp, psiDisp, nest=False)
	  		if randomPixelnr in ROIpixels:  
	 			 randomPixelROIindex = np.where(ROIpixels==randomPixelnr)
	 			 eventmap[randomPixelROIindex,6] = eventmap[randomPixelROIindex,6] + 1
	 			 totalPoissonAllEventsCheck = totalPoissonAllEventsCheck + 1
	 		else:
	 			print "WARNING: event at pixel that is not contained in ROI."
	 			totalPoissonMissedEventsCheck = totalPoissonMissedEventsCheck + 1
	 			
	f.write(" ")
	f.write(str(totalExpectedElectronEvents))
	f.write(" ")
	f.write(str(totalPoissonElectronEvents))
	f.close()
	
	print ""
	
	###########################################################################
	# now check event counts in bin:
 	print " check event counts..."
 	
	totalExpectedSignalEventsCheck = sum(eventmap[:,1])
	totalExpectedIGRBastroEventsCheck = sum(eventmap[:,3])
	totalExpectedBackgroundEventsCheck = sum(eventmap[:,5])
	totalPoissonSignalEventsCheck = sum(eventmap[:,2].astype(int))
	totalPoissonIGRBastroEventsCheck = sum(eventmap[:,4].astype(int))
	totalPoissonBackgroundEventsCheck = sum(eventmap[:,6].astype(int))
	
	ONevents = 0
	ONeventsBG = 0.0
	ONeventsSignal = 0.0
	if signalModel[:10] == "astroPower":
		thetaONregion = np.sqrt(thetaSquared)*np.pi/180.0
		for i in range(len(eventmap)):
			theta1, psi1 = hp.pix2ang(nside, int(eventmap[i,0]))
			if MoritzSphericalTools.angularDistance(thetaSource, psiSource, theta1, psi1) < thetaONregion:
				ONevents = ONevents + eventmap[i,2] + eventmap[i,4] + eventmap[i,6]
				ONeventsBG = ONeventsBG + eventmap[i,3] + eventmap[i,5]
				ONeventsSignal = ONeventsSignal + eventmap[i,2]
				
	print "  check expected total number of signal events in this bin:",totalExpectedSignalEventsCheck
	print "  check expected total number of IGRB events in this bin:",totalExpectedIGRBastroEventsCheck
	print "  check expected total number of background events in this bin:",totalExpectedBackgroundEventsCheck
	print ""	
	print "  check 'measured' total number of signal events in this bin:",totalPoissonSignalEventsCheck
	print "  check 'measured' total number of IGRB events in this bin:",totalPoissonIGRBastroEventsCheck
	print "  check 'measured' total number of background events in this bin:",totalPoissonBackgroundEventsCheck
	print ""

	###########################################################################
	# write output file:
	filename = "eventmap_bin"+str(energyBin)
	outputpath_eventmap_healpix = str(outputdirectory)+"/%s.healpix" % filename
	print " write event maps to file:", outputpath_eventmap_healpix
	
	f = open(outputpath_eventmap_healpix, 'w')
	f.write("# number of pixels in healpix map: ")
	f.write(str(npix))
	f.write("\n")   
	f.write("# healpix map pixel # | expected # signal events | Poisson & PSF displaced # signal events | expected # astrophys. IGRB events | Poisson & PSF displaced # astrophys. IGRB events | expected # bg events | Poisson & PSF displaced # bg events")
	f.write("\n")
	for ROIpixelnr in range(npixROI):
		f.write(str(int(eventmap[ROIpixelnr,0])))
		f.write(" ")
		f.write(str(eventmap[ROIpixelnr,1]))
		f.write(" ")
		f.write(str(int(eventmap[ROIpixelnr,2])))
		f.write(" ")
		f.write(str(eventmap[ROIpixelnr,3]))
		f.write(" ")
		f.write(str(int(eventmap[ROIpixelnr,4])))
		f.write(" ")
		f.write(str(eventmap[ROIpixelnr,5]))
		f.write(" ")
		f.write(str(int(eventmap[ROIpixelnr,6])))
		f.write("\n")
	f.close()
	#outputpath_eventmap_fits = str(outputdirectory)+"/%s.fits" % filename 
	#hp.write_map(outputpath_eventmap_fits, eventmap)
	print ""	

	###########################################################################
	# give console output:
		
	print " *************************************************************************"
	print ""
	print "  expected total number of signal events in this bin:",round(totalExpectedSignalEvents,4),"+/-",round(np.sqrt(totalExpectedSignalEvents),4)
	print "  expected total number of astrophys. IGRB events in this bin:",round(totalExpectedIGRBastroEvents,4),"+/-",round(np.sqrt(totalExpectedIGRBastroEvents),4)
	print "  expected total number of background events in this bin:",round(totalExpectedBackgroundEvents,4),"+/-",round(np.sqrt(totalExpectedBackgroundEvents),4)
	print "                                   ... thereof electrons:",round(totalExpectedElectronEvents,4),"+/-",round(np.sqrt(totalExpectedElectronEvents),4)
	print ""	
	print "  'measured' total number of signal events in this bin:",totalPoissonSignalEvents
	print "  'measured' total number of astrophys. IGRB events in this bin:",totalPoissonIGRBastroEvents
	print "  'measured' total number of background events in this bin:",totalPoissonBackgroundEvents
	print "                                     ... thereof electrons:",totalPoissonElectronEvents
	print ""
	print "  check 'measured' total number of all events in this bin:",totalPoissonAllEventsCheck
	print "  check missed 'measured' total number of all events in this bin:",totalPoissonMissedEventsCheck
	if signalModel[:10] == "astroPower":
		print ""
		print " differential flux at bin center: dPhi/dE(",thisEnergy,"GeV ) =",integratedSignalBinFactor/DeltaEnergy*Phi0astroflux,"cm^-2 s^-1 GeV^-1."
		print " integrated flux in this bin: Phi =",integratedSignalBinFactor * Phi0astroflux,"cm^-2 s^-1."
		print " effective area at point source:",AeffPointSource,"cm^2.",TwoPointsBool
		print " check wobble offset of point source:",offsetPointSource,"degrees."
		print " angular resolution 68% containment radius at this bin r_68% =:",angularResolution,"degrees."
		print ""
		print "  size of ON region:",thetaSquared,"degree^2." 
		print "  alpha for size of OFF-region = ON-region/alpha:",alpha
		print "  expected total number of events in ON region:",ONevents
		print "  expected number of background events in ON region:",ONeventsBG
		print "  missed signal events in ON region:",totalPoissonSignalEvents - ONeventsSignal
		print "  significance for refl. region background, ON region = alpha * OFF region:",MoritzIACTTools.liAndMaSignificance(ONevents, ONeventsBG/alpha, alpha),"sigma"
	print "  one sigma of 2D Gaussian point spread function in this bin is: sigmaPSFdeg =",sigmaPSFdeg,"degs."
	print ""
	print " *************************************************************************"
	print ""
	print " program anisotropySimulation.montecarlo.py successfully finished."
	print " total computation time:", round((time.time() - start_time)/60,2), "minutes."

##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################
