#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
import time
import os
import os.path
import sys
import getopt
import MoritzSphericalTools
from matplotlib import pyplot as plt
import healpy as hp

##    main part:   ############################################################
###############################################################################

def main(argv):

#     alphaIntDeg = 0.02
#     TWOsigmaFOVdeg = 3.5
#     psi0deg = 90.0
#     theta0degGal = 0.0
#     totalBins = 30
#     irffile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v430/EffectiveAreas/effArea-d20131031-cut-N3-Point-005CU-Moderate-ATM21-V6-T1234-d20131115.root"
#     sensitivityFile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v430/VTS.sensitivity.V6.moderate.root"
#     noise = 325
#     elevation = 20 
#     azimutBin = 2
#     tobs = 100
#     sigmav = 3e-26
#     mchi = 500.0
#     signalModel = "astroPower2.62"
#     backgroundModel = "VTSdata"
#     inputdirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/14-09-11-CrabTutorialVERITAS-EFFv430V6-A_MC" 
#     outputdirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/14-07-11-CrabTestBackgroundDataDynPSF2"
#     thresholdBin = 7
#     Phi0astroflux = 2.05e-6
#     MCvsREC = "A_MC"
#     pointSourceWobbleOffset = 0.5
#     thetaSquared = 0.008
#     showBinMapsBool = True

    ###########################################################################
    # read input variables
    showDMskymapBool = 0
    showBinMapsBool = 0
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"ha:f:l:b:c:d:r:q:n:v:z:t:s:m:y:k:j:x:g:u:w:p:e:i:o:",["alphaIntDeg=","fov=","psi=","theta=","totalbins=","thresholdbin=","irffile=","sensitivityfile=","noise=","elevation=","azimutbin=","tobs=","sigmav=","mchi=","signalmodel=","backgroundmodel=","astroanisotropiesmodel=","phizeroastro=","alpha=","thetasquared=","wobbleoffset=","plotboolskymap=","plotboolbins=","dirin=","column="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'Wrong input, EXIT. The input options are:'
        print ' -a <AlphaIntDeg>'
        print ' -f <TWOsigmaFOVdeg>'
        print ' -l <psi0deg>'
        print ' -b <theta0degGal>'
        print ' -c <totalbins>'
        print ' -d <thresholdbin>'
        print ' -r <iRffile>'
        print ' -q <sensitivityfile>'
        print ' -n <Noise>'
        print ' -v <eleVation>'
        print ' -z <aZimutbin>'
        print ' -t <Tobs>'
        print ' -s <Sigmav>'
        print ' -m <Mchi>'
        print ' -y <signalmodel>'
        print ' -k <backgroundmodel>'
        print ' -j <astroanisotropiesmodel>'
        print ' -x <phizeroastro>'
        print ' -g <alpha>'
        print ' -u <thetasquared>'
        print ' -w <wobbleoffset>'
        print ' -p <plotboolskymap>'
        print ' -e <plotboolbins>'   
        print ' -i <input directory>'
        print ' -o <output directory>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'HELP: The input options are:'
            print ' -a <AlphaIntDeg>'
            print ' -f <TWOsigmaFOVdeg>'
            print ' -l <psi0deg>'
            print ' -b <theta0degGal>'
            print ' -c <totalbins>'
            print ' -d <thresholdbin>'
            print ' -r <iRffile>'
            print ' -q <sensitivityfile>'
            print ' -n <Noise>'
            print ' -v <eleVation>'
            print ' -z <aZimut>'
            print ' -t <Tobs>'
            print ' -s <Sigmav>'
            print ' -m <Mchi>'
            print ' -y <signalmodel>'
            print ' -k <backgroundmodel>'
            print ' -j <astroanisotropiesmodel>'
            print ' -x <phizeroastro>'
            print ' -g <alpha>'
            print ' -u <thetasquared>'
            print ' -w <wobbleoffset>'
            print ' -p <plotboolskymap>'
            print ' -e <plotboolbins>'             
            print ' -i <input directory>'
            print ' -o <column>'
            sys.exit()
        elif opt in ("-a", "--alphaIntDeg"):
            alphaIntDeg = float(arg)
        elif opt in ("-f", "--fov"):
            TWOsigmaFOVdeg = float(arg)
        elif opt in ("-l", "--psi"):
            psi0deg = float(arg)
        elif opt in ("-b", "--theta"):
            theta0degGal = float(arg)
        elif opt in ("-c", "--totalbins"):
            totalBins = int(arg)
        elif opt in ("-d", "--thresholdbin"):
            thresholdBin = int(arg)
        elif opt in ("-r", "--irffile"):
            irffile = arg
        elif opt in ("-q", "--sensitivityfile"):
            sensitivityFile = arg
        elif opt in ("-n", "--noise"):
            noise = int(arg)
        elif opt in ("-v", "--elevation"):
            elevation = float(arg) 
        elif opt in ("-z", "--azimutbin"):
            azimutBin = int(arg) 
        elif opt in ("-t", "--tobs"):
            tobs = float(arg) 
        elif opt in ("-s", "--sigmav"):
            sigmav = float(arg)
        elif opt in ("-m", "--mchi"):
            mchi = float(arg)
        elif opt in ("-y", "--signalmodel"):
            signalModel = str(arg)
        elif opt in ("-k", "--backgroundmodel"):
            backgroundModel = str(arg)
        elif opt in ("-j", "--astroanisotropiesmodel"):
            astroAnisotropiesModel = str(arg)
        elif opt in ("-x", "--phizeroastro"):
            Phi0astroflux = float(arg)
        elif opt in ("-g", "--alpha"):
            alpha = float(arg)
        elif opt in ("-u", "--thetasquared"):
            thetaSquared = float(arg)
        elif opt in ("-w", "--wobbleoffset"):
            pointSourceWobbleOffset = float(arg)
        elif opt in ("-p", "--plotboolskymap"):
            showDMskymapBool = float(arg)
        elif opt in ("-e", "--plotboolbins"):
            showBinMapsBool = float(arg)
        elif opt in ("-i", "--dirin"):
            inputdirectory = arg 
        elif opt in ("-o", "--column"):
            column = int(arg)
    
    start_time = time.time()
    print " initializing program anisotropySimulation.quickPlotEventmap.py"
    
    #showDMskymapBool = 0
    
    ###########################################################################
    ## first computations from input variables:
    
    # convert alphaInt into radians:
    alphaInt = alphaIntDeg/180.0*np.pi
        
    # determine dimension of healpix map, adapted to resolution of clumpy data: 
    nside = MoritzSphericalTools.alphaInt2nside(alphaInt)
    # give resolution of skymap in rads, determined by grid resolution:
    resolution = hp.nside2resol(nside) * 2.0 / np.sqrt(np.pi)
    resolutionDeg = resolution * 180.0 / np.pi
    
    
    ###########################################################################
    # recalculate input values:
    
    # calculate number of pixels on sphere from NSIDE:
    npix=hp.nside2npix(nside)
    pixelareaHealpix = 4*np.pi/npix
    
    # give celestial position in rads:
    psi0 =  np.pi*psi0deg/180.0 # attention: source of possible error: healpy counts phi clockwise
    theta0deg=90.0-theta0degGal # got to interval theta in [0,180 deg]
    theta0 = np.pi*theta0deg/180.0
    # determine maximum calculable multipole element lmax:
    lmax = int(np.pi/hp.nside2resol(nside))
    # give observation time in seconds:
    tobs = tobs * 3600
     
    print " check again input values:"
    print " NSIDE of healpix map:",nside, "pixels"
    print " resolution of healpix map (full pixel diameter for approximately circular pixels):", round(resolutionDeg,3), "degrees"
    print " size of healpix map:",npix, "pixels"
    print " size of healpix map pixel:",pixelareaHealpix, "sr"

    if showDMskymapBool == 0:
        
        ####################
        # column to print:
        print " show column",column
        ####################
        
        if column == 1:
            TITLE1 = 'expected signal events'
        elif column == 2:
            TITLE1 = 'Poisson & PSF displaced signal events'
        elif column == 3:
            TITLE1 = 'expected astrophys. IGRB events/pixel'
        elif column == 4:    
            TITLE1 = 'Poisson & PSF displaced astrophys. IGRB events'
        elif column == 5:    
            TITLE1 = 'expected background events/pixel'
        elif column == 6:    
            TITLE1 = 'Poisson & PSF displaced background events'
        elif column == 7 and showBinMapsBool == 0:
            TITLE1 = 'expected all events'     
        elif column == 8 and showBinMapsBool == 0:
            TITLE1 = 'Poisson & PSF displaced all events'
        else:
            print " ERROR. Column number",column,"does not exist."
            sys.exit()    
        
        ####################    
        
        if signalModel[:10] == "astroPower":
            # choose coordinates of source for this wobble offset:
            psiSource = psi0
            thetaSource = theta0 + pointSourceWobbleOffset/180.*np.pi
        if signalModel == "noDarkMatter":
            print "IGRB and background without Dark Matter modeled."
        print " "
    
#         energyBinInfoTableFile = inputdirectory+"/binInformation"+".txt"   
#         print " read energy bin informations from file:",energyBinInfoTableFile
#         energyBinInfoTable = np.loadtxt(energyBinInfoTableFile)
#         
#         firstBin = int(energyBinInfoTable[0,0])
#         numberOfBins = len(energyBinInfoTable[:,0])
#         if numberOfBins + firstBin - 1 != totalBins:
#             print " ERROR: Some mismatch of bins in binInfo file with number of total bins",totalBins,"in input parameter file."
        
        ###########################################################################
        # plot eventmaps for each energy bin:
        
        figureindex = 1
        
        if showBinMapsBool == 1:
        
            for energyBin in range(firstBin,firstBin+15): #totalBins+1
                outputFileName = "eventmap_bin"+str(energyBin)+".healpix"
                TITLE = TITLE1 + "\n for energy interval ["+str(int(energyBinInfoTable[energyBin-firstBin,2]))+" GeV, "+str(int(energyBinInfoTable[energyBin-firstBin,3]))+" GeV]. sigma_v = "+str(sigmav)+"cm^3/s."
                inputpath_eventmap = str(inputdirectory)+"/"+outputFileName
                print "load eventmap data of energy bin",energyBin
                inputDataMatrix = np.loadtxt(inputpath_eventmap)
                npixROI = len(inputDataMatrix[:,0])
                eventmap = np.zeros(npix) - 1.6375*10**(30)
                for i in range(npixROI):
                    eventmap[int(inputDataMatrix[i,0])] = inputDataMatrix[i,column]
                # plot:
                plot_skymap = plt.figure(figsize=(9,11))
                hp.cartview(eventmap, fig=figureindex,rot=[90,0], flip='geo',xsize=10000, lonra= [-TWOsigmaFOVdeg,TWOsigmaFOVdeg], latra= [-TWOsigmaFOVdeg,+TWOsigmaFOVdeg],  title=TITLE,unit='events/pixel')
                figureindex = figureindex + 1        
                grat = TWOsigmaFOVdeg/5.0
                hp.graticule(dpar=grat, dmer=grat/2)  
                angularResolution = energyBinInfoTable[energyBin-firstBin,4]/180*np.pi
                # get circle of thetaSquared cut and 68% containment radius:
                if signalModel[:10] == "astroPower":
                    thetaCut = np.sqrt(thetaSquared)/180*np.pi
                    thetaSquaredCircleTheta = np.zeros(41)
                    thetaSquaredCirclePsi = np.zeros(41)
                    sixtyeightContainmentCircleTheta = np.zeros(41)
                    sixtyeightContainmentCirclePsi = np.zeros(41)
                    for i in range(41):
                        phi = np.pi/20 * i
                        thetaSquaredCircleTheta[i], thetaSquaredCirclePsi[i] = MoritzSphericalTools.polarCoordinatesOnSphere(thetaSource, psiSource, thetaCut, phi)
                        sixtyeightContainmentCircleTheta[i], sixtyeightContainmentCirclePsi[i] = MoritzSphericalTools.polarCoordinatesOnSphere(thetaSource, psiSource, angularResolution, phi)
                    hp.projplot(thetaSquaredCirclePsi/np.pi*180, 90-thetaSquaredCircleTheta/np.pi*180,'k-', lonlat=True, coord='G',linewidth=1.5)
                    hp.projplot(sixtyeightContainmentCirclePsi/np.pi*180, 90-sixtyeightContainmentCircleTheta/np.pi*180,'r-', lonlat=True, coord='G',linewidth=1)

        ###########################################################################
        # plot combined eventmap above threshold:
      
        lowerThresholdBound = 100 #int(energyBinInfoTable[thresholdBin-firstBin,2])
        outputFileName = "eventmap_combined_above"+str(lowerThresholdBound)+"GeV.healpix"
        TITLE = TITLE1 + "\n combined map above "+str(lowerThresholdBound)+"GeV. sigma_v = "+str(sigmav)+"cm^3/s."
        inputpath_eventmap = str(inputdirectory)+"/"+outputFileName
        
        print "load eventmap data of combined map"
        inputDataMatrix = np.loadtxt(inputpath_eventmap)
        npixROI = len(inputDataMatrix[:,0]) 
        eventmap = np.zeros(npix) - 1.6375*10**(30)
        for i in range(npixROI):
            eventmap[int(inputDataMatrix[i,0])] = inputDataMatrix[i,column]
         # plot:
        plot_skymap = plt.figure(figsize=(9,11))
        hp.cartview(eventmap, fig=figureindex,rot=[90,0], flip='geo',xsize=10000, lonra= [-TWOsigmaFOVdeg,TWOsigmaFOVdeg], latra= [-TWOsigmaFOVdeg,+TWOsigmaFOVdeg],  title=TITLE,unit='events/pixel')
        grat = TWOsigmaFOVdeg/5.0
        hp.graticule(dpar=grat, dmer=grat/2)
        # get circle of thetaSquared cut:
        if signalModel[:10] == "astroPower":
            thetaCut = np.sqrt(thetaSquared)/180*np.pi
            thetaSquaredCircleTheta = np.zeros(41)
            thetaSquaredCirclePsi = np.zeros(41)
            for i in range(41):
                phi = np.pi/20 * i
                thetaSquaredCircleTheta[i], thetaSquaredCirclePsi[i] = MoritzSphericalTools.polarCoordinatesOnSphere(thetaSource, psiSource, thetaCut, phi)
            hp.projplot(thetaSquaredCirclePsi/np.pi*180, 90-thetaSquaredCircleTheta/np.pi*180,'k-', lonlat=True, coord='G',linewidth=1.5)
    
        # plot 2D -profile of combined map:
        plot_2d = plt.figure(figsize=(9,11))
        nside = hp.npix2nside(npix)
        resolution = hp.nside2resol(nside)*2.0/np.sqrt(np.pi)
        resolutionDeg = resolution*180.0/np.pi
        gridWidthDeg = 2*TWOsigmaFOVdeg
        print ''
        twoDplotPixelnr = int(gridWidthDeg/resolutionDeg/2)+1 # always an odd number
        print ' number of pixels in 2D plot:', twoDplotPixelnr
        deltaPsiDeg = gridWidthDeg/(twoDplotPixelnr-1)/2 # in deg
        print ' deltaPsi in 2D plot:', round(deltaPsiDeg,3), "degs."
        psiDeg2D = np.zeros(twoDplotPixelnr)
        psi2D = np.zeros(twoDplotPixelnr)
        theta2D = np.zeros(twoDplotPixelnr)
        thetaDeg2D = np.zeros(twoDplotPixelnr)
        pixelnr = np.zeros(twoDplotPixelnr)
        profile = np.zeros(twoDplotPixelnr)
        foldedProfile = np.zeros(twoDplotPixelnr)
         
        for i in range(twoDplotPixelnr):
            psiDeg2D[i] =  i * deltaPsiDeg + psi0deg
            thetaDeg2D[i] =  theta0degGal - i * deltaPsiDeg 
            psi2D[i] =  i * deltaPsiDeg/180*np.pi + psi0
            theta2D[i] =  i * deltaPsiDeg/180*np.pi + theta0
            #pixelnr[i] = hp.ang2pix(nside, theta0, psi2D[i],nest=False)
            pixelnr[i] = hp.ang2pix(nside, theta2D[i], psi0,nest=False)
            profile[i] = eventmap[pixelnr[i]]
        plt.title(TITLE+" profile")
        plt.plot(thetaDeg2D,profile,'b',linewidth=1.5, label="original profile")    
         
    if showDMskymapBool == 1:

        ####################
        # column to print:
        print " show column",column
        ####################
        
        if column == 1:
            TITLE2 = 'Jsmooth'
        elif column == 2:
            TITLE2 = '<Jsub>'
        elif column == 3:
            TITLE2 = 'Jcrossprod'
        elif column == 4:    
            TITLE2 = 'Jclumps (drawn clumps)'
        elif column == 5:    
            TITLE2 = 'Jtot'
        elif column == 6:    
            TITLE2 = 'Poisson & PSF displaced background events'
        else:
            print " ERROR. Column number",column,"does not exist."
            sys.exit()    
        
        ####################
        TITLE="CLUMPY J-factor skymap\n"+TITLE2

        outputFileName = "skymap.healpix"
        inputpath_skymap = str(inputdirectory)+"/"+outputFileName
        
        print "load clumpy skymap data from output file"
        inputDataMatrix = np.loadtxt(inputpath_skymap)
        npixROI = len(inputDataMatrix[:,0]) 
        DMskymap = np.zeros(npix) - 1.6375*10**(30)
        for i in range(npixROI):
            DMskymap[int(inputDataMatrix[i,0])] = inputDataMatrix[i,column]
         # plot:
        plot_skymap = plt.figure(figsize=(9,11))
        hp.cartview(DMskymap, fig=1,rot=[90,0], flip='geo',xsize=10000, lonra= [-TWOsigmaFOVdeg,TWOsigmaFOVdeg], latra= [-TWOsigmaFOVdeg,+TWOsigmaFOVdeg],  title=TITLE,unit='[Msol^2/kpc^5/sr]', min=10**15, max=10**18, norm='log')
        grat = TWOsigmaFOVdeg/5.0
        hp.graticule(dpar=grat, dmer=grat/2)
                
    plt.show()    

##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################