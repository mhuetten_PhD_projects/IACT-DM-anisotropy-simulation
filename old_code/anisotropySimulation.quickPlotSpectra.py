# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os
import sys
import getopt
import shutil # for file copying
import pyfits as pf
import MoritzCTAtools
from matplotlib import rc,rcParams




##    main part:   ############################################################
###############################################################################

def main(argv):

    ###########################################################################
    # read input variables
    try:
        opts, args = getopt.getopt(sys.argv[1:],"ha:f:p:l:b:c:d:r:q:n:v:z:t:s:m:y:k:j:x:g:w:i:o:",["alphaIntDeg=","fov=","psf=","psi=","theta=","totalbins=","thresholdbin=","irffile=","sensitivityfile=","noise=","elevation=","azimutbin=","tobs=","sigmav=","mchi=","signalmodel=","backgroundmodel=","astroanisotropiesmodel=","phizeroastro=","realisations=","backgroundonlypath=","dirin=","dirout="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'The input options are:'
        print ' -a <AlphaIntDeg>'
        print ' -f <TWOsigmaFOVdeg>'
        print ' -p <TWOsigmaPSFdeg>'
        print ' -l <psi0deg>'
        print ' -b <theta0degGal>'
        print ' -c <totalbins>'
        print ' -d <thresholdbin>'
        print ' -r <iRffile>'
        print ' -q <sensitivityfile>'
        print ' -n <Noise>'
        print ' -v <eleVation>'
        print ' -z <aZimutbin>'
        print ' -t <Tobs>'
        print ' -s <Sigmav>'
        print ' -m <Mchi>'
        print ' -y <signalmodel>'
        print ' -k <backgroundmodel>'
        print ' -j <astroanisotropiesmodel>'
        print ' -x <phizeroastro>'
        print ' -g <realisations>'
        print ' -w <backgroundonlypath>'
        print ' -i <input directory>'
        print ' -o <output directory>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print ' -a <AlphaIntDeg>'
            print ' -f <TWOsigmaFOVdeg>'
            print ' -p <TWOsigmaPSFdeg>'
            print ' -l <psi0deg>'
            print ' -b <theta0degGal>'
            print ' -c <totalbins>'
            print ' -d <thresholdbin>'
            print ' -r <iRffile>'
            print ' -q <sensitivityfile>'
            print ' -n <Noise>'
            print ' -v <eleVation>'
            print ' -z <aZimut>'
            print ' -t <Tobs>'
            print ' -s <Sigmav>'
            print ' -m <Mchi>'
            print ' -y <signalmodel>'
            print ' -k <backgroundmodel>'
            print ' -j <astroanisotropiesmodel>'
            print ' -x <phizeroastro>'
            print ' -g <realisations>'
            print ' -w <backgroundonlypath>'            
            print ' -i <input directory>'
            print ' -o <output directory>'
            sys.exit()
        elif opt in ("-a", "--alphaIntDeg"):
            alphaIntDeg = float(arg)
        elif opt in ("-f", "--fov"):
            TWOsigmaFOVdeg = float(arg)
        elif opt in ("-p", "--psf"):
            TWOsigmaPSFdeg = float(arg)
        elif opt in ("-l", "--psi"):
            psi0deg = float(arg)
        elif opt in ("-b", "--theta"):
            theta0degGal = float(arg)
        elif opt in ("-c", "--totalbins"):
            totalBins = int(arg)
        elif opt in ("-d", "--thresholdbin"):
            thresholdBin = int(arg)
        elif opt in ("-r", "--irffile"):
            irffile = arg
        elif opt in ("-q", "--sensitivityfile"):
            sensitivityFile = arg
        elif opt in ("-n", "--noise"):
            noise = int(arg)
        elif opt in ("-v", "--elevation"):
            elevation = float(arg) 
        elif opt in ("-z", "--azimutbin"):
            azimutBin = int(arg) 
        elif opt in ("-t", "--tobs"):
            tobs = float(arg) 
        elif opt in ("-s", "--sigmav"):
            sigmav = float(arg)
        elif opt in ("-m", "--mchi"):
            mchi = float(arg)
        elif opt in ("-y", "--signalmodel"):
            signalModel = str(arg)
        elif opt in ("-k", "--backgroundmodel"):
            backgroundModel = str(arg)
        elif opt in ("-j", "--astroanisotropiesmodel"):
            astroAnisotropiesModel = str(arg)
        elif opt in ("-x", "--phizeroastro"):
            Phi0astroflux = float(arg)
        elif opt in ("-g", "--realisations"):
            realisations = int(arg)
        elif opt in ("-w", "--backgroundonlypath"):
            backgroundOnlyDirectory = arg 
        elif opt in ("-i", "--dirin"):
            inputDirectory = arg 
        elif opt in ("-o", "--dirout"):
            outputDirectory = arg
            
    start_time = time.time()

    rc('text',usetex=True)
    rcParams.update({'font.size': 32})
    
        #Load first mc simulation output spectrum:
#     firstSpectrumFilePath = "/lustre/fs5/group/cta/users/mhuetten/workdata/anisotropyMC/events10e7Res4096FullAstroSignal1/0.5_clOutput.fits"
#           
#     firstSpectrumData = pf.getdata(firstSpectrumFilePath)
#     clOUT = firstSpectrumData.field(0)
#     firstSpectrumVector = clOUT[1:]
#         
#     clmatrix = np.ndarray((6, len(firstSpectrumVector)))
#       
#     #Load  spectrum:
#      
#     clmatrix 
#      
#     svector = [0.5, 1.0, 1.5, 2.0, 2.5]
#  
#  
#     # plot means for NORMALIZED spectrum:
#     plot_powerspectrumNormalized = plt.figure(figsize=(18,8.5)) 
#      
#     COLORS = ['red', 'darkred' ,'darkmagenta','darkviolet','blue']
#      
#     for i in range(len(svector)):
#         averageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
#         for n in range(6):
#           
#       
#             spectrumFilePath = "/lustre/fs5/group/cta/users/mhuetten/workdata/anisotropyMC/events10e7Res4096FullAstroSignal"+str(n+1)+"/"+str(svector[i])+"_clOutput.fits"
#             
#             spectrumData = pf.getdata(spectrumFilePath)
#             clOUT = spectrumData.field(0)
#             spectrumVector = clOUT[1:]
#             clmatrix[n,:] = spectrumVector
#       
#       
#               
#               
#         for k in range(len(firstSpectrumVector)):
#             averageSpectrum[0,k] = np.mean(clmatrix[:,k])
#             averageSpectrum[1,k] = np.std(clmatrix[:,k])
#           
#         ellVector = np.arange(len(spectrumVector))
#   
#         p1 = plt.plot(ellVector, ellVector * (ellVector+1) * (averageSpectrum[0])/(2*np.pi),color=COLORS[i],alpha=0.4,linestyle='-', linewidth=1., label=r"$s = %s$" %(svector[i])) 
#  
#         p4 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (averageSpectrum[0]+averageSpectrum[1])/(2*np.pi),ellVector * (ellVector+1) *  (averageSpectrum[0]-averageSpectrum[1])/(2*np.pi),color=COLORS[i],alpha=0.4, label="s = %s" %(svector[i]))
#  
#     averageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
#     for n in range(6):
#         spectrumFilePath = "/lustre/fs5/group/cta/users/mhuetten/workdata/anisotropyMC/events10e7Res4096Background"+str(n+1)+"/0.0_clOutput.fits"
#        
#         spectrumData = pf.getdata(spectrumFilePath)
#         clOUT = spectrumData.field(0)
#         spectrumVector = clOUT[1:]
#         clmatrix[n,:] = spectrumVector
#          
#     for k in range(len(firstSpectrumVector)):
#         averageSpectrum[0,k] = np.mean(clmatrix[:,k])
#         averageSpectrum[1,k] = np.std(clmatrix[:,k])
#      
#     ellVector = np.arange(len(spectrumVector))
#  
#     p1 = plt.plot(ellVector, ellVector * (ellVector+1) * (averageSpectrum[0])/(2*np.pi),color='black',alpha=0.4, linestyle='-', linewidth=1., label="isotropic background") 
#     p4 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (averageSpectrum[0]+averageSpectrum[1])/(2*np.pi),ellVector * (ellVector+1) *  (averageSpectrum[0]-averageSpectrum[1])/(2*np.pi),color='black',alpha=0.4, label="isotropic background")
#  
#     plt.legend(loc='lower left',prop={'size':28})
#     rcParams.update({'figure.autolayout': True})
#     plt.xlabel('multipole index $l$'); plt.ylabel(r'$l(l+1)C_l/2\pi$'); plt.grid()
#     plt.xscale('log')
#     plt.yscale('log')
#     pylab.xlim([1,10**(4)])
#     pylab.ylim([10**(-4),10**(4)])
#     #plt.text(5, 0.2,'gray bands: Isotropic background', horizontalalignment='center',verticalalignment='baseline')
#     plt.savefig("/afs/ifh.de/user/m/mhuetten/Desktop/RipkenRepro.png", dpi = 300)
# 
# 
# ##############################################################################
# 
# 
#     NRUNS = 5
#     plot_powerspectrumNormalized = plt.figure(figsize=(18, 8.5)) 
#     clmatrix = np.ndarray((NRUNS, len(firstSpectrumVector)))
#     COLORS = ['red','blue']
#     svector = ["10^6", "10^7"]
#     ssvector = [6,7]
#     for i in range(len(svector)):
#         averageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
#         for n in range(NRUNS):
#          
#      
#             spectrumFilePath = "/lustre/fs5/group/cta/users/mhuetten/workdata/anisotropyMC/14-06-11-1000BackgroundRuns1e"+str(ssvector[i])+"events/spectra-mc/mc-realisation-"+str(n+1)+".spectrum.fits"
#            
#             spectrumData = pf.getdata(spectrumFilePath)
#             clOUT = spectrumData.field(0)
#             spectrumVector = clOUT[1:]
#             clmatrix[n,:] = spectrumVector
#      
#      
#              
#              
#         for k in range(len(firstSpectrumVector)):
#             averageSpectrum[0,k] = np.mean(clmatrix[:,k])
#             averageSpectrum[1,k] = np.std(clmatrix[:,k])
#          
#         ellVector = np.arange(len(spectrumVector))
#         
#         CN = 4*np.pi/10**ssvector[i]
#         CNVector = np.zeros(len(spectrumVector)) +CN
#         p1 = plt.plot(ellVector, ellVector * (ellVector+1) * CNVector/(2*np.pi),color="black",alpha=0.5,linestyle='-', linewidth=1.) 
# 
#         p1 = plt.plot(ellVector, ellVector * (ellVector+1) * (averageSpectrum[0])/(2*np.pi),color=COLORS[i],alpha=0.4,linestyle='-', linewidth=1., label=r"$%s$ events" %(svector[i]) ) 
# 
#         p4 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (averageSpectrum[0]+averageSpectrum[1])/(2*np.pi),ellVector * (ellVector+1) *  (averageSpectrum[0]-averageSpectrum[1])/(2*np.pi),color=COLORS[i],alpha=0.4, label=r" %s" %(svector[i]))
# 
# 
#     plt.legend(loc='lower left',prop={'size':28})
#     plt.xlabel('multipole index $l$'); plt.ylabel(r'$l(l+1)C_l/2\pi$'); plt.grid()
#     plt.xscale('log')
#     plt.yscale('log')
#     pylab.xlim([1,10**(4)])
#     pylab.ylim([10**(-4),10**(4)])
#     #plt.text(5, 0.2,'gray bands: Isotropic background', horizontalalignment='center',verticalalignment='baseline')
#     plt.savefig("/afs/ifh.de/user/m/mhuetten/Desktop/RipkenRepro2.png", dpi = 300)
# 
# 
#     plt.show()

###############################################################################
#1. linie
        #Load first mc simulation output spectrum:
    firstSpectrumFilePath = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/Background-300h-500realisations/spectra-mc/combinedNormalizedMap_thresholdBin7-realisation-1.spectrum.fits"
          
    firstSpectrumData = pf.getdata(firstSpectrumFilePath)
    clOUT = firstSpectrumData.field(0)
    firstSpectrumVector = clOUT[1:]
    #firstSpectrumVector = firstSpectrumVector[:-50]  
      
    clmatrix = np.ndarray((12, len(firstSpectrumVector)))
      
    #Load  spectrum:
     
    realisations = 10
    realizationVector=np.arange(realisations) 
    svector = ["8e-23","8e-23","isotropic background"]
    sigmavString = "sdf"
 
#    plot means for NORMALIZED spectrum:
    plot_powerspectrumNormalized = plt.figure(figsize=(18,8.5)) 
      
    COLORS = ['red', 'darkred' ,'darkmagenta','darkviolet','blue']
      
    firstSpectrumData = pf.getdata(firstSpectrumFilePath)
    clOUT = firstSpectrumData.field(0)
    firstSpectrumVector = clOUT[1:]
    ellVector = np.arange(len(firstSpectrumVector))      
    mcNormalizedSpectrum = np.ndarray((len(realizationVector), len(firstSpectrumVector)))
    #print " dimension of combined MC spectra data matrix: ", mcNormalizedSpectrum.shape
     
    inputDirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/mchi750GeV-tautau-300h-10realisations/sigmav-8e-23/spectra-mc" 
 
    # combine all realisations into one matrix:
    for realisation in range(len(realizationVector)):
        normalizedSpectrumFilePath = inputDirectory+"/combinedNormalizedMap_thresholdBin7-realisation-"+str(realizationVector[realisation]+1)+".spectrum.fits"
        
        normalizedSpectrumData = pf.getdata(normalizedSpectrumFilePath)
        clOUTnormalized = normalizedSpectrumData.field(0)
          
        normalizedSpectrumVector = clOUTnormalized[1:]
        
        for j in range(len(normalizedSpectrumVector)):
            mcNormalizedSpectrum[realisation,j] = normalizedSpectrumVector[j]
            
    #save combined spectra matrix to file:
    #spectraCombinedOutputFilePath = outputDirectory+"/combinedNormalizedSpectra.spectra"
    #np.savetxt(spectraCombinedOutputFilePath, mcNormalizedSpectrum)
     
    # calculate mean and standard deviation for each l by assuming normal distributed values:
    #print " calculate mean and standard deviation for each l:"
    normalizedAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    rawAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    for i in range(len(firstSpectrumVector)):
        normalizedAverageSpectrum[0,i] = np.mean(mcNormalizedSpectrum[:,i])
        normalizedAverageSpectrum[1,i] = np.std(mcNormalizedSpectrum[:,i])

        
    p2 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]+normalizedAverageSpectrum[1,:])/(2*np.pi),ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]-normalizedAverageSpectrum[1,:])/(2*np.pi),color='r', alpha=0.4)#, label="realisation "+str(runnumbers[i]+1)) 
    p1 = plt.plot(ellVector, ellVector * (ellVector+1) * normalizedAverageSpectrum[0,:]/(2*np.pi),color='r',  linestyle='-', linewidth=1., label=r"$\langle \sigma v \rangle=8\cdot 10^{-23}\,\mathrm{cm^3\,s^{-1}}$") 
    
    


###############################################################################
#2. linie
        #Load first mc simulation output spectrum:
    firstSpectrumFilePath = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/Background-300h-500realisations/spectra-mc/combinedNormalizedMap_thresholdBin7-realisation-1.spectrum.fits"
          
    firstSpectrumData = pf.getdata(firstSpectrumFilePath)
    clOUT = firstSpectrumData.field(0)
    firstSpectrumVector = clOUT[1:]
    #firstSpectrumVector = firstSpectrumVector[:-50]  
      
    clmatrix = np.ndarray((12, len(firstSpectrumVector)))
      
    #Load  spectrum:
     
    realisations = 10
    realizationVector=np.arange(realisations) 
    svector = ["8e-23"]
    sigmavString = "sdf"
 
#    plot means for NORMALIZED spectrum:
      
    COLORS = ['red', 'darkred' ,'darkmagenta','darkviolet','blue']
      
    firstSpectrumData = pf.getdata(firstSpectrumFilePath)
    clOUT = firstSpectrumData.field(0)
    firstSpectrumVector = clOUT[1:]
    ellVector = np.arange(len(firstSpectrumVector))      
    mcNormalizedSpectrum = np.ndarray((len(realizationVector), len(firstSpectrumVector)))
    #print " dimension of combined MC spectra data matrix: ", mcNormalizedSpectrum.shape
     
    inputDirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/mchi750GeV-tautau-300h-10realisations/sigmav-3e-23/spectra-mc" 
 
    # combine all realisations into one matrix:
    for realisation in range(len(realizationVector)):
        normalizedSpectrumFilePath = inputDirectory+"/combinedNormalizedMap_thresholdBin7-realisation-"+str(realizationVector[realisation]+1)+".spectrum.fits"
        
        normalizedSpectrumData = pf.getdata(normalizedSpectrumFilePath)
        clOUTnormalized = normalizedSpectrumData.field(0)
          
        normalizedSpectrumVector = clOUTnormalized[1:]
        
        for j in range(len(normalizedSpectrumVector)):
            mcNormalizedSpectrum[realisation,j] = normalizedSpectrumVector[j]
            
    #save combined spectra matrix to file:
    #spectraCombinedOutputFilePath = outputDirectory+"/combinedNormalizedSpectra.spectra"
    #np.savetxt(spectraCombinedOutputFilePath, mcNormalizedSpectrum)
     
    # calculate mean and standard deviation for each l by assuming normal distributed values:
    #print " calculate mean and standard deviation for each l:"
    normalizedAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    rawAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    for i in range(len(firstSpectrumVector)):
        normalizedAverageSpectrum[0,i] = np.mean(mcNormalizedSpectrum[:,i])
        normalizedAverageSpectrum[1,i] = np.std(mcNormalizedSpectrum[:,i])

        
    p2 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]+normalizedAverageSpectrum[1,:])/(2*np.pi),ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]-normalizedAverageSpectrum[1,:])/(2*np.pi),color='blue', alpha=0.4)#, label="realisation "+str(runnumbers[i]+1)) 
    p1 = plt.plot(ellVector, ellVector * (ellVector+1) * normalizedAverageSpectrum[0,:]/(2*np.pi),color='blue',  linestyle='-', linewidth=1., label=r"$\langle \sigma v \rangle=3\cdot 10^{-23}\,\mathrm{cm^3\,s^{-1}}$") 
    
    
    
    #########################################################################3
    # background:
    inputDirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/VTS/Background-300h-500realisations/spectra-mc" 

    realisations = 10
    realizationVector=np.arange(realisations) 
    mcNormalizedSpectrum = np.ndarray((len(realizationVector), len(firstSpectrumVector)))
        # combine all realisations into one matrix:
    for realisation in range(len(realizationVector)):
        normalizedSpectrumFilePath = inputDirectory+"/combinedNormalizedMap_thresholdBin7-realisation-"+str(realizationVector[realisation]+1)+".spectrum.fits"
        
        normalizedSpectrumData = pf.getdata(normalizedSpectrumFilePath)
        clOUTnormalized = normalizedSpectrumData.field(0)
          
        normalizedSpectrumVector = clOUTnormalized[1:]
        
        for j in range(len(normalizedSpectrumVector)):
            mcNormalizedSpectrum[realisation,j] = normalizedSpectrumVector[j]
            
    #save combined spectra matrix to file:
    #spectraCombinedOutputFilePath = outputDirectory+"/combinedNormalizedSpectra.spectra"
    #np.savetxt(spectraCombinedOutputFilePath, mcNormalizedSpectrum)
     
    # calculate mean and standard deviation for each l by assuming normal distributed values:
    #print " calculate mean and standard deviation for each l:"
    normalizedAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    rawAverageSpectrum = np.ndarray((2, len(firstSpectrumVector)))
    for i in range(len(firstSpectrumVector)):
        normalizedAverageSpectrum[0,i] = np.mean(mcNormalizedSpectrum[:,i])
        normalizedAverageSpectrum[1,i] = np.std(mcNormalizedSpectrum[:,i])

        
    p2 = plt.fill_between(ellVector, ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]+normalizedAverageSpectrum[1,:])/(2*np.pi),ellVector * (ellVector+1) * (normalizedAverageSpectrum[0,:]-normalizedAverageSpectrum[1,:])/(2*np.pi),color='black', alpha=0.4)#, label="realisation "+str(runnumbers[i]+1)) 
    p1 = plt.plot(ellVector, ellVector * (ellVector+1) * normalizedAverageSpectrum[0,:]/(2*np.pi),color='black',  linestyle='-', linewidth=1., label="isotropic background") 
    
    
    
    
        

    plt.subplots_adjust(top=0.85)
    plt.legend(loc='lower left')
    plt.xlabel('multipole index l'); plt.ylabel('$l(l+1)C_l/2pi$'); plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1,10**4])
    pylab.ylim([10**(-4),10**(4)])
    #plt.text(5, 0.2,'gray bands: Isotropic background', horizontalalignment='center',verticalalignment='baseline')
    plt.savefig("/afs/ifh.de/user/m/mhuetten/Desktop/VTSplot.png", dpi = 300)

    plt.show()
    
    print " program anisotropySimulation.evaluatePowerSpectra.py successfully finished."
    print " total computation time:", round((time.time() - start_time)/60,2), "minutes."

##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################