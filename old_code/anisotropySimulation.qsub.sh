#$ -S /bin/tcsh
#
# script to perform Dark Matter clumpy simulations AND detector response
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script

set PARAMETER=RRRRR
set RUNDIR=RUUUN
set QLOG=PEEED
set ODIR=OODIR
set SCRIPTDIR=DIIIR
set DATE=DAATE

# Geometrical parameters:
set psiZeroDeg=AAAAA
set thetaZeroDegGal=BBBBB
set TWOsigmaFOVdeg=ZIIII
set TWOsigmaPSFdeg=ZJJJJ

set psiWidthDeg=CCCCC
set thetaWidthDeg=DDDDD
set alphaIntDeg=EEEEE
set user_rse=FFFFF

# CLUMPY parameters:
set user_rse=FFFFF
set seed=ZHHHH

# Galactic Halo:
set gGAL_TOT_FLAG_PROFILE=HHHHH	
set gGAL_TOT_SHAPE_PARAMS_0=IIIII
set gGAL_TOT_SHAPE_PARAMS_1=JJJJJ
set gGAL_TOT_SHAPE_PARAMS_2=KKKKK
set gGAL_TOT_RSCALE=LLLLL
set gGAL_RHOSOL=MMMMM
set gGAL_RSOL=NNNNN
set gGAL_RVIR=OOOOO

# Clumps distribution:
set gGAL_DPDV_FLAG_PROFILE=PPPPP
set gGAL_DPDV_SHAPE_PARAMS_0=QQQQQ
set gGAL_DPDV_SHAPE_PARAMS_1=SSSSS
set gGAL_DPDV_SHAPE_PARAMS_2=TTTTT
set gGAL_DPDV_RSCALE=UUUUU
set gGAL_DPDM_SLOPE=VVVVV
set gDM_MMIN_SUBS=WWWWW
set gGAL_SUBS_N_INM1M2=XXXXX

# Clumps inner profile:
set gGAL_CLUMPS_FLAG_PROFILE=YYYYY
set gGAL_CLUMPS_SHAPE_PARAMS_0=ZZZZZ
set gGAL_CLUMPS_SHAPE_PARAMS_1=ZAAAA
set gGAL_CLUMPS_SHAPE_PARAMS_2=ZBBBB
set gGAL_CLUMPS_FLAG_CVIRMVIR=ZCCCC

# Additional clumpy parameters:
set gDM_MMAXFRAC_SUBS=ZDDDD
set gDM_RHOSAT=ZEEEE
set gGAL_SUBS_M1=ZFFFF
set gGAL_SUBS_M2=ZGGGG

# Halo list:
set gLIST_HALOES=ZZHHH
set gLIST_Bool=ZZIII

# Particle physics parameters:
set gSIMU_IS_ANNIHIL_OR_DECAY=GGGGG
set mchi=ZOOOO
set DMspectrumName=ZPPPP
set sigmav=ZRRRR

# Instrument response properties:
set effAreaFile=ZQQQQ
set energyBins=ZYYYY
set noise=ZKKKK
set elevation=ZLLLL
set azimutBin=ZMMMM
set IRFspectralindex=ZZFFF
set MCvsREC=ZZGGG
set sensitivityFile=ZSSSS

# Simulated physics:
set tobs=ZTTTT
set backgroundmodel=ZUUUU
set astroAnisotropies=ZVVVV
set	thresholdBin=ZZBBB

# Point source simulation values:
set phiZeroAstroFlux=ZZAAA
set	alpha=ZZCCC
set wobbleOffset=ZZDDD
set thetaSquared=ZZEEE

# technical:
set realisationnumber=ZNNNN
set onlyOneRunBool=ZWWWW
set moduleBool=ZXXXX

# temporary directory
set DDIR=$TMPDIR/Moritz
mkdir -p $DDIR
rm -f $DDIR/*
echo $DDIR

echo
echo " **********************************************************************"
echo " This is a a full simulation of Dark Matter anisotropy signals for IACT"
echo " **********************************************************************"
echo 
echo " *** INPUT PARAMETERS for CLUMPY simulation: ***"
echo 
echo -n " Psi position of line of sight: " $psiZeroDeg "degrees\n"
echo -n " ThetaGal position of line of sight: " $thetaZeroDegGal "degrees\n"
echo -n " Full width of skymap around line of sight in psi direction: " $psiWidthDeg "degrees\n"
echo -n " Full width of skymap around line of sight in theta direction: " $thetaWidthDeg "degrees\n"
echo -n " CLUMPY skymap grid resolution: " $alphaIntDeg "degrees\n"
echo -n " user_rse: " $user_rse "\n"
echo
echo " *** INPUT PARAMETERS for detector response simulation: ***"
echo
#echo -n "Number of grid pixels on whole sphere: " npix
echo -n " Full diameter of instrument's FOV: " $TWOsigmaFOVdeg "degrees\n"
echo -n " Full diameter of Gaussian PSF (1 sigma): " $TWOsigmaPSFdeg "degrees\n"
echo -n " Using Effective Area File: " $effAreaFile "\n"
echo -n " Total number of energy bins: " $energyBins "\n"
if ( $moduleBool == "2" ) then
	echo -n " This job's energy bin: " $SGE_TASK_ID "\n"
endif
echo
echo " *** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS SEE anisotropySimulation.sub.sh ***"
echo 

###############################################################################
# explanation of different directories:
# RUNDIR:  parent directory of run, the one set in line (08) of
#		   anisotropySimulation.parameters file, e.g. 14-07-13-CrabTest
# ODIR:	   parent directory of specific runparameter variation 
# WORKDIR: directory within ODIR for the number of identical realizations
# if no runparameter file: ODIR= RUNDIR
# if realizationNumber = 1: WORKDIR = ODIR

###############################################################################
# clean up output directory and create subfolders for each run:

# in case of multiple runs with runparameter: RUNDIR: parent directory, ODIR: directory of specific runparameter

# make temporary directory for each realisation:
if ( $onlyOneRunBool == "0" ) then
	set WORKDIR=$ODIR/realisation-${realisationnumber}
	mkdir -p $WORKDIR
	cd $WORKDIR
else
	set WORKDIR=$ODIR # = current directory"$(pwd)"
	cd $WORKDIR
endif

rm clInput.fits
rm clOutput.fits

###############################################################################
# start clumpy routine for creating Dark Matter skymap:
if ( $moduleBool == "1" ) then

	###########################################################################
	# set the right environmental variables to be used with CLUMPY
	#source $EVNDISPSYS/setObservatory.tcsh VERITAS
	source /afs/ifh.de/user/m/mhuetten/setbatchenvironment_oldROOT.tcsh VERITAS
	echo
	
	###########################################################################
	# modify clumpy parameter file:
	echo " *** modify clumpy parameter file: ***"
	sed  's/EEEEZ/'$alphaIntDeg'/' $SCRIPTDIR/anisotropySimulation.clumpy_params.txt > $ODIR/Inputfiles/tmp1.txt
	sed	 's/GGGGZ/'$gSIMU_IS_ANNIHIL_OR_DECAY'/' $ODIR/Inputfiles/tmp1.txt > $ODIR/Inputfiles/tmp2.txt
	sed  's/HHHHZ/'$gGAL_TOT_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp2.txt > $ODIR/Inputfiles/tmp3.txt
	sed  's/IIIIZ/'$gGAL_TOT_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp3.txt > $ODIR/Inputfiles/tmp4.txt
	sed  's/JJJJZ/'$gGAL_TOT_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp4.txt > $ODIR/Inputfiles/tmp5.txt
	sed  's/KKKKZ/'$gGAL_TOT_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp5.txt > $ODIR/Inputfiles/tmp6.txt
	sed  's/LLLLZ/'$gGAL_TOT_RSCALE'/' $ODIR/Inputfiles/tmp6.txt > $ODIR/Inputfiles/tmp7.txt
	sed  's/MMMMZ/'$gGAL_RHOSOL'/' $ODIR/Inputfiles/tmp7.txt > $ODIR/Inputfiles/tmp8.txt
	sed  's/NNNNZ/'$gGAL_RSOL'/' $ODIR/Inputfiles/tmp8.txt > $ODIR/Inputfiles/tmp9.txt
	sed  's/OOOOZ/'$gGAL_RVIR'/' $ODIR/Inputfiles/tmp9.txt > $ODIR/Inputfiles/tmp10.txt
	sed  's/PPPPZ/'$gGAL_DPDV_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp10.txt > $ODIR/Inputfiles/tmp11.txt
	sed  's/QQQQZ/'$gGAL_DPDV_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp11.txt > $ODIR/Inputfiles/tmp12.txt
	sed  's/SSSSZ/'$gGAL_DPDV_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp12.txt > $ODIR/Inputfiles/tmp13.txt
	sed  's/TTTTZ/'$gGAL_DPDV_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp13.txt > $ODIR/Inputfiles/tmp14.txt
	sed  's/UUUUZ/'$gGAL_DPDV_RSCALE'/' $ODIR/Inputfiles/tmp14.txt > $ODIR/Inputfiles/tmp15.txt
	sed  's/VVVVZ/'$gGAL_DPDM_SLOPE'/' $ODIR/Inputfiles/tmp15.txt > $ODIR/Inputfiles/tmp16.txt
	sed  's/WWWWZ/'$gDM_MMIN_SUBS'/' $ODIR/Inputfiles/tmp16.txt > $ODIR/Inputfiles/tmp17.txt
	sed  's/XXXXZ/'$gGAL_SUBS_N_INM1M2'/' $ODIR/Inputfiles/tmp17.txt > $ODIR/Inputfiles/tmp18.txt
	sed  's/YYYYZ/'$gGAL_CLUMPS_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp18.txt > $ODIR/Inputfiles/tmp19.txt
	sed  's/ZZZZX/'$gGAL_CLUMPS_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp19.txt > $ODIR/Inputfiles/tmp20.txt
	sed  's/ZAAAZ/'$gGAL_CLUMPS_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp20.txt > $ODIR/Inputfiles/tmp21.txt
	sed  's/ZBBBZ/'$gGAL_CLUMPS_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp21.txt > $ODIR/Inputfiles/tmp22.txt
	sed  's/ZCCCZ/'$gGAL_CLUMPS_FLAG_CVIRMVIR'/' $ODIR/Inputfiles/tmp22.txt > $ODIR/Inputfiles/tmp23.txt
	sed  's/ZDDDZ/'$gDM_MMAXFRAC_SUBS'/' $ODIR/Inputfiles/tmp23.txt > $ODIR/Inputfiles/tmp24.txt
	sed  's/ZEEEZ/'$gDM_RHOSAT'/' $ODIR/Inputfiles/tmp24.txt > $ODIR/Inputfiles/tmp25.txt
	sed  's/ZFFFZ/'$gGAL_SUBS_M1'/' $ODIR/Inputfiles/tmp25.txt > $ODIR/Inputfiles/tmp26.txt
	sed  's/ZHHHZ/'$seed'/' $ODIR/Inputfiles/tmp26.txt > $ODIR/Inputfiles/tmp27.txt
	sed  's/ZGGGZ/'$gGAL_SUBS_M2'/' $ODIR/Inputfiles/tmp27.txt > $ODIR/Inputfiles/tmp28.txt
	sed  's/ZIIIZ/'$gLIST_HALOES'/' $ODIR/Inputfiles/tmp28.txt > $ODIR/Inputfiles/anisotropySimulation.clumpy_params.txt
	rm $ODIR/Inputfiles/tmp*
	chmod u+x $ODIR/Inputfiles/anisotropySimulation.clumpy_params.txt
	
	rm skymap.fits
	# copy clumpy binary into realisation directory:
	cp $CLUMPY/bin/clumpy $DDIR

	###########################################################################
	# start clumpy routine:
	echo " *** start clumpy routine: ***"
	if ( $gLIST_Bool == "1" ) then
		$DDIR/clumpy -gp8 $ODIR/Inputfiles/anisotropySimulation.clumpy_params.txt $psiZeroDeg $thetaZeroDegGal $psiWidthDeg $thetaWidthDeg $user_rse 1 | tee $ODIR/Logfiles/module1-clumpy-realisation-${realisationnumber}.log
	else
		$DDIR/clumpy -gp7 $ODIR/Inputfiles/anisotropySimulation.clumpy_params.txt $psiZeroDeg $thetaZeroDegGal $psiWidthDeg $thetaWidthDeg $user_rse | tee $ODIR/Logfiles/module1-clumpy-realisation-${realisationnumber}.log
	endif
	mv -f  -v $DDIR/* $WORKDIR/
	rm *.drawn
	echo

	###########################################################################
	# transform clumpy output file into healpix format:
	echo " *** transform clumpy output file into healpix format and prepare power spectrum calculation: ***"
	python $SCRIPTDIR/anisotropySimulation.skymap.py -p $psiZeroDeg -t $thetaZeroDegGal -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $WORKDIR -j Jtot  -z fits -m $gDM_MMIN_SUBS -d $gLIST_Bool

	###########################################################################
	# evaluate clumpy output spectrum with anafast routine:	
	echo " *** evaluate clumpy output spectrum with anafast routine: ***"
	cd $ODIR/
	mkdir -p spectra-clumpy
	
	cd $HEALPIX 
	if ( $onlyOneRunBool == "0" ) then
		$HEALPIX/binfortran/anafast -s $WORKDIR/anafast_pseudo.par | tee $ODIR/Logfiles/module1-anafast-realisation-${realisationnumber}.log
	else
		$HEALPIX/binfortran/anafast -s $WORKDIR/anafast_pseudo.par | tee $ODIR/Logfiles/module1-anafast.log
	endif

	# move output spectrum file into parent folder:
	cd $WORKDIR/
	mv *.spectrum.fits $ODIR/spectra-clumpy/clumpy-realisation-${realisationnumber}.spectrum.fits
	mv *.healpix $WORKDIR/skymap.healpix
	rm anafast.par
	rm *.fits
	rm *.dat
	rm clumpy
	rm -f $DDIR/*
endif

###############################################################################
# Monte Carlo simulation of instrument response:
if ( $moduleBool == "2" ) then
	
	###########################################################################
	# set the right environmental variables to be used with PyROOT
	#source $EVNDISPSYS/setObservatory.tcsh VERITAS
	source /afs/ifh.de/user/m/mhuetten/setbatchenvironment_newROOT.tcsh VERITAS
	echo
	
	###########################################################################
	# now start Monte Carlo simulation:
	echo " *** perform detector response simulation: ***"

	$SCRIPTDIR/anisotropySimulation.montecarlo.py -a $alphaIntDeg -f $TWOsigmaFOVdeg -l $psiZeroDeg -b $thetaZeroDegGal -e $SGE_TASK_ID -r $effAreaFile -q $sensitivityFile -n $noise -v $elevation -z $azimutBin -t $tobs -s $sigmav -m $mchi -y $DMspectrumName -k $backgroundmodel -j $astroAnisotropies -x $phiZeroAstroFlux -g $alpha -u $thetaSquared -w $wobbleOffset -p $IRFspectralindex -c $MCvsREC -i $RUNDIR/skymap.healpix -o $WORKDIR #| tee $ODIR/Logfiles/module2-energyBin{$SGE_TASK_ID}-realisation-${realisationnumber}.log
	echo
endif

###############################################################################
# Combine energy bins:
if ( $moduleBool == "3" ) then
	
	###########################################################################
	# set the right environmental variables to be used with PyROOT
	#source $EVNDISPSYS/setObservatory.tcsh VERITAS
	source /afs/ifh.de/user/m/mhuetten/setbatchenvironment_newROOT.tcsh VERITAS
	echo
	
	###########################################################################
	# now combine energy bins:
	echo " *** combine energy bins: ***"
		if ( $onlyOneRunBool == "0" ) then
			$SCRIPTDIR/anisotropySimulation.combineBins.py -a $alphaIntDeg -l $psiZeroDeg -b $thetaZeroDegGal -c $energyBins -d $thresholdBin -r $effAreaFile -q $sensitivityFile -n $noise -v $elevation -z $azimutBin -t $tobs -s $sigmav -m $mchi -y $DMspectrumName -k $backgroundmodel -j $astroAnisotropies -x $phiZeroAstroFlux -g $alpha -u $thetaSquared -w $wobbleOffset -p $IRFspectralindex -f $MCvsREC -i $WORKDIR -o $WORKDIR | tee $ODIR/Logfiles/module3-combined_thresholdBin{$thresholdBin}-realisation-${realisationnumber}.log
		else
			$SCRIPTDIR/anisotropySimulation.combineBins.py -a $alphaIntDeg -l $psiZeroDeg -b $thetaZeroDegGal -c $energyBins -d $thresholdBin -r $effAreaFile -q $sensitivityFile -n $noise -v $elevation -z $azimutBin -t $tobs -s $sigmav -m $mchi -y $DMspectrumName -k $backgroundmodel -j $astroAnisotropies -x $phiZeroAstroFlux -g $alpha -u $thetaSquared -w $wobbleOffset -p $IRFspectralindex -f $MCvsREC -i $WORKDIR -o $WORKDIR | tee $ODIR/Logfiles/module3-combined_thresholdBin{$thresholdBin}.log
		endif
	# remove flux tables:
	#rm $WORKDIR/averageFluxes_bin*
	echo
	
	###########################################################################
	# now run anafast routine:
	
	cd $ODIR/
	mkdir -p spectra-mc

	echo " *** run anafast routine: ***"
	cd $HEALPIX #/afs/ifh.de/group/cta/scratch/mhuetten/Programs/healpix
	$HEALPIX/binfortran/anafast -s $WORKDIR/anafastRAW.par #| tee $ODIR/Logfiles/module3-anafast-realisation-${realisationnumber}.log
	$HEALPIX/binfortran/anafast -s $WORKDIR/anafastNORMALIZED.par #| tee $ODIR/Logfiles/module3-anafast-realisation-${realisationnumber}.log
	cd $WORKDIR
	mv clOutputRAW.fits $ODIR/spectra-mc/combinedRawMap_thresholdBin{$thresholdBin}-realisation-${realisationnumber}.spectrum.fits
	mv clOutputNORMALIZED.fits $ODIR/spectra-mc/combinedNormalizedMap_thresholdBin{$thresholdBin}-realisation-${realisationnumber}.spectrum.fits
	rm anafast*
	rm *.fits
endif

##sleep 20
echo " *** Job successfully finished ***"
##exit
