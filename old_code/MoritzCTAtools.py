#!/usr/bin/python

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
import healpy as hp
import ROOT
from root_numpy import root2array, root2rec, tree2rec
from root_numpy.testdata import get_filepath
import ast
import decimal
import pickle
import os
import sys
import getopt
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d

##    define functions:    ####################################################
###############################################################################

def treevartolist(treevar,npoints):
     templist = []
     for point in range(0,npoints):
         templist.append(treevar[point])
     return templist 

###############################################################################

def rootentriestonumerics(rootentry):
     tmpentry = rootentry[0]
     return tmpentry

###############################################################################
 
def rootarraytolist(rootarray):
     arraylength = rootarray.shape[0]
     tmparray = []
     for index in range(arraylength):
         tmparray.append(rootentriestonumerics(rootarray[index]))
     return tmparray  

###############################################################################

def namestr(obj, namespace):
    return [name for name in namespace if namespace[name] is obj]

###############################################################################

def getDimensionVector(listvariable, rounddigits=None):
    indexarray = [0]
    if rounddigits is None:
        valuearray = [listvariable[0]]
        for i in range(1,len(listvariable)):
            if listvariable[i] != listvariable[i-1]:
                   if listvariable[i] not in valuearray:
                       valuearray.append(listvariable[i])
                       indexarray.append(i)
    else:
        valuearray = [round(listvariable[0],rounddigits)]
        for i in range(1,len(listvariable)):
            if round(listvariable[i],rounddigits) != round(listvariable[i-1],rounddigits):
                if round(listvariable[i],rounddigits) not in valuearray:
                   valuearray.append(round(listvariable[i],rounddigits))
                   indexarray.append(i)
    return indexarray, valuearray

###############################################################################
#####    big function to load effectve areas:    ##############################

def loadEffectiveAreas(effAreaRootFile,CTAbool = False):
    if CTAbool == True:
        CTAeffAreaRootFile = effAreaRootFile
        # ROOT file from where the bin sizes, etc. are extracted:
        effAreaRootFile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v430/EffectiveAreas/effArea-d20131031-cut-N3-Point-005CU-Moderate-ATM21-V6-T1234-d20131115.root"
        
    effAreaRootFile = str(effAreaRootFile)
    effAreFile = ROOT.TFile(effAreaRootFile)
     
    print " List effAreFile contents:"
    effAreFile.ls()
    print ""
     
    fEffAreaTree = effAreFile.Get("fEffArea")
     
    #print " List effAreFile fEffArea tree contents:"
    #fEffAreaTree.Print()
     
    # load root data:
    ze = rootarraytolist(tree2rec(fEffAreaTree,branches=['ze']))
    print " loaded entry 'ze'..."
    az = rootarraytolist(tree2rec(fEffAreaTree,branches=['az']))
    print " loaded entry 'az'..."
    azMin = rootarraytolist(tree2rec(fEffAreaTree,branches=['azMin']))
    print " loaded entry 'azMin'..."
    azMax = rootarraytolist(tree2rec(fEffAreaTree,branches=['azMax']))
    print " loaded entry 'azMax'..."
    # Xoff = rootarraytolist(tree2rec(fEffAreaTree,branches=['Xoff']))
    # print "loaded entry 'Xoff'..."
    # Yoff = rootarraytolist(tree2rec(fEffAreaTree,branches=['Yoff']))
    # print "loaded entry 'Yoff'..."
    Woff = rootarraytolist(tree2rec(fEffAreaTree,branches=['Woff']))
    print " loaded entry 'Woff'..."
    noise = rootarraytolist(tree2rec(fEffAreaTree,branches=['noise']))
    print " loaded entry 'noise'..."
    pedvar = rootarraytolist(tree2rec(fEffAreaTree,branches=['pedvar']))
    print " loaded entry 'pedvar'..."
    index = rootarraytolist(tree2rec(fEffAreaTree,branches=['index']))
    print " loaded entry 'index'..."
    nbins = rootarraytolist(tree2rec(fEffAreaTree,branches=['nbins']))
    print " loaded entry 'nbins'..."
    e0 = rootarraytolist(tree2rec(fEffAreaTree,branches=['e0']))
    print " loaded entry 'e0'..."
    eff = rootarraytolist(tree2rec(fEffAreaTree,branches=['eff']))
    print " loaded entry 'eff'..."
    Rec_nbins = rootarraytolist(tree2rec(fEffAreaTree,branches=['Rec_nbins']))
    print " loaded entry 'Rec_nbins'..."
    Rec_e0 = rootarraytolist(tree2rec(fEffAreaTree,branches=['Rec_e0']))
    print " loaded entry 'Rec_e0'..."
    Rec_eff = rootarraytolist(tree2rec(fEffAreaTree,branches=['Rec_eff']))
    print " loaded entry 'Rec_eff'..."  
    
    
    ze_indexarray, ze_values = getDimensionVector(ze)
    az_indexarray, az_values = getDimensionVector(az)
    azMin_indexarray, azMin_values = getDimensionVector(azMin)
    azMax_indexarray, azMax_values = getDimensionVector(azMax)    
    Woff_indexarray, Woff_values = getDimensionVector(Woff,2) 
    noise_indexarray, noise_values = getDimensionVector(noise) 
    pedvar_indexarray, pedvar_values = getDimensionVector(pedvar) 
    index_indexarray, index_values = getDimensionVector(index,2)
    #nbins_indexarray, nbins_values = getDimensionVector(nbins)
     
    ParameterValues = [[] for i in range(10)]
    ParameterValues[0] = ze_values    # corresponds to 1st entry in AeffOverEnergyMatrix
    ParameterValues[1] = az_values    # corresponds to 2nd entry in AeffOverEnergyMatrix 
    ParameterValues[2] = azMin_values # corresponds to 2nd entry in AeffOverEnergyMatrix 
    ParameterValues[3] = azMax_values # corresponds to 2nd entry in AeffOverEnergyMatrix 
    ParameterValues[4] = Woff_values  # corresponds to 3rd entry in AeffOverEnergyMatrix 
    ParameterValues[5] = noise_values # corresponds to 4th entry in AeffOverEnergyMatrix 
    ParameterValues[6] = pedvar_values# corresponds to 4th entry in AeffOverEnergyMatrix 
    ParameterValues[7] = index_values # corresponds to 5th entry in AeffOverEnergyMatrix 
                                      # the 6th entry in the AeffOverEnergyMatrix switches
                                      # between the eff. areas over true [n = 0] and 
                                      # reconstructed [n = 1] energy (for n, see below).
                                      
    # Define 6-dim. AeffOverEnergyMatrix. Access list later by: AeffOverEnergyMatrix[i][j][k][l][m][n]                                   
    AeffOverEnergyMatrix = [[[[[[[] for n in range(2)] \
                          for m in range(len(index_indexarray))] \
                          for l in range(len(noise_indexarray)) ] \
                          for k in range(len(Woff_indexarray)) ] \
                          for j in range(len(az_indexarray)) ] \
                          for i in range(len(ze_indexarray)) ]
      
    nbinE0_max = 0
    nbinErec_max = 0  
    print " writing ROOT data into 6-dim. nested Python list..."  
    for ze_index in range(len(ze_indexarray)):
        for Woff_index in range(len(Woff_indexarray)):
            for noise_index in range(len(noise_indexarray)):
                for index_index in range(len(index_indexarray)):
                    for az_index in range(len(az_indexarray)):
                        fEffArea_vectorindex = ze_index * len(az_indexarray) * len(index_indexarray) * len(noise_indexarray) * len(Woff_indexarray) + \
                                               Woff_index * len(az_indexarray) * len(index_indexarray) * len(noise_indexarray) + \
                                               noise_index * len(az_indexarray) * len(index_indexarray) + \
                                               index_index * len(az_indexarray) + \
                                               az_index
                        # check length of energy bin vector:
                        nbins_E0 = len(e0[fEffArea_vectorindex][:])
                        nbins_Erec = len(Rec_e0[fEffArea_vectorindex][:])
                        if nbins_E0 > nbinE0_max:
                            nbinE0_max = nbins_E0
                            ze_index_nbinE0_max = ze_index
                            Woff_index_nbinE0_max = Woff_index
                            noise_index_nbinE0_max = noise_index
                            index_index_nbinE0_max = index_index
                            az_index_nbinE0_max = az_index
                        if nbins_Erec > nbinErec_max:
                            nbinErec_max = nbins_Erec
                            ze_index_nbinErec_max = ze_index
                            Woff_index_nbinErec_max = Woff_index
                            noise_index_nbinErec_max = noise_index
                            index_index_nbinErec_max = index_index
                            az_index_nbinErec_max = az_index
                        # save effective areas in lists:
                        AeffOverE0 = [[[] for n in range(nbins_E0)] for m in range(2)]
                        AeffOverErec = [[[] for n in range(nbins_Erec)] for m in range(2)]
                        if nbins_Erec==nbins_E0:
                            for i in range(nbins_E0):
                                # save energies in GeV:
                                AeffOverE0[0][i] = int(1000*10**float(e0[fEffArea_vectorindex][i]))+1
                                AeffOverE0[1][i] = float(eff[fEffArea_vectorindex][i])
                                AeffOverErec[0][i] = int(1000*10**float(Rec_e0[fEffArea_vectorindex][i]))+1
                                AeffOverErec[1][i] = float(Rec_eff[fEffArea_vectorindex][i])
                        else:
                            for i in range(nbins_E0):
                                AeffOverE0[0][i] = int(1000*10**float(e0[fEffArea_vectorindex][i]))+1
                                AeffOverE0[1][i] = float(eff[fEffArea_vectorindex][i])
                            for i in range(nbins_Erec):
                                AeffOverErec[0][i] = int(1000*10**float(Rec_e0[fEffArea_vectorindex][i]))+1
                                AeffOverErec[1][i] = float(Rec_eff[fEffArea_vectorindex][i])                                                   
                        # write effective areas over true energies:
                        AeffOverEnergyMatrix[ze_index][az_index][Woff_index][noise_index][index_index][0] = AeffOverE0
                        # write effective areas over reconstructed energies:
                        AeffOverEnergyMatrix[ze_index][az_index][Woff_index][noise_index][index_index][1] = AeffOverErec
    print " done." 
    ParameterValues[8] = AeffOverEnergyMatrix[ze_index_nbinE0_max][az_index_nbinE0_max][Woff_index_nbinE0_max][noise_index_nbinE0_max][index_index_nbinE0_max][0][0]
    ParameterValues[9] = AeffOverEnergyMatrix[ze_index_nbinErec_max][az_index_nbinErec_max][Woff_index_nbinErec_max][noise_index_nbinErec_max][index_index_nbinErec_max][1][0]
    if len(ParameterValues[8]) != max(nbins):
        print "Warning: length =",len(ParameterValues[8])," of true energies bin vector"
        print "shorter than max(nbins) =",max(nbins)
    if len(ParameterValues[9]) != max(Rec_nbins):
        print "Warning: length =",len(ParameterValues[9])," of Rec. energies bin vector"
        print "shorter than max(Rec_nbins) =",max(Rec_nbins)
    
    if CTAbool == True:
     
        CTAeffAreaRootFile = str(CTAeffAreaRootFile)
        CTAeffAreaFile = ROOT.TFile(CTAeffAreaRootFile)
          
        effAreaHist = CTAeffAreaFile.Get("EffectiveAreaEtrue_offaxis")
             
        energyBinNumber = effAreaHist.GetNbinsX()+2
        energyBinVector = np.zeros(energyBinNumber)
          
        WoffBinNumber = effAreaHist.GetNbinsY()+1
        WoffBinVector = np.zeros(WoffBinNumber-1)
     
        for i in range(0,energyBinNumber):
            energyBinVector[i] = float(effAreaHist.GetXaxis().GetBinCenter(i))
        for i in range(1,WoffBinNumber):
            WoffBinVector[i-1] = float(effAreaHist.GetYaxis().GetBinCenter(i))
          
        CTAParameterValues = [[] for i in range(2)]
        CTAParameterValues[0] = energyBinVector
        CTAParameterValues[1] = WoffBinVector
          
        CTAeffAreaMatrix = np.zeros((energyBinNumber,WoffBinNumber-1))
        for j in range(1,WoffBinNumber):
            for i in range(energyBinNumber):
                index = j * energyBinNumber + i
                CTAeffAreaMatrix[i,j-1] = effAreaHist.GetBinContent(index)
                # correct for unsteady root file data:
                if j > 1:
                    if CTAeffAreaMatrix[i,j-1] > CTAeffAreaMatrix[i,j-2]:
                        CTAeffAreaMatrix[i,j-1] = CTAeffAreaMatrix[i,j-2]
                        if j > 2:
                            CTAeffAreaMatrix[i,j-1] = 2 * CTAeffAreaMatrix[i,j-2] - CTAeffAreaMatrix[i,j-3]
                            if 2 * CTAeffAreaMatrix[i,j-2] - CTAeffAreaMatrix[i,j-3] < 0:
                                CTAeffAreaMatrix[i,j-1] = CTAeffAreaMatrix[i,j-2]
                     
        CTAeffAreaMatrixVTSbins = interpolateBackgroundRates(CTAeffAreaMatrix, CTAParameterValues, ParameterValues[8])
        AeffOverEnergyMatrix = CTAeffAreaMatrixVTSbins
        ParameterValues[4] = CTAParameterValues[1].tolist()
       
         
    return ParameterValues, AeffOverEnergyMatrix

##    end of heart function    ################################################

def pickEffectiveArea(ParameterValues, AeffOverEnergyMatrix, ze = 0.0, az = 0, Woff = 0.0, noise = 75, index = 2.0, energy = "A_MC"):
   
    if float(ze) in ParameterValues[0]:
        ze_index = ParameterValues[0].index(float(ze))
        if __name__ == "__main__":
            print " ze index:", ze_index
    if float(ze) not in ParameterValues[0]:
        print "Chosen value ze = %s does not exist in EffectiveArea file."%(ze)
        print "Possible ze values are:", ParameterValues[0]
        return None
          
    if float(az) in ParameterValues[1]:
        az_index = ParameterValues[1].index(float(az))
        if __name__ == "__main__":
            print " az index:", az_index
    if float(az) not in ParameterValues[1]:
        print "Chosen value az = %s does not exist in EffectiveArea file."%(az)
        print "Possible az values are:", ParameterValues[1]
        return None
          
    if float(Woff) in ParameterValues[4]:
        Woff_index = ParameterValues[4].index(float(Woff))
        if __name__ == "__main__":
            print " Woff index:", Woff_index
    if float(Woff) not in ParameterValues[4]:
        print "Chosen value Woff = %s does not exist in EffectiveArea file."%(Woff)
        print "Possible Woff values are:", ParameterValues[4]
        return None
          
    if float(noise) in ParameterValues[5]:
        noise_index = ParameterValues[5].index(float(noise))
        if __name__ == "__main__":
            print " noise index:", noise_index
    if float(noise) not in ParameterValues[5]:
        print "Chosen value noise = %s does not exist in EffectiveArea file."%(noise)
        print "Possible noise values are:", ParameterValues[5]
        return None
      
    if float(index) in ParameterValues[7]:
        index_index = ParameterValues[7].index(float(index))
        if __name__ == "__main__":
            print " index index:", index_index
    if float(index) not in ParameterValues[7]:
        print "Chosen value index = %s does not exist in EffectiveArea file."%(index)
        print "Possible index values are:", ParameterValues[7]
        return None
          
    if str(energy) == "A_MC":
        energy_index = 0
        if __name__ == "__main__":
            print " energy index:", energy_index 
    elif str(energy) == "A_REC":
        energy_index = 1
    else:
        print 'energy must be either "A_MC" or "A_REC"'
        return None
         
    AeffOverEnergy = AeffOverEnergyMatrix[ze_index][az_index][Woff_index][noise_index][index_index][energy_index]
     
    return AeffOverEnergy

###############################################################################

def makeAeffOverWoff(ParameterValues, AeffOverEnergyMatrix, Energy, ZE = 0.0, AZ = 0, NOISE = 75, INDEX = 2.0, ENERGY = "A_MC", CTAbool = False):
    # this extracts a vector of effective area over wobble offset for a given energy from the
    # full matrix and prepares (i.e., manipulates) the vector for fitting later. 
    WoffVector = ParameterValues[4]
    Woffnumbers = len(WoffVector)
     
    # do the following to cancel strange behavior that ParameterValues Matrix is changed by this function. Why???
    WoffValues = [] # = WoffVector
    for i in range(Woffnumbers):
        WoffValues.append(ParameterValues[4][i])    
    ParameterValues[4] = []
    for i in range(Woffnumbers):
        ParameterValues[4].append(WoffValues[i])
    
    WoffVector.append(WoffVector[-1]+0.25) # append value Woff = 2.25 (in the most cases)
    AeffOverWoffAllEbins = [[] for n in range(len(WoffVector))]
    
    last_i = 0
    WoffVectorEbin = []
    AeffOverWoffVector = []
    if CTAbool == False:
    # pick effective areas over energy and wobble offset (all other parameters fixed)
    # index i: loop over all wobble offset
        for i in range(Woffnumbers):
            AeffOverWoffAllEbins[i] = pickEffectiveArea(ParameterValues, AeffOverEnergyMatrix, ZE, AZ, ParameterValues[4][i], NOISE, INDEX, ENERGY)
             
            # i: wobble offset, 0: log energy vector (1: Aeff(logE)) 
            if Energy in AeffOverWoffAllEbins[i][0]:
                if __name__ == "__main__":
                    print i, " success!"
                energy_bin = AeffOverWoffAllEbins[i][0].index(Energy)
                last_i = last_i + 1
                #print WoffVector[i], AeffOverWoffAllEbins[i][1][energy_bin]
                WoffVectorEbin.append(float(WoffVector[i]))
                AeffOverWoffVector.append(float(AeffOverWoffAllEbins[i][1][energy_bin])) # j is energy bin!
         
        if last_i == 0:
            print "Chosen Energy bin value E = %s GeV does not exist in EffectiveArea file."%(Energy)
            print "Possible Energy bin values are:", AeffOverWoffAllEbins[0][0]
            print "EXIT (MoritzCTAtools.py, makeAeffOverWoff function)"
            return None
            sys.exit()
    
    if CTAbool == True:
        # for the CTA data, all energy bins have values for all Woffs!
        energy_bin = ParameterValues[8].index(Energy)
        for i in range(Woffnumbers):
            AeffOverWoffVector.append(float(AeffOverEnergyMatrix[energy_bin][i]))
            WoffVectorEbin.append(float(ParameterValues[4][i]))
        last_i = Woffnumbers
            
    # append the negative of the last value for the eff. area to improve fitting.
    # see also the iteration part below.
    AeffOverWoffVector.append(-AeffOverWoffVector[-1])
    # otherwise:
    #  AeffOverWoffVector.append(0.0)
    WoffVectorEbin.append(WoffVector[last_i])
    
    # do this here to improve speed for later doing many iterations fast:
    # So the parameters don't have to be calculated at each skymap pixel.
    popt, pcov = makeWobbleOffsetfitParameters(WoffVectorEbin, AeffOverWoffVector)

    return np.array(WoffVectorEbin), np.array(AeffOverWoffVector), ParameterValues, popt, pcov
    
###############################################################################

def makeWobbleOffsetfitParameters(WoffVectorEbin, AeffOverWoffVector):
    if len(WoffVectorEbin) < 3:
        print " too few Woff values to do polynomial fit. Do linear interpolation instead."
        popt = []
        pcov = []
    elif len(WoffVectorEbin) < 6:
        popt, pcov = curve_fit(fitfunction2, WoffVectorEbin, AeffOverWoffVector)
    elif len(WoffVectorEbin) < 8:
        popt, pcov = curve_fit(fitfunction4, WoffVectorEbin, AeffOverWoffVector)
    elif len(WoffVectorEbin) < 10:
        popt, pcov = curve_fit(fitfunction6, WoffVectorEbin, AeffOverWoffVector)
    else:
        popt, pcov = curve_fit(fitfunction10, WoffVectorEbin, AeffOverWoffVector)
    
    return popt, pcov

###############################################################################

def interPolateWobbleOffset(WoffVectorEbin, AeffOverWoffVector, Woff):
    # Interpolate with Splines:
    AeffOverWoffInterpolated_func = UnivariateSpline(WoffVectorEbin, AeffOverWoffVector, k=2)
    AeffOverWoffInterpolated = float(AeffOverWoffInterpolated_func(Woff))
    if AeffOverWoffInterpolated < 0.0:
        AeffOverWoffInterpolated = 0.0
    if Woff > WoffVectorEbin[-1]:
        AeffOverWoffInterpolated = 0.0    
    return AeffOverWoffInterpolated

###############################################################################

def fitfunction14(x, a, b, c, d, e, f, g, h):
    # this polynomial seems to be the best choice for n=9 points: for higher powers, the result
    # the result starts to fluctuate.
    return h * x**14 + g * x**12 + f * x**10 + e * x**8 + d * x**6 + c * x**4 + b * x**2 + a

def fitfunction12(x, a, b, c, d, e, f, g):
    return g * x**12 + f * x**10 + e * x**8 + d * x**6 + c * x**4 + b * x**2 + a

def fitfunction10(x, a, b, c, d, e, f):
    # this polynomial seems to be the best choice for n=9 points: for higher powers, the result
    # the result starts to fluctuate.
    return f * x**10 + e * x**8 + d * x**6 + c * x**4 + b * x**2 + a

def fitfunction8(x, a, b, c, d, e):
    return e * x**8 + d * x**6 + c * x**4 + b * x**2 + a

def fitfunction6(x, a, b, c, d):
    # this polynomial seems to be the best choice for n<8 points: for higher powers, the result
    # starts to fluctuate.
    return d * x**6 + c * x**4 + b * x**2 + a

def fitfunction4(x, a, b, c):
    # this polynomial seems to be the best choice for n<8 points: for higher powers, the result
    # starts to fluctuate.
    return c * x**4 + b * x**2 + a

def fitfunction2(x, a, b):
    # this polynomial seems to be the best choice for n<8 points: for higher powers, the result
    # starts to fluctuate.
    return b * x**2 + a

###############################################################################

def fitWobbleOffset(WoffVectorEbin, AeffOverWoffVector, popt, Woff, iteration = 0,CTAbool=False):
    if CTAbool == False:
        if WoffVectorEbin[0] > 0.2:
            print " no VTS effective area for wobble offset = 0."
            print "EXIT (MoritzCTAtools.py, fitWobbleOffset function)"
            sys.exit()
    elif CTAbool == True:
        if WoffVectorEbin[0] > 0.6:
            print " no CTA effective area for wobble offset = 0."
            print "EXIT (MoritzCTAtools.py, fitWobbleOffset function)"
            sys.exit()
    
    if len(WoffVectorEbin) < 3:
        AeffOverWoffInterpolated_func = interp1d(WoffVectorEbin, AeffOverWoffVector, kind='linear')
        if Woff > WoffVectorEbin[-1]:
            AeffOverWoffFitted = 0.0
        else:
            AeffOverWoffFitted = AeffOverWoffInterpolated_func(Woff)
    # Fit:
    elif len(WoffVectorEbin) < 6:
        AeffOverWoffFitted = fitfunction2(Woff, popt[0], popt[1])
    elif len(WoffVectorEbin) < 8:
        AeffOverWoffFitted = fitfunction4(Woff, popt[0], popt[1], popt[2]) 
    elif len(WoffVectorEbin) < 10:
        AeffOverWoffFitted = fitfunction6(Woff, popt[0], popt[1], popt[2], popt[3])
    else:
        AeffOverWoffFitted = fitfunction10(Woff, popt[0], popt[1], popt[2], popt[3], popt[4], popt[5])
        
    if AeffOverWoffFitted < 0.0:
        AeffOverWoffFitted = 0.0
    if Woff > WoffVectorEbin[-1]:
        AeffOverWoffFitted = 0.0
    if Woff > WoffVectorEbin[-2]:
        if AeffOverWoffFitted > AeffOverWoffVector[-2]:
            if __name__ == "__main__":
                print " Iteration",iteration
            max_iteration = len(AeffOverWoffVector)-2
            if max_iteration > 5:
                max_iteration = 5
            if iteration < max_iteration:
                backindex = iteration + 1
                iteration = iteration + 1
                AeffOverWoffVector[-1] = - AeffOverWoffVector[-backindex]
                popt, pcov = makeWobbleOffsetfitParameters(WoffVectorEbin, AeffOverWoffVector)
                # recursion!
                AeffOverWoffFitted, iteration = fitWobbleOffset(WoffVectorEbin, AeffOverWoffVector, popt, Woff, iteration,CTAbool)
            #AeffOverWoffFitted = AeffOverWoffVector[-2]    
    return AeffOverWoffFitted, iteration


###############################################################################
#####    big function to load background rates:    ############################

def loadBackgroundRates(sensitivityRootFile):
    sensitivityRootFile = str(sensitivityRootFile)
    sensitivityFile = ROOT.TFile(sensitivityRootFile)
     
    backgroundRatesHist = sensitivityFile.Get("BGRatePerSqDeg_offaxis")
        
    energyBinNumber = backgroundRatesHist.GetNbinsX()+2
    energyBinVector = np.zeros(energyBinNumber)
     
    WoffBinNumber = backgroundRatesHist.GetNbinsY()+1
    WoffBinVector = np.zeros(WoffBinNumber-1)

    for i in range(0,energyBinNumber):
        energyBinVector[i] = float(backgroundRatesHist.GetXaxis().GetBinCenter(i))
    for i in range(1,WoffBinNumber):
        WoffBinVector[i-1] = float(backgroundRatesHist.GetYaxis().GetBinCenter(i))
     
    ParameterValues = [[] for i in range(2)]
    ParameterValues[0] = energyBinVector
    ParameterValues[1] = WoffBinVector
     
    backgroundRatesMatrix = np.zeros((energyBinNumber,WoffBinNumber-1))
    for i in range(energyBinNumber):
        for j in range(1,WoffBinNumber):        
            index = j * energyBinNumber + i
            backgroundRatesMatrix[i,j-1] = backgroundRatesHist.GetBinContent(index)
            # correct for unsteady root file data:
            if j > 1:
                if backgroundRatesMatrix[i,j-1] > backgroundRatesMatrix[i,j-2]:
                    backgroundRatesMatrix[i,j-1] = backgroundRatesMatrix[i,j-2]
                    if j > 2:
                        backgroundRatesMatrix[i,j-1] = 2 * backgroundRatesMatrix[i,j-2] - backgroundRatesMatrix[i,j-3]
                        if 2 * backgroundRatesMatrix[i,j-2] - backgroundRatesMatrix[i,j-3] < 0:
                            backgroundRatesMatrix[i,j-1] = backgroundRatesMatrix[i,j-2]
    return backgroundRatesMatrix, ParameterValues

    #print backgroundRatesHist.FindBin(0.15,-1.6)
    #print backgroundRatesHist.GetBinCenter(124)
    #print backgroundRatesHist.GetBin(2,0)

def interpolateBackgroundRates(backgroundRatesMatrix, ParameterValues, IRFenergyBinVector):
    IRFenergyBinNumber = len(IRFenergyBinVector) # with energies in GeV:
    IRFlogEnergyBinVector = np.zeros(IRFenergyBinNumber)
    for i in range(IRFenergyBinNumber):
        IRFlogEnergyBinVector[i] = round(np.log10(IRFenergyBinVector[i]/1000.0),2)
    WoffBinNumber = len(ParameterValues[1])
    backgroundRatesMatrixIRFbins = np.zeros((IRFenergyBinNumber,WoffBinNumber))
    
    for j in range(WoffBinNumber):
        backgroundRateOverEnergy_func = interp1d(ParameterValues[0], backgroundRatesMatrix[:,j], kind='linear')
        for i in range(IRFenergyBinNumber):
            backgroundRateOverEnergyInterpolated = backgroundRateOverEnergy_func(IRFlogEnergyBinVector[i])
            backgroundRatesMatrixIRFbins[i,j] = backgroundRateOverEnergyInterpolated
    return backgroundRatesMatrixIRFbins

def prepareBackgroundRateOverWoffForFitting(WoffVectorEbin, backgroundRateOverWoffVector):
    # this prepares (manipulates) the vectors WoffVectorEbin (x values) and
    # backgroundRateOverWoffVector (y values) for fitting. 
    WoffVectorEbin =  list(WoffVectorEbin)
    backgroundRateOverWoffVector = list(backgroundRateOverWoffVector)
    while backgroundRateOverWoffVector[-2] < 1e-4:
        if len(backgroundRateOverWoffVector) > 2:
            del backgroundRateOverWoffVector[-1]
            del WoffVectorEbin[-1]
        else:
            break
    DeltaWoff = WoffVectorEbin[-1] - WoffVectorEbin[-2]
    WoffVectorEbin.append(WoffVectorEbin[-1] + DeltaWoff)
    backgroundRateOverWoffVector.append(-backgroundRateOverWoffVector[-1])
    popt, pcov = makeWobbleOffsetfitParameters(WoffVectorEbin, backgroundRateOverWoffVector)
    return np.array(WoffVectorEbin), np.array(backgroundRateOverWoffVector), popt, pcov

def loadAngularResolution(sensitivityRootFile):
    sensitivityRootFile = str(sensitivityRootFile)
    sensitivityFile = ROOT.TFile(sensitivityRootFile)
     
    angularResolutionHist = sensitivityFile.Get("AngRes")
    energyBinNumber = angularResolutionHist.GetNbinsX()+2
    energyBinVector = np.zeros(energyBinNumber)
    angularResolutionOverEnergyVector = np.zeros(energyBinNumber)
    
    for i in range(0,energyBinNumber):
        energyBinVector[i] = float(angularResolutionHist.GetXaxis().GetBinCenter(i))
        
    for i in range(energyBinNumber):   
        angularResolutionOverEnergyVector[i] =  angularResolutionHist.GetBinContent(i)
        j = 0

        if i >= energyBinNumber-2:
            angularResolutionOverEnergyVector[i] = angularResolutionOverEnergyVector[i-1]
        while angularResolutionOverEnergyVector[i] == 0.0:
            j = j + 1
            angularResolutionOverEnergyVector[i] =  angularResolutionHist.GetBinContent(i+j)
            if j == 500:
                print " stop angularResolutionOverEnergyVector iteration. Got lost in loop."
                break
        
    return energyBinVector, angularResolutionOverEnergyVector

def interpolateAngularResolution(angularResolutionOverEnergyVector, angResEnergyBinVector, IRFenergyBinVector):
    IRFenergyBinNumber = len(IRFenergyBinVector) # with energies in GeV:
    IRFlogEnergyBinVector = np.zeros(IRFenergyBinNumber)
    for i in range(IRFenergyBinNumber):
        IRFlogEnergyBinVector[i] = round(np.log10(IRFenergyBinVector[i]/1000.0),2)
        
    angularResolutionOverEnergyVectorIRFbins = np.zeros(IRFenergyBinNumber)
    angularResolutionOverEnergy_func = interp1d(angResEnergyBinVector, angularResolutionOverEnergyVector, kind='linear')
    for i in range(IRFenergyBinNumber):
        angularResolutionOverEnergyInterpolated = angularResolutionOverEnergy_func(IRFlogEnergyBinVector[i])
        angularResolutionOverEnergyVectorIRFbins[i] = angularResolutionOverEnergyInterpolated
    return angularResolutionOverEnergyVectorIRFbins
    
    
###############################################################################
##    healpix and spherical calculations related functions:    ################
###############################################################################

# algorithm to determine NSIDE as function of alphaInt, the resolution on
# a rectangular grid:  
def alphaInt2nside(alphaInt): 
    n = 0 
    resolution = np.pi
    while resolution > alphaInt:
        n = n + 1   
        nside = 2 ** n
        resolution = hp.nside2resol(nside) * 2.0 / np.sqrt(np.pi)  # compare with healpix documentation: this calculation gives the full diameter of a circular assumed region.
    nside = 2 ** (n - 1)
    return nside

def angularDistance(theta0, phi0, theta1, phi1):
    # theta in [0,pi]
    # phi in [0,2pi]
    distance = np.arccos(np.cos(theta0)*np.cos(theta1) + np.sin(theta0)*np.sin(theta1)*np.cos(phi1-phi0))
    return distance

##    statistical calculations related functions:    ##########################
###############################################################################

# significance acc. to eq. 17 of Li and Ma (1983):
def liAndMaSignificance(Non, Noff, alpha):
    alpha = float(alpha)
    significance = np.sqrt(2) * np.sqrt( Non * np.log( (1+alpha)/alpha * Non/(Non + Noff) ) + Noff * np.log( (1+alpha) * Noff/(Non + Noff) ) )
    return significance

##    main part:   ############################################################
###############################################################################

def main(argv):
    inputBGfile = []
    found_i = False
    found_o = False
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:o:",["inputfile=","outputfile="])
    except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'Wrong input option. The input options are:'
        print ' -i <inputfile>'
        print ' -o <outputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
        # help option
            print 'The input options are:'
            print ' -i <inputfile>'
            print ' -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--inputfile"):
            inputfile = str(arg)
            found_i = True
        elif opt in ("-o", "--outputfile"):
            outputfile = str(arg)
            found_o = True
            
    if not found_i:
        print "Required option -i was not given"
        #sys.exit(2)
        inputIRFfile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v430/EffectiveAreas/effArea-d20131031-cut-N3-Point-005CU-Moderate-ATM21-V6-T1234-d20131115.root"
        #inputBGfile = "/afs/ifh.de/group/cta/scratch/mhuetten/CTA/EffectiveAreasCTA/DESY.d20140309.Erec1.R2.ID0NIM2.prod2-LeoncitoPP-NS.S.2a.180000s.root"        
        #inputBGfile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v4XX/VTS.sensitivity.V6.moderate.root"
        #inputIRFfile = inputBGfile
    if not found_o:
        print "Required option -o was not given. No output file will be written."
        #sys.exit(2)

#     # main part for testing CTA effective area file loading:
    if "CTA" in inputIRFfile or "CTA" in inputBGfile:
        CTAbool = True
        print " IRF file detected as CTA IRF file..."
    elif "VTS" in inputIRFfile:
        CTAbool = False
        print " IRF file detected as VTS IRF file..."
        
# #    main part for testing CTa effective area loading: 
#     IRFParameterValues, AeffOverEnergyMatrix = loadEffectiveAreas(inputIRFfile,CTAbool)
#     #print AeffOverEnergyMatrix
#     print IRFParameterValues
#   
#     ze = 20.0
#     az = 2
#     noise = 100
#     index = 2.6
#   
#     print IRFParameterValues[8]
#     chosen_energy = 113  
#     print "ParameterValues no1:", IRFParameterValues[4]
#     print "ParameterValues no2:", IRFParameterValues[4]
#     print "chosen energy:", chosen_energy, "GeV"
#   
#     IRFenergyBinVector = IRFParameterValues[8]
#     for i in range(3,len(IRFenergyBinVector)):
#         WoffVectorEbin, AeffOverWoffVector, IRFParameterValues, popt, cov = makeAeffOverWoff(IRFParameterValues, AeffOverEnergyMatrix, IRFenergyBinVector[i], ze, az, noise, index, "A_MC", CTAbool)
#    
#         xs = np.linspace(0, WoffVectorEbin[-1], 200)
#         ys = np.zeros(len(xs))
#         for k in range(len(xs)):
#             ys[k], iteration = fitWobbleOffset(WoffVectorEbin, AeffOverWoffVector, popt, xs[k], 0, CTAbool)
#         AeffOverWoff_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_bin"+str(i)+".p"
#         AeffOverWoff_smooth_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_smooth_bin"+str(i)+".p"
#         pickle.dump( [xs, ys], open( AeffOverWoff_smooth_outputfile, "wb" ) )
#         pickle.dump( [WoffVectorEbin, AeffOverWoffVector], open( AeffOverWoff_outputfile, "wb" ) )
  
    
#     # main part for testing sensitivity file loading:
#   
#     IRFParameterValues, AeffOverEnergyMatrix = loadEffectiveAreas(inputIRFfile,CTAbool)
#     IRFenergyBinVector = IRFParameterValues[8]
#         
#     AngResLogEnergyBinVector, angularResolutionOverEnergyVector = loadAngularResolution(inputBGfile)
#     angularResolutionOverEnergyVectorIRFbins = interpolateAngularResolution(angularResolutionOverEnergyVector, AngResLogEnergyBinVector, IRFenergyBinVector)
#     print angularResolutionOverEnergyVectorIRFbins
#        
#     backgroundRatesMatrix, BGParameterValues = loadBackgroundRates(inputBGfile)
#     
#     print BGParameterValues
#     backgroundRatesMatrixIRFbins = interpolateBackgroundRates(backgroundRatesMatrix, BGParameterValues, IRFenergyBinVector)
#     #backgroundRatesMatrixIRFbins = (180.0/np.pi)**2 * backgroundRatesMatrixIRFbins
#         
#     
#     print backgroundRatesMatrix[:,0]
#     print backgroundRatesMatrixIRFbins[:,0]
#     
#         
#         
#     for i in range(len(IRFenergyBinVector)):
#         WoffVectorEbin =  BGParameterValues[1]
#         backgroundRateOverWoffVector = backgroundRatesMatrixIRFbins[i,:]
#         WoffVectorEbin, backgroundRateOverWoffVector, popt, pcov = prepareBackgroundRateOverWoffForFitting(WoffVectorEbin, backgroundRateOverWoffVector)
#         xs = np.linspace(0, 6, 200)
#         ys = np.zeros(len(xs))
#         for k in range(len(xs)):
#             ys[k], iteration = fitWobbleOffset(WoffVectorEbin, backgroundRateOverWoffVector, popt, xs[k],0, CTAbool)
#         backgroundRateOverWoff_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/backgroundRateOverWoff_bin"+str(i)+".p"
#         backgroundRateOverWoff_smooth_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/backgroundRateOverWoff_smooth_bin"+str(i)+".p"
#         pickle.dump( [xs, ys], open( backgroundRateOverWoff_smooth_outputfile, "wb" ) )
#         pickle.dump( [WoffVectorEbin, backgroundRateOverWoffVector], open( backgroundRateOverWoff_outputfile, "wb" ) )
#      
#         print "loop success!"
        
        
        
# main part for testing effective area file loading:    
    ParameterValues, AeffOverEnergyMatrix = loadEffectiveAreas(inputIRFfile)
     
    ze = 20.0
    az = 2
    noise = 100
    index = 2.6
    chosen_energy =  ParameterValues[8][12]
     
    print ParameterValues[8]
     
    print "ParameterValues no1:", ParameterValues[4]
    WoffVectorEbin, AeffOverWoffVector, ParameterValues, popt, cov = makeAeffOverWoff(ParameterValues, AeffOverEnergyMatrix, chosen_energy,  ze, az, noise, index, "A_MC")
    print "ParameterValues no2:", ParameterValues[4]
    print "chosen energy:", chosen_energy, "GeV"
    
    print "az Bin:", ParameterValues[1]
    print "azMin:", ParameterValues[2]
    print "azMax:", ParameterValues[3]
    print "noise:", ParameterValues[5]
    print "pedvar:", ParameterValues[6] 
    xs = np.linspace(0, WoffVectorEbin[-1], 200)
    ys = np.zeros(len(xs))
    for k in range(len(xs)):
        ys[k], iteration = fitWobbleOffset(WoffVectorEbin, AeffOverWoffVector, popt, xs[k])
    AeffOverWoff_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff.p"
    AeffOverWoff_smooth_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_smooth.p"
    pickle.dump( [xs, ys], open( AeffOverWoff_smooth_outputfile, "wb" ) )
    pickle.dump( [WoffVectorEbin, AeffOverWoffVector], open( AeffOverWoff_outputfile, "wb" ) )
      
    AeffOverEnergyAllWoff = []
    for bla in range(len(ParameterValues[4])):
        AeffOverEnergy = pickEffectiveArea(ParameterValues, AeffOverEnergyMatrix, ze, az, ParameterValues[4][bla], noise, index, "A_MC")
        AeffOverEnergyAllWoff.append(AeffOverEnergy)
    AeffOverEnergyAllWoff_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverEnergyAllWoff.p"
    WoffVector_outputfile = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/WoffVector.p"
    pickle.dump( AeffOverEnergyAllWoff, open( AeffOverEnergyAllWoff_outputfile, "wb" ) )
    pickle.dump( ParameterValues[4], open( WoffVector_outputfile, "wb" ) )
     
    # save output file: 
    if found_o:
        print "save 6-dim. matrix to file:"                  
        pickle.dump( AeffOverEnergyAllWoff, open( outputfile, "wb" ) )
     
    print ""
    print "finished"

##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    from matplotlib import pyplot as plt
    import pylab
    from textwrap import wrap
    
    main(sys.argv[1:])
    
##    end of file    ##########################################################
###############################################################################
    