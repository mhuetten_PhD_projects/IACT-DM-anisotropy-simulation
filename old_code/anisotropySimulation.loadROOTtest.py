import ROOT
import numpy as np
from root_numpy import root2array, root2rec, tree2rec
from root_numpy.testdata import get_filepath
import ast
import decimal
from matplotlib import pyplot as plt
import pylab
import pickle
from textwrap import wrap

print "load file:"                    
fEffAreaPYTHON = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/fEffAreaPYTHON.p", "rb" ) )

 
AeffOverE0_0 = fEffAreaPYTHON[0][0][0][0][0][0]
AeffOverE0_1 = fEffAreaPYTHON[0][0][1][0][0][0]
AeffOverE0_2 = fEffAreaPYTHON[0][0][2][0][0][0]
AeffOverE0_3 = fEffAreaPYTHON[0][0][3][0][0][0]
AeffOverE0_4 = fEffAreaPYTHON[0][0][4][0][0][0]
AeffOverE0_5 = fEffAreaPYTHON[0][0][5][0][0][0]
AeffOverE0_6 = fEffAreaPYTHON[0][0][6][0][0][0]
AeffOverE0_7 = fEffAreaPYTHON[0][0][7][0][0][0]
AeffOverE0_8 = fEffAreaPYTHON[0][0][8][0][0][0]

plot1 = plt.figure(figsize=(12, 10))
p0 = plt.plot(AeffOverE0_0[0,:],AeffOverE0_0[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 0.0 degs.") 
p1 = plt.plot(AeffOverE0_1[0,:],AeffOverE0_1[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 0.25 degs.")
p2 = plt.plot(AeffOverE0_2[0,:],AeffOverE0_2[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 0.5 degs.")
p3 = plt.plot(AeffOverE0_3[0,:],AeffOverE0_3[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 0.75 degs.")
p4 = plt.plot(AeffOverE0_4[0,:],AeffOverE0_4[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 1.0 degs.")
p5 = plt.plot(AeffOverE0_5[0,:],AeffOverE0_5[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 1.25 degs.")
p6 = plt.plot(AeffOverE0_6[0,:],AeffOverE0_6[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 1.5 degs.")
p7 = plt.plot(AeffOverE0_7[0,:],AeffOverE0_7[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 1.75 degs.")
p8 = plt.plot(AeffOverE0_8[0,:],AeffOverE0_8[1,:],alpha=1.,linestyle='-',marker='o', label="Woff = 2.0 degs.")
plt.xlabel('log (energy/1 TeV)'); plt.ylabel('Aeff [m^2]'); plt.grid()
#plt.xscale('log')
plt.yscale('log')
plt.legend(loc='lower right')
title = 'Effective areas A_MC for ze = 0.0, az = 1, noise = 75, spectral index = 2.0'
plt.title('\n'.join(wrap(title,90)))
plot2 = plt.figure(figsize=(12, 10))
p0 = plt.plot([0.0,0.25,0.5,0.75,1.0,1.25,1.5,1.75,2.0],[AeffOverE0_0[1,16],AeffOverE0_1[1,16],AeffOverE0_2[1,16],AeffOverE0_3[1,16],AeffOverE0_4[1,16],AeffOverE0_5[1,16],AeffOverE0_6[1,16],AeffOverE0_7[1,16],AeffOverE0_8[1,16]],alpha=1.,linestyle='-',marker='o', label="extrapolation\n(Abdo et al., 2010)")
plt.xlabel('wobble offset from the camera center in degs.'); plt.ylabel('Aeff [m^2]'); plt.grid()
title = 'Effective areas A_MC for ze = 0.0, az = 1, noise = 75, spectral index = 2.0 at 350 GeV'
plt.title('\n'.join(wrap(title,90)))
print AeffOverE0_0[0,:]
print AeffOverE0_0[1,:]
print AeffOverE0_8[1,:]

plt.show()
print ""
print "finished"