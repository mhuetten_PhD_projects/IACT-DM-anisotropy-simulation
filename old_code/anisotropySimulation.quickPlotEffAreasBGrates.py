print "initializing..."

import ROOT
import numpy as np
from root_numpy import root2array, root2rec, tree2rec
from root_numpy.testdata import get_filepath
import ast
import decimal
from matplotlib import pyplot as plt
import pylab
import pickle
from textwrap import wrap
import MoritzCTAtools
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from matplotlib import rc,rcParams
from matplotlib.ticker import FormatStrFormatter
rc('text',usetex=True)
rcParams.update({'font.size': 26})

# load effective area plots:

print "load file:"                    
AeffOverEnergyAllWoff = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverEnergyAllWoff.p", "rb" ) )
WoffVector = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/WoffVector.p", "rb" ) )
 
AeffOverWoff = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff.p", "rb" ) )
AeffOverWoff_smooth = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_smooth.p", "rb" ) )
 
WoffVector.append(2.25)
 
print "WoffVector 1st", WoffVector
print "AeffOverEnergyAllWoff[0]", AeffOverEnergyAllWoff[0] 
print "AeffOverEnergyAllWoff[1]", AeffOverEnergyAllWoff[1]
  
WoffVector.append(2.25)
print "WoffVector 2nd", WoffVector
 
 
# plot1 = plt.figure(figsize=(12, 8))
# for i in range(len(AeffOverEnergyAllWoff[:])):
#     EnergyVector = AeffOverEnergyAllWoff[i][0][:]
#     AeffVector = AeffOverEnergyAllWoff[i][1][:]
#     plt.plot(EnergyVector,AeffVector,alpha=1.,linestyle='-',marker='o', label="Woff = %s degs."%(WoffVector[i]))
#  
# plt.xlabel('energy (GeV)'); plt.ylabel('Aeff [m^2]'); plt.grid()
# plt.xscale('log')
# plt.yscale('log')
# plt.legend(loc='lower right')
# title = 'Effective areas A_MC'
# plt.title('\n'.join(wrap(title,90)))
# pylab.xlim([10,10**6])
#  
plot2 = plt.figure(figsize=(14, 8)) 
# loop over all energy bins for the longest vector:
#for j in range(len(AeffOverEnergyAllWoff[0][0,:])):
for j in range(4,16,2):
    Energy = AeffOverEnergyAllWoff[0][0][j]
  
# new:
  
    WoffVectorEbin = []
    OffsetVector = []
    # now loop over all wobble offsets:
    last_i = 0
    for i in range(len(AeffOverEnergyAllWoff[:])):    
        # check if energy bin is populated for the corresponding wobble offset:
        if Energy in AeffOverEnergyAllWoff[i][0][:]:
            energy_bin = AeffOverEnergyAllWoff[i][0].index(Energy)
            print Energy, "GeV", "bin:", energy_bin, "Offset:", WoffVector[i]
            last_i = last_i + 1
            WoffVectorEbin.append(WoffVector[i])
            OffsetVector.append(AeffOverEnergyAllWoff[i][1][energy_bin]) # j is energy bin!
    OffsetVector.append(-OffsetVector[-1])
    #OffsetVector.append(0.0)
    WoffVectorEbin.append(WoffVector[last_i])
    print OffsetVector
    print WoffVectorEbin
    print "last_i: ", last_i
     
    xs = np.linspace(0, WoffVector[last_i], 300)
    ys = np.zeros(len(xs))
    print len(WoffVectorEbin)
  
    popt, pcov = MoritzCTAtools.makeWobbleOffsetfitParameters(WoffVectorEbin, OffsetVector)
                  
    for k in range(len(xs)):
        ys[k], iteration =  MoritzCTAtools.fitWobbleOffset(WoffVectorEbin, OffsetVector, popt, xs[k], iteration = 0)
 
 
                ##   old:
                      
                #     WoffVectorEbin = []
                #     OffsetVector = []
                #     # now loop over all wobble offsets:
                #     last_i = 0
                #     for i in range(len(AeffOverEnergyAllWoff[:])):    
                #         # check if energy bin is populated for the corresponding wobble offset:
                #         if Energy in AeffOverEnergyAllWoff[i][0][:]:
                #             energy_bin = AeffOverEnergyAllWoff[i][0].index(Energy)
                #             print Energy, "GeV", "bin:", energy_bin, "Offset:", WoffVector[i]
                #             last_i = last_i + 1
                #             WoffVectorEbin.append(WoffVector[i])
                #             OffsetVector.append(AeffOverEnergyAllWoff[i][1][energy_bin]) # j is energy bin!
                #     OffsetVector.append(-OffsetVector[-1])
                #     #OffsetVector.append(0.0)
                #     WoffVectorEbin.append(WoffVector[last_i])
                #     print OffsetVector
                #     print WoffVectorEbin
                #     print "last_i: ", last_i
                #   
                #     xs = np.linspace(0, WoffVector[last_i], 100)
                #     ys = np.zeros(len(xs))
                #     print len(WoffVectorEbin)
                #     if len(WoffVectorEbin) < 5:
                #         AeffOverWoffInterpolated_func = interp1d(WoffVectorEbin, OffsetVector, kind='linear')
                #         for k in range(len(xs)):
                #             if AeffOverWoffInterpolated_func(xs[k]) > 0:
                #                 ys[k] = AeffOverWoffInterpolated_func(xs[k])
                #     elif len(WoffVectorEbin) < 8:
                #         print "ghhj"
                #         popt, pcov = curve_fit(MoritzCTAtools.fitfunction4, WoffVectorEbin, OffsetVector)
                #         for k in range(len(xs)):
                #             if MoritzCTAtools.fitfunction4(xs[k], popt[0], popt[1], popt[2]) > 0:
                #                 ys[k] = MoritzCTAtools.fitfunction4(xs[k], popt[0], popt[1], popt[2])
                #     elif len(WoffVectorEbin) < 10:
                #         print "dgsfs"
                #         popt, pcov = curve_fit(MoritzCTAtools.fitfunction8, WoffVectorEbin, OffsetVector)
                #         for k in range(len(xs)):
                #             if MoritzCTAtools.fitfunction8(xs[k], popt[0], popt[1], popt[2], popt[3], popt[4]) > 0:
                #                 ys[k] = MoritzCTAtools.fitfunction8(xs[k], popt[0], popt[1], popt[2], popt[3], popt[4])
                #     else:
                #         print "lolo"
                #         popt, pcov = curve_fit(MoritzCTAtools.fitfunction12, WoffVectorEbin, OffsetVector)
                #         for k in range(len(xs)):
                #             if MoritzCTAtools.fitfunction12(xs[k], popt[0], popt[1], popt[2], popt[3], popt[4], popt[5], popt[6]) > 0:
                #                 ys[k] = MoritzCTAtools.fitfunction12(xs[k], popt[0], popt[1], popt[2], popt[3], popt[4], popt[5], popt[6])
                #     for k in range(len(xs)):
                #         if xs[k] > WoffVectorEbin[-2]:
                #             if ys[k] > OffsetVector[-2]:
                #                 ys[k] = OffsetVector[-2] 
                     
                     
                     
                 
                #     #Interpolation:
                #     
                #      s = UnivariateSpline(WoffVectorEbin, OffsetVector, k=2)
                #      xs = np.linspace(0, WoffVector[last_i], 100)
                #      ys = np.zeros(len(xs))
                #      for k in range(len(xs)):
                #          if s(xs[k]) > 0:
                #              ys[k] = s(xs[k])
                #     
                #     
                #             
    #plt.plot(WoffVectorEbin[:-1],OffsetVector[:-1],alpha=1.,linestyle=' ',marker='o', color='black')
    plt.plot(xs,ys, alpha=1., linestyle='-', linewidth = '1.5', label='VERITAS radial profile at %s GeV'%(Energy))
    del OffsetVector 
       
plt.xlabel(' offset from the pointing direction (camera center)'); plt.ylabel('$A_{eff}\;[\mathrm{m}^2]$'); plt.grid()
#title = 'Effective areas A_MC  at %s GeV'%(Energy)
plt.legend(loc='upper right',prop={'size':20})
#plt.title('\n'.join(wrap(title,90)))
pylab.xlim([0,2.2])
#ticks=(["$0^{\circ}$","$0.5^{\circ}$","$1.0^{\circ}$","$1.5^{\circ}$","$2.0^{\circ}$"])
#plt.xticks(range(len(ticks)), ticks)
pylab.xticks([0,0.5,1.0,1.5, 2.0], ["$0^{\circ}$","$0.5^{\circ}$","$1.0^{\circ}$","$1.5^{\circ}$","$2.0^{\circ}$"])
#plt.yscale('log')
plt.gca().set_ylim(bottom=0)
plt.savefig("/afs/ifh.de/user/m/mhuetten/hu-data/Promotion Dokumente/Grafiken/Plots-eigene/VTSeffectiveAreas.png", dpi = 300)
 
 
# plot3 = plt.figure(figsize=(12, 10))
# plt.plot(AeffOverWoff[0][:-1], AeffOverWoff[1][:-1], alpha=1.,linestyle=' ',marker='o', color='black', )
# plt.plot(AeffOverWoff_smooth[0], AeffOverWoff_smooth[1], alpha=1., linestyle='-', linewidth = '1.5', label='profile at x GeV')
# plt.xlabel('wobble offset from the camera center in degs.'); plt.ylabel('Aeff [m^2]'); plt.grid()
# pylab.ylim([0,10**2])
# pylab.gca().set_ylim(bottom=0)


# #load background rateplots:
# plot3 = plt.figure(figsize=(12, 10))
# print "load file:"                    
# for i in range(21,25): 
#     #Energy = AeffOverEnergyAllWoff[0][0][i]
#     backgroundRateOverWoff = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/backgroundRateOverWoff_bin"+str(i-1)+".p", "rb" ) )
#     backgroundRateOverWoff_smooth = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/backgroundRateOverWoff_smooth_bin"+str(i)+".p", "rb" ) )
#     plt.plot(backgroundRateOverWoff[0][:-1], backgroundRateOverWoff[1][:-1], alpha=1.,linestyle=' ',marker='o', color='black', )
#     plt.plot(backgroundRateOverWoff_smooth[0], backgroundRateOverWoff_smooth[1], alpha=1., linestyle='-', linewidth = '1.5', label='profile at bin %s '%(i-1))
# plt.xlabel('wobble offset from the camera center in degs.'); plt.ylabel('rate [s-1 sr^-1]'); plt.grid()
# plt.legend(loc='upper right')


# #load CTA effective area plots:
# plot3 = plt.figure(figsize=(12, 10))
# print "load file:"                    
# for i in range(5,15): 
#     #Energy = AeffOverEnergyAllWoff[0][0][i]
#     effAreaOverWoff = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_bin"+str(i)+".p", "rb" ) )
#     effAreaOverWoff_smooth = pickle.load( open( "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/AeffOverWoff_smooth_bin"+str(i)+".p", "rb" ) )
#     plt.plot(effAreaOverWoff[0][:-1], effAreaOverWoff[1][:-1], alpha=1.,linestyle=' ',marker='o', color='black', )
#     plt.plot(effAreaOverWoff_smooth[0], effAreaOverWoff_smooth[1], alpha=1., linestyle='-', linewidth = '1.5', label='profile at bin %s'%(i))
# plt.xlabel('wobble offset from the camera center in degs.'); plt.ylabel('rate [s-1 sr^-1]'); plt.grid()
# plt.legend(loc='upper right')



plt.show()
print ""
print "finished"