#!/usr/bin/python

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################

##    import modules:    ######################################################

import numpy as np
from matplotlib import pyplot as plt
import pylab
from textwrap import wrap



def PlotEffectiveArea(ParameterValues, AeffOverEnergyMatrix, ze = 0.0, az = 0, Woff = 0.0, noise = 75, index = 2.0, energy = "A_MC"):
   
    if float(ze) in ParameterValues[0]:
        ze_index = ParameterValues[0].index(float(ze))
        print "ze index:", ze_index
    if float(ze) not in ParameterValues[0]:
        print "Chosen value ze = %s does not exist in EffectiveArea file."%(ze)
        print "Possible ze values are:", ParameterValues[0]
        return None
         
    if float(az) in ParameterValues[1]:
        az_index = ParameterValues[1].index(float(az))
        print "az index:", az_index
    if float(az) not in ParameterValues[1]:
        print "Chosen value az = %s does not exist in EffectiveArea file."%(az)
        print "Possible az values are:", ParameterValues[1]
        return None
         
    if float(Woff) in ParameterValues[4]:
        Woff_index = ParameterValues[4].index(float(Woff))
        print "Woff index:", Woff_index
    if float(Woff) not in ParameterValues[4]:
        print "Chosen value Woff = %s does not exist in EffectiveArea file."%(Woff)
        print "Possible Woff values are:", ParameterValues[4]
        return None
         
    if float(noise) in ParameterValues[5]:
        noise_index = ParameterValues[5].index(float(noise))
        print "noise index:", noise_index
    if float(noise) not in ParameterValues[5]:
        print "Chosen value noise = %s does not exist in EffectiveArea file."%(noise)
        print "Possible noise values are:", ParameterValues[5]
        return None
     
    if float(index) in ParameterValues[7]:
        index_index = ParameterValues[7].index(float(index))
        print "index index:", index_index
    if float(index) not in ParameterValues[7]:
        print "Chosen value index = %s does not exist in EffectiveArea file."%(index)
        print "Possible index values are:", ParameterValues[7]
        return None
         
    if str(energy) == "A_MC":
        energy_index = 0
        print "energy index:", energy_index 
    elif str(energy) == "A_REC":
        energy_index = 1
    else:
        print 'energy must be either "A_MC" or "A_REC"'
        return None
#         
    AeffOverEnergy = AeffOverEnergyMatrix[ze_index][az_index][Woff_index][noise_index][index_index][energy_index]     
    #AeffOverEnergy = AeffOverEnergyMatrix[0][0][0][0][0][0]
    
    EnergyVector = AeffOverEnergy[0,:] 
    AeffVector = AeffOverEnergy[1,:]  
#     print EnergyVector
    EnergyVector = 10**EnergyVector    
#     print EnergyVector
#      

    
    plot = plt.figure() 
    plt.plot(EnergyVector, AeffVector, color='gray', alpha=1., marker='o')
    plt.xlabel('log (energy/1 TeV)'); plt.ylabel('Aeff [m^2]') #; plt.grid()
    plt.xscale('log')
    plt.yscale('log')


#     plt.plot([1,2,3,4], [1,4,9,16], 'ro')
#     plt.axis([0, 6, 0, 20])
    
    plt.show()

    