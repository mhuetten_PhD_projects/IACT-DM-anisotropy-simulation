#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
import time
import os
import os.path
import sys
import getopt
import MoritzIRFfilesTools
import MoritzSphericalTools
import MoritzIACTTools

##    main part:   ############################################################
###############################################################################

def main(argv):

# 	alphaIntDeg = 0.02
# 	psi0deg = 90.0
# 	theta0degGal = 0.0
# 	totalBins = 18
# 	irffile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v4XX/EffectiveAreas/effArea-d20131031-cut-N3-Point-005CU-Moderate-ATM21-V6-T1234-d20131115.root"
# 	sensitivityFile = "/lustre/fs5/group/cta/users/mhuetten/VERITAS/analysis/AnalysisData-VTS-v4XX/VTS.sensitivity.V6.moderate.root"
# 	noise = 325
# 	elevation = 20 
# 	azimutBin = 2
# 	tobs = 100
# 	sigmav = 3e-26
# 	mchi = 500.0
# 	signalModel = "astroPower2.62"
# 	backgroundModel = "VTSdata"
# 	inputdirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/14-07-11-CrabTestBackgroundDataDynPSF2" 
# 	outputdirectory = "/lustre/fs5/group/cta/users/mhuetten/workdata/IACT-anisotropy-simulation/14-07-11-CrabTestBackgroundDataDynPSF2"
#  	thresholdBin = 7
#  	Phi0astroflux = 2.05e-6
#	MCvsREC = "A_MC"

	###########################################################################
	# read input variables
	try:
		opts, args = getopt.getopt(sys.argv[1:],"ha:l:b:c:d:r:q:n:v:z:t:s:m:y:k:j:x:g:u:w:p:f:i:o:",["alphaIntDeg=","psi=","theta=","totalbins=","thresholdbin=","irffile=","sensitivityfile=","noise=","elevation=","azimutbin=","tobs=","sigmav=","mchi=","signalmodel=","backgroundmodel=","astroanisotropiesmodel=","phizeroastro=","alpha=","thetasquared=","wobbleoffset=","irfspectrum=","mcvsrec=","dirin=","dirout="])
	except getopt.GetoptError:
		# This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
		# The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
		print 'Wrong input, EXIT. The input options are:'
		print ' -a <AlphaIntDeg>'
		print ' -l <psi0deg>'
		print ' -b <theta0degGal>'
		print ' -c <totalbins>'
		print ' -d <thresholdbin>'
		print ' -r <iRffile>'
		print ' -q <sensitivityfile>'
		print ' -n <Noise>'
		print ' -v <eleVation>'
		print ' -z <aZimutbin>'
		print ' -t <Tobs>'
		print ' -s <Sigmav>'
		print ' -m <Mchi>'
		print ' -y <signalmodel>'
		print ' -k <backgroundmodel>'
		print ' -j <astroanisotropiesmodel>'
		print ' -x <phizeroastro>'
		print ' -g <alpha>'
		print ' -u <thetasquared>'
		print ' -w <wobbleoffset>'
		print ' -p <irfspectrum>'
		print ' -f <mcvsrec>'			
		print ' -i <input directory>'
		print ' -o <output directory>'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			# help option
			print 'HELP: The input options are:'
			print ' -a <AlphaIntDeg>'
			print ' -l <psi0deg>'
			print ' -b <theta0degGal>'
			print ' -c <totalbins>'
			print ' -d <thresholdbin>'
			print ' -r <iRffile>'
			print ' -q <sensitivityfile>'
			print ' -n <Noise>'
			print ' -v <eleVation>'
			print ' -z <aZimut>'
			print ' -t <Tobs>'
			print ' -s <Sigmav>'
			print ' -m <Mchi>'
			print ' -y <signalmodel>'
			print ' -k <backgroundmodel>'
			print ' -j <astroanisotropiesmodel>'
			print ' -x <phizeroastro>'
			print ' -g <alpha>'
			print ' -u <thetasquared>'
			print ' -w <wobbleoffset>'
			print ' -p <irfspectrum>'
			print ' -f <mcvsrec>'				
			print ' -i <input directory>'
			print ' -o <output directory>'
			sys.exit()
		elif opt in ("-a", "--alphaIntDeg"):
			alphaIntDeg = float(arg)
		elif opt in ("-l", "--psi"):
			psi0deg = float(arg)
		elif opt in ("-b", "--theta"):
			theta0degGal = float(arg)
		elif opt in ("-c", "--totalbins"):
			totalBins = int(arg)
		elif opt in ("-d", "--thresholdbin"):
			thresholdBin = int(arg)
		elif opt in ("-r", "--irffile"):
			irffile = arg
		elif opt in ("-q", "--sensitivityfile"):
			sensitivityFile = arg
		elif opt in ("-n", "--noise"):
			noise = int(arg)
		elif opt in ("-v", "--elevation"):
			elevation = float(arg) 
		elif opt in ("-z", "--azimutbin"):
			azimutBin = int(arg) 
		elif opt in ("-t", "--tobs"):
			tobs = float(arg) 
		elif opt in ("-s", "--sigmav"):
			sigmav = float(arg)
		elif opt in ("-m", "--mchi"):
			mchi = float(arg)
		elif opt in ("-y", "--signalmodel"):
			signalModel = str(arg)
		elif opt in ("-k", "--backgroundmodel"):
			backgroundModel = str(arg)
		elif opt in ("-j", "--astroanisotropiesmodel"):
			astroAnisotropiesModel = str(arg)
		elif opt in ("-x", "--phizeroastro"):
			Phi0astroflux = float(arg)
		elif opt in ("-g", "--alpha"):
			alpha = float(arg)
		elif opt in ("-u", "--thetasquared"):
			thetaSquared = float(arg)
		elif opt in ("-w", "--wobbleoffset"):
			pointSourceWobbleOffset = float(arg)
		elif opt in ("-p", "--irfspectrum"):
			IRFspectralindex = float(arg)
		elif opt in ("-f", "--mcvsrec"):
			MCvsREC = str(arg)
		elif opt in ("-i", "--dirin"):
			inputdirectory = arg 
		elif opt in ("-o", "--dirout"):
			outputdirectory = arg
	
	start_time = time.time()
	print " initializing program anisotropySimulation.combineBins.py"
	
	###########################################################################
	## first computations from input variables:
	
	# convert alphaInt into radians:
	alphaInt = alphaIntDeg/180.0*np.pi
		
	# determine dimension of healpix map, adapted to resolution of clumpy data: 
	nside = MoritzSphericalTools.alphaInt2nside(alphaInt)
	# give resolution of skymap in rads, determined by grid resolution:
	resolution = hp.nside2resol(nside) * 2.0 / np.sqrt(np.pi)
	resolutionDeg = resolution * 180.0 / np.pi
	
	###########################################################################
	# recalculate input values:
	
	# calculate number of pixels on sphere from NSIDE:
	npix=hp.nside2npix(nside)
	pixelareaHealpix = 4*np.pi/npix
	
	# give celestial position in rads:
	psi0 =  np.pi*psi0deg/180.0 # attention: source of possible error: healpy counts phi clockwise
	theta0deg=90.0-theta0degGal # got to interval theta in [0,180 deg]
	theta0 = np.pi*theta0deg/180.0
	# determine maximum calculable multipole element lmax:
	lmax = int(np.pi/hp.nside2resol(nside))
	# give observation time in seconds:
	tobs = tobs * 3600
	 
	print " check again input values:"
	print " NSIDE of healpix map:",nside, "pixels"
	print " resolution of healpix map (full pixel diameter for approximately circular pixels):", round(resolutionDeg,3), "degrees"
	print " size of healpix map:",npix, "pixels"
	print " size of healpix map pixel:",pixelareaHealpix, "sr"


	if signalModel[:10] == "astroPower":
		powerLawSpectrumIndex = float(signalModel[10:])
		IRFspectralindex = round(powerLawSpectrumIndex,1)
		print " Astrophysical point source with power-law energy spectrum for testing modeled. For detailed parameters see summary below."
		print " "
		# choose coordinates of source for this wobble offset:
		psiSource = psi0
		thetaSource = theta0 + pointSourceWobbleOffset/180.*np.pi
	if signalModel == "noDarkMatter":
		print "IGRB and background without Dark Matter modeled. For detailed parameters see summary below."
	print " "
	 
	###########################################################################
	# load instrument response file:

	if "CTA" in irffile:
		CTAbool = True
		print " IRF file detected as CTA IRF file..."
	elif "VTS" or "VERITAS" in irffile:
		CTAbool = False
		print " IRF file detected as VTS IRF file..."
	
	ParameterValues, AeffOverEnergyMatrix = MoritzIRFfilesTools.loadEffectiveAreas(irffile,CTAbool)
	print ""
	# extract vectos with energy bin intervals:
	IRFenergyBinVector = ParameterValues[8]
	totalIRFenergyBins = len(ParameterValues[8])
	print " total number of energy bins in IRF file: ", totalIRFenergyBins
	print " number of energy bins in simulation input: ", totalBins
	print ""
	
	# extract pedvar (as function of noise) and mean azimut values:
	az_index = ParameterValues[1].index(float(azimutBin))
	noise_index = ParameterValues[5].index(float(noise))
	pedvar = ParameterValues[6][noise_index]
	azMin = ParameterValues[2][az_index]
	azMax = ParameterValues[3][az_index]
	meanAzimut =  (azMax + azMin)/2
	
	###########################################################################
	# load point spread function from sensitivity file:
	angResLogEnergyBinVector, angularResolutionOverEnergyVector = MoritzIRFfilesTools.loadAngularResolution(sensitivityFile)
	angularResolutionOverEnergyVectorIRFbins = MoritzIRFfilesTools.interpolateAngularResolution(angularResolutionOverEnergyVector, angResLogEnergyBinVector, IRFenergyBinVector)
	
	###########################################################################	 
	# make table with all informations from energy bins for printout at the end
	# of this program:
	
	energyTable = np.zeros((totalBins,15))

	# Explanation of content of table:
	#
	# energyTable[:,0]: bin number
	# energyTable[:,1]: mean energy of bin
	# energyTable[:,2]: lower energy threshold of bin
	# energyTable[:,3]: upper energy threshold of bin
	# energyTable[:,4]: expected signal events in this bin
	# energyTable[:,5]: signal events in this bin as result of Poisson distribution
	#					with mean as expected signal events
	# energyTable[:,6]: expected IGRB events in this bin
	# energyTable[:,7]: IGRB events in this bin as result of Poisson distribution
	#					with mean as expected IGRB events
	# energyTable[:,8]: expected background events in this bin
	# energyTable[:,9]: background events in this bin as result of Poisson 
	#					distribution with mean as expected IGRB events
	#
	# only filled in the case of a point source:
	# energyTable[:,10]: Non (events in ON region)
	# energyTable[:,11]: expected background and IGRB events in the ON region
	#					 for this energy bin (= alpha Noff)
	# energyTable[:,12]: Non - alpha Noff
	# energyTable[:,13]: Signal events in ON region (compare with Non - alpha Noff)
	# energyTable[:,14]: significance for Non - alpha Noff for this energy bin
	
	
	###########################################################################	 
	# get first energy bin eventmaps file for getting the size information:
	
	firstFileName = "inputdirectory"+"eventmap_bin1.healpix"
	energyBinChecked = 1
	while os.path.isfile(firstFileName) == False:
		energyBinChecked = energyBinChecked + 1
		firstFileName = inputdirectory+"/eventmap_bin"+str(energyBinChecked)+".healpix"
		
	firstEnergyBin = energyBinChecked
	firstFileName = inputdirectory+"/eventmap_bin"+str(firstEnergyBin)+".healpix"
	print " first existing energy bin file:",firstFileName
		
	testMap = np.loadtxt(firstFileName)
	ROIpixels = testMap[:,0]
	npixROI = len(ROIpixels)
	npixROI = int(npixROI)
	del testMap
	print " number of pixels in ROI:",npixROI
	
	combinedEventMap = np.zeros((npixROI,9)) #- 1.6375*10**(30)
	
	# combinedEventMap[:,0]: vector containing pixel numbers of healpix map
	# combinedEventMap[:,1]: expected signal events in each pixel
	# combinedEventMap[:,2]: Poisson & PSF displaced number of  signal events in each pixel
	# combinedEventMap[:,3]: expected number  astrophys. IGRB events in each pixel
	# combinedEventMap[:,4]: Poisson & PSF displaced # astrophys. IGRB events 
	# combinedEventMap[:,5]: expected # bg events
	# combinedEventMap[:,6]: Poisson & PSF displaced # bg events
	# combinedEventMap[:,7]: all expected events per pixel (sum 1+3+5)
	# combinedEventMap[:,8]: all Poisson & PSF displaced events per pixel (sum 2+4+6)
	
	combinedEventMap[:,0] = ROIpixels
	combinedEventMapThreshold = np.zeros((npixROI,9)) #- 1.6375*10**(30)
	combinedEventMapThreshold[:,0] = ROIpixels
	
	print ""
	
	###########################################################################	 	
	# get information about threshold energies:

	lowestBinEnergy = ParameterValues[8][firstEnergyBin-1]
	loglowestBinEnergy = np.log10(lowestBinEnergy/1000.0)
	logNextbinEnergy = np.log10(ParameterValues[8][firstEnergyBin]/1000.0)
	DeltaLogEnergy = round(np.abs(loglowestBinEnergy-logNextbinEnergy),1)
	lowestBinLowerBound = int(1000.0*10**(loglowestBinEnergy-0.5*DeltaLogEnergy))
	
	thresholdEnergy = ParameterValues[8][thresholdBin-1]
	logThresholdEnergy = np.log10(thresholdEnergy/1000.0)
	logNextbinEnergy = np.log10(ParameterValues[8][thresholdBin]/1000.0)
	DeltaLogEnergy = round(np.abs(logThresholdEnergy-logNextbinEnergy),1)
	thresholdEnergyBinLowerBound = int(1000.0*10**(logThresholdEnergy-0.5*DeltaLogEnergy))
	
	print " lower bound of lowest energy bin in combined eventmap is",lowestBinLowerBound,"GeV (energy bin",firstEnergyBin,")"
	print " lower threshold energy for combined threshold eventmap is",thresholdEnergyBinLowerBound,"GeV (energy bin",thresholdBin,")"
	print ""
	
	###########################################################################	
	# now load energy bin event skymaps, one after the other:
	
	totalExpectedSignalEvents = 0.0
	totalPoissonSignalEvents = 0
	totalExpectedBackgroundEvents = 0.0
	totalPoissonBackgroundEvents = 0
	totalExpectedIGRBastroEvents = 0.0
	totalPoissonIGRBastroEvents = 0
	totalExpectedElectronEvents = 0.0
	totalPoissonElectronEvents = 0
	
	totalExpectedSignalEventsAboveThreshold = 0.0
	totalPoissonSignalEventsAboveThreshold = 0
	totalExpectedBackgroundEventsAboveThreshold = 0.0
	totalPoissonBackgroundEventsAboveThreshold = 0
	totalExpectedIGRBastroEventsAboveThreshold = 0.0
	totalPoissonIGRBastroEventsAboveThreshold = 0
	totalExpectedElectronEventsAboveThreshold = 0.0
	totalPoissonElectronEventsAboveThreshold = 0

	totalPhiSignalAverage = 0.0
	totalPhiIGRBAverage = 0.0
	totalPhiIGRBastroAverage = 0.0
	totalPhiBackground = 0.0
	totalPhiElectrons = 0.0
	
	totalPhiSignalAverageAboveThreshold = 0.0
	totalPhiIGRBAverageAboveThreshold = 0.0
	totalPhiIGRBastroAverageAboveThreshold = 0.0
	totalPhiBackgroundAboveThreshold = 0.0
	totalPhiElectronsAboveThreshold = 0.0

	###########################################################################
	# write some parameters to text file (for plotting later)
	# module 4 (anisotropySimulation.quickPlotEventmap.py):
	
	filename = "binInformation"
	outputpath_binInformation = str(outputdirectory)+"/%s.txt" % filename
	print " write bin informations to file:", outputpath_binInformation
	
	f = open(outputpath_binInformation, 'w')
	f.write("# bin | mean energy (GeV) | energy interval lower bound (GeV) | energy interval upper bound (GeV) | angular resolution 68% containment radius in degs. | effective area (only for point source) in m^2 | ")
	f.write("\n")
	
	# now the results for each energy bin are read in:
	for energyBin in range(1,totalBins+1): #totalBins+1
		print " evaluating energy bin",energyBin,"..."
		energyBinFileName = inputdirectory+"/eventmap_bin"+str(energyBin)+".healpix"
		energyBinFluxTableName = inputdirectory+"/averageFluxes_bin"+str(energyBin)+".txt"
		energyBinIndex = energyBin-1
		
		# write energy information about this bin into table:
		if energyBin <= totalIRFenergyBins:
			thisEnergy = ParameterValues[8][energyBinIndex]
			logThisEnergy = np.log10(thisEnergy/1000.0)
			if energyBinIndex == 0:
				logNeighbourBinEnergy = np.log10(ParameterValues[8][energyBinIndex+1]/1000.0)
				notFirstBinBool = False
			else:
				logNeighbourBinEnergy = np.log10(ParameterValues[8][energyBinIndex-1]/1000.0)
				notFirstBinBool = True
				
			DeltaLogEnergy = round(np.abs(logThisEnergy-logNeighbourBinEnergy),1)
			thisEnergyBinUpperBound = int(1000.0*10**(logThisEnergy+0.5*DeltaLogEnergy))
			if notFirstBinBool == True:
				lastEnergy = ParameterValues[8][energyBinIndex-1]
				logLastEnergy = np.log10(lastEnergy/1000.0)
				lastEnergyBinUpperBound = int(1000.0*10**(logLastEnergy+0.5*DeltaLogEnergy))
				thisEnergyBinLowerBound = lastEnergyBinUpperBound
			else:
				thisEnergyBinLowerBound = int(1000.0*10**(logThisEnergy-0.5*DeltaLogEnergy))
			 
			energyTable[energyBinIndex,0] = int(energyBin)
			energyTable[energyBinIndex,1] = int(thisEnergy)
			energyTable[energyBinIndex,2] = int(thisEnergyBinLowerBound)
			energyTable[energyBinIndex,3] = int(thisEnergyBinUpperBound)
				
			print "  ->",thisEnergy,"GeV with interval [",thisEnergyBinLowerBound,"GeV,",thisEnergyBinUpperBound,"GeV ]"			
		
		else:
			print " number of energy bin (",energyBin,") exceeds number",totalIRFenergyBins,"of energy bins in IRF file"
		
		# now read eventmap and flux table:
		if os.path.isfile(energyBinFileName) == True:

			f.write(str(int(energyBin)))
			f.write(" ")			
			f.write(str(thisEnergy))
			f.write(" ")
			f.write(str(thisEnergyBinLowerBound))
			f.write(" ")
			f.write(str(thisEnergyBinUpperBound))
			f.write(" ")
			
			# get effective area for point source for bin: 
			if signalModel[:10] == "astroPower":
				IRFWoffVectorEbin, AeffOverWoffVector, IRFParameterValues, IRFpopt, IRFcov = MoritzIRFfilesTools.makeAeffOverWoff(ParameterValues, AeffOverEnergyMatrix, thisEnergy,  elevation, azimutBin, noise, IRFspectralindex, MCvsREC,CTAbool)
				Aeff, IRFiteration = MoritzIRFfilesTools.fitWobbleOffset(IRFWoffVectorEbin, AeffOverWoffVector, IRFpopt, pointSourceWobbleOffset, 0, CTAbool)
				print "  effective area at point source in this bin:",Aeff,"m^2"
			# angularResolution: 68% containment radius
			angularResolution = angularResolutionOverEnergyVectorIRFbins[energyBinIndex]
			print "  angular resolution 68% containment radius r_68% =:",angularResolution,"degrees."
			print ""
			f.write(str(angularResolution))
			f.write(" ")
			if signalModel[:10] == "astroPower":
				f.write(str(Aeff))
				f.write(" ")
			f.write("\n")
				
			energyBinEventMap = np.loadtxt(energyBinFileName)
			print "  Read intensities from file", energyBinFluxTableName
			energyBinAverageFluxes = np.loadtxt(energyBinFluxTableName)
			if signalModel[:10] == "astroPower":
				print "  ->  integrated signal flux in this bin Phi_signal =",energyBinAverageFluxes[0],"cm^-2 s^-1."
			else:
				print "  ->  integrated average signal intensity in this bin Phi_signal =",energyBinAverageFluxes[0],"cm^-2 s^-1 sr^-1."
			# check number of events in eventmap
			thisBinExpectedSignalEvents = sum(energyBinEventMap[:,1])
			thisBinExpectedIGRBastroEvents = sum(energyBinEventMap[:,3])
			thisBinExpectedBackgroundEvents = sum(energyBinEventMap[:,5])
				 
			thisBinPoissonSignalEvents = sum(energyBinEventMap[:,2].astype(int))
			thisBinPoissonIGRBastroEvents = sum(energyBinEventMap[:,4].astype(int))
			thisBinPoissonBackgroundEvents = sum(energyBinEventMap[:,6].astype(int))
			
			combinedEventMap[:,1] = np.add(combinedEventMap[:,1], energyBinEventMap[:,1])
			combinedEventMap[:,2] = np.add(combinedEventMap[:,2], energyBinEventMap[:,2])
			combinedEventMap[:,3] = np.add(combinedEventMap[:,3], energyBinEventMap[:,3])
			combinedEventMap[:,4] = np.add(combinedEventMap[:,4], energyBinEventMap[:,4])
			combinedEventMap[:,5] = np.add(combinedEventMap[:,5], energyBinEventMap[:,5])
			combinedEventMap[:,6] = np.add(combinedEventMap[:,6], energyBinEventMap[:,6])
			
			# all expected events:
			combinedEventMap[:,7] = combinedEventMap[:,7] + energyBinEventMap[:,1] + energyBinEventMap[:,3] + energyBinEventMap[:,5]
			# all Poisson events:
			combinedEventMap[:,8] = combinedEventMap[:,8] + energyBinEventMap[:,2] + energyBinEventMap[:,4] + energyBinEventMap[:,6]
				
			if energyBin >= thresholdBin:

				combinedEventMapThreshold[:,1] = np.add(combinedEventMapThreshold[:,1], energyBinEventMap[:,1])
				combinedEventMapThreshold[:,2] = np.add(combinedEventMapThreshold[:,2], energyBinEventMap[:,2])
				combinedEventMapThreshold[:,3] = np.add(combinedEventMapThreshold[:,3], energyBinEventMap[:,3])
				combinedEventMapThreshold[:,4] = np.add(combinedEventMapThreshold[:,4], energyBinEventMap[:,4])
				combinedEventMapThreshold[:,5] = np.add(combinedEventMapThreshold[:,5], energyBinEventMap[:,5])
				combinedEventMapThreshold[:,6] = np.add(combinedEventMapThreshold[:,6], energyBinEventMap[:,6])
				
				# all expected events:
				combinedEventMapThreshold[:,7] = combinedEventMapThreshold[:,7] + energyBinEventMap[:,1] + energyBinEventMap[:,3] + energyBinEventMap[:,5]
				# all Poisson events:
				combinedEventMapThreshold[:,8] = combinedEventMapThreshold[:,8] + energyBinEventMap[:,2] + energyBinEventMap[:,4] + energyBinEventMap[:,6]
			
			# for the case of a point source only:
			thisBinPoissonONevents = 0
			thisBinExpectedONeventsBG = 0.0
			thisBinPoissonONeventsBG = 0
			thisBinPoissonONeventsSignal = 0			
			# loop over all pixels in maps:	EVIL LOOP!!!
			if signalModel[:10] == "astroPower":
				thetaONregion = np.sqrt(thetaSquared)*np.pi/180.0
				for i in range(npixROI):
					theta1, psi1 = hp.pix2ang(nside, int(energyBinEventMap[i,0]))
					if MoritzSphericalTools.angularDistance(thetaSource, psiSource, theta1, psi1) < thetaONregion:
						thisBinPoissonONevents = thisBinPoissonONevents + int(energyBinEventMap[i,2]) + int(energyBinEventMap[i,4]) + int(energyBinEventMap[i,6])
						thisBinExpectedONeventsBG = thisBinExpectedONeventsBG + energyBinEventMap[i,3] + energyBinEventMap[i,5]
						thisBinPoissonONeventsSignal = thisBinPoissonONeventsSignal + int(energyBinEventMap[i,2])
						thisBinPoissonONeventsBG = thisBinPoissonONeventsBG + int(energyBinEventMap[i,4]) + int(energyBinEventMap[i,6])
			
			del energyBinEventMap
			
			# print out event number checks:
			print ""
			print "  expected total number of signal events in this energy bin:",thisBinExpectedSignalEvents
			print "  expected total number of astrophys. IGRB events in this energy bin:",thisBinExpectedIGRBastroEvents
			print "  expected total number of background events in this energy bin:",thisBinExpectedBackgroundEvents
			print ""	
			print "  Poissonian total number of signal events in this energy bin:",thisBinPoissonSignalEvents
			print "  Poissonian total number of astrophys. IGRB events in this energy bin:",thisBinPoissonIGRBastroEvents
			print "  Poissonian total number of background events in this energy bin:",thisBinPoissonBackgroundEvents
			print ""
			print "  total Poissonian events in ON region in this energy bin:",thisBinPoissonONevents
			print "  expected background events in ON region in this energy bin (= alpha*Noff):",thisBinExpectedONeventsBG
			print "  Poissonian background events in ON region in this energy bin:",thisBinPoissonONeventsBG
			print "  missed Poissonian signal events in ON region:",thisBinPoissonSignalEvents - thisBinPoissonONeventsSignal
			
			# write event statistics into table:
			energyTable[energyBinIndex,4] = thisBinExpectedSignalEvents
			energyTable[energyBinIndex,5] = thisBinPoissonSignalEvents
			energyTable[energyBinIndex,6] = thisBinExpectedIGRBastroEvents
			energyTable[energyBinIndex,7] = thisBinPoissonIGRBastroEvents
			energyTable[energyBinIndex,8] = thisBinExpectedBackgroundEvents
			energyTable[energyBinIndex,9] = thisBinPoissonBackgroundEvents
			
			# only filled for point source:
			if signalModel[:10] == "astroPower":
				# energyTable[:,10]: Non (events in ON region)
				energyTable[energyBinIndex,10] = thisBinPoissonONevents
				# energyTable[:,11]: expected background and IGRB events in the OFF region
				#					 for this energy bin (= BG events in ON region/alpha)
				noff = int(np.random.poisson(thisBinExpectedONeventsBG/alpha))
				energyTable[energyBinIndex,11] = noff
				# energyTable[:,12]: Non - alpha Noff
				energyTable[energyBinIndex,12] = thisBinPoissonONevents - alpha * noff
				# energyTable[:,13]: Signal events in ON region (compare with Non - alpha Noff)
				energyTable[energyBinIndex,13] = thisBinPoissonONeventsSignal
				# energyTable[:,14]: significance for Non - alpha Noff for this energy bin
				energyTable[energyBinIndex,14] = MoritzIACTTools.liAndMaSignificance(thisBinPoissonONevents, thisBinExpectedONeventsBG/alpha, alpha)

			# count up total events over all energy bins:
			totalExpectedSignalEvents = totalExpectedSignalEvents + thisBinExpectedSignalEvents
			totalPoissonSignalEvents = totalPoissonSignalEvents + thisBinPoissonSignalEvents
			totalExpectedBackgroundEvents = totalExpectedBackgroundEvents + thisBinExpectedBackgroundEvents
			totalPoissonBackgroundEvents = totalPoissonBackgroundEvents + thisBinPoissonBackgroundEvents
			totalExpectedIGRBastroEvents = totalExpectedIGRBastroEvents + thisBinExpectedIGRBastroEvents
			totalPoissonIGRBastroEvents = totalPoissonIGRBastroEvents + thisBinPoissonIGRBastroEvents
			
			if energyBin >= thresholdBin:
				totalExpectedSignalEventsAboveThreshold = totalExpectedSignalEventsAboveThreshold + thisBinExpectedSignalEvents
				totalPoissonSignalEventsAboveThreshold = totalPoissonSignalEventsAboveThreshold + thisBinPoissonSignalEvents
				totalExpectedBackgroundEventsAboveThreshold = totalExpectedBackgroundEventsAboveThreshold + thisBinExpectedBackgroundEvents
				totalPoissonBackgroundEventsAboveThreshold = totalPoissonBackgroundEventsAboveThreshold + thisBinPoissonBackgroundEvents
				totalExpectedIGRBastroEventsAboveThreshold = totalExpectedIGRBastroEventsAboveThreshold + thisBinExpectedIGRBastroEvents
				totalPoissonIGRBastroEventsAboveThreshold = totalPoissonIGRBastroEventsAboveThreshold + thisBinPoissonIGRBastroEvents
			
			# now integrate flux table:
			totalPhiSignalAverage = totalPhiSignalAverage + energyBinAverageFluxes[0]
			totalPhiIGRBAverage = totalPhiIGRBAverage + energyBinAverageFluxes[1]
			totalPhiIGRBastroAverage = totalPhiIGRBastroAverage + energyBinAverageFluxes[2]
			totalPhiBackground = totalPhiBackground + energyBinAverageFluxes[3]
			totalPhiElectrons = totalPhiElectrons + energyBinAverageFluxes[4]
			
			# get electron events contributing to background also also from flux table:
			totalExpectedElectronEvents = totalExpectedElectronEvents +  energyBinAverageFluxes[5]
			totalPoissonElectronEvents = totalPoissonElectronEvents +  energyBinAverageFluxes[6]
			
			if energyBin >= thresholdBin:
				totalPhiSignalAverageAboveThreshold = totalPhiSignalAverageAboveThreshold + energyBinAverageFluxes[0]
				totalPhiIGRBAverageAboveThreshold = totalPhiIGRBAverageAboveThreshold + energyBinAverageFluxes[1]
				totalPhiIGRBastroAverageAboveThreshold = totalPhiIGRBastroAverageAboveThreshold + energyBinAverageFluxes[2]
				totalPhiBackgroundAboveThreshold = totalPhiBackgroundAboveThreshold + energyBinAverageFluxes[3]
				totalPhiElectronsAboveThreshold = totalPhiElectronsAboveThreshold + energyBinAverageFluxes[4]
				totalExpectedElectronEventsAboveThreshold = totalExpectedElectronEventsAboveThreshold +  energyBinAverageFluxes[5]
				totalPoissonElectronEventsAboveThreshold = totalPoissonElectronEventsAboveThreshold +  energyBinAverageFluxes[6]
			
		else:
			print "  there is no output file for energy bin",energyBin
		print ""
		
	f.close()
		
	totalExpectedEvents = totalExpectedSignalEvents + totalExpectedIGRBastroEvents + totalExpectedBackgroundEvents
	totalExpectedEventsAboveThreshold = totalExpectedSignalEventsAboveThreshold + totalExpectedIGRBastroEventsAboveThreshold + totalExpectedBackgroundEventsAboveThreshold
	totalPoissonEvents = totalPoissonSignalEvents + totalPoissonIGRBastroEvents + totalPoissonBackgroundEvents
	totalPoissonEventsAboveThreshold = totalPoissonSignalEventsAboveThreshold + totalPoissonIGRBastroEventsAboveThreshold + totalPoissonBackgroundEventsAboveThreshold
	
	###########################################################################	 
	# check total number of events in all bins:
	print " check event counts summarized over all bins..."	
	
	totalExpectedSignalEventsCheck = sum(combinedEventMap[:,1])
	totalExpectedIGRBastroEventsCheck = sum(combinedEventMap[:,3])
	totalExpectedBackgroundEventsCheck = sum(combinedEventMap[:,5])
	totalExpectedEventsCheck = sum(combinedEventMap[:,7])
	
	totalPoissonSignalEventsCheck = sum(combinedEventMap[:,2].astype(int))
	totalPoissonIGRBastroEventsCheck = sum(combinedEventMap[:,4].astype(int))
	totalPoissonBackgroundEventsCheck = sum(combinedEventMap[:,6].astype(int))
	totalPoissonEventsCheck = sum(combinedEventMap[:,8].astype(int))
	
	totalExpectedSignalEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,1])
	totalExpectedIGRBastroEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,3])
	totalExpectedBackgroundEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,5])
	totalExpectedEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,7])
	
	totalPoissonSignalEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,2].astype(int))
	totalPoissonIGRBastroEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,4].astype(int))
	totalPoissonBackgroundEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,6].astype(int))
	totalPoissonEventsAboveThresholdCheck = sum(combinedEventMapThreshold[:,8].astype(int))
	
	if totalExpectedSignalEventsCheck != totalExpectedSignalEvents:
		print "WARNING! totalExpectedSignalEventsCheck =",totalExpectedSignalEventsCheck," not equal totalExpectedSignalEvents =",totalExpectedSignalEvents,"!"
	if totalExpectedIGRBastroEventsCheck != totalExpectedIGRBastroEvents:
		print "WARNING! totalExpectedIGRBastroEventsCheck =",totalExpectedIGRBastroEventsCheck," not equal totalExpectedIGRBastroEvents =",totalExpectedIGRBastroEvents,"!"
	if totalExpectedBackgroundEventsCheck != totalExpectedBackgroundEvents:
		print "WARNING! totalExpectedBackgroundEventsCheck =",totalExpectedBackgroundEventsCheck," not equal totalExpectedBackgroundEvents =",totalExpectedBackgroundEvents,"!"
	if totalExpectedEventsCheck != totalExpectedEvents:
		print "WARNING! totalExpectedEventsCheck =",totalExpectedEventsCheck," not equal totalExpectedEvents =",totalExpectedEvents,"!"
		
	if totalPoissonSignalEventsCheck != totalPoissonSignalEvents:
		print "WARNING! totalPoissonSignalEventsCheck =",totalPoissonSignalEventsCheck," not equal totalPoissonSignalEvents =",totalPoissonSignalEvents,"!"
	if totalPoissonIGRBastroEventsCheck != totalPoissonIGRBastroEvents:
		print "WARNING! totalPoissonIGRBastroEventsCheck =",totalPoissonIGRBastroEventsCheck," not equal totalPoissonIGRBastroEvents =",totalPoissonIGRBastroEvents,"!"
	if totalPoissonBackgroundEventsCheck != totalPoissonBackgroundEvents:
		print "WARNING! totalPoissonBackgroundEventsCheck =",totalPoissonBackgroundEventsCheck," not equal totalPoissonBackgroundEvents =",totalPoissonBackgroundEvents,"!"
	if totalPoissonEventsCheck != totalPoissonEvents:
		print "WARNING! totalPoissonEventsCheck =",totalPoissonEventsCheck," not equal totalPoissonEvents =",totalPoissonEvents,"!"
		
	if totalExpectedSignalEventsAboveThresholdCheck != totalExpectedSignalEventsAboveThreshold:
		print "WARNING! totalExpectedSignalEventsAboveThresholdCheck =",totalExpectedSignalEventsAboveThresholdCheck," not equal totalExpectedSignalEventsAboveThreshold =",totalExpectedSignalEventsAboveThreshold,"!"
	if totalExpectedIGRBastroEventsAboveThresholdCheck != totalExpectedIGRBastroEventsAboveThreshold:
		print "WARNING! totalExpectedIGRBastroEventsAboveThresholdCheck =",totalExpectedIGRBastroEventsAboveThresholdCheck," not equal totalExpectedIGRBastroEventsAboveThreshold =",totalExpectedIGRBastroEventsAboveThreshold,"!"
	if totalExpectedBackgroundEventsAboveThresholdCheck != totalExpectedBackgroundEventsAboveThreshold:
		print "WARNING! totalExpectedBackgroundEventsAboveThresholdCheck =",totalExpectedBackgroundEventsAboveThresholdCheck," not equal totalExpectedBackgroundEventsAboveThreshold =",totalExpectedBackgroundEventsAboveThreshold,"!"
	if totalExpectedEventsAboveThresholdCheck != totalExpectedEventsAboveThreshold:
		print "WARNING! totalExpectedEventsAboveThresholdCheck =",totalExpectedEventsAboveThresholdCheck," not equal totalExpectedEventsAboveThreshold =",totalExpectedEventsAboveThreshold,"!"
		
	if totalPoissonSignalEventsAboveThresholdCheck != totalPoissonSignalEventsAboveThreshold:
		print "WARNING! totalPoissonSignalEventsAboveThresholdCheck =",totalPoissonSignalEventsAboveThresholdCheck," not equal totalPoissonSignalEventsAboveThreshold =",totalPoissonSignalEventsAboveThreshold,"!"
	if totalPoissonIGRBastroEventsAboveThresholdCheck != totalPoissonIGRBastroEventsAboveThreshold:
		print "WARNING! totalPoissonIGRBastroEventsAboveThresholdCheck =",totalPoissonIGRBastroEventsAboveThresholdCheck," not equal totalPoissonIGRBastroEventsAboveThreshold =",totalPoissonIGRBastroEventsAboveThreshold,"!"
	if totalPoissonBackgroundEventsAboveThresholdCheck != totalPoissonBackgroundEventsAboveThreshold:
		print "WARNING! totalPoissonBackgroundEventsAboveThresholdCheck =",totalPoissonBackgroundEventsAboveThresholdCheck," not equal totalPoissonBackgroundEventsAboveThreshold =",totalPoissonBackgroundEventsAboveThreshold,"!"
	if totalPoissonEventsAboveThresholdCheck != totalPoissonEventsAboveThreshold:
		print "WARNING! totalPoissonEventsAboveThresholdCheck =",totalPoissonEventsAboveThresholdCheck," not equal totalPoissonEventsAboveThreshold =",totalPoissonEventsAboveThreshold,"!"
	print ""
	
	poissonONevents = 0
	expectedONeventsBG = 0.0
	poissonONeventsSignal = 0
	  
	poissonONeventsAboveThreshold = 0
	poissonONeventsBGaboveThreshold = 0.0
	poissonONeventsSignalAboveThreshold = 0
	
	if signalModel[:10] == "astroPower":
		thetaONregion = np.sqrt(thetaSquared)*np.pi/180.0
		for i in range(npixROI): # EVIL LOOP!!!
			theta1, psi1 = hp.pix2ang(nside, int(combinedEventMap[i,0]))
			if MoritzSphericalTools.angularDistance(thetaSource, psiSource, theta1, psi1) < thetaONregion:
				poissonONevents = poissonONevents + int(combinedEventMap[i,2]) + int(combinedEventMap[i,4]) + int(combinedEventMap[i,6])
				expectedONeventsBG = expectedONeventsBG + combinedEventMap[i,3] + combinedEventMap[i,5]
				poissonONeventsSignal = poissonONeventsSignal + int(combinedEventMap[i,2])
				 
				poissonONeventsAboveThreshold = poissonONeventsAboveThreshold + int(combinedEventMapThreshold[i,2]) + int(combinedEventMapThreshold[i,4]) + int(combinedEventMapThreshold[i,6])
				poissonONeventsBGaboveThreshold = poissonONeventsBGaboveThreshold + combinedEventMapThreshold[i,3] + combinedEventMapThreshold[i,5]
				poissonONeventsSignalAboveThreshold = poissonONeventsSignalAboveThreshold + int(combinedEventMapThreshold[i,2])
	
	###########################################################################	 
	# save output file of combined bins:
	outputFileName_healpix = "eventmap_combined.healpix"
	outputFileNameThreshold_healpix = "eventmap_combined_above"+str(int(thresholdEnergyBinLowerBound))+"GeV.healpix"
	outputpath_eventmap_healpix = str(outputdirectory)+"/"+outputFileName_healpix
	outputpath_eventmap_healpix_threshold = str(outputdirectory)+"/"+outputFileNameThreshold_healpix
	
	print " write event maps to files:", outputpath_eventmap_healpix
	print "                           ", outputpath_eventmap_healpix_threshold
	f = open(outputpath_eventmap_healpix, 'wb')
	g = open(outputpath_eventmap_healpix_threshold, 'wb')
	f.write("# number of pixels in healpix map: ")
	f.write(str(npix))
	f.write("\n")   
	f.write("# healpix map pixel # | expected # signal events | Poisson & PSF displaced # signal events | expected # astrophys. IGRB events | Poisson & PSF displaced # astrophys. IGRB events | expected # bg events | Poisson & PSF displaced # bg events | expected all events | Poisson & PSF displaced all events")
	f.write("\n")
	g.write("# number of pixels in healpix map: ")
	g.write(str(npix))
	g.write("\n")   
	g.write("# healpix map pixel # | expected # signal events | Poisson & PSF displaced # signal events | expected # astrophys. IGRB events | Poisson & PSF displaced # astrophys. IGRB events | expected # bg events | Poisson & PSF displaced # bg events | expected all events | Poisson & PSF displaced all events")
	g.write("\n")
	for ROIpixelnr in range(npixROI):
		f.write(str(int(combinedEventMap[ROIpixelnr,0])))
		f.write(" ")
		f.write(str(combinedEventMap[ROIpixelnr,1]))
		f.write(" ")
		f.write(str(int(combinedEventMap[ROIpixelnr,2])))
		f.write(" ")
		f.write(str(combinedEventMap[ROIpixelnr,3]))
		f.write(" ")
		f.write(str(int(combinedEventMap[ROIpixelnr,4])))
		f.write(" ")
		f.write(str(combinedEventMap[ROIpixelnr,5]))
		f.write(" ")
		f.write(str(int(combinedEventMap[ROIpixelnr,6])))
		f.write(" ")
		f.write(str(combinedEventMap[ROIpixelnr,7]))
		f.write(" ")
		f.write(str(int(combinedEventMap[ROIpixelnr,8])))
		f.write("\n")
		g.write(str(int(combinedEventMapThreshold[ROIpixelnr,0])))
		g.write(" ")
		g.write(str(combinedEventMapThreshold[ROIpixelnr,1]))
		g.write(" ")
		g.write(str(int(combinedEventMapThreshold[ROIpixelnr,2])))
		g.write(" ")
		g.write(str(combinedEventMapThreshold[ROIpixelnr,3]))
		g.write(" ")
		g.write(str(int(combinedEventMapThreshold[ROIpixelnr,4])))
		g.write(" ")
		g.write(str(combinedEventMapThreshold[ROIpixelnr,5]))
		g.write(" ")
		g.write(str(int(combinedEventMapThreshold[ROIpixelnr,6])))
		g.write(" ")
		g.write(str(combinedEventMapThreshold[ROIpixelnr,7]))
		g.write(" ")
		g.write(str(int(combinedEventMapThreshold[ROIpixelnr,8])))
		g.write("\n")
	f.close()
	g.close()
	print ""
	
	###########################################################################	
	# save fits file of combined bins with all events summed up:
	print " write total Poisson eventmap to fits files to perform multipole analysis later..."
	 
	outputFileName_fits = "eventmap_combined.fits"
	outputFileNameThreshold_fits = "eventmap_combined_above"+str(int(thresholdEnergyBinLowerBound))+"GeV.fits"
	normalizedOutputFileName_fits = "eventmap_combined_normalized.fits"
	normalizedOutputFileNameThreshold_fits = "eventmap_combined_normalized_above"+str(int(thresholdEnergyBinLowerBound))+"GeV.fits"
	 
	outputpath_eventmap_fits = str(outputdirectory)+"/"+outputFileName_fits
	outputpath_eventmap_fits_threshold = str(outputdirectory)+"/"+outputFileNameThreshold_fits
	outputpath_normalizedEventmap_fits = str(outputdirectory)+"/"+normalizedOutputFileName_fits
	outputpath_normalizedEventmap_fits_threshold = str(outputdirectory)+"/"+normalizedOutputFileNameThreshold_fits
	
	print " write fits map to file:",outputpath_eventmap_fits_threshold
	eventmapThreshold_fits = np.zeros(npix)
	for i in range(npixROI):
		eventmapThreshold_fits[int(ROIpixels[i])] = combinedEventMapThreshold[i,8]
	hp.write_map(outputpath_eventmap_fits_threshold, eventmapThreshold_fits)
	del eventmapThreshold_fits
	eventmapThreshold_fits = np.zeros(npix)
	print ""
	
	print " normalize eventmap..."
	# normalize eventmap:
	normalizedCombinedEventMapThreshold = combinedEventMapThreshold[:,8]*npix/totalPoissonEventsAboveThreshold
	
	print " write normalized fits map to file:",outputpath_normalizedEventmap_fits_threshold
	normalizedEventmapThreshold_fits = np.zeros(npix)
	for i in range(npixROI):
		normalizedEventmapThreshold_fits[int(ROIpixels[i])] = normalizedCombinedEventMapThreshold[i]
	hp.write_map(outputpath_normalizedEventmap_fits_threshold, normalizedEventmapThreshold_fits)
	del normalizedEventmapThreshold_fits
	print ""
	
	###########################################################################	 
	# Create Anafast Input parameter file:
	print " Write anafast parameter files..."
	 
	# Create anafast input parameter file for raw event map:
	filename = "clOutputRAW"
	outputname_clOutput = str(outputdirectory)+"/%s.fits" % filename 
	filename =  "anafastRAW"
	outputname_anafastpar = str(outputdirectory)+"/%s.par" % filename 
	outputfile_anafastpar = open(outputname_anafastpar, 'wb')
	
	nside = hp.npix2nside(npix)
	lmax = int(np.pi/hp.nside2resol(nside))
		
	# fill anafast parameter file anafastRAW.par:
	outputfile_anafastpar.write("simul_type = 1\n\n")
	outputfile_anafastpar.write("nlmax = ")
	outputfile_anafastpar.write(str(lmax))
	outputfile_anafastpar.write("\n\n")
	outputfile_anafastpar.write("infile = ")
	outputfile_anafastpar.write(outputpath_eventmap_fits_threshold)
	outputfile_anafastpar.write("\n\n") 
	outputfile_anafastpar.write("maskfile = ''\n\n")
	outputfile_anafastpar.write("theta_cut_deg = 0.00\n\n")
	outputfile_anafastpar.write("regression = 1\n\n") # removes monopole term
	outputfile_anafastpar.write("plmfile = ''\n\n")
	outputfile_anafastpar.write("outfile = ")
	outputfile_anafastpar.write(outputname_clOutput)
	outputfile_anafastpar.write("\n\n")
	outputfile_anafastpar.write("outfile_alms = ''\n\n") #outfile_alms=$ODIR/${SPARAMETER}_alm.fits 
	outputfile_anafastpar.write("won = 0\n\n")
	outputfile_anafastpar.write("iter_order = 0")
	outputfile_anafastpar.close()
	
	# Create anafast input parameter file for normalized event map:
	filename = "clOutputNORMALIZED"
	outputname_clOutput = str(outputdirectory)+"/%s.fits" % filename 
	filename =  "anafastNORMALIZED"
	outputname_anafastpar = str(outputdirectory)+"/%s.par" % filename 
	outputfile_anafastpar = open(outputname_anafastpar, 'wb')
	
	nside = hp.npix2nside(npix)
	lmax = int(np.pi/hp.nside2resol(nside))
		
	# Fill Anafast parameter file Anafast.par:
	outputfile_anafastpar.write("simul_type = 1\n\n")
	outputfile_anafastpar.write("nlmax = ")
	outputfile_anafastpar.write(str(lmax))
	outputfile_anafastpar.write("\n\n")
	outputfile_anafastpar.write("infile = ")
	outputfile_anafastpar.write(outputpath_normalizedEventmap_fits_threshold)
	outputfile_anafastpar.write("\n\n") 
	outputfile_anafastpar.write("maskfile = ''\n\n")
	outputfile_anafastpar.write("theta_cut_deg = 0.00\n\n")
	outputfile_anafastpar.write("regression = 1\n\n") # removes monopole term
	outputfile_anafastpar.write("plmfile = ''\n\n")
	outputfile_anafastpar.write("outfile = ")
	outputfile_anafastpar.write(outputname_clOutput)
	outputfile_anafastpar.write("\n\n")
	outputfile_anafastpar.write("outfile_alms = ''\n\n") #outfile_alms=$ODIR/${SPARAMETER}_alm.fits 
	outputfile_anafastpar.write("won = 0\n\n")
	outputfile_anafastpar.write("iter_order = 0")
	outputfile_anafastpar.close()
	
	print ""

	###########################################################################
	# give console output:
		
	print " ******************************  RESULTS SUMMARY:  *****************************"
	print " PHYSICS PROPERTIES:"
	print ""
	print " Signal model:",signalModel
	if signalModel[:10] == "astroPower":
		print "  Astrophysical point source modeled with:"
		print "  spectral index of emission:",powerLawSpectrumIndex,", flux normalization dPhi/dE(1 GeV) =",Phi0astroflux,"cm^-2 s^-1 sr^-1 GeV^-1"
		
	elif signalModel != "noDarkMatter":
		print " Dark Matter particle mass m_chi =",mchi,"GeV"
		print " velocity averaged cross section <sigma v> =",sigmav,"cm^3 s-1"
	print ""
	if backgroundModel == "VTSdata":
		print " Background model:",backgroundModel, "(direct read-out of background events after cuts, total background intensity not known.)"
	else:
		print " Background model:",backgroundModel
	print ""
	print " Intensities integrated over all evaluated bins (threshold energy of",lowestBinLowerBound,"GeV (energy bin",firstEnergyBin,"):"	
	if signalModel[:10] == "astroPower":
		print "  integrated signal flux: Phi_source =",totalPhiSignalAverage,"cm^-2 s^-1"
	else:
		print "  integrated average signal intensity: Phi_DM =",totalPhiSignalAverage,"cm^-2 s^-1 sr^-1"
		print "                                    =",totalPhiSignalAverage/(totalPhiIGRBAverage+totalPhiElectrons),"* electron and IGRB intensity."		
	print "  integrated average IGRB intensity: Phi_IGRB =",totalPhiIGRBAverage,"cm^-2 s^-1 sr^-1"
	print "                                    =",totalPhiIGRBAverage/(totalPhiElectrons),"* electron intensity."
	print "  integrated average IGRB intensity from astrophysical sources:",totalPhiIGRBastroAverage,"cm^-2 s^-1 sr^-1"
	print "  integrated background intensity:",totalPhiBackground,"cm^-2 s^-1 sr^-1"
	print "  integrated electron intensity (contributing to background intensity): Phi_el =",totalPhiElectrons,"cm^-2 s^-1 sr^-1"
	print "                                                                =",round(totalPhiElectrons/totalPhiBackground,4)*100,"% of background intensity."
	print ""
	print " Intensities above threshold energy of",thresholdEnergyBinLowerBound,"GeV (energy bin",thresholdBin,"):"
	if signalModel[:10] == "astroPower":
		print "  integrated average signal flux above threshold: Phi_source =",totalPhiSignalAverageAboveThreshold,"cm^-2 s^-1"
	else:
		print "  integrated average signal intensity above threshold: Phi_DM =",totalPhiSignalAverageAboveThreshold,"cm^-2 s^-1 sr^-1"
		print "                                                    =",round(totalPhiSignalAverageAboveThreshold/(totalPhiIGRBAverageAboveThreshold+totalPhiElectronsAboveThreshold),4)*100,"% of electron and IGRB intensity."
	print "  integrated average IGRB intensity above threshold: Phi_IGRB =",totalPhiIGRBAverageAboveThreshold,"cm^-2 s^-1 sr^-1"
	print "                                                    =",totalPhiIGRBAverageAboveThreshold/(totalPhiElectronsAboveThreshold),"* electron intensity."
	print "  integrated average IGRB intensity from astrophysical sources above threshold:",totalPhiIGRBastroAverageAboveThreshold,"cm^-2 s^-1 sr^-1"
	print "  integrated background intensity above threshold:",totalPhiBackgroundAboveThreshold,"cm^-2 s^-1 sr^-1"
	print "  integrated electron intensity (contributing to background intensity) above threshold: Phi_el =",totalPhiElectronsAboveThreshold,"cm^-2 s^-1 sr^-1"
	print "                                                                                =",round(totalPhiElectronsAboveThreshold/totalPhiBackgroundAboveThreshold,4)*100,"% of background intensity."
	print " ________________________"
	print ""
	print " OBSERVATION PROPERTIES:"
	print ""
	print "  Observation time =",tobs,"s =",tobs/3600.0,"h."
	print ""
	print "  IRF file: =",irffile
	print "  sensitivity file (defining PSF): =",sensitivityFile
	print ""
	print "  Observation parameters determing the effective area value:"
	print "   elevation of observation (zenit angle) =",elevation,"degrees."
	print "   mean azimut of observation = ",meanAzimut," degrees (azimutbin =",azimutBin,")"
	print "   noise level =",noise," <->  pedvar =",pedvar
	print "   energy table:",MCvsREC
	if signalModel[:10] == "astroPower":
		print "   spectral index of IRFs =",IRFspectralindex,"(was set to spectral index of source)"
	else:
		print "   spectral index of IRFs =",IRFspectralindex	
	print ""
	print "  Spatial bin (pixel) size:",pixelareaHealpix,"sr =",pixelareaHealpix*(180.0/np.pi)**2,"degs^2"
	print ""
	print "      _||_"
	print "      \  /    this yields:"
	print "       \/"
	print ""
	print " Expected total number of events in all evaluated bins (threshold energy of",lowestBinLowerBound,"GeV (energy bin",firstEnergyBin,"):"	
	print "  signal events:",round(totalExpectedSignalEvents,4),"+/-",round(np.sqrt(totalExpectedSignalEvents),4)
	print "  astrophys. IGRB events:",round(totalExpectedIGRBastroEvents,4),"+/-",round(np.sqrt(totalExpectedIGRBastroEvents),4)
	print "  background events:",round(totalExpectedBackgroundEvents,4),"+/-",round(np.sqrt(totalExpectedBackgroundEvents),4)
	print "   ... thereof electrons:",round(totalExpectedElectronEvents,4),"+/-",round(np.sqrt(totalExpectedElectronEvents),4)
	print "                          =",round(totalExpectedElectronEvents/totalExpectedBackgroundEvents,4)*100,"% of background events."
	print "  overall number of events:",round(totalExpectedEvents,4),"+/-",round(np.sqrt(totalExpectedEvents),4)
	print ""
	print " Expected number of events above threshold energy of",thresholdEnergyBinLowerBound,"GeV (energy bin",thresholdBin,"):"
	print "  signal events:",round(totalExpectedSignalEventsAboveThreshold,4),"+/-",round(np.sqrt(totalExpectedSignalEventsAboveThreshold),4)
	print "  astrophys. IGRB events:",round(totalExpectedIGRBastroEventsAboveThreshold,4),"+/-",round(np.sqrt(totalExpectedIGRBastroEventsAboveThreshold),4)
	print "  background events:",round(totalExpectedBackgroundEventsAboveThreshold,4),"+/-",round(np.sqrt(totalExpectedBackgroundEventsAboveThreshold),4)
	print "   ... thereof electrons:",round(totalExpectedElectronEventsAboveThreshold,4),"+/-",round(np.sqrt(totalExpectedElectronEventsAboveThreshold),4)
	print "                         =",round(totalExpectedElectronEventsAboveThreshold/totalExpectedBackgroundEventsAboveThreshold,4)*100,"% of background events."
	print "  overall number of events:",round(totalExpectedEventsAboveThreshold,4),"+/-",round(np.sqrt(totalExpectedEventsAboveThreshold),4)
	print ""
	print " Total number of events in all evaluated bins with Poissonian statistical fluctuation:"	
	print "  signal events:",totalPoissonSignalEvents
	print "  astrophys. IGRB events:",totalPoissonIGRBastroEvents
	print "  background events:",totalPoissonBackgroundEvents
	print "   ... thereof electrons:",totalPoissonElectronEvents
	print "                         =",round(totalPoissonElectronEvents/totalPoissonBackgroundEvents,4)*100,"% of background events."
	print "  overall number of events:", totalPoissonEvents
	print ""
	print " Total number of events in all bins with Poissonian statistical fluctuation above threshold energy of",thresholdEnergyBinLowerBound,"GeV (energy bin",thresholdBin,"):"	
	print "  signal events:",totalPoissonSignalEventsAboveThreshold
	print "  astrophys. IGRB events:",totalPoissonIGRBastroEventsAboveThreshold
	print "  background events:",totalPoissonBackgroundEventsAboveThreshold
	print "   ... thereof electrons:",totalPoissonElectronEventsAboveThreshold
	print "                         =",round(totalPoissonElectronEventsAboveThreshold/totalPoissonBackgroundEventsAboveThreshold,4)*100,"% of background events."
	print "  overall number of events:", totalPoissonEventsAboveThreshold

	if signalModel[:10] == "astroPower":
		print ""
		print " Evaluation of events in ON-region around point source:"
		print "  size of ON-region:",thetaSquared,"degree^2."
		print "  alpha for size of OFF-region = ON-region/alpha:",alpha 
		print "  point source wobble offset:",pointSourceWobbleOffset
		print ""
		print "  total counted number of events in ON region (Non):",poissonONevents
		print "  expected number of background and astrophys. IGRB events in ON region (= alpha*Noff):",expectedONeventsBG
		print "                        ... in refl. region OFF region = ON region/alpha (= Noff):",expectedONeventsBG/alpha
		print "  missed signal events in ON region:",totalPoissonSignalEvents - poissonONeventsSignal
		print ""
		print "  overall significance for refl. region background, ON region = alpha * OFF region::",round(MoritzIACTTools.liAndMaSignificance(poissonONevents, expectedONeventsBG/alpha, alpha),2),"sigma"
		print ""
		print "  Above threshold:"
		print "    total counted number of events in ON region above threshold:",poissonONeventsAboveThreshold
		print "    expected number of background and astrophys. IGRB events in ON region (= alpha*Noff):",poissonONeventsBGaboveThreshold
		print "                         ... in refl. region OFF region = ON region/alpha (= Noff):",poissonONeventsBGaboveThreshold/alpha
		print "    missed signal events in ON region:",totalPoissonSignalEventsAboveThreshold - poissonONeventsSignalAboveThreshold
		print ""
		print "    overall significance for refl. region background, ON region = alpha * OFF region::",round(MoritzIACTTools.liAndMaSignificance(poissonONeventsAboveThreshold, poissonONeventsBGaboveThreshold/alpha, alpha),2),"sigma"
		print ""
		print "  in the energy bin Result table below,  Noff is given for OFF region = ON region/alpha with alpha =",str(alpha),"!"
	print ""	
	print " Result table for each energy bin: "
	if signalModel[:10] == "astroPower":
		print " (all event numbers correspond to the events in the whole FOV, except the last columns for the ON-OFF region calculations for a point source)"	 
	print " ________________________________________________________________________________________________________________________________________________________________________________________________________________________________"
	print " | bin # | mean Energy\t| Energy interval [GeV]\t| exp. signal \t| Poiss. sign. \t| exp. astro. IGRB \t| Poiss. IGRB \t| exp. bg. \t| Poisson bg. \t|| Poisson Non\t| Poisson Noff\t| Non-a*Noff\t| sign. in ON\t| sigma\t|"
	print " |_______|______________|_______________________|_______________|_______________|_______________________|_______________|_______________|_______________||______________|_______________|_______________|_______________|_______|"
	for binNumber in range(totalBins):
		if int(energyTable[binNumber,3]) >= 1000:
			tab1 = "]\t|"
		else:
			tab1 = "]\t\t|"
		
		if energyTable[binNumber,4] == 0.0:	
			tab2 = "\t\t|"
		else:
			tab2 = "\t|"
		
		if int(energyTable[binNumber,5]) >= 10000:
			tab2a = "\t|"
		else:
			tab2a = "\t\t|"
		
		if energyTable[binNumber,6] == 0.0:	
			tab3 = "\t\t\t|"
		else:
			tab3 = "\t\t|"
		
		if energyTable[binNumber,8] > 1e-6:
			if int(energyTable[binNumber,8]) >= 100000:
				tab4 = "|"	
			else:
				tab4 = "\t|"
		else:	
			tab4 = "\t\t|"

		if energyTable[binNumber,9] >= 10000:	
			tab5 = "\t||"
		else:
			tab5 = "\t\t||"
		
		if energyTable[binNumber,10] >= 1000:	
			tab8 = "\t|"
		else:
			tab8 = "\t\t|"
		
		if energyTable[binNumber,11] >= 10:	
			tab9 = "\t|"
		else:
			tab9 = "\t\t|"
		
		if energyTable[binNumber,12] >= 10:
			if 	abs(round(energyTable[binNumber,12],2) - round(energyTable[binNumber,12],1)) < 1e-3 :
				if energyTable[binNumber,12] >= 100:
					tab6 = "\t|"
				else:
					tab6 = "\t\t|"
			else:
				tab6 = "\t|"
		elif round(energyTable[binNumber,12],2) < 0:
			if abs(round(energyTable[binNumber,12],2)) < 1e-2:	
				tab6 = "\t\t|"
			else:
				tab6 = "\t|"
		else:
			tab6 = "\t\t|"
	
		if energyTable[binNumber,13] >= 10000:	
			tab10 = "\t|"
		else:
			tab10 = "\t\t|"
	
		if round(energyTable[binNumber,14],2) >= 10:	
			tab7 = "|"
		else:	
			tab7 = "\t|"
	
		print " | ",int(energyTable[binNumber,0]),"\t |",\
		int(energyTable[binNumber,1]),"GeV \t| [",\
		int(energyTable[binNumber,2]),",",int(energyTable[binNumber,3]),tab1,\
		round(energyTable[binNumber,4],6),tab2,\
		int(energyTable[binNumber,5]),tab2a,\
		round(energyTable[binNumber,6],6),tab3,\
		int(energyTable[binNumber,7]),"\t\t|",\
		round(energyTable[binNumber,8],6),tab4,\
		int(energyTable[binNumber,9]),tab5,\
		int(energyTable[binNumber,10]),tab8,\
		int(energyTable[binNumber,11]),tab8,\
		round(energyTable[binNumber,12],2),tab6,\
		int(energyTable[binNumber,13]),tab10,\
		round(energyTable[binNumber,14],2),tab7
	print " |_______|______________|_______________________|_______________|_______________|_______________________|_______________|_______________|_______________||______________|_______________|_______________|_______________|_______|"
	print ""
	print " *******************************************************************************"
	print ""	
		
	print " program anisotropySimulation.combineBins.py successfully finished."
	print " total computation time:", round((time.time() - start_time)/60,2), "minutes."

	# energyTable[:,10]: Non (events in ON region)
	# energyTable[:,11]: expected background and IGRB events in the ON region
	#					 for this energy bin (= alpha Noff)
	# energyTable[:,12]: Non - alpha Noff
	# energyTable[:,13]: Signal events in ON region (compare with Non - alpha Noff)
	# energyTable[:,14]: significance for Non - alpha Noff for this energy bin

	
##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################
