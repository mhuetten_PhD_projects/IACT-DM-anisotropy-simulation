#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

###############################################################################
# Changelog:

#
###############################################################################

##    import modules:    ######################################################
###############################################################################

import MoritzSphericalTools
import numpy as np
import os
import sys
import getopt

##    define local functions:    ##############################################
###############################################################################






##    main part:   ############################################################
###############################################################################

def main(argv):
    
    ###########################################################################
    # read input variables
    
    gLIST_Bool = 0
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:o:",["inputfilepath=","outputfilepath="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'The input options are:'
        print 'xxx'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'The input options are:'
            print 'xxx'
            sys.exit()
        elif opt in ("-i", "--inputfilepath"):
            inputfilepath = str(arg)
        elif opt in ("-o", "--outputfilepath"):
            outputfilepath = str(arg)
    
    print " initializing script anisotropySimulation.convertFitsSpectra.py"

    clOUT = MoritzSphericalTools.loadfitsspectrum(inputfilepath)
    
    np.savetxt(outputfilepath, clOUT)
    
    print " finished."
    print ""


if __name__ == "__main__":
     
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################