#!/usr/bin/python
# -*- coding: utf-8 -*-

###############################################################################
#    Authors:
#    This file belongs to the Clumpy code, http://lpsc.in2p3.fr/clumpy
#    For any questions, please contact moritz.huetten@desy.de
###############################################################################
 
# Minimal example Python script for transforming FITS files created by Clumpy 
# with the -o2 option) into FITS images according to the WCS standard.
#
# For a more complete introduction into healpy and its plotting features, please
# have a look at the nice documentation at http://healpy.readthedocs.org
#
# You need to have installed 
#   - matplotlib
#   - numpy 
#   - healpy 
#   - astropy.io
# to run this script.

# example for usage:
# 1.) ./bin/clumpy -g7 clumpy_params.txt 180. 0. 4. 4. 1
# 2.) ./bin/clumpy -o2 output/annihil_gal2D_LOS180,0_FOV4x4_rse1_alphaint0.10deg_nside1024.fits 1 1
# 3.) python python_helperscripts/makeFitsImage.py --infile output/annihil_gal2D_LOS180,0_FOV4x4_rse1_alphaint0.10deg_nside1024-JFACTOR-Jtot.fits
# if you hace healpy version >= 1.9.0, directly do
# 2.) python python_helperscripts/makeFitsImage.py --infile output/annihil_gal2D_LOS180,0_FOV4x4_rse1_alphaint0.10deg_nside1024.fits --extension 1 --column 1

###############################################################################
#    import modules:
###############################################################################

import os
import sys
import getopt
import warnings
import matplotlib
matplotlib.use('Agg') # turn of interactive renderer (may otherwise not work on clusters)
import healpy as hp # warning: due to a bug in healpy, importing it before pylab can cause a segmentation fault in some circumstances.
if float(hp.__version__[:3]) < 1.89:
        warnings.warn('WARNING: Your healpy version is older than 1.9.0.\n Reading part-sky map in/or explicit indexing will not work.\n')
import numpy as np
from astropy.io import fits

###############################################################################
##    main part: 
###############################################################################

def main(argv):
    
    ###########################################################################
    #  read input variables:
    
    infiles_string = 'empty'
    outdir = 'none'
    suffix = 'none'
    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:o:s:",["infiles=","outdir=","suffix="])
    except getopt.GetoptError:
        print('Wrong input. The input options are:')
        print('-i or --infiles for reading the input FITS files')
        print('-h for help')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print(' HELP option')
            sys.exit()
        elif opt in ("-i", "--infiles"):
            infiles_string = str(arg)
        elif opt in ("-o", "--outdir"):
            outdir = str(arg)
        elif opt in ("-s", "--suffix"):
            suffix = str(arg)


    
    if infiles_string == 'empty':
        print(' Please parse an input FITS file with -i file.fits or --infile file.fits\n\
 Use the -h option for further help.')
        sys.exit()

    ###########################################################################
    
    
    # separate input files in string:
    inputfiles = infiles_string.split()
    print " Input files:"
    print inputfiles


    if len(inputfiles) == 0:
        print "ERROR: No input file in input string."
        sys.exit()
    if len(inputfiles) > 1:
        print "ERROR: This script requires exactly one input file. More than one found. Quit."
        sys.exit()

    #  read header:
    nside_check = -1
    
    nevents     = 0
    nevents_sig = 0
    nevents_bck = 0
    

    file = inputfiles[0]
    print "min file:",file
    
    
    
    i_ext = 1
    
    hdulist_in = fits.open(file)
    pixtype = hdulist_in[i_ext].header['PIXTYPE']
    if pixtype[:7] != 'HEALPIX':
        raise IOError(' No Healpix map in file. Exit')
    
    partial = hdulist_in[i_ext].header['OBJECT']
    if partial[:7] == 'PARTIAL':
        fullsky = False
    elif partial[:7] == 'FULLSKY':
        fullsky = True
    else:
        raise IOError(' Wring OBJECT keyword. Must be PARTIAL or FULLSKY')

    ext_name = hdulist_in[i_ext].header['EXTNAME']

    try:
        nside = hdulist_in[i_ext].header['NSIDE']
    except:
        raise IOError('  Neccessary input information missing.\n Must contain Healpix full-sky map, e.g. created by Clumpy > 2015.06_corr3 with the -o2 option.')
    
    try:
        ra_obj_deg  = hdulist_in[i_ext].header['PSI_0'] # last target position
        dec_obj_deg = hdulist_in[i_ext].header['THETA_0']
        size_x      = hdulist_in[i_ext].header['SIZE_X']
        size_y      = hdulist_in[i_ext].header['SIZE_Y']
        emin_TeV    = hdulist_in[i_ext].header['EMIN']
        emax_TeV    = hdulist_in[i_ext].header['EMAX']
    except:
        raise IOError('  Missing info to merge output maps.')

    # get upper bound file:
    emin_GeV = int(1000*emin_TeV)
    if emin_GeV == 30:
        emax_GeV = 100
    elif emin_GeV == 100:
        emax_GeV = 300
    elif emin_GeV == 300:
        emax_GeV = 1000
    elif emin_GeV == 500:
        emax_GeV = 1000
    elif emin_GeV == 1000:
        emax_GeV = 100000
        
    emax_TeV = emax_GeV/1000.
    # reconstruct filename:
    file_dir = os.path.dirname(file)
    lastchars = file[-8:]
    if lastchars == 'Bck.fits':
        lastchars = 'Sig' + lastchars
    
    file_max = file_dir + '/rawmap_'+str(emax_GeV)+'GeV_combined'+lastchars
    print 'file max:', file_max
    # open file:
    try:
        hdulist_max = fits.open(file_max)
    except:
        raise IOError('  File max does not exist.')
    

    # quit if map is already existing (therefore, create file names):
    if suffix != 'none':
        outfile_map = 'rawmap_'+str(emin_GeV)+'GeV_'+str(emax_GeV)+'GeV_combined'+suffix+'.fits'
    else:
        outfile_map = 'rawmap_'+str(emin_GeV)+'GeV_'+str(emax_GeV)+'GeV_combined.fits'
    outfile_cl = 'cl_'+outfile_map
    if outdir != 'none':
        outpath_map = outdir + '/' + outfile_map
        outpath_cl = outdir + '/' + outfile_cl
    else:
        outpath_map = outfile_map
        outpath_cl = outfile_cl
    if os.path.isfile(outpath_cl):
        print "output spectrum",outpath_cl,"already existing: QUIT!"
        #sys.exit()
    
    # check for consistent nside between all maps:
    if nside_check != -1:
        if nside != nside_check:
            raise IOError('  All input maps must have same NSIDE.')
        else:
            nside_check = nside
    else:
        nside_check = nside
        # create combined map:
        map_combined = np.zeros(hp.nside2npix(nside))

    try:    
        nevents_min     = hdulist_in[i_ext].header['NEVENTS']
        nevents_sig_min = hdulist_in[i_ext].header['N_SIG']
        nevents_bck_min = hdulist_in[i_ext].header['N_BCK']
        nevents_max     = hdulist_max[i_ext].header['NEVENTS']
        nevents_sig_max = hdulist_max[i_ext].header['N_SIG']
        nevents_bck_max = hdulist_max[i_ext].header['N_BCK']
    except:
        raise IOError('  Missing info to merge output maps.')

    # create combined map:

    #  read map:
    print 'read input map...'
    
    if fullsky == True:
        #map_in = tbdata[0][:]
        map_min = hp.read_map(file)
        map_max = hp.read_map(file_max)
    else:
        #for i in range(len(tbdata)):
            #map_combined[tbdata[i][0]] += tbdata[i][1]
        map_min = hp.read_map(file, partial=True)
        map_max = hp.read_map(file_max, partial=True)
    
    hdulist_in.close()
    hdulist_max.close()
    "... done."
       
    map_min[map_min == -1.6375e+30] = 0
    map_max[map_max == -1.6375e+30] = 0

    map_combined = map_min - map_max
    nevents = nevents_min - nevents_max
    nevents_sig = nevents_sig_min - nevents_sig_max
    nevents_bck = nevents_bck_min - nevents_bck_max
    
    map_combined[map_combined == 0] = -1.6375e+30
        
    # save output map:
    if os.path.isfile(outpath_map):
        print "output map already existing..."
    else:
        print "write output map:", outpath_map
        hp.write_map(outpath_map, map_combined, partial=True, coord='C', dtype=np.int16)#, column_names="Countsmap", column_units="counts")
        
        hdulist_out = fits.open(outpath_map, mode='update')
        hdulist_out[1].header.set('EXTNAME', 'Skymap')
        hdulist_out[1].header.set('COORDSYS', 'C')
        hdulist_out[1].header.set('PSI_0', ra_obj_deg ) # last target position
        hdulist_out[1].header.set('THETA_0', dec_obj_deg ) # last target position
        hdulist_out[1].header.set('SIZE_X', size_x )
        hdulist_out[1].header.set('SIZE_Y', size_y)
        hdulist_out[1].header.set('NEVENTS', nevents, 'total # of events in map')
        print "------------------------------"
        print nevents, " total events in", emin_TeV,emax_TeV, "TeV"
        hdulist_out[1].header.set('N_SIG', nevents_sig, '# of signal events in map')
        print "  ", nevents_sig, " signal events in", emin_TeV,emax_TeV, "TeV"
        hdulist_out[1].header.set('N_BCK', nevents_bck, '# of background events in map')
        print "  ", nevents_bck, " background events in", emin_TeV,emax_TeV, "TeV"
        print "------------------------------"
        hdulist_out[1].header.set('EMIN', emin_TeV, 'lower bound of energy bin in TeV')
        hdulist_out[1].header.set('EMAX', emax_TeV, 'upper bound of energy bin in TeV')
        hdulist_out[1].header.set('TTYPE1', 'PIXEL')
        hdulist_out[1].header.set('TTYPE2', 'Counts')
        hdulist_out.flush()
        hdulist_out.close()
    
    
    # calculate power spectrum:
    if os.path.isfile(outpath_cl):
        print "output spectrum already existing..."
    else:
        print "calculate power spectrum..."
        cl = hp.anafast(map_combined, lmax=nside)
     
        print "... done. Write spectrum to file:", outpath_cl
        hp.write_cl(outpath_cl, cl)
        hdulist_out = fits.open(outpath_cl, mode='update')
        hdulist_out[1].header.set('EXTNAME', 'CountsMapAPS')
        hdulist_out[1].header.set('NSIDE', nside, ' HEALPIX resolution of binning')
        hdulist_out[1].header.set('NEVENTS', nevents, '# of events in map')
        hdulist_out[1].header.set('N_SIG', nevents_sig, '# of signal events in map')
        hdulist_out[1].header.set('N_BCK', nevents_bck, '# of background events in map')
        hdulist_out[1].header.set('EMIN', emin_TeV, 'lower bound of energy bin in TeV')
        hdulist_out[1].header.set('EMAX', emax_TeV, 'upper bound of energy bin in TeV')
        hdulist_out[1].header.set('TTYPE1', 'Counts')
        hdulist_out.flush()
        hdulist_out.close()
        del cl

    print( "finished.")
    print( " ")
    print( " ")
    
if __name__ == "__main__":
     
    main(sys.argv[1:])

##    end of file    ##########################################################
############################################################################### 
