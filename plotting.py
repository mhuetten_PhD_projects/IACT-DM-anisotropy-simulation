#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
from matplotlib import pyplot as plt
import os
import sys
import pandas
import getopt
import pickle
from ctools import *
import obsutils
import make_pointings
from gammalib import *
from math import *
import glob
import math
from multiprocessing import Process, Queue, Pool
#import veripy
from time import *
from astropy.io import fits

##    main part:   ############################################################
###############################################################################
def main(argv):


    # survey or patterned observation:
    survey = 0
    # source position (center of obs, only for pointed observation)
     
    offset  = 1.5
     
    roi_rad_deg = 4.5
     
 
#     caldb = "nathan"
#     irf   = "data/veritas/nathan/bcf/dummy/irf_file.fits"
#     instrument = "veritas"
#     instrument_name = instrument
#     
    instrument = "CTA"
    instrument_name = instrument
#     
    # folder where events are saved:
    #events_outdir = "/lustre/fs19/group/cta/users/mhuetten/CTA/FitsData/Eventlists"
     
    dead_time = 0.95
    emin_TeV = 0.1 #pow(10,-0.5)
    emax_TeV = pow(10,1.9)
    dmmass=200
    scaling=1
    deltalog=0.2
    pointingfile="/afs/ifh.de/user/m/mhuetten/projects/IACT-DM-anisotropy-simulation/pointings.dat"

    ra_obj_deg = 0
    dec_obj_deg = 0
    offset = 1.5
    
    obsdeflist = obsutils.set_obs_patterns("four", ra_obj_deg, dec_obj_deg, offset)
        
    nb_of_obs = len(obsdeflist)
        
    for i in range(nb_of_obs):
        obsdeflist[i]['deadc']      = dead_time
        obsdeflist[i]['emin']       = emin_TeV
        obsdeflist[i]['emax']       = emax_TeV
        obsdeflist[i]['rad']        = roi_rad_deg
        obsdeflist[i]['duration']   = 3600
    # set observation container!
    observations = obsutils.set_obs_list(obsdeflist)



#     #---------------------------------------------------------
#     # PlotSource is a quick way to zoom in on a veritas source, 
#     # try healpy.mollview for a full-sky plot
#     # drawsrcpos will circle the target with a 'r'ed circle
#     ps = veripy.PlotSource( eng, zoomdir=target, radius=roi_rad_deg + 1, unit='counts', title='GC dark matter counts Map', grid=1, drawsrcpos='r')    
# #     #---------------------------------------------------------
# #     # Generate spectral points
# #     spectrum = obsutils.spectrum(obs_simu, "Crab", ebounds)
# #     plt.figure(1)
# #     plt.title("Crab spectrum")
# #     
# #     # Plot spectrum
# #     plt.loglog(spectrum['energy']['value'], \
# #                spectrum['flux']['value'], 'ro', label='Crab')
# #     plt.errorbar(spectrum['energy']['value'], \
# #                  spectrum['flux']['value'], \
# #                  spectrum['flux']['ed_value'], fmt=None, ecolor='r')
# #     
# #     # Put labels
# #     plt.xlabel("Energy ("+spectrum['energy']['unit']+")")
# #     plt.ylabel("Flux ("+spectrum['flux']['unit']+")")
# #     plt.legend(loc="lower left")
# # 
# #     #---------------------------------------------------------
# #     # show events within theta² cut:
# #     # get observation properties from first run in observations:
# #     thetasquarecut(obs_simu, ebounds, target, 0.09)
# #     
# #     # Show plots
# #     
# #     plt.show()
# 
# #     for r in simu:
# #         run = GCTAObservation(r)
# #         print run
# #       # 'run' is a GCTAObservation object
# #       
# # #       # loop over each event in each chunk
# # #         for event in run.events():
# # #           evid   = event.event_id()   # event number
# # #           chron  = event.time()       # gammalib.GTime()
# # #           energy = event.energy()     # gammalib.GEnergy()
# # #           skydir = event.dir().dir()  # gammalib.GSkyDir()
# # #           detx   = event.dir().detx() # float (x position of event in camera coordinates (radians), 0 = camera center)
# # #           dety   = event.dir().dety() # float (y position of event in camera coordinates (radians), 0 = camera center)
# # #           print 'event %d, %.1f GeV, from %s' % (evid, energy.GeV(), skydir )
# # 
# 
# 







    # pick energy range and # of bins
    energy_min = GEnergy( 50.0, 'GeV')
    energy_max = GEnergy(400.0, 'TeV')
      
    energy_slice = GEnergy(500.0, 'GeV')
      
    nenergy = 400 
      
    # pick offset range and # of bins
    ntheta, theta_min, theta_max = 500, 0.0, 7
      
    sigma_gauss = 4
      
    # loop over each bin in each theta/energy axis, 
    # and get the effective area at the bin center
    EN, OFF, AREA, AREA_GAUSS = [], [], [], []
      
    edelta = ( energy_max.log10TeV() - energy_min.log10TeV() ) / nenergy
    thetadelta = ( theta_max - theta_min ) / ntheta
      
    # get the target chunk
    run = observations[0]
      
    # loop over each energy/theta offset bin
    for i in range(nenergy) :
      en = ( (i+0.5) * edelta ) + energy_min.log10TeV()
        
      for j in range(ntheta) :
        th = ( (j+0.5) * thetadelta ) + theta_min
          
        # calculate our effective area at this point
        eff = run.response().aeff()( en, th / 180. * np.pi) # cm^2
        eff = eff / 1.0e4 # now in m^2
          
        # save this point in the parameter space
        EN.append(  en )
        OFF.append( th )
        AREA.append(eff)
  
      
      
    # pcolormesh requires an array of points,
    # so we reshape our list into a numpy array
    EN   = np.reshape( EN  , (nenergy,ntheta) )
    OFF  = np.reshape( OFF , (nenergy,ntheta) )
    AREA = np.reshape( AREA, (nenergy,ntheta) )
      
    # construct our plot
    fig = plt.figure(figsize=(10,5))
    ax = fig.add_subplot( 1,1,1, adjustable='box', aspect=1.0 )
    im = plt.pcolormesh( EN, OFF, AREA, vmin=0)
    plt.title('Effective Area' )
    plt.xlabel('energy log10(TeV)')
    plt.ylabel('offset from camera center (deg)')
    plt.gca().set_xlim([energy_min.log10TeV(), energy_max.log10TeV()])
    plt.gca().set_ylim([theta_min,theta_max])
    cb = fig.colorbar( im )
    cb.set_label('effective area (m^2)')
  
    fig = plt.figure(figsize=(10,5))
    plt.title('CTA Effective Areas')#, pow(10, EN_slice[0] ))
    for energy_slice in [GEnergy( 100.0, 'GeV'), GEnergy( 200.0, 'GeV'), GEnergy( 300.0, 'GeV'), GEnergy( 500.0, 'GeV')]:
        ENslice = energy_slice.log10TeV()
        ienergy = 0
        for i in range(nenergy) :
          en = ( (i+0.5) * edelta ) + energy_min.log10TeV()
          if en > ENslice:
              ienergy = i
              break
          
        OFF_slice = []
        AREA_slice = []
      
        # plot slices:
        for j in range(ntheta):
            OFF_slice.append(OFF[ienergy, j] )
            AREA_slice.append(AREA[ienergy, j] )
        LEGEND = 'ctools South_50h, E = ' + str(pow(10, ENslice )) + ' TeV'
        plt.plot(OFF_slice, AREA_slice, label = LEGEND)
          
        #plt.plot(OFF_slice, AREA_GAUSS)
    AREA_gauss100, AREA_gauss300, OFF_gauss = [], [], []
    AREA_gauss100_0 = 100000 # m²
    AREA_gauss300_1 = 300000 # m²
    for j in range(ntheta):
        OFF_gauss.append(OFF[ienergy, j] )
        AREA_gauss100.append(AREA_gauss100_0 * np.exp(- 0.5 * (OFF[ienergy, j] / sigma_gauss)**2))   
        AREA_gauss300.append(AREA_gauss300_1 * np.exp(- 0.5 * (OFF[ienergy, j] / sigma_gauss)**2)) 
    plt.plot(OFF_gauss, AREA_gauss100, label = 'Gauss 100 GeV, sigma=4deg', color='black', linestyle='--')
    plt.plot(OFF_gauss, AREA_gauss300, label = 'Gauss 300 GeV, sigma=4deg', color='black', linestyle=':')
          
    plt.legend(loc='upper right',prop={'size':12})

    plt.show()




# ================================================================= #
# Generate a theta square cut #
# ================================================================= #
def thetasquarecut(obs, ebounds, pointing, theta):
    """
    Returns spectral points in the given binning for the passed observations
    and source.

    Please note that specpoints can make spectra for any given spectral
    model, as long as it has a "Prefactor" or "Integral" parameter which
    scales the function
    
    Parameters:
     obs     - Observation container
     source  - Source name for which spectrum should be computed
     ebounds - GEbounds energy boundaries
    Keywords:
     None
    """
    # get observation properties from first run in observations:
    obs0 = GCTAObservation(obs[0])
    ra_deg = pointing.ra_deg()
    dec_deg = pointing.dec_deg()
    roi_rad_deg = obs0.roi().radius()
    tmin = obs0.events().tstart().secs()
    tmax = obs0.events().tstop().secs()
    
    # Loop over energy bins, fit and write result to spectrum
    for i in range(len(ebounds)):
        # Clone observations and reset energy thresholds
        select = ctselect(obs)
        select["ra"].real(ra_deg)
        select["dec"].real(dec_deg)
        select["rad"].real(theta)
        select["tmin"].real(0.0)
        select["tmax"].real(0.0)
        select["emin"].real(ebounds.emin(i).TeV())
        select["emax"].real(ebounds.emax(i).TeV())
        select.run()
        obs_select = select.obs()
        for j in obs_select:
            run = GCTAObservation(j)
            # 'run' is a GCTAObservation object
            print ""
            print run.events()
    

##    execute main part:    ###################################################
###############################################################################
    
if __name__ == "__main__":
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################
