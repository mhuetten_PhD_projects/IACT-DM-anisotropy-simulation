#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

##    import modules:    ######################################################
###############################################################################

import numpy as np
# warning: due to a bug in healpy, importing it before pylab can cause
# a segmentation fault in some circumstances.
import healpy as hp
from matplotlib import pyplot as plt
import os
import sys
import pandas
import getopt
import pickle
#from ctools import *
import obsutils
import make_pointings
from gammalib import *
from math import *
import glob
import math
from multiprocessing import Process, Queue, Pool
#import veripy
from time import *
from astropy.io import fits


import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.

import pickle 
import time

from matplotlib.backends.backend_pdf import PdfPages as pdf
from matplotlib.patches import Rectangle
from matplotlib.transforms import Bbox
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
import scipy.stats as stats
import scipy
import bisect

##    main part:   ############################################################
###############################################################################
def main(argv):


    CALDB_PATH = os.environ['CALDB']
    
    IRF1 = "0.5h_avg"
    IRF2 = "South_0.5h"
    CALDB_READ1 = "paranal_ts_smoothoff"
    CALDB_READ2 = "prod2_orig"
    CALDB_WRITE = "paranal_ts_onaxis"
    INSTRUMENT = 'cta'
    
    
    a_prefac = -1/2.
    a_power= 6
    b_prefac = -1./4.
    b_power= 2
    
    lin_param = 0.0
    sigma_fix = 4 # degrees
    sigma_emin= 2.5 # degrees
    sigma_emax= 5 # degrees

    
    fitspath_in1  = CALDB_PATH + '/data/' + INSTRUMENT + '/' + CALDB_READ1 + '/bcf/' + IRF1 + '/'
    fitspath_in2  = CALDB_PATH + '/data/' + INSTRUMENT + '/' + CALDB_READ2 + '/bcf/' + IRF2 + '/'

    fitspath_out = CALDB_PATH + '/data/' + INSTRUMENT + '/' + CALDB_WRITE + '/bcf/' + IRF1 + '/'
    

    hdulist = fits.open(fitspath_in2 +'irf_file.fits.gz')
    hdulist_paranal = fits.open(fitspath_in1 +'irf_file.fits.gz')
    
    print "*****************"
    print "INFO:"
    print hdulist.info()
    print "*****************"
    print "EXTENSION NAMES:"
    for extension in hdulist:
        print " ", extension.name
    print "*****************"
    print "Scale effective area..."
    
    effarea_cols = hdulist[1].columns
    effarea_data = hdulist[1].data
 
 
     
    # fit Gauss to each eff-area curve:
     
    theta_low  = effarea_data["THETA_LO"][:][0]
    theta_high = effarea_data["THETA_HI"][:][0]
    theta = np.array((theta_high + theta_low)/2)
    
    energy_low  = effarea_data["ENERG_LO"][:][0]
    energy_high = effarea_data["ENERG_HI"][:][0]
    energy_mean = np.array((energy_high + energy_low )/2)
    energy_logmean = np.array(10**((np.log10(energy_high) + np.log10(energy_low) )/2))
     
     
    n_energy = len(effarea_data["EFFAREA"][:][0][0][:])
    n_theta  = len(theta)
     
     
    aeff = np.zeros(len(theta))
     
    aeff_new = np.zeros((n_energy, 45))
     
    sigma_fit = np.zeros(n_energy)
    sigma_log = np.zeros(n_energy)
     
    print effarea_data["EFFAREA"][:][0]
    
    pntdir = GSkyDir(); pntdir.radec_deg(0, 0)
    run = obsutils.set_obs(pntdir, tstart=0.0, duration=1800.0, deadc=0.95, \
            emin=0.1, emax=100.0, rad=5.0, \
            irf=IRF1, caldb=CALDB_READ1, id="000000", instrument="CTA")
    
    
    for i_energy in range(n_energy):
        
        en = energy_logmean[i_energy]
        print "energy:",en
        log_en = float(np.log10(en))
        
        aeff_paranal_Center = run.response().aeff()( log_en, 0.)/1e4
 
        for i_theta in range(n_theta):
            aeff[i_theta] = effarea_data["EFFAREA"][:][0][i_theta][i_energy]
            
        print "Aeff prod2",aeff[0], ", aeff paranal:",aeff_paranal_Center
     
        sigma_log[i_energy] = sigma_emin + (sigma_emax - sigma_emin) * float(i_energy)/float(n_energy)
     
        if aeff[0] != 0.0:
            def optm(a,  x):
                return aeff[0] * gaussian(x, a) - aeff
         
            ## begin fit
            sigma = 1.
            leastsquarefit = scipy.optimize.leastsq(optm, [sigma], (theta,))
            sigma_fit[i_energy] = leastsquarefit[0][0]
             
         
            print "  Fitted sigma:", sigma_fit[i_energy]
 
            # print "  Fitted sigma:", popt[2]
            for i_theta in range(len(theta)):
                effarea_data["EFFAREA"][:][0][i_theta][i_energy] =  aeff_paranal_Center * gaussianX(theta[i_theta], a_prefac, sigma_log[i_energy], a_power) * gaussianX(theta[i_theta], b_prefac, sigma_log[i_energy], b_power)#(1 - lin_param * theta[i_theta])
                #*= aeff_paranal_Center / aeff[0] # variant 2. uncomment the following and next line to choose variant 1 #\
        
                #effarea_data["EFFAREA"][:][0][i_theta][i_energy] = 0.
        else:
            print i_energy, " unchanged"
 
    #plt.plot(theta,aeff)
    #plt.plot(theta, aeff[0] * gaussianX(theta, sigma_fit, 2))
    #plt.semilogy(sigma_fit)
    #plt.semilogy(sigma_log)
    #plt.show()
     
    #effarea_data[effarea_cols.names[5]][:] = effarea_data[effarea_cols.names[4]][:]
     
    # change eff area columns
    #effarea_cols.del_col("EFFAREA")
     
    effarea_cols.info()
 
     
#     effarea_cols.change_attrib("EFFAREA", "format", "945E")
#     effarea_cols.change_attrib("EFFAREA", "dim", "(21,45)")
#     
#     effarea_cols.info()
#     for i_energy in range(n_energy):
#         for i_theta in range(n_theta):
#             a=4


    
    print "*****************"
    print "Scale background..."
     
    background_cols = hdulist[4].columns
    background_data = hdulist[4].data
    
    background_data_paranal = hdulist_paranal[4].data
    
    background_cols.info()
 
    detx_low_column  = 0
    detx_high_column = 1
    dety_low_column  = 2
    dety_high_column = 3
     
    bck_column = 6
     
    n_x = 200 
    n_y = 200
     
    theta_high_max = 15 #deg.
     
    delta_theta_x = float(n_x)/float(2 * theta_high_max)
    delta_theta_y = float(n_y)/float(2 * theta_high_max)
    print 
    print background_data[background_cols.names[detx_low_column]]
     
    #background_data[background_cols.names[detx_low_column]][:] = np.linspace(-theta_high_max, theta_high_max-delta_theta_x, n_x)
    #background_data[background_cols.names[detx_high_column]][:] = np.linspace(-theta_high_max+delta_theta_x, theta_high_max, n_x)
    #background_data[background_cols.names[dety_low_column]][:] = np.linspace(-theta_high_max, theta_high_max-delta_theta_y, n_y)
    #background_data[background_cols.names[dety_high_column]][:] = np.linspace(-theta_high_max+delta_theta_y, theta_high_max, n_y)
     
    background_data[background_cols.names[detx_low_column]][:] *= 4./3.
    background_data[background_cols.names[detx_high_column]][:] *= 4./3.
    background_data[background_cols.names[dety_low_column]][:] *= 4./3.
    background_data[background_cols.names[dety_high_column]][:] *= 4./3.
     
    n_energy = len(background_data[background_cols.names[bck_column]][:][0])
    sigma_log = np.zeros(n_energy)
     
    for i_energy in range(n_energy):
         
        sigma_log[i_energy] = sigma_emin + (sigma_emax - sigma_emin) * float(i_energy)/float(n_energy)
         
        detx_low  = background_data[background_cols.names[detx_low_column]][:][0]
        detx_high  = background_data[background_cols.names[detx_high_column]][:][0]
        detx_center = np.array((detx_high + detx_low)/2)
        dety_low  = background_data[background_cols.names[dety_low_column]][:][0]
        dety_high  = background_data[background_cols.names[dety_high_column]][:][0]
        dety_center = np.array((dety_high + dety_low)/2)
         
        n_x = len(detx_center) 
        n_y = len(dety_center)
         
        rate_max = max(background_data[background_cols.names[bck_column]][:][0][i_energy].flatten())
        rate_max_paranal = max(background_data_paranal[background_cols.names[bck_column]][:][0][i_energy].flatten())
        print "max rate Prod2:", rate_max,"max rate paranal:", rate_max_paranal
         
        for i_x in range(n_x):
            for i_y in range(n_y):
                theta = np.sqrt(detx_center[i_x]**2 + dety_center[i_y]**2)
                background_data[background_cols.names[bck_column]][:][0][i_energy][i_x][i_y] = rate_max_paranal * gaussianX(theta, a_prefac, sigma_log[i_energy], a_power) * gaussianX(theta, b_prefac, sigma_log[i_energy], b_power)
                #*= rate_max_paranal / rate_max #\
                #
 

    print "*****************"
    print "Replace PSF..."
 
    del hdulist[2]
    hdulist.append(hdulist_paranal[1])
    
    print "*****************"
    print "Replace Edisp..."
 
    del hdulist[2]
    hdulist.append(hdulist_paranal[2])
     
    print "write output to:", fitspath_out + 'irf_file.fits.gz'
    hdulist.writeto(fitspath_out + 'irf_file.fits.gz', clobber='true')
    
    hdulist.close()
    

##    execute main part:    ###################################################
###############################################################################

def gaussian(x, sig):
    return np.exp(-np.power(x, 2.) / (2 * np.power(sig, 2.)))

def gaussianX(x, a, sig, power):
    return np.exp(a * np.power(x/sig, power))

if __name__ == "__main__":
    main(sys.argv[1:])
    


##    end of file    ##########################################################
###############################################################################
