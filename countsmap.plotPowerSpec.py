#!/usr/bin/python
# -*- coding: utf-8 -*-

from matplotlib import pyplot as plt
import healpy as hp # warning: due to a bug in healpy, importing it before pylab can cause a segmentation fault in some circumstances.
import numpy as np
import os
import sys
import getopt
import pyfits as pf

##    main part:   ############################################################
###############################################################################

def main(argv):
    
    ###########################################################################
    # read input variables

    
    try:
        opts, args = getopt.getopt(sys.argv[1:],"i:",["infile="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'The input options are:'
        print 'xxx'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'The input options are:'
            print 'xxx'
            sys.exit()
        elif opt in ("-i", "--infile"):
            infile = str(arg)

    
    print " initializing script clumpyV2.plotPowerSpec.py"

    # read and plot power spectra:

    powspecname = infile
    powspec_raw = hp.mrdfits(powspecname)

    powspec1 = powspec_raw[0].flatten()
    ellVector = np.arange(len(powspec1))
    
    plot_intensityAPS = plt.figure(figsize=(10, 8)) 
    plt.plot(ellVector, ellVector * (ellVector+1) * powspec1/(2*np.pi),color="black",alpha=0.5,linestyle='-', linewidth=1., label='APS of total J-factor') 
    plt.legend(loc='lower right',prop={'size':12})
    plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    
    plt.show()
    
    print " finished."
    print ""


if __name__ == "__main__":
     
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################    