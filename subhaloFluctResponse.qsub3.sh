#$ -S /bin/bash
#
# script to perform Dark Matter clumpy simulations AND detector response
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script

ODIR=OODIR
SIGNALODIR=SODIR
BACKGROUNDODIR=BODIR
BACKGROUNDODIR2=BODR2
RUNDIR=NNNNN
ISSIGNAL=OOOOO
ISBACKGROUND=PPPPP
EMIN=QQQQQ
i_realisation=IREAL

source /afs/ifh.de/user/m/mhuetten/setbatchenvironment.sh VERITAS
echo ""

cd $ODIR
cd ..
WORKDIR="$(pwd)"

cd $ODIR

if [[ $EMIN == "1000" ]]; then
	EMINVALS="[30 100 300 500 1000]"
else
	EMINVALS=`python -c "import numpy as np; a=np.linspace(30, $EMIN, 8);print a.astype(int)"`
fi
EMINVALS=${EMINVALS:1}
EMINVALS=${EMINVALS%?}
EMINVALS=($EMINVALS)

mkdir -p Logfiles
if [[ $SGE_TASK_ID == "1000" ]]; then
	
	backgrounddir=$BACKGROUNDODIR
	
	cp $RUNDIR/Inputfiles/subhaloFluctResponse.combineSkymaps.py .
	
	for ETHRESH in  "${EMINVALS[@]}"
	do
		# background simulation
		backgroundfile=`echo ${backgrounddir}/rawmap_${ETHRESH}GeV_0*`
		#backgroundfile2=`echo $BACKGROUNDODIR2/rawmap_${ETHRESH}GeV_0*`
backgroundfiles=${backgroundfile} #" "${backgroundfile2}
		python subhaloFluctResponse.combineSkymaps.py --infiles "$backgroundfiles" | tee $ODIR/Logfiles/subhaloSim-BackgroundAPS-${ETHRESH}GeV.log
		echo "... finished background "$ETHRESH" GeV..."
	    echo""
	done
	
	rm subhaloFluctResponse.combineSkymaps.py
	rm subhaloFluctResponse.combine_with_holes.py 
	
else
	
	signaldir=$SIGNALODIR/relicX$SGE_TASK_ID
	xdirectory=$ODIR/relicX$SGE_TASK_ID
	mkdir -p $xdirectory
	
	cp $RUNDIR/Inputfiles/subhaloFluctResponse.combineSkymaps.py $xdirectory
	# grep subdirectories:

	for ETHRESH in  "${EMINVALS[@]}"
	do
		signalfiles=`echo $signaldir/rawmap_${ETHRESH}GeV_0*`
		python $xdirectory/subhaloFluctResponse.combineSkymaps.py --infiles "$signalfiles" --outdir "$xdirectory" --suffix "Sig"  | tee $ODIR/Logfiles/subhaloSim-relicX${SGE_TASK_ID}-combineSig-${ETHRESH}GeV.log
		#python $RUNDIR/Inputfiles/makeFitsImage.py -i $xdirectory/rawmap_${ETHRESH}GeV_combinedSig.fits
		echo "... finished Signal "$ETHRESH" GeV..."
	    echo""
		if [[ $ISBACKGROUND == "1" ]]; then
			backgroundfile=`echo $BACKGROUNDODIR/Background/rawmap_${ETHRESH}GeV_0*`
			backgroundfile2=`echo $BACKGROUNDODIR2/Background/rawmap_${ETHRESH}GeV_0*`
			signalfiles=${signalfiles}" "${backgroundfile} #" "${backgroundfile2}
			python $xdirectory/subhaloFluctResponse.combineSkymaps.py --infiles "$signalfiles"  --outdir "$xdirectory" --suffix "SigBck" | tee $ODIR/Logfiles/subhaloSim-relicX${SGE_TASK_ID}-combineSigBck-${ETHRESH}GeV.log
		    #python $RUNDIR/Inputfiles/makeFitsImage.py -i $xdirectory/rawmap_${ETHRESH}GeV_combinedSigBck.fits
		    echo "... finished Signal + background "$ETHRESH" GeV..."
	    echo""
	    fi

	    #cd $xdirectory
		#python ~/projects/IACT-DM-anisotropy-simulation/subhaloFluctResponse.plotAnimated.py
		#mv plot_raw.png /afs/ifh.de/group/cta/scratch/mhuetten/AnimatedPlots/plot_relicX${SGE_TASK_ID}_realisation_${i_realisation}.png
		#rm *Skymap-Counts-image.fits
	    
	done
	rm $xdirectory/subhaloFluctResponse.combineSkymaps.py

fi



##sleep 20
echo " *** Job successfully finished ***"
##exit
