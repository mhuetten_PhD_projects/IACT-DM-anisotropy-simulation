#! /usr/bin/env python
# ==========================================================================
# This script generates pointing patterns for CTA observations.
#
# Usage:
#   ./make_pointings.py
#
# ==========================================================================
#
# Copyright (C) 2015 Juergen Knoedlseder
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# ==========================================================================
import sys
import math
import gammalib
import healpy as hp
import matplotlib
from matplotlib import pyplot as plt
import numpy as np
import veripy
import obsutils
from astropy.io import fits
import astropy.coordinates as co
import astropy.time as T
from astropy.coordinates import FK5
from astropy import units as u
from astropy import wcs

# ============================================================ #
# Get positions of pointings on sky for an interleaved pattern #
# ============================================================ #
def get_positions(xmin, xmax, ymin, ymax, step):
    """
    Get positions for a patch of interleaved pointings on the sky.
    The function does respect the latitude dependence of the
    pointing distance. The step parameter is for a latitude of
    zero.
    """
    
    paranal_coords = co.EarthLocation(lat=-24.626*u.deg, lon=-70.403*u.deg, height=2635.43*u.m)
    
    

    
    # Initialise positions
    positions = []
    
    # Determine nside of healpix pixelisation;
    nside_pow = 1
    while hp.max_pixrad(2**nside_pow) * gammalib.rad2deg > step/2:
        nside_pow +=1
    nside = 2**(nside_pow-1)
    print "final nside = ", nside
    print "final max. pix diameter = ", hp.max_pixrad(nside)*2 * gammalib.rad2deg, "deg."
    area = hp.nside2pixarea(nside)
    diameter = math.acos(1- area/(2 * math.pi)) * 2 * gammalib.rad2deg
    print "final average. pix diameter = ", diameter, "deg."
    npix_pointings = hp.nside2npix(nside)
    
    # pointings figure
    fig1 = plt.figure(figsize=(12,8))
    
        
    hp_map_pointings = np.full(npix_pointings, -1.6375e+30)
    
    hp.cartview(map=hp_map_pointings, fig=1, rot=[0,-90], unit='', xsize=800, lonra=None, latra=None, title='Cartesian view', flip='astro', format='%.3g',  hold=False, sub=None, margins=None, notext=False, return_projected_map=False)
    
    i=0
    # now select all positions within xmin, xmax, ymin, ymax
    for i_pix in range(npix_pointings):
        
        [lat, lon] = hp.pix2ang(nside, i_pix)
        
        lat *= gammalib.rad2deg
        lat = 90 - lat
        lon *= gammalib.rad2deg
        lon -= 180.
        
        position = gammalib.GSkyDir()
        position.lb_deg(lon , lat)
        
#         time_start = T.Time('2016-01-01 00:00:00', scale='utc')
#         time_start_mjd = time_start.mjd#
#         print "Start time in MJD:", time_start_mjd
#         
#         days = np.arange(365)
#         altitude = np.zeros(365)
#         for day in days:
#             time_nr = time_start_mjd + day
#             time = T.Time(time_nr, scale='utc', format='mjd')
#             frame = co.AltAz(obstime=time, location=paranal_coords)
#             radecs = co.SkyCoord(0, -80, frame='fk5', unit=(u.deg,u.deg))
#             altaz = radecs.transform_to(frame)
#             altitude[day] = altaz.alt.degree
#         plt.figure()
#         plt.plot(days, altitude)
#         plt.show()
        
        
        if lat >= ymin and lat <= ymax and lon >= xmin and lon <= xmax:
            # additionally account for incomplete survey:
            #if position.dec_deg() < -50. or position.dec_deg() > 0.:
               # continue
            positions.append({'x': lon, 'y': lat})
            i +=1
            #print pointing and pixel on graph:
            
            dir = gammalib.GSkyDir()
            dir.radec_deg( lon, lat )
            [theta_circ, phi_circ] = veripy.encircle_points( dir, radius=3.0, npoints=40 )
            
            
            pix_boundaries = hp.boundaries(nside,i_pix)
            for i_bound in range(4):
                if i_bound != 3:
                    up_bound = i_bound + 1
                else:
                    up_bound = 0
                theta_low = hp.vec2ang(hp.boundaries(nside,i_pix)[:,i_bound])[0][0]
                phi_low = hp.vec2ang(hp.boundaries(nside,i_pix)[:,i_bound])[1][0]
                theta_up = hp.vec2ang(hp.boundaries(nside,i_pix)[:,up_bound])[0][0]
                phi_up = hp.vec2ang(hp.boundaries(nside,i_pix)[:,up_bound])[1][0]
                bound1 = [theta_low, phi_low]
                bound2 = [theta_up, phi_up]
                theta = [90 - bound1[0]  * gammalib.rad2deg, 90 - bound2[0]  * gammalib.rad2deg]
                phi = [bound1[1]  * gammalib.rad2deg - 180, bound2[1]  * gammalib.rad2deg - 180]
                
                hp.projplot(phi, theta, 'white', lonlat=True, alpha=0.5)
            hp.projplot(theta_circ, phi_circ, 'b')  # plot 'o' in blue at coord (theta, phi)
            #hp.projscatter(lon, lat, s=700, c='b', marker='o', alpha=0.4, lonlat=True)
            
            print "... set pointing",i,"lon:",lon,"lat:",round(lat,1),"ra:",round(position.ra_deg(),1), "dec:",round(position.dec_deg(),1)
        #if i == 200:
            #break
    hp.graticule()

        
#     # Determine ystep and number of rows
#     ystep   = step * math.sqrt(0.75)
#     ny      = int((ymax-ymin)/ystep+1.5)
#     rescale = (ymax-ymin)/(ystep*(ny-1))
#     ystep  *= rescale
#     print("Number of rows ....: %d" % (ny))
# 
#     # Loop over rows
#     y = ymin
#     for row in range(ny):
# 
#         # Determine xstep and number of pointings in row
#         xstep = step / math.cos(gammalib.deg2rad * y)
#         nx    = int((xmax-xmin)/xstep+1.5)
#         rescale = (xmax-xmin)/(xstep*(nx-1))
#         xstep  *= rescale
# 
#         # Set x offset
#         if row % 2 == 0:
#             x = xmin
#         else:
#             x = xmin + 0.5 * xstep
#         #print(row, y, nx)
# 
#         # Append pointings
#         for pnt in range(nx):
#             positions.append({'x': x, 'y': y})
#             x += xstep
#             if x >= 360.0:
#                 x -= 360.0
# 
#         # Increment y
#         y += ystep

    # Return positions
    return positions


# ====================== #
# Setup patch on the sky #
# ====================== #
def set_patch(lmin=-30.0, lmax=30.0, bmin=-1.5, bmax=+1.5,
              separation=2.0, hours=500.0,
              site=None, lst=True, autodec=0.0):
    """
    Setup pointing patch on the sky.
    """
    # Initialise observation definition
    obsdef = []

    # Get pointing positions
    pointings = get_positions(lmin, lmax, bmin, bmax, separation)
    
    # Determine nside of healpix pixelisation;
    nside_pow = 1
    while hp.max_pixrad(2**nside_pow) * gammalib.rad2deg > separation/2:
        nside_pow +=1
    nside = 2**(nside_pow-1)
    print "final nside = ", nside
    pix_area = hp.nside2pixarea(nside)
    area_tot = pix_area * len(pointings)
    print "total observed area:",area_tot,"sr."

    # Determine number of pointings and pointing duration
    n_pnt    = len(pointings)
    duration_mean = hours*3600.0/float(n_pnt)
    
    # scatter duration with relative scatter of 10%:
    duration_sigma = duration_mean/10.
       
    # Observing time collectors
    exposure_south = 0.0
    exposure_north = 0.0
    
    # exposure map figure:
    fig2 = plt.figure(figsize=(12,8))
    nside_exposure = 512
    npix_exposure = hp.nside2npix(nside_exposure)
    hp_map_exposure = np.zeros(npix_exposure)

    # initialize arrays:
    ntheta, theta_min, theta_max = 100, 0., 10
    thetadelta = ( theta_max - theta_min ) / ntheta
    dtheta_rad = np.deg2rad(thetadelta)
    theta = np.zeros(ntheta)
    rad_accept_1D = np.zeros(ntheta)
    for j in range(ntheta):
        theta[j] = ( (j+0.5) * thetadelta ) + theta_min
    
    nenergy = 200
    emin_TeV = 0.1
    emax_TeV = 100.
    ebounds = gammalib.GEbounds()
    ebounds.set_log(nenergy, gammalib.GEnergy( emin_TeV, 'TeV'), gammalib.GEnergy(emax_TeV, 'TeV'))
    deg2tosr = 3.0462e-4

    # Set observations
    i_pnt = 0
    for pnt in pointings:
        i_pnt += 1

        duration = duration_mean #np.random.normal(loc=duration_mean, scale=duration_sigma)

        # Set positions and duration
        obs = {'lon': pnt['x'], 'lat': pnt['y'], 'duration': duration}

        # Optionally add-in site dependent IRF
        if site != None:

            # Handle automatic site switching
            if site == "Automatic":
                pos = gammalib.GSkyDir()
                pos.lb_deg(pnt['x'], pnt['y'])
                dec = pos.dec_deg()
                if (dec >= autodec):
                    obs_site = "North"
                else:
                    obs_site = "South"
            else:
                obs_site = site

            # Set site IRF
            caldb = "paranal_smoothoff"
            if obs_site == "North":
                if lst:
                    irf = "North_50h"
                else:
                    irf = "North_50h"
                exposure_north += duration
            else:
                if lst:
                    irf = "0.5h_avg"
                else:
                    irf = "0.5h_avg"
                exposure_south += duration

            # Set IRF information
            obs['caldb'] = caldb
            obs['irf']   = irf

        # Append observation
        obsdef.append(obs)
        
        # add exposure to exposure map:
        pntdir = gammalib.GSkyDir()
        pntdir.radec_deg(0, 0)
        run =obsutils.set_obs(pntdir, tstart=0.0, duration=1800.0, deadc=0.95, \
            emin=0.1, emax=100.0, rad=15.0, \
            irf=irf, caldb=caldb, id="000000", instrument="CTA")

        # get radial acceptance shape:
        for i in range(nenergy) :
            log_en = ebounds.elogmean(i).log10TeV()
            for j in range(ntheta):
                bck = run.response().background()( log_en, theta[j] / 180. * np.pi, 0)
                bck *= 1000 # now in HzGeV-1sr-1
                #rad_accept_1D[j] = run.response().aeff()( logen_tev, theta[j] / 180. * np.pi)
                rad_accept_1D[j] += bck * ebounds.ewidth(i).GeV()
        # normalize:
        onaxis_rate = rad_accept_1D[0]
        rad_accept_1D /= rad_accept_1D[0]
        # get integrated area:
        rad_area = 0.
        for j in range(ntheta):
            rad_area += np.deg2rad(theta[j]) * rad_accept_1D[j] * np.deg2rad(thetadelta)
        rad_area *= 2. * np.pi
        avg_radius_deg = np.rad2deg(np.sqrt(rad_area/np.pi))
        
        pointing_vec = hp.dir2vec( pnt['x'], pnt['y'], lonlat=True)
        fov_pix_exposure = hp.query_disc(nside_exposure, pointing_vec, np.deg2rad(theta_max))
        for i_pix_exposure in fov_pix_exposure:
            # get distance to camera center:
            pos_pix_vec = hp.pix2vec(nside_exposure, i_pix_exposure)
            ang_dist_deg = np.rad2deg(np.arccos(np.dot(pos_pix_vec,pointing_vec)))
            i_theta = np.searchsorted(theta, ang_dist_deg)
            hp_map_exposure[i_pix_exposure] += duration * rad_accept_1D[i_theta-1]
        mean_exposure = rad_area / pix_area * duration_mean
             
        print "... compute exposure for pointing",i_pnt,"duration=",duration,"s, area =",round(rad_area,4),"sr. radius=",round(avg_radius_deg,2), \
        "deg, mean exposure:",round(mean_exposure/60.,1),"min., on-axis bck. rate:", round(onaxis_rate * deg2tosr,2), "Hz deg-2"
    hp_map_exposure /= 60. # minutes.
    hp_map_exposure[hp_map_exposure == 0] = -1.6375e+30
    filename = "exposure_map_100GeVintegrated_2deg_bck.fits"
    hp.write_map(filename, [hp_map_exposure], coord="G", partial=True, column_names=["exposure"], column_units=["min."])
    hdulist_out = fits.open(filename, mode='update')
    hdulist_out[1].header.set('EXTNAME', 'Exposure')
    hdulist_out[1].header.set('PSI_0', 0 ) # last target position
    hdulist_out[1].header.set('THETA_0', -90) # last target position
    hdulist_out[1].header.set('SIZE_X', 2.1 * 60. )
    hdulist_out[1].header.set('SIZE_Y', 2.1 * 60. )
    hdulist_out.flush()
    hdulist_out.close()
    
#     hp.cartview(map=hp_map_exposure, fig=2, rot=[0,-90], unit='', xsize=3200, lonra=None, latra=None, title='exposure in minutes', flip='astro', format='%.3g',  hold=False, sub=None, margins=None, notext=False, return_projected_map=False)
#     hp.graticule()
#    plt.show()
    

    # Dump statistics
    print("Number of pointings: %d (%.2f sec)" % (n_pnt,duration_mean))
    print("South array .......: %.2f hours (%.2f%%)" %
          (exposure_south/3600.0,
           100.0*exposure_south/(exposure_south+exposure_north)))
    print("North array .......: %.2f hours (%.2f%%)" %
          (exposure_north/3600.0,
           100.0*exposure_north/(exposure_south+exposure_north)))

    # Return observation definition
    return obsdef


# =========================== #
# Setup Galactic plane survey #
# =========================== #
def set_gps(separation=3.0, bmin=-1.3, bmax=1.3, lst=True):
    """
    Setup Galactic plane survey.
    """
    # Initialise observation definition
    obsdef = []

    # Add inner region South
    obsdef.extend(set_patch(lmin=-60.0, lmax=60.0, bmin=bmin, bmax=bmax,
                            separation=separation, hours=780,
                            site="South", lst=lst))

    # Add Vela & Carina region
    obsdef.extend(set_patch(lmin=240.0, lmax=300.0, bmin=bmin, bmax=bmax,
                            separation=separation, hours=180,
                            site="South", lst=lst))

    # Add 210-240 region
    obsdef.extend(set_patch(lmin=210.0, lmax=240.0, bmin=bmin, bmax=bmax,
                            separation=separation, hours=60,
                            site="South", lst=lst))

    # Add Cygnus, Perseus
    obsdef.extend(set_patch(lmin=60.0, lmax=150.0, bmin=bmin, bmax=bmax,
                            separation=separation, hours=450,
                            site="North", lst=lst))

    # Add Anticentre
    obsdef.extend(set_patch(lmin=150.0, lmax=210.0, bmin=bmin, bmax=bmax,
                            separation=separation, hours=150,
                            site="North", lst=lst))

    # Return observation definition
    return obsdef


# ========================== #
# Setup Extragalactic survey #
# ========================== #
def set_extgal(separation=2.0, lst=True):
    """
    Setup Extragalactic survey.
    """
    # Initialise observation definition
    obsdef = []

    # Set patch
    obsdef.extend(set_patch(lmin=-180, lmax=180, bmin=-90, bmax=-32, #-32
                            separation=separation, hours=500,
                            site="South", lst=lst, autodec=10.0))

    # Return observation definition
    return obsdef


# ========================= #
# Setup Galactic centre KSP #
# ========================= #
def set_gc(lst=True):
    """
    Setup Galactic centre KSP.
    """
    # Initialise observation definition
    obsdef = []

    # Central wobble
    obsdef.extend(set_patch(lmin=-0.5, lmax=0.5, bmin=-0.5, bmax=0.5,
                            separation=0.1, hours=525,
                            site="South", lst=lst))

    # Extended region
    obsdef.extend(set_patch(lmin=-10.0, lmax=10.0, bmin=-10.0, bmax=10.0,
                            separation=1.5, hours=300,
                            site="South", lst=lst))

    # Return observation definition
    return obsdef


# ============= #
# Setup LMC KSP #
# ============= #
def set_lmc(hours=250.0, lst=True):
    """
    Setup LMC KSP.
    """
    # Initialise observation definition
    obsdef = []

    # Set LMC centre
    centre = gammalib.GSkyDir()
    centre.radec_deg(80.0, -69.0)

    # Set offset angle and number of pointings
    offset = 1.0 # degrees
    n_pnt  = 12

    # Prepare computations
    dphi     = 360.0/n_pnt
    duration = hours*3600.0/float(n_pnt)

    # Loop over pointings
    for ipnt in range(n_pnt):

        # Compute pointing direction
        pnt = centre.copy()
        pnt.rotate_deg(ipnt*dphi, offset)
        lon = pnt.l_deg()
        lat = pnt.b_deg()

        # Set positions and duration
        obs = {'lon': lon, 'lat': lat, 'duration': duration}

        # Add IRF
        caldb = "prod2"
        if lst:
            irf = "South_50h"
        else:
            irf = "South_50h"
        obs['caldb'] = caldb
        obs['irf']   = irf

        # Append observation
        obsdef.append(obs)

    # Dump statistics
    print("Number of pointings: %d (%.2f sec)" % (n_pnt,duration))

    # Return observation definition
    return obsdef


# ======================================= #
# Write observation definition dictionary #
# ======================================= #
def write_obsdef(filename, obsdef):
    """
    Write observation definition file.
    """
    # Open file
    file = open(filename, 'w')

    # Write header
    file.write("lgal,bgal,duration,caldb,irf\n")

    # Loop over pointings
    for obs in obsdef:

        # If we have ra,dec then convert into lon,lat
        if "ra" in obs and "dec" in obs:
            ra  = obs["ra"]
            dec = obs["dec"]
            dir = gammalib.GSkyDir()
            dir.radec_deg(ra,dec)
            lon  = dir.lon_deg()
            lat = dir.lat_deg()
        else:
            lon = obs["lon"]
            lat = obs["lat"]

        # Write information
        file.write("%8.4f,%8.4f,%.4f,%s,%s\n" %
                   (lon, lat, obs['duration'], obs['caldb'], obs['irf']))

    # Close file
    file.close()

    # Return
    return


# ======================== #
# Main routine entry point #
# ======================== #
if __name__ == '__main__':
    """
    CTA pointing pattern generation.
    """
    # Initialise flags
    need_help = False

    # Test for command line arguments
    if (len(sys.argv) > 1):
        if sys.argv[1] == "-h":
            need_help = True
        else:
            obsname = sys.argv[1]
    else:
        need_help = True

    # Print help if needed and exit
    if need_help:
        print("Usage: make_pointing.py [OPTIONS]")
        print("     -h       Display this usage message")
        print("     gps      Galactic plane survey (2 row scheme)")
        print("     gps3     Galactic plane survey (3 row scheme)")
        print("     extgal   Extragalactic survey")
        print("     gc       Galactic centre survey")
        print("     lmc      LMC survey")
        sys.exit()

    # Galactic plane survey
    if obsname == "gps":
        obsdef = set_gps(lst=True)
        write_obsdef("gps.dat", obsdef)
    elif obsname == "gps3":
        obsdef = set_gps(separation=1.5, lst=True)
        write_obsdef("gps3.dat", obsdef)

    # Extragalactic survey
    elif obsname == "extgal":
        obsdef = set_extgal(lst=True)
        write_obsdef("extgal.dat", obsdef)

    # Galactic centre
    elif obsname == "gc":
        obsdef = set_gc(lst=True)
        write_obsdef("gc.dat", obsdef)

    # LMC
    elif obsname == "lmc":
        obsdef = set_lmc(lst=True)
        write_obsdef("lmc.dat", obsdef)

    # Invalid pattern
    else:
        print('Unknown option "' + obsname + '"')
